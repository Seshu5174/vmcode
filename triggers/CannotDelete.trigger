trigger CannotDelete on Teacher__c (before delete) {
    if(system.trigger.isdelete){
       for (teacher__c t : trigger.old){
              if(t.course__c == 'Science')
       t.name.adderror('Can not delete the account with course science');
}
}
}