public class childToParent {

    
    public list<contact> con {get;set;}
    public List<students__c> stds {get;set;}
    
    public Pagereference showConts(){
         con = [SELECT name, account.name, account.industry FROM contact order by name asc limit 5];
        return null;
    }
    
    public Pagereference showStdnt(){
        stds = [SELECT Name, Gender__c, Care_taker__r.Name, Care_taker__r.Course__c From Students__c];
        return null;
    }
}