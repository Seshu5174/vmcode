public with sharing class ODFD_OMNI_Intake_Trigger_Helper 
{
	public static void onAfterUpdate(Map<Id,OMNI_Intake__c> oldIntakeMap, Map<Id,OMNI_Intake__c> newIntakeMap)
    {
        List<String> emailsList = new List<String>();
    	Set<Id> intakeIds = new Set<Id>();
        Boolean isStatusChange = false;
        Boolean isDateChange = false;
        String templateId='';
        String recordId='';
        String lastmodifiedId='';
        String submitterId='';
        
        for(OMNI_Intake__c intake : newIntakeMap.values())
        {
            
            OMNI_Intake__c old  = oldIntakeMap.get(intake.Id);
             
            submitterId = intake.CreatedById;
            
             if(old.isExecuted__c)  
             { 
                
               
                 if(old.Omni_Delivery_Agent_Email__c != intake.Omni_Delivery_Agent_Email__c )
                 {
                     if(intake.Omni_Delivery_Agent_Email__c!=null) {
                        emailsList.add(intake.Omni_Delivery_Agent_Email__c);
                        recordId = intake.Id;
                     }
                        
            	 }
                 
                 if(old.Experience_Owner_1_Email__c != intake.Experience_Owner_1_Email__c )
                 {
                     if(intake.Experience_Owner_1_Email__c!=null) {
                        emailsList.add(intake.Experience_Owner_1_Email__c); 
                         recordId = intake.Id;
                     }
            	 }
                 
                 if(old.Experience_Owner_2_Email__c != intake.Experience_Owner_2_Email__c )
                 {
                     if(intake.Experience_Owner_2_Email__c!=null) {
                        emailsList.add(intake.Experience_Owner_2_Email__c);
                         recordId = intake.Id;
                     }
                       
            	 }
                 
                 if(old.Process_Owner_1_Email__c != intake.Process_Owner_1_Email__c )
                 {
                     if(intake.Process_Owner_1_Email__c!=null) {
                       emailsList.add(intake.Process_Owner_1_Email__c); 
                         recordId = intake.Id;
                     }
                        
            	 }
                 
                 if(old.Process_Owner_2_Email__c != intake.Process_Owner_2_Email__c )
                 {
                     if(intake.Process_Owner_2_Email__c!=null) {
                         emailsList.add(intake.Process_Owner_2_Email__c);
                         recordId = intake.Id;
                     }
                        
            	 }
                 
                 if(old.Project_Manager_1_Email__c != intake.Project_Manager_1_Email__c )
                 {
                     if(intake.Project_Manager_1_Email__c!=null){
                        emailsList.add(intake.Project_Manager_1_Email__c); 
                         recordId = intake.Id;
                     } 
                        
            	 }
                 
                 if(old.Project_Manager_2_Email__c != intake.Project_Manager_2_Email__c )
                 {
                     if(intake.Project_Manager_2_Email__c!=null) {
                        emailsList.add(intake.Project_Manager_2_Email__c);  
                         recordId = intake.Id;
                     }
                        
            	 }
            
                 if(intake.Status__c != old.Status__c && old.Status__c!=null) 
                 {
                     
                     recordId = intake.Id;
                     lastmodifiedId = intake.LastModifiedById;
                     isStatusChange = true;
                     intakeIds.add(intake.id);
                     if(intake.Experience_Owner_1_Email__c!=null) emailsList.add(intake.Experience_Owner_1_Email__c);
                     if(intake.Experience_Owner_2_Email__c!=null) emailsList.add(intake.Experience_Owner_2_Email__c );
                     if(intake.Omni_Delivery_Agent_Email__c!=null) emailsList.add(intake.Omni_Delivery_Agent_Email__c );
                     if(intake.Primary_Business_POC_Email__c!=null) emailsList.add(intake.Primary_Business_POC_Email__c );
                     if(intake.Process_Owner_1_Email__c!=null) emailsList.add(intake.Process_Owner_1_Email__c );
                     if(intake.Process_Owner_2_Email__c!=null)emailsList.add(intake.Process_Owner_2_Email__c );
                     if(intake.Project_Manager_1_Email__c!=null)emailsList.add(intake.Project_Manager_1_Email__c );
                     if(intake.Project_Manager_2_Email__c!=null) emailsList.add(intake.Project_Manager_2_Email__c );
                     
                     String templateName = System.Label.ODFD_Status_Change_Email_Template;
                     EmailTemplate et = [Select Id, Name,DeveloperName From EmailTemplate where DeveloperName=:templateName];
                     templateId =et.Id; 
                 }
            
            	if(intake.Initial_Channel_Impact_Date__c  != old.Initial_Channel_Impact_Date__c ) 
            	{
                   recordId = intake.Id;
                   lastmodifiedId = intake.LastModifiedById;
                   isDateChange = true;
                   intakeIds.add(intake.id);
                   if(intake.Experience_Owner_1_Email__c!=null) emailsList.add(intake.Experience_Owner_1_Email__c);
                   if(intake.Experience_Owner_2_Email__c!=null) emailsList.add(intake.Experience_Owner_2_Email__c );
                   if(intake.Omni_Delivery_Agent_Email__c!=null) emailsList.add(intake.Omni_Delivery_Agent_Email__c );
                   if(intake.Primary_Business_POC_Email__c!=null) emailsList.add(intake.Primary_Business_POC_Email__c );
                   if(intake.Process_Owner_1_Email__c!=null) emailsList.add(intake.Process_Owner_1_Email__c );
                   if(intake.Process_Owner_2_Email__c!=null)emailsList.add(intake.Process_Owner_2_Email__c );
                   if(intake.Project_Manager_1_Email__c!=null)emailsList.add(intake.Project_Manager_1_Email__c );
                   if(intake.Project_Manager_2_Email__c!=null) emailsList.add(intake.Project_Manager_2_Email__c );
                    
                   String templateName = System.Label.ODFD_Channel_impact_Date_Change_Email_Template;
                   EmailTemplate et = [Select Id, Name,DeveloperName From EmailTemplate where DeveloperName=:templateName];
                   templateId =et.Id;
            	}
        
             }    
            
  		
        system.debug('Intake Ids >> '+ intakeIds);
		system.debug('emailsList from Intake >> '+ emailsList);
		
        if(!intakeIds.isEmpty())
        {
          for(OMNI_Assignment__c  assignment : [SELECT Id, Name, Assigned_User_Email__c FROM OMNI_Assignment__c WHERE OMNI_Intake__c  IN : intakeIds])
			{
       			if(assignment.Assigned_User_Email__c!=null) emailsList.add(assignment.Assigned_User_Email__c);
    		}  
        }
  		
        system.debug('Final emailsList  >> '+ emailsList);
        
        if(!emailsList.isEmpty())
        {
          if(isStatusChange) ODFD_EmailHandler.sendEmail(emailsList, recordId, lastModifiedId, templateId);
          else if(isDateChange) ODFD_EmailHandler.sendEmail(emailsList, recordId, lastModifiedId, templateId);
          else {
                
                String templateName = System.Label.ODFD_Intake_POC_Update_Email_Template ;
                EmailTemplate et = [Select Id, Name,DeveloperName From EmailTemplate where DeveloperName=:templateName];
                templateId =et.Id;
                ODFD_EmailHandler.sendEmail(emailsList, recordId, submitterId, templateId);
            }
        }

	}
    }
}