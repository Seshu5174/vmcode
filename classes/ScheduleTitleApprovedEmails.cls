global class ScheduleTitleApprovedEmails implements schedulable
{
  global void execute (SchedulableContext sc)
  {
      TitleSendApprovedEmailToNominator obj = new TitleSendApprovedEmailToNominator();
      Database.executeBatch(obj, 200);
  }
}