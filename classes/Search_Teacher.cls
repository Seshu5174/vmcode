public class Search_Teacher{

    string something;
    list<teacher__c> tea;
    
    public string getSome(){
    return something;
    }
    
    public list<teacher__c> getList(){
    return tea;
    }
    
    public PageReference show(){
    tea= [select name, course__c, phone__c, email__c, join_date__c, salary__c from teacher__c where course__c = :something];
    return null;
    }
    
    public void setSome(string s){
    something=s;
    }
    
 }