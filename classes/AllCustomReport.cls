public with sharing class AllCustomReport{
    
    
    public List<SelectOption> repOptions{get;set;}
    public List<SelectOption> lobOptions{get;set;}
    public List<SelectOption> cffOptions{get;set;}
    Public String selReport{get; set;}
    Public String selLOB{get; set;}
    
    public List<SelectOption> pdfOptions{get;set;}
    Public String selPdfRep{get; set;}
    
    Public String selCFF{get; set;}
    
    public List<SelectOption> resultOptions{get;set;}
    Public String selResultRep{get; set;}
    //pdf variablesPublic list <AggregateResult> cffList;
    //Public list <GroupEmp> GroupEmpList {get; set;} 
    Public list <GroupEmp1> GroupEmpList {get; set;}
    Public String vLOB{get; set;}
    Public String folderName{get; set;}
    Public Integer noffiles{get; set;}
    Public boolean ispdfREport {get; set;}
    public map<String,String> groupListJsonMap{get;set;}

    //pdf variables end
    
    public AllCustomReport()
    {
        ispdfREport =false;
        selReport = 'None';
        selPdfRep ='None';
        selLOB = 'None';
        selCFF ='None';
        selResultRep='None';
        
        resultOptions = new List<SelectOption>();
        resultOptions.add(new SelectOption('None','None'));
        resultOptions.add(new SelectOption('CFF_RespSummaryReport','CFF_RespSummaryReport'));
        resultOptions.add(new SelectOption('CFF_StatusByLOB','CFF_StatusByLOB'));
        resultOptions.add(new SelectOption('CFF_EnterpriseLevelByLOB', 'CFF_EnterpriseLevelByLOB'));
        
        repOptions = new List<SelectOption>();
        //repOptions.add(new SelectOption('None','None'));
        repOptions.add(new SelectOption('CFF_StatusByFunction','CFF_StatusByFunction'));
        
        pdfOptions = new List<SelectOption>();
        pdfOptions.add(new SelectOption('None','None'));
        pdfOptions.add(new SelectOption('CFFMSWordPage','CFF MSWord Page'));
        pdfOptions.add(new SelectOption('CFFPDFPage','CFF PDF Page'));
        
        cffOptions = new List<SelectOption>();
        cffOptions.add(new SelectOption('None','None'));
        cffOptions.add(new SelectOption('Corporate Audit','Corporate Audit'));
        cffOptions.add(new SelectOption('CFO Group','CFO Group'));
        cffOptions.add(new SelectOption('Global Compliance','Global Compliance'));
        cffOptions.add(new SelectOption('Global Human Resources','Global Human Resources'));
        cffOptions.add(new SelectOption('Legal','Legal'));
        cffOptions.add(new SelectOption('Global Risk','Global Risk'));
        
        lobOptions = new List<SelectOption>();
        lobOptions.add(new SelectOption('None','None'));
        lobOptions.add(new SelectOption('CAO','CAO'));
        lobOptions.add(new SelectOption('CEO','CEO'));
        lobOptions.add(new SelectOption('Compliance','Compliance'));
        lobOptions.add(new SelectOption('Consumer','Consumer'));
        lobOptions.add(new SelectOption('GBAM','GBAM'));
        lobOptions.add(new SelectOption('GHR','GHR'));
        lobOptions.add(new SelectOption('GMCA','GMCA'));
        lobOptions.add(new SelectOption('GT&O', 'GT&O'));
        lobOptions.add(new SelectOption('GWIM', 'GWIM'));
        lobOptions.add(new SelectOption('Legal','Legal'));
        lobOptions.add(new SelectOption('Risk','Risk'));
        
    }
    
    public PageReference goToReport() {
        
        PageReference pageRef = new PageReference('/apex/CFF_RespSummaryReport');
        pageRef.setRedirect(true);
        return pageRef;
        
    }
    
    public PageReference passValueToCtl() {
        //selReport ='CFF_RespSummaryReport';
        return null;
    }
    
    public void getPdfREport(){
        list<GroupEmp1SerializeWraper> serializationRecords;
        ispdfREport =true;
        integer vOffSet = 0;
        noffiles = 0;
        integer vTotal = 500; //this should be from count of all 
        if(selLOB == '')
        {
            selLOB='Legal';
        } 
        vLOB =selLOB;
        
        
        folderName = '\"'+vLOB+'.zip'+'"';
        
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,' volb.'+selLOB+'actualvar:'+vLOB);
        ApexPages.addMessage(myMsg);
        
        GroupEmpList = new List<GroupEmp1>();
        GroupEmp1 empCFFRecs ;
        /*
cffList = [SELECT  Covered_Employee_Name__c from Control_Function_Feedback__c  where LOB__c=:vLOB  and Feedback_Year__c = '2016' GROUP BY Covered_Employee_Name__c ];
// Query CFF Standard object 
for(AggregateResult a:cffList)
{
String empId = String.valueOf(a.get('Covered_Employee_Name__c'));
empCFFRecs = new GroupEmp(empId);

GroupEmpList.add(empCFFRecs);

}
*/  
        //Add set and take care of Salesforce SQL limit 
        list<Control_Function_Feedback__c> allRecords = [SELECT Id,Covered_Employee_Name__c, Name, LNFN__c, LOB__c, Audit_Type__c, CFF_Rating__c,CF_Commentary__c  from Control_Function_Feedback__c where  LOB__c=:vLOB and Feedback_Year__c = '2016'  ORDER BY Covered_Employee_Name__c] ;
        Set <Id> holdKeys = new Set<Id>();
        for(Control_Function_Feedback__c Cf:allRecords)
        {
            if(holdKeys.Contains(Cf.Covered_Employee_Name__c) )
            {
                empCFFRecs.addMoreRecords(Cf);
            }
            else
            {
                if(holdKeys.size() == 999)
                {
                    holdKeys.removeall(holdKeys);
                }
                holdKeys.add(Cf.Covered_Employee_Name__c);
                if(empCFFRecs== null)
                {
                    empCFFRecs =new GroupEmp1(Cf);
                }
                else
                {
                    GroupEmpList.add(empCFFRecs);
                    empCFFRecs =new GroupEmp1(Cf);
                    
                }
                
            }
             
             
            
        }
        for(GroupEmp1 record : GroupEmpList){
            if(String.isNotBlank(record.empId)){
            serializationRecords =  new list<GroupEmp1SerializeWraper>();
            if(record.cffRecs.size()>0){
                for(Control_Function_Feedback__c cff : record.cffRecs){
                             GroupEmp1SerializeWraper wraperRecord = new GroupEmp1SerializeWraper();
                    wraperRecord.Audit_Type= cff.Audit_Type__c!=NULL?cff.Audit_Type__c:'';
                    wraperRecord.CFF_Rating= cff.CFF_Rating__c!=NULL?cff.CFF_Rating__c:'' ;
                    wraperRecord.CF_Commentary= cff.CF_Commentary__C!=NULL?cff.CF_Commentary__c:'' ;

                    serializationRecords.add(wraperRecord);
                }
            }
            groupListJsonMap.put(record.empId,JSON.Serialize(serializationRecords));
            }
        }
        
    }  
    
    
    public class GroupEmp{
        Public String FullName{get;set;}
        Public String LOBName{get; set;}
        Public String empId;
        Public List <Control_Function_Feedback__c > cffRecs{get; set;}
        GroupEmp(String cffempId)
        {
            empId = cffempId;
            cffRecs = [SELECT Id, Name, LNFN__c, LOB__c, Audit_Type__c, CFF_Rating__c,CF_Commentary__c  from Control_Function_Feedback__c where Covered_Employee_Name__c =:cffempId];
            if(cffRecs.size()>0)
            {
                FullName = cffRecs[0].LNFN__c;
                LOBName = cffRecs[0].LOB__c;
            }
        }
        
    }
    
    public class GroupEmp1{
        Public String FullName{get;set;}
        Public String LOBName{get; set;}
        Public String empId{get; set;}
        Public List <Control_Function_Feedback__c > cffRecs{get; set;}
        GroupEmp1(Control_Function_Feedback__c cRecord)
        {
            cffRecs = new List<Control_Function_Feedback__c>();
            //empId = cffempId;
            // cffRecs = [SELECT Id, Name, LNFN__c, LOB__c, Audit_Type__c, CFF_Rating__c,CF_Commentary__c  from Control_Function_Feedback__c where Covered_Employee_Name__c =:cffempId];
            FullName = cRecord.LNFN__c;
            LOBName = cRecord.LOB__c;
            cffRecs.add(cRecord);
        }
        
        public void addMoreRecords(Control_Function_Feedback__c cRecord)
        {
            cffRecs.add(cRecord);
        }
    }
    public Class GroupEmp1SerializeWraper{ 
        public String Audit_Type ='';  
        public String CFF_Rating='';
        public String CF_Commentary='';
    }
    
    public void hidePageContent(){
        ispdfREport = false;
    }
    
}