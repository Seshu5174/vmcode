public with sharing class CFFPdfReport {
 
    public pagereference goBack(){
    if(vLOB =='GT&O')
        vLOB = 'GT_O';
        pagereference pg = new pagereference('/apex/CFF_AllCustomReports');
        pg.setredirect(true);
        return pg;
    }
    
    public String currentYear;
    public String previousYear;

Public list <AggregateResult> cffList;
//Public list <GroupEmp> GroupEmpList {get; set;}  
Public transient list <GroupEmp1> GroupEmpList {get; set;}
Public String vLOB{get; set;}
Public String folderName{get; set;}
Public Integer noffiles{get; set;}
Public boolean ispdfREport {get; set;}
public transient map<Integer,String> groupListJsonMap{get;set;}

        //---Constructor----
        public CFFPdfReport()
        {

            list<GroupEmp1SerializeWraper> serializationRecords;
            ispdfREport =true;
            integer vOffSet = 0;
            noffiles = 0;
            integer vTotal = 500; //this should be from count of all 
            vLOB=ApexPages.currentPage().getParameters().get('LOBName');
            if(vLOB == '')
            {
                vLOB='Legal';
            }

            if(vLOB =='GT_O')
            vLOB = 'GT&O';

            folderName = '\"'+vLOB+'.zip'+'"';

            CFF_Setting__mdt[] cff_settings = [SELECT Feedaback_Year__c, DeveloperName FROM CFF_Setting__mdt];
            map < String, String > cff_map = new map < String,  String > ();
            for (CFF_Setting__mdt cff_data: cff_settings) {
                cff_map.put(cff_data.DeveloperName, cff_data.Feedaback_Year__c);
            }

            currentYear = cff_map.get('Current_Year'); //'2018'; //String.valueOf(Date.Today().Year());
            previousYear = cff_map.get('Previous_Year'); //String.valueOf(Date.Today().Year()-1);
            
            GroupEmpList = new List<GroupEmp1>();

            List<GroupEmp1> GroupEmpListReference = new List<GroupEmp1>();
            GroupEmp1 empCFFRecs ;
            //Add set and take care of Salesforce SQL limit 
            list<Control_Function_Feedback__c> allRecords = [SELECT Id,Covered_Employee_Name__c, Name, Covered_Employee_Person_Number__c, LNFN__c, LOB__c, Audit_Type__c, CFF_Rating__c,CF_Commentary__c  from Control_Function_Feedback__c where  LOB__c=:vLOB and Feedback_Year__c =:currentYear  ORDER BY Covered_Employee_Name__c, Audit_Type__c ASC] ;
            Set <Id> holdKeys = new Set<Id>();
            system.debug('lob:'+vLOB);
            system.debug('lob:recrds'+allRecords);
            for(Control_Function_Feedback__c Cf:allRecords)
            {
            if(holdKeys.Contains(Cf.Covered_Employee_Name__c) )
            {
            empCFFRecs.addMoreRecords(Cf);
            }
            else
            {
            if(holdKeys.size() == 999)
            {
            holdKeys.removeall(holdKeys);
            }
            holdKeys.add(Cf.Covered_Employee_Name__c);
            if(empCFFRecs== null)
            {
            empCFFRecs =new GroupEmp1(Cf);
            }
            else
            {
            empCFFRecs =new GroupEmp1(Cf);

            }
            GroupEmpListReference.add(empCFFRecs);

            }



            }
         
            Integer referId = 0;
            groupListJsonMap  = new map<Integer,String>();

            for(GroupEmp1 record : GroupEmpListReference){
                ////ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.info,' record s:wraper:'+referId));

                record.referenceId =referId;
                GroupEmpList.add(record);

                //if(String.isNotBlank(record.empId))
                {
                    serializationRecords =  new list<GroupEmp1SerializeWraper>();

                    if(record.cffRecs.size()>0){
                        for(Control_Function_Feedback__c cff : record.cffRecs){
                            GroupEmp1SerializeWraper wraperRecord = new GroupEmp1SerializeWraper();
                            wraperRecord.Audit_Type= cff.Audit_Type__c!=NULL?cff.Audit_Type__c:'';
                            wraperRecord.CFF_Rating= cff.CFF_Rating__c!=NULL?cff.CFF_Rating__c:'' ;
                            wraperRecord.CF_Commentary= cff.CF_Commentary__C!=NULL?cff.CF_Commentary__c:'' ;
                            //ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.info,' record s:wraper:'+wraperRecord));

                            serializationRecords.add(wraperRecord);
                        }
                    }
                    groupListJsonMap.put(referId,JSON.Serialize(serializationRecords));

                    referId++;
                }
            }
                 
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.info,'keyset:'+String.valueof(groupListJsonMap.keyset())));
        }  

public class GroupEmp1{
Public String FullName{get;set;}
Public String LOBName{get; set;}
Public String PersonNumber{get; set;}
Public String empId{get; set;}
public Integer referenceId {get; set;}
Public List <Control_Function_Feedback__c > cffRecs{get; set;}
GroupEmp1(Control_Function_Feedback__c cRecord)
{
cffRecs = new List<Control_Function_Feedback__c>();

FullName = cRecord.LNFN__c;
LOBName = cRecord.LOB__c;
PersonNumber = cRecord.Covered_Employee_Person_Number__c;
cffRecs.add(cRecord);
}

public void addMoreRecords(Control_Function_Feedback__c cRecord)
{
cffRecs.add(cRecord);
}
}
public Class GroupEmp1SerializeWraper{ 
public String Audit_Type ='';  
public String CFF_Rating='';
public String CF_Commentary='';
}

 

}