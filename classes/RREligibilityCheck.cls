public class RREligibilityCheck implements Database.Batchable<SObject>, Schedulable {

public final string query;

public static string currentYear {get;set;} 
public static string previousYear {get;set;}

public RREligibilityCheck() {

RR_Setting__mdt[] RR_settings = [SELECT Eligibility_Year__c,DeveloperName FROM RR_Setting__mdt];
map<String,String> year_map = new map<String,String>();
for(RR_Setting__mdt year_data: RR_settings){
year_map.put(year_data.DeveloperName, year_data.Eligibility_Year__c);
}

currentYear = year_map.get('Current_Year');
previousYear = year_map.get('Previous_Year');

string tenYear = String.ValueOf(Integer.ValueOf(currentYear)-10);
string fifteenYear = String.ValueOf(Integer.ValueOf(currentYear)-15);
string twentyYear = String.ValueOf(Integer.ValueOf(currentYear)-20);
string twentyFiveYear = String.ValueOf(Integer.ValueOf(currentYear)-25);
string thirtyYear = String.ValueOf(Integer.ValueOf(currentYear)-30);
string thirtyFiveYear = String.ValueOf(Integer.ValueOf(currentYear)-35);
string fortyYear = String.ValueOf(Integer.ValueOf(currentYear)-40);
string fortyFiveYear = String.ValueOf(Integer.ValueOf(currentYear)-45);
string fiftyYear = String.ValueOf(Integer.ValueOf(currentYear)-50);
string fiftyFiveYear = String.ValueOf(Integer.ValueOf(currentYear)-55);
string sixtyYear = String.ValueOf(Integer.ValueOf(currentYear)-60);
string sixtyFiveYear = String.ValueOf(Integer.ValueOf(currentYear)-65);


system.debug('currentYear '+ currentYear);
system.debug('tenYear '+ tenYear);

if(currentYear=='2020')
{
query = 'Select Name, Person_Number__c, Level_1_LOB__c, Level_2_LOB__c, Manager_Full_Name__c, Employee_Most_Recent_Hire_Date__c, Continuous_Service_Date__c,  Employee_Type_Description__c, Worker_Type__c from Account where ID NOT IN (SELECT Employee_Name_Lkp__c FROM Recharge_and_Refocus__c ) and (LOB_ID__c!=\'1242425\' and LOB_ID__c!=\'1409167\' and LOB_ID__c!=\'1556182\' and LOB_ID__c!=\'2\' and LOB_ID__c!=\'6564174\') and Employment_Status__c=\'A\' and Worker_Type__c=\'Associate\' and Severance_Flag__c=\'No\' and ((Continuous_Service_Date__c LIKE \'%' + tenYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + fifteenYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + twentyYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + twentyFiveYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + thirtyYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + thirtyFiveYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + fortyYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + fortyFiveYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + fiftyYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + fiftyFiveYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + sixtyYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + sixtyFiveYear + '%\')) and CSD_Effective_Date__c<=2020-06-01';

}
else{
    
query = 'Select Name, Person_Number__c, Level_1_LOB__c, Level_2_LOB__c, Manager_Full_Name__c, Employee_Most_Recent_Hire_Date__c, Continuous_Service_Date__c,  Employee_Type_Description__c, Worker_Type__c from Account where Employment_Status__c=\'A\' and Worker_Type__c=\'Associate\' and Severance_Flag__c=\'No\' and ((Continuous_Service_Date__c LIKE \'%' + tenYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + fifteenYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + twentyYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + twentyFiveYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + thirtyYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + thirtyFiveYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + fortyYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + fortyFiveYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + fiftyYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + fiftyFiveYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + sixtyYear + '%\') OR (Continuous_Service_Date__c LIKE \'%' + sixtyFiveYear + '%\'))';
}
}    

public void execute(SchedulableContext sc) {
Database.executeBatch(this, 200);
}

public Database.QueryLocator start(Database.BatchableContext bc) {
return Database.getQueryLocator(query);
}

public void execute(Database.BatchableContext bc, List<sObject> scope) {

list<Recharge_and_Refocus__c> newObjects = new list<Recharge_and_Refocus__c>();
RR_Setting__mdt[] RR_settings = [SELECT Eligibility_Year__c,DeveloperName FROM RR_Setting__mdt];
map<String,String> year_map = new map<String,String>();
for(RR_Setting__mdt year_data: RR_settings){
year_map.put(year_data.DeveloperName, year_data.Eligibility_Year__c);
}

string year =  year_map.get('Current_Year');

for(Sobject s : scope){
Account obj = (Account) s;
newObjects.add(new Recharge_and_Refocus__c (
Employee_Name_Lkp__c = obj.Id,
Eligibility_Year__c = year 
));
}
insert newObjects;


}

public void finish(Database.BatchableContext bc) {
system.debug('JOB IS FINISHED');
}

}