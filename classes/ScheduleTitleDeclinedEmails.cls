global class ScheduleTitleDeclinedEmails implements schedulable
{
  global void execute (SchedulableContext sc)
  {
      TitleSendDeclinedEmailToNominator obj = new TitleSendDeclinedEmailToNominator();
      Database.executeBatch(obj, 200);
  }
}