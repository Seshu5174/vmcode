({
    SearchHelper: function(component, event, helper,pNum,rCunt,isOnload) {
        
        console.log('SearchHelper');
        // show spinner message
        if(component.find("Id_spinner")!=undefined)
            component.find("Id_spinner").set("v.class" , 'slds-show');
        
        var action = component.get("c.fetchHistoryRecords");
        
        action.setParams({
            'recordId': component.get("v.recordId"),                
            "pageNumber" : pNum.toString(),
            "currnetPagesCount" : rCunt.toString()
            
        });
        action.setCallback(this, function(response) {
            console.log('SearchHelper response');
            
            // hide spinner when response coming from server 
            if(component.find("Id_spinner")!=undefined)
                component.find("Id_spinner").set("v.class" , 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                var Responseop = response.getReturnValue();
                var storeResponse = Responseop.employees;
                console.log('Responseop.employees');
                console.log(storeResponse);
                
                component.set("v.TotalNumberOfRecord", Responseop.RecordsCount);
                component.set("v.recordsPerPage", 10);                    
                component.set("v.pageResult", storeResponse);
                var pageCounterInfo = {};
                var maxPageNumberInfo = Responseop.totalPages;
                console.log('maxPageNumberInfo:'+maxPageNumberInfo);
                
                if(isOnload)
                    pageCounterInfo = {"currentPageNumber":"1","totalPages": maxPageNumberInfo.toString()};
                else{
                    pageCounterInfo = {"currentPageNumber":component.get("v.pageNumber"),"totalPages": maxPageNumberInfo.toString()};    
                }
                
                component.set("v.pageCounterInfo",pageCounterInfo);
                component.set("v.maxPage", maxPageNumberInfo==0?1:maxPageNumberInfo);                    
                
                
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + 
                              errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        
    }
})