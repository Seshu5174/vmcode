trigger updateLOBPicklist on Incentive_Rationale__c (before Insert, after insert)

{

    for (Incentive_Rationale__c inc: trigger.new)
    {
        if(trigger.isInsert && trigger.isafter)
        
        {
        system.debug ('before Inserting ');
            inc.LOB_Picklist__c = inc.Employee_Name_Lkp__r.Level_1_LOB__c;
          system.debug ('***' + inc);
        }
    }
    
    }