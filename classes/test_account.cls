@isTest
public class test_account{
    static TestMethod void test(){
        List<Account> acts = new List<Account>();
        for (integer i=0; i<200; i++){
        Account a = new Account (Name = 'Test Teacher ' + i, Industry = 'Energy');
        acts.add(a);
        }
           Test.startTest();
           insert acts;
           Test.stopTest();
           
            
            list<account> insertedAccounts = [select name, description from account where id in: acts ];
            for (account a1 : insertedAccounts) {
            system.AssertEquals ('Energy Account', a1.description);
            }
            }
}