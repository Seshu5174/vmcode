public class testNav{

public String myVar { get; set; }

public pageReference TestNav(){
    PageReference pageRef = new PageReference('/apex/' + myVar);
    Id id = System.currentPageReference().getParameters().get('id');       
    pageRef.getParameters().put('id',id);
    pageRef.setRedirect(true);
    return pageRef;
}

}