public with sharing class paramprocessing {
public paramprocessing (){
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'First Error Message added from apex'+EncodingUtil.urlEncode(String.valueof(URL.getCurrentRequestUrl()),'UTF-8')));

map<String,string> prm = new map<String,string> ();
prm.putall(ApexPages.currentPage().getParameters());
 
String parmString = String.valueof(ApexPages.currentPage().getParameters().remove('core.apexpages.devmode.url'));
prm.remove('core.apexpages.devmode.url');
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'prams:'+URL.getCurrentRequestUrl()));

// Create a new account called Acme that we will create a link for later.
 

// Get the base URL.
String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Base URL: ' + sfdcBaseURL ));       

// Get the URL for the current request.
String currentRequestURL = URL.getCurrentRequestUrl().toExternalForm();
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Current request URL: ' + currentRequestURL));        
 
// Get some parts of the base URL.
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Host: ' + URL.getSalesforceBaseUrl().getHost()));   
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Protocol: ' + URL.getSalesforceBaseUrl().getProtocol()));

// Get the query string of the current request.
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Query: ' + URL.getCurrentRequestUrl().getQuery()));

}
}