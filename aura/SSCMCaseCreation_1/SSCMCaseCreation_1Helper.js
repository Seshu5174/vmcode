({
SearchHelper: function(component, event, helper) {
if($A.util.isEmpty(component.get("v.searchStandardID"))&&$A.util.isEmpty(component.get("v.searchCostCenter"))&&$A.util.isEmpty(component.get("v.searchManagerID"))&&$A.util.isEmpty(component.get("v.searchManagerPersonNumber"))&&$A.util.isEmpty(component.get("v.searchPersonNumber"))&&$A.util.isEmpty(component.get("v.searchDoB"))&&$A.util.isEmpty(component.get("v.searchLastName"))&&$A.util.isEmpty(component.get("v.searchFirstName"))&&$A.util.isEmpty(component.get("v.searchPreferredName"))&&$A.util.isEmpty(component.get("v.searchInquirerPhone"))&&$A.util.isEmpty(component.get("v.searchInquirerEmail"))&&$A.util.isEmpty(component.find('select').get('v.value'))){
var toastEvent = $A.get("e.force:showToast");
toastEvent.setParams({

message:'Please search by atleast one field',
duration:' 5000',
key: 'info_alt',
type: 'error',
mode: 'pester'
});
toastEvent.fire();
}
else{
// show spinner message
if(component.find("Id_spinner")!=undefined)
component.find("Id_spinner").set("v.class" , 'slds-show');

var action = component.get("c.fetchEmployee");

action.setParams({
'searchStandardID': component.get("v.searchStandardID"),
'searchPersonNumber': component.get("v.searchPersonNumber"),
'searchDoB': component.get("v.searchDoB"),
'searchFirstName': component.get("v.searchFirstName"),
'searchLastName': component.get("v.searchLastName"),
'searchPreferredName': component.get("v.searchPreferredName"),
'searchPhone': component.get("v.searchInquirerPhone"),
'searchEmail': component.get("v.searchInquirerEmail"),
'searchManagerID': component.get("v.searchManagerID"),
'searchManagerPersonNumber': component.get("v.searchManagerPersonNumber"),
'searchCostCenter': component.get("v.searchCostCenter"),
'searchEmpLevel':component.find('select')!=undefined?component.find('select').get('v.value'):''

});
action.setCallback(this, function(response) {
// hide spinner when response coming from server 
if(component.find("Id_spinner")!=undefined)
component.find("Id_spinner").set("v.class" , 'slds-hide');
var state = response.getState();

component.set("v.isSearch",true);
component.set("v.isDetail",false);
if (state === "SUCCESS") {
var storeResponse = response.getReturnValue();

// if storeResponse size is 0 ,display no record found message on screen.
if (storeResponse.length == 0) {
component.set("v.Message", true);
component.set("v.anonymousText", true);
} else {
component.set("v.Message", false);
component.set("v.anonymousText", false);
}

component.set("v.TotalNumberOfRecord", storeResponse.length);

// set searchResult list with return value from server.
component.set("v.searchResult", storeResponse); 

component.set("v.recordsPerPage", 10);
component.set("v.searchResult", storeResponse);
var maxPageNumberInfo = Math.floor((storeResponse.length+9)/10);
component.set("v.maxPage", maxPageNumberInfo==0?1:maxPageNumberInfo);

this.sortBy(component,helper, "LastName__c");


}else if (state === "INCOMPLETE") {
alert('Response is Incompleted');
}else if (state === "ERROR") {
var errors = response.getError();
if (errors) {
if (errors[0] && errors[0].message) {
alert("Error message: " + 
errors[0].message);
}
} else {
alert("Unknown error");
}
}
});
$A.enqueueAction(action);
}
},
showDetailsHelper: function(component, event,helper) {
component.set("v.isDetail",true);


var recordIdValue = event.target.name;
console.log(recordIdValue);
component.set("v.recordIdInfo",recordIdValue);
this.retrieveAccountInfo(component, event);
},    
renderPage: function(component,recSize) {       
var records = component.get("v.searchResult"),
pageNumber = component.get("v.pageNumber"),
pageRecords = records.slice((pageNumber-1)*recSize, pageNumber*recSize);
component.set("v.pageResult", []);

component.set("v.pageResult", pageRecords);
/*var recordsInfo = component.get("v.dupCnts");
recordsInfo[0].isExpanded = false;
component.set("v.dupCnts", recordsInfo);*/
//component.set("v.showBlockSpinner", false);
},
sortBy: function(component, helper,field) {
var sortAsc = component.get("v.sortAsc"),
sortField = component.get("v.sortField"),
records = component.get("v.searchResult");

sortAsc = sortField != field || !sortAsc;

records.sort(function(a,b){
var t1 = a[field] == b[field],
t2 = (!a[field] && b[field]) || (a[field] < b[field]);
return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
});
component.set("v.sortAsc", sortAsc);
component.set("v.sortField", field);
component.set("v.searchResult", records);
this.renderPage(component,component.get("v.recordsPerPage"));
},
retrieveAccountInfo: function(component, event,helper) {

var action = component.get("c.fetchAccountDetails");

action.setParams({
'recordId': event.target.name

});
action.setCallback(this, function(response) {
// hide spinner when response coming from server 
var state = response.getState();

if (state === "SUCCESS") {
var storeResponse = response.getReturnValue();
console.log(storeResponse);
component.set("v.accountInfo",storeResponse.ac);
component.set("v.recordTypeIdInfo",storeResponse.recordTypeId);
component.set("v.isSCRepresetative",storeResponse.isSCRepresetative);

}

});
$A.enqueueAction(action);
},
resetSearchFilterHelper  : function(component, event, helper) {
component.set("v.searchStandardID",'');
component.set("v.searchPersonNumber",'');
component.set("v.searchDoB",null);
component.set("v.searchLastName",'');
component.set("v.searchFirstName",'');
component.set("v.searchPreferredName",'');
component.set("v.searchInquirerPhone",''); 
component.set("v.searchManagerID",''); 
component.set("v.searchManagerPersonNumber",''); 
component.set("v.searchCostCenter",''); 
component.set("v.searchInquirerEmail");
if(component.find('select')!=undefined)
component.find('select').set('v.value','');
component.set("v.pageResult",[]);
component.set("v.searchResult",[]);
component.set("v.TotalNumberOfRecord", 0);
component.set("v.recordsPerPage", 10);
component.set("v.maxPage", 1);
component.set("v.pageNumber",1)
component.set("v.isSearch",false);
component.set("v.isDetail",false);
component.set("v.isSearch",false);
component.set("v.anonymousText",false);
//helper.renderPage(component, event, helper,component.get("v.recordsPerPage"),true);

},
maintainSelectedList  : function(component, event, helper) {
var subList = component.get("v.subjectInfoList");
var subListIds= [];
subListIds = component.get("v.subjectsInfoIds");
var subAccountIds= [];
subAccountIds = component.get("v.subjectsInfoAccountIds");
var emailInfoList= [];
emailInfoList = component.get("v.emailInfoList");
var namesList= [];
namesList = component.get("v.namesList");
var emailsInfoIds= [];
emailsInfoIds = component.get("v.emailsInfoIds");
var pageInfo = component.get("v.pageResult");
var allrecords = component.find("employeeNameRec");
var isChecked = component.find("selectAll").get("v.checked");
for(var i = 0;i<allrecords.length;i++){
if(pageInfo[i]!=undefined){
var recordInfo = pageInfo[i].ename;
console.log('recordInfo==>');
console.log(recordInfo);
allrecords[i].set("v.checked",isChecked);

if(isChecked){
subListIds.push(recordInfo.Id);
subList.push(recordInfo.Name);
subAccountIds.push(recordInfo.Employee__c);
emailInfoList.push(recordInfo.Name+':'+recordInfo.Email__c);
namesList.push(recordInfo.Name);
emailsInfoIds.push(recordInfo.Email__c);
}
else{
subListIds.splice(i,1);
subList.splice(i,1);

subAccountIds.splice(i,1);
emailInfoList.splice(i,1);
namesList.splice(i,1);
emailsInfoIds.splice(i,1);
}
}
}
console.log(subListIds);
console.log(subList);
console.log(subAccountIds); 

}
})