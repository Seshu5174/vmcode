@isTest
public class SSCMSearchEmployeeControllerTest {
    public static testMethod void testQueries(){
        Account ac = new Account(Name='testName',Person_Number__c='estPerson_Number__c');
        insert ac;
        Employee_Name__c enm =  new Employee_Name__c(Person_Number__c='estPerson_Number__c',Employee__c= ac.Id);
        
        insert enm;
        Inquiry_Summary_Topic__c isp = new Inquiry_Summary_Topic__c(Name='test name' , Inquiry_Category_1__c = 'Benefits',Inquiry_Category_2__c = 'Employee Programs');
        insert isp;
        Case cs = new Case(accountId=ac.Id,subject='test suject');
        insert cs;
        
        SSCMsearchEmployeeController.searchUser ('personNumber');
        SSCMsearchEmployeeController.fetchAccountDetails(enm.Id);
        SSCMsearchEmployeeController.retrieveEmployeeNamenfo('recordId');
        SSCMsearchEmployeeController.retrieveInquirySummaryTopic(isp.Id);
        SSCMsearchEmployeeController.CreateCase(ac.Id);
        
        SSCMsearchEmployeeController.insertSubjects(new list<SSCM_Subject__c>{new SSCM_Subject__c(Name='testname', Employee__c=ac.Id,Case_Id__c=cs.Id)});
                SSCMsearchEmployeeController.getDependentPicklistValues(Case.Inquiry_Category_3__c,isp.Inquiry_Category_1__c);

        SSCMsearchEmployeeController.fetchEmployee('searchStandardID', 'searchPersonNumber','searchDoB','searchFirstNam','searchLastName',  'searchPreferredName', 'searchPhone', 'searchEmail', 'searchEmpLevel', 'searchManagerID', 'searchManagerPersonNumber',  'searchCostCenter');
                SSCMsearchEmployeeController.fetchEmployee('', '','searchDoB','','',  '', '', '', '', '', '',  '');

    }
}