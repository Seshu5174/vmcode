trigger AcctPhoneUpdate on Contact (after insert) {
    List<account> acc =  new List<account> ();
    for(contact c: trigger.new){
        Account a = [Select id, phone from account where Id=:c.accountID];
        a.phone = c.phone;
        acc.add(a);
    }
    update acc;
}