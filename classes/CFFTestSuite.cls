@isTest(SeeAllData=true)

public class CFFTestSuite{

static testMethod void testCaseForProvider() {
Profile p = [SELECT Id FROM Profile WHERE Name='CF Manager Provider - force.com']; 
User u = new User(Alias = 'standt', Email='CFProvider@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='CFProvider@testorg.com');
insert u;
GroupMember GM = new GroupMember();
Group grp = [Select Id from Group where name='CF Providers'];
GM.GroupId = grp.Id;
GM.UserOrGroupId = U.Id;
insert GM;

System.runAs(u) {
Control_Function_Feedback__c cf = new Control_Function_Feedback__c();
CFFException ex1 = new CFFException(3);
CFFException ex2 = new CFFException();



apexpages.standardcontroller stdTask = new apexpages.standardcontroller(cf);  
    
ProviderOnly controller = new ProviderOnly(stdTask);

controller.currentYear= '';
controller.camelCasing= '';
controller.previousYear= '';
controller.urlForReports= '';
controller.feedbackRecordPopUp = new CFF_Historical_Feedback__c();
controller.currentFeedbackIds = new Set<String>();
controller.whereClause = '';
controller.CFAdminFilter = new Set<String>();
controller.LOBAdminFilter= new Set<String>();
controller.queryStringCurrentIds= '';
controller.controlFunction= '';
controller.selectedSlobs= new List<String>();
controller.queryString= '';
controller.queryStringHistory= '';
controller.loggedInUserId= '';
controller.isPopUpOpened= true;
controller.filtersApplied = false;
controller.isLOBSelected = false;
//controller.tempShareListForLOBAdmin= new List<Control_Function_Feedback__share>();
controller.cffForLOBAdmin= new Control_Function_Feedback__c();
controller.chfForLOBAdmin= new CFF_Historical_Feedback__c();
controller.uniqueLOBsList = new List<String>();
controller.feedbackDtl= new Control_Function_Feedback__c();
controller.feedbackDtlHistory= new CFF_Historical_Feedback__c();
controller.CFAdminControlFunction = ''; 
controller.feedbackYear= '';
controller.CFAdminControlFunctionForLOBAdmin = '';
controller.CFAdminControlFunctionForLOBAdminHistory = '';
controller.selectedLOBInTheFilter= new List<String>();
controller.selectedLOBs= '';
controller.selectedSLOBSInTheFilter= '';
controller.LOBAdmin= '';
controller.selectedTab = '';
controller.status= '';
controller.defaultTab= '';
controller.profile = new List<Profile>();
controller.myProfileName = '';
controller.feedbackStatus = new Map<String,Integer>();
controller.profileIndicator = new Map<String,Boolean>();
controller.classification = new Map<String,Integer>();
controller.checkEmployee = new Map<String,String>();
controller.checkEmployeeHistory = new Map<String,String>();
controller.reportForLOBAdmin = new Map<String,String>();
controller.reportForLOBAdminHistory = new Map<String,String>();
controller.CFFRating = '';
controller.CFFRatingTemp = '';
controller.recordId = '';
controller.buttonClicked = '';
controller.accountId = '';
controller.showCommentaryForLOBAdmin = false;
controller.showCommentaryForLOBAdminHistoryTab = false;
controller.showCommentaryForLOBAdminHistoryPopUp = false;
controller.tabClicked = '';
controller.fromTab = '';
controller.fromPopUp= '';
controller.accountIdPopUp= '';
controller.controlFunctionPopUp= '';
controller.openAsExcel();
controller.myProfileName = 'CF Manager Provider - force.com'; 
controller.loggedInUserId = userInfo.getUserId();
controller.currentYear = String.valueOf(Date.Today().Year());
controller.previousYear = String.valueOf(Date.Today().Year()-1);
List<Control_Function_Feedback__c> CFFeedback = null;
List<CFF_Historical_Feedback__c> CHFeedback = null;

List<Control_Function_Feedback__c> filterList = null;
controller.filtersApplied = true;
filterList = [select LOB__c,SUB_LOB__c from Control_Function_Feedback__c limit 2];
controller.selectedLOBInTheFilter = new List<String>();
controller.selectedSlobs = new List<String>();
Integer count = 0;
for(Control_Function_Feedback__c cff :filterList){
controller.selectedLOBInTheFilter.add(cff.LOB__c);
controller.selectedSlobs.add(cff.SUB_LOB__c);
}
/*controller.whereClause = ' where LOB__c  =:selectedLOBInTheFilter and SUB_LOB__c =:selectedSlobs';*/

controller.whereClause = ' where LOB__c  =:selectedLOBInTheFilter' ;

controller.applyFilters();

controller.selectedTab = '1';
controller.restoreDefaults();
CFFeedback = controller.getDataGrid();
controller.getLOBInfilter();
controller.getdependentValues();


controller.selectedTab = '11';
controller.restoreDefaults();
CHFeedback = controller.getHistoricalData();
controller.getLOBInfilter();
controller.getdependentValues();
controller.filtersApplied = false;

controller.selectedTab = '1';
controller.restoreDefaults();
CFFeedback = controller.getDataGrid();

controller.selectedTab = '11';
controller.restoreDefaults();
CHFeedback = controller.getHistoricalData();

controller.getCFFRatingOptions();
controller.takeMeToReports();
System.assertNotEquals(null,CFFeedback);



}
}

static testMethod void testCaseForPopUp() {

Pagereference pgref = null;
Profile p = [SELECT Id FROM Profile WHERE Name='CF Manager Provider - force.com']; 
User u = new User(Alias = 'standt', Email='CFProvider@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='CFProvider@testorg.com');
insert u;
GroupMember GM = new GroupMember();
Group grp = [Select Id from Group where name='CF Providers'];
GM.GroupId = grp.Id;
GM.UserOrGroupId = U.Id;
insert GM;

Control_Function_Feedback__c CFF = new Control_Function_Feedback__c();
CFF_Historical_Feedback__c CHF = new CFF_Historical_Feedback__c();

System.runAs(u) {
Control_Function_Feedback__c insertForTest = new Control_Function_Feedback__c();
insert insertForTest;
Control_Function_Feedback__c cf = new Control_Function_Feedback__c();
CFF_Historical_Feedback__c cfh = new CFF_Historical_Feedback__c();
cfh.name='test';
insert cfh;
apexpages.standardcontroller stdTask = new apexpages.standardcontroller(cf);  
     
ProviderOnly controller = new ProviderOnly(stdTask);
controller.myProfileName = 'CF Manager Provider - force.com';

controller.loggedInUserId = userInfo.getUserId();
controller.currentYear = String.valueOf(Date.Today().Year());
controller.previousYear = String.valueOf(Date.Today().Year()-1);
CFF =[select id from Control_Function_Feedback__c LIMIT 1] ;
CHF =[select id from CFF_Historical_Feedback__c LIMIT 1] ;

controller.selectedTab = '1';
controller.recordId = CFF.id;
controller.emplInfo();

controller.selectedTab = '11';
controller.recordId = CHF.id;
controller.emplInfo();


pgref = controller.emplInfo();         
system.debug('Current User: ' + UserInfo.getUserName());
system.debug('Current Profile: ' + UserInfo.getProfileId()); 
System.assertEquals(null,pgref);

}
}

static testMethod void testCaseForAutoSave(){

Profile p = [SELECT Id FROM Profile WHERE Name='CF Manager Provider - force.com']; 
User u = new User(Alias = 'standt', Email='CFProvider@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='CFProvider@testorg.com');
insert u;
GroupMember GM = new GroupMember();
Group grp = [Select Id from Group where name='CF Providers'];
GM.GroupId = grp.Id;
GM.UserOrGroupId = U.Id;
insert GM;

Control_Function_Feedback__c CFF = new Control_Function_Feedback__c();
String recordIdTemp = null;

System.runAs(u) {
Control_Function_Feedback__c insertForTest = new Control_Function_Feedback__c();
insert insertForTest;
Control_Function_Feedback__c cf = new Control_Function_Feedback__c();

apexpages.standardcontroller stdTask = new apexpages.standardcontroller(cf);   
   
ProviderOnly controller = new ProviderOnly(stdTask);
controller.myProfileName = 'CF Manager Provider - force.com'; 

controller.loggedInUserId = userInfo.getUserId();
controller.currentYear = String.valueOf(Date.Today().Year());
controller.previousYear = String.valueOf(Date.Today().Year()-1);
CFF =[select id from Control_Function_Feedback__c LIMIT 1] ;
recordIdTemp = CFF.id;
ProviderOnly.SaveTheForm('Meets','this is a test',CFF.id,true,'Draft');

System.assertNotEquals(null,recordIdTemp);
}
}

static testMethod void testCaseForUpdate() {

Profile p = [SELECT Id FROM Profile WHERE Name='CF Manager Provider - force.com']; 
User u = new User(Alias = 'standt', Email='CFProvider@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='CFProvider@testorg.com');
insert u;
GroupMember GM = new GroupMember();
Group grp = [Select Id from Group where name='CF Providers'];
GM.GroupId = grp.Id;
GM.UserOrGroupId = U.Id;
insert GM;

Control_Function_Feedback__c CFF = new Control_Function_Feedback__c();
String recordIdTemp = null;


System.runAs(u) {
Control_Function_Feedback__c insertForTest = new Control_Function_Feedback__c();
insert insertForTest;
Control_Function_Feedback__c cf = new Control_Function_Feedback__c();

apexpages.standardcontroller stdTask = new apexpages.standardcontroller(cf); 
      
ProviderOnly controller = new ProviderOnly(stdTask);
controller.myProfileName = 'CF Manager Provider - force.com';

controller.loggedInUserId = userInfo.getUserId();
controller.currentYear =  '2017';     /*String.valueOf(Date.Today().Year()); */
controller.previousYear =  '2016'; /*String.valueOf(Date.Today().Year()-1); */
CFF =[select id from Control_Function_Feedback__c LIMIT 1] ;
recordIdTemp = CFF.id;


System.assertNotEquals(null,recordIdTemp);
ProviderOnly.updFeedback('Meets','truejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjj','MAC',string.valueof(CFF.id)); 
 //updFeedback(String CFFRating, String commentary, String buttonClicked, String recId) {
//(String CFFRating, String commentary,String devlopmentCommentary, String buttonClicked, String recId) {
ProviderOnly.updFeedback('Meets','truejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjtruejjjjjjjjjjjjjjjjjjjjjjjjjthis is a test','SAD',string.valueof(CFF.id)); 
ProviderOnly.updFeedback('Meets','truejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj','MAC',string.valueof(CFF.id)); 

//(String CFFRating, String commentary,String devlopmentCommentary, String buttonClicked, String recId) {
ProviderOnly.updFeedback('Meets','true','SAD',string.valueof(CFF.id)); 
}
}


static testMethod void testCaseForManager() {

Profile p = [SELECT Id FROM Profile WHERE Name='CF Manager Provider - force.com']; 
User u = new User(Alias = 'standt', Email='CFManager@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='CFProvider@testorg.com');
insert u;

GroupMember GM = new GroupMember();
Group grp = [Select Id from Group where name='CF Manager'];
GM.GroupId = grp.Id;
GM.UserOrGroupId = U.Id;
insert GM;
System.runAs(u) {
Control_Function_Feedback__c cf = new Control_Function_Feedback__c();

apexpages.standardcontroller stdTask = new apexpages.standardcontroller(cf);   
    
ProviderOnly controller = new ProviderOnly(stdTask);
controller.myProfileName = 'CF Manager Provider - force.com';

controller.loggedInUserId = userInfo.getUserId();
controller.currentYear = String.valueOf(Date.Today().Year());
controller.previousYear = String.valueOf(Date.Today().Year()-1);
controller.reportForLOBAdmin = null;
controller.checkEmployee = null;
controller.CFAdminControlFunction = null;
controller.LOBAdmin = null;




controller.showCommentaryForLOBAdmin = null;


//controller.groupShareInfo = null;


List<Control_Function_Feedback__c> CFFeedback = null;
List<CFF_Historical_Feedback__c> CHFeedback = null;
List<Control_Function_Feedback__c> filterList = null;
controller.filtersApplied = true;
filterList = [select LOB__c,SUB_LOB__c from Control_Function_Feedback__c limit 2];
controller.selectedLOBInTheFilter = new List<String>();
controller.selectedSlobs = new List<String>();
Integer count = 0;
for(Control_Function_Feedback__c cff :filterList){
controller.selectedLOBInTheFilter.add(cff.LOB__c);
controller.selectedSlobs.add(cff.SUB_LOB__c);
}
/*controller.whereClause = ' where LOB__c IN :selectedLOBInTheFilter and SUB_LOB__c IN :selectedSlobs';*/

controller.whereClause = ' where LOB__c IN :selectedLOBInTheFilter' ;

controller.applyFilters();

controller.selectedTab = '2';
controller.restoreDefaults();
CFFeedback = controller.getDirectReportsForManager();


controller.selectedTab = '21';
controller.restoreDefaults();
CHFeedback = controller.getHistoricalData();


controller.filtersApplied = false;

controller.selectedTab = '2';
controller.restoreDefaults();
CFFeedback = controller.getDirectReportsForManager();

controller.selectedTab = '21';
controller.restoreDefaults();
CHFeedback = controller.getHistoricalData();

controller.takeMeToReports();
System.assertNotEquals(null,CFFeedback);

}
}

static testMethod void testCaseForCFAdminDefault() {

Profile p = [SELECT Id FROM Profile WHERE Name='CF Admin - force.com']; 
User u = new User(Alias = 'standt', Email='CFAdmin@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='CFProvider@testorg.com');
insert u;

GroupMember GM1 = new GroupMember();
Group grp1 = [Select Id from Group where name='CF Admins'];
GM1.GroupId = grp1.Id;
GM1.UserOrGroupId = U.Id;
insert GM1;

GroupMember GM2 = new GroupMember();
Group grp2 = [Select Id from Group where name='CF Admins Legal'];
GM2.GroupId = grp2.Id;
GM2.UserOrGroupId = U.Id;
insert GM2;

GroupMember GM3 = new GroupMember();
Group grp3 = [Select Id from Group where name='CF Admins CFO Group'];
GM3.GroupId = grp3.Id;
GM3.UserOrGroupId = U.Id;
insert GM3;

GroupMember GM4 = new GroupMember();
Group grp4 = [Select Id from Group where name='CF Admins Corp Audit'];
GM4.GroupId = grp4.Id;
GM4.UserOrGroupId = U.Id;
insert GM4;

GroupMember GM5 = new GroupMember();
Group grp5 = [Select Id from Group where name='CF Admins Global Compliance'];
GM5.GroupId = grp5.Id;
GM5.UserOrGroupId = U.Id;
insert GM5;

GroupMember GM6 = new GroupMember();
Group grp6 = [Select Id from Group where name='CF Admins Global Human Resources'];
GM6.GroupId = grp6.Id;
GM6.UserOrGroupId = U.Id;
insert GM6;

GroupMember GM7 = new GroupMember();
Group grp7 = [Select Id from Group where name='CF Admins Global Risk'];
GM7.GroupId = grp7.Id;
GM7.UserOrGroupId = U.Id;
insert GM7;

List<Control_Function_Feedback__c> filterList = null;
System.runAs(u) {
Control_Function_Feedback__c cf = new Control_Function_Feedback__c();

apexpages.standardcontroller stdTask = new apexpages.standardcontroller(cf); 
    
ProviderOnly controller = new ProviderOnly(stdTask);
controller.myProfileName = 'CF Admin - force.com';  

controller.loggedInUserId = userInfo.getUserId();
controller.currentYear = String.valueOf(Date.Today().Year());
controller.previousYear = String.valueOf(Date.Today().Year()-1);
controller.buildGroupSharedInfo();
controller.getCFAdminOptions();
List<Control_Function_Feedback__c> CFFeedback = null;
List<CFF_Historical_Feedback__c> CHFeedback = null;
controller.filtersApplied = true;

filterList = [select LOB__c,SUB_LOB__c from Control_Function_Feedback__c limit 2];
controller.selectedLOBInTheFilter = new List<String>();
controller.selectedSlobs = new List<String>();
Integer count = 0;
for(Control_Function_Feedback__c cff :filterList){
controller.selectedLOBInTheFilter.add(cff.LOB__c);
controller.selectedSlobs.add(cff.SUB_LOB__c);
}
controller.whereClause = ' where LOB__c IN :selectedLOBInTheFilter and SUB_LOB__c IN :selectedSlobs';
controller.applyFilters();
controller.selectedTab = '3';
controller.restoreDefaults(); 
CFFeedback = controller.getCFAdminGrid();


controller.selectedTab = '31';
controller.restoreDefaults(); 
CHFeedback = controller.getHistoricalData();



controller.filtersApplied = false;

controller.selectedTab = '3';
controller.restoreDefaults(); 
CFFeedback = controller.getCFAdminGrid();

controller.selectedTab = '31';
controller.restoreDefaults(); 
CHFeedback = controller.getHistoricalData();
         
controller.takeMeToReports();
System.assertNotEquals(null,CFFeedback);

}

}

static testMethod void testCaseForLOBAdminDefault() {

Profile p = [SELECT Id FROM Profile WHERE Name='LOB Admin - force.com']; 
User u = new User(Alias = 'standt', Email='CFLOBAdmin@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='CFProvider@testorg.com');
insert u;

GroupMember GM8 = new GroupMember();
Group grp8 = [Select Id from Group where name='LOB Admins Audit'];
GM8.GroupId = grp8.Id;
GM8.UserOrGroupId = U.Id;
insert GM8;

GroupMember GM9 = new GroupMember();
Group grp9 = [Select Id from Group where name='LOB Admins CFO'];
GM9.GroupId = grp9.Id;
GM9.UserOrGroupId = U.Id;
insert GM9;

GroupMember GM10 = new GroupMember();
Group grp10 = [Select Id from Group where name='LOB Admins Compliance'];
GM10.GroupId = grp10.Id;
GM10.UserOrGroupId = U.Id;
insert GM10;

GroupMember GM11 = new GroupMember();
Group grp11 = [Select Id from Group where name='LOB Admins Consumer'];
GM11.GroupId = grp11.Id;
GM11.UserOrGroupId = U.Id;
insert GM11;

GroupMember GM12 = new GroupMember();
Group grp12 = [Select Id from Group where name='LOB Admins GBAM'];
GM12.GroupId = grp12.Id;
GM12.UserOrGroupId = U.Id;
insert GM12;

GroupMember GM13 = new GroupMember();
Group grp13 = [Select Id from Group where name='LOB Admins GHR'];
GM13.GroupId = grp13.Id;
GM13.UserOrGroupId = U.Id;
insert GM13;

GroupMember GM14 = new GroupMember();
Group grp14 = [Select Id from Group where name='LOB Admins GMCA'];
GM14.GroupId = grp14.Id;
GM14.UserOrGroupId = U.Id;
insert GM14;

GroupMember GM15 = new GroupMember();
Group grp15 = [Select Id from Group where name='LOB Admins GT&O'];
GM15.GroupId = grp15.Id;
GM15.UserOrGroupId = U.Id;
insert GM15;

GroupMember GM16 = new GroupMember();
Group grp16 = [Select Id from Group where name='LOB Admins GWIM'];
GM16.GroupId = grp16.Id;
GM16.UserOrGroupId = U.Id;
insert GM16;

GroupMember GM17 = new GroupMember();
Group grp17 = [Select Id from Group where name='LOB Admins Legal'];
GM17.GroupId = grp17.Id;
GM17.UserOrGroupId = U.Id;
insert GM17;

GroupMember GM18 = new GroupMember();
Group grp18 = [Select Id from Group where name='LOB Admins Risk'];
GM18.GroupId = grp18.Id;
GM18.UserOrGroupId = U.Id;
insert GM18;

GroupMember GM19 = new GroupMember();
Group grp19 = [Select Id from Group where name='LOB Admins Strat Initiatives'];
GM19.GroupId = grp19.Id;
GM19.UserOrGroupId = U.Id;
insert GM19;

List<Control_Function_Feedback__c> filterList = null;

System.runAs(u) {

Control_Function_Feedback__c cf = new Control_Function_Feedback__c();

apexpages.standardcontroller stdTask = new apexpages.standardcontroller(cf);   
    
ProviderOnly controller = new ProviderOnly(stdTask);
controller.myProfileName = 'LOB Admin - force.com';

controller.loggedInUserId = userInfo.getUserId();
controller.currentYear = String.valueOf(Date.Today().Year());
controller.previousYear = String.valueOf(Date.Today().Year()-1);
controller.buildDataSetForLOBAdmin(); 
controller.buildDataSetForLOBAdminCFFHistory(); 
controller.getControlFunctionOptions();
controller.getLOBAdminOptions();

controller.CFAdminControlFunction = 'Corporate Audit';
controller.cmntryForLOBAdmin();
controller.cmntryForLOBAdminHistory();
controller.filtersApplied = true;
filterList = [select LOB__c,SUB_LOB__c from Control_Function_Feedback__c limit 2];
controller.selectedLOBInTheFilter = new List<String>();
controller.selectedSlobs = new List<String>();
Integer count = 0;
for(Control_Function_Feedback__c cff :filterList){
controller.selectedLOBInTheFilter.add(cff.LOB__c);
controller.selectedSlobs.add(cff.SUB_LOB__c);
}
controller.whereClause = ' where LOB__c IN :selectedLOBInTheFilter and SUB_LOB__c IN :selectedSlobs';
controller.applyFilters();

List<Control_Function_Feedback__c> CFFeedback = null;
List<CFF_Historical_Feedback__c> CHFeedback = null;

controller.selectedTab = '4';
controller.restoreDefaults(); 
CFFeedback = controller.getLOBAdminGrid();


controller.selectedTab = '41';

controller.restoreDefaults(); 
CHFeedback = controller.getHistoricalData();


controller.filtersApplied = false;


controller.selectedTab = '4';
controller.restoreDefaults();  
CFFeedback = controller.getLOBAdminGrid();

controller.selectedTab = '41';
controller.restoreDefaults(); 
CHFeedback = controller.getHistoricalData();
       
system.debug('Current User: ' + UserInfo.getUserName());
system.debug('Current Profile: ' + UserInfo.getProfileId()); 
controller.takeMeToReports();
controller.applyDefaultView();


controller.DummyAutoSave();
controller.WhenPopUpClosed();
controller.dummy();
controller.openFilters();
controller.enableFilterButton();
controller.myProfileName='CF Super User';
controller.getCFFRatingOptions();
controller.fromPopUp = 'true';
controller.cmntryForLOBAdminHistory(); 
controller.fromTab = 'true';
controller.cmntryForLOBAdminHistory(); 
System.assertNotEquals(null,CFFeedback);

    controller.redirect();
	  controller.toProperCase('test1,test2');
	  controller.convertStrToList('test1,test2');

}

}
/*
static testMethod void testAddToPublicGroup() {
Profile p = [SELECT Id FROM Profile WHERE Name='CF Manager Provider - force.com']; 
Profile superUser = [SELECT Id FROM Profile WHERE Name='CF Super User'];
User u = new User(Alias = 'standt',  Email='CFProvider@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='CFProvider@testorg.com');
User sp = new User(Alias = 'super',  Email='super@testorg.com',EmailEncodingKey='UTF-8', LastName='superTesting',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = superUser.Id,TimeZoneSidKey='America/Los_Angeles', UserName='SuperUser@testorg.com');

insert u;
insert sp;
u.ProfileId = superUser.Id; 
sp.ProfileId = p.Id;
update u;
update sp;
}
*/
/*
static testMethod void testManagedSharing(){
Profile p = [SELECT Id FROM Profile WHERE Name='CF Manager Provider - force.com']; 
Profile superUser = [SELECT Id FROM Profile WHERE Name='CF Super User'];

User testUser = new User(Alias = 'test',  Email='test@testorg.com',EmailEncodingKey='UTF-8', LastName='TestingTrigger',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='changereviewer@testorg.com');
User u = new User(Alias = 'standt',  Email='CFProvider@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='CFProvider@testorg.com');
User u2 = new User(Alias = 'super',  Email='super@testorg.com',EmailEncodingKey='UTF-8', LastName='superTesting',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = superUser.Id,TimeZoneSidKey='America/Los_Angeles', UserName='SuperUser@testorg.com');
User u3 = new User(Alias = 'super3',  Email='super3@testorg.com',EmailEncodingKey='UTF-8', LastName='superTesting3',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = superUser.Id,TimeZoneSidKey='America/Los_Angeles', UserName='SuperUser3@testorg.com');
 insert testUser;
 insert u;
 insert u2;
 insert u3;
System.runAs(testUser) {

     Test.startTest();
     try{
 Control_Function_Feedback__c cff = new Control_Function_Feedback__c ();
      cff.Audit_Type__c = 'Corporate Audit';
      cff.CF_Commentary__c = 'Hello' ;
      cff.Reviewer_1__c = u.id;
      cff.Reviewer_2__c = u2.id;
      cff.Reviewer_3__c = u3.id;
      cff.ownerid = testUser.id;
      insert cff;
      Control_Function_Feedback__c updReviewer = [select id from Control_Function_Feedback__c where id = :cff.id];
      updReviewer.Reviewer_1__c = u3.Id;
      updReviewer.Reviewer_2__c = u.id;
      updReviewer.Reviewer_3__c = u2.id; 
      updReviewer.ownerid = testUser.id;    
      update updReviewer ; 
      }catch(Exception ex){
      }
     // updReviewer.Reviewer_1__c = u2.Id;
     // updReviewer.Reviewer_2__c =  u3.id;
     // updReviewer.Reviewer_3__c =u.id;
     //update updReviewer ;
  
      Test.stopTest();
      
      }
    
  }*/

  

}