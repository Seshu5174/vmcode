public with sharing class ATOCT_Talon_Callouts_Batch implements Database.Batchable<ATOCT_ATO_Interface_Result__c>,Database.Stateful, Database.AllowsCallouts{
    DateTime initiatedTime = System.now(); 
    public Iterable<ATOCT_ATO_Interface_Result__c> start(Database.BatchableContext bc) 
    {
        return [Select Id,ATOCT_Record_Id__c,ATOCT_USAA_Number__c, ATOCT_Status__c, ATOCT_Counter__c from ATOCT_ATO_Interface_Result__c where ( ATOCT_Counter__c<3 And (ATOCT_Status__c = 'Failed' OR ATOCT_Status__c = 'Ready to Send' OR ATOCT_Status__c='Talon API Down'))];
    }
    
    public void execute(Database.BatchableContext bc, List<ATOCT_ATO_Interface_Result__c> interfaceList)
    {
        List<interfaceRequestWrapper> requestList = new List<interfaceRequestWrapper>();
        
        Map<Id,ATOCT_ATO_Interface_Result__c> interfaceMapResults = new Map<Id,ATOCT_ATO_Interface_Result__c>(interfaceList);
        
        for(ATOCT_ATO_Interface_Result__c inr: interfaceList)
        {
            interfaceRequestWrapper record = new interfaceRequestWrapper(inr.ATOCT_USAA_Number__c,inr.ATOCT_Record_Id__c,inr.Id);
            requestList.add(record);          
        }
        
        ATOCT_Auth_Setting__mdt authSetting = new ATOCT_Auth_Setting__mdt();
        
        authSetting = [Select EndPoint__c From ATOCT_Auth_Setting__mdt WHERE DeveloperName='ATOCT_Talon_POST_Data_Url' WITH SECURITY_ENFORCED];
        
        string accessToken = ATOCT_TalonAPI_Oauth.doAuth().accessToken;
        system.debug('accessToken >>>'+ accessToken);
        String authHeader = 'Bearer '+accessToken;   
        system.debug('authHeader >>>'+authHeader);
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http http = new Http();
        request.setMethod('POST');
        request.setEndpoint(authSetting.EndPoint__c);
        request.setHeader('Content-Type','application/json');      
        if(!Test.isRunningTest())
            request.setHeader('Authorization ', authHeader);
        request.setBody(JSON.serialize(requestList));
        
        system.debug('request body>> '+ request.getBody());
        
        if(String.isNotBlank(accessToken))
        {
            try
            {
                if(!Test.isRunningTest())
                {
                    response = http.send(request);  
                }
                
                else
                { 
                    ATOCT_ATO_Interface_Result__c interfaceRes =interfaceList[0];
                    MockHttpResponseGenerator mc = new MockHttpResponseGenerator(200, 'Ok', '[{"messageSent":true,"interfaceId":"'+interfaceRes.Id+'"}]', new Map<String, String>());
                    response = mc.respond(request);    
                }
                system.debug('Status ' + response.getStatus());
                system.debug('Status body' + response.getbody());
                system.debug('Status code' + response.getStatusCode());               
            }
            catch (Exception ex)
            {
                system.debug('callout exception >>> '+ ex.getMessage());
                // errorMessage += ex.getMessage()+ex.getStackTraceString();
            }
        }
        
        List<ATOCT_ATO_Interface_Result__c> interfacesOutput = new List<ATOCT_ATO_Interface_Result__c>();
        
        String result = response.getBody();
        integer code = response.getStatusCode();
        String statusResult = response.getStatus();
        string errorResponse = code+','+statusResult+','+result;
        
        if(response.getStatusCode()==200)
        {
            
            system.debug(' response body >> '+response.getBody());
            
            Map<Decimal,String> successMessageCount = new Map<Decimal,String>{0=>'First',1=>'Second',2=>'Third'};
                
            Boolean isSuccess = false;
            String respBody = response.getBody();
            isSuccess = String.isNotBlank(respBody)?respBody.containsIgnoreCase('True'):False;
            
            for(ATOCT_ATO_Interface_Result__c inr: interfaceList)
            {
                Id interfaceRecordId = inr.Id;
                Decimal counter = interfaceMapResults.get(interfaceRecordId).ATOCT_Counter__c;
                ATOCT_ATO_Interface_Result__c interfaceRecord = new ATOCT_ATO_Interface_Result__c(Id=interfaceRecordId,ATOCT_Status__c=isSuccess?successMessageCount.get(counter)+' Attempt Success':'Failed',ATOCT_Counter__c =counter+1);
                
                if(counter==0)
                    interfaceRecord.ATOCT_First_Attempt_Initiated__c = initiatedTime;
                else if(counter==1)
                    interfaceRecord.ATOCT_Second_Attempt_Initiated__c = initiatedTime;
                else if(counter==2)
                    interfaceRecord.ATOCT_Third_Attempt_Initiated__c = initiatedTime;
                
                interfacesOutput.add(interfaceRecord);
            }
            
        }
        
        // Bad request
        else if (response.getStatusCode() == 400)
        {
            ATOCT_Utility.errorLogCreationFromBatch(0, 0, 0, 'ATOCT_Talon_Callouts_Batch', 'Execute', 'ERROR', system.now(), errorResponse, 'Apex', 'Medium');
            system.abortJob(bc.getJobId()); 
            
        }
        
        // When Talon API down
        else
        {
            for(ATOCT_ATO_Interface_Result__c inr: interfaceList)
            {
                Id interfaceRecordId = inr.Id;
                Decimal counter = inr.ATOCT_Counter__c;
                ATOCT_ATO_Interface_Result__c interfaceRecord = new ATOCT_ATO_Interface_Result__c(Id=interfaceRecordId,ATOCT_Status__c='Talon API Down',ATOCT_Counter__c =counter+1);
                
                if(counter==0)
                    interfaceRecord.ATOCT_First_Attempt_Initiated__c = initiatedTime;
                else if(counter==1)
                    interfaceRecord.ATOCT_Second_Attempt_Initiated__c = initiatedTime;
                else if(counter==2)
                    interfaceRecord.ATOCT_Third_Attempt_Initiated__c = initiatedTime;
                interfacesOutput.add(interfaceRecord);  
                
            }
            ATOCT_Utility.errorLogCreationFromBatch(0, 0, 0, 'ATOCT_Talon_Callouts_Batch', 'Execute', 'ERROR', system.now(), errorResponse, 'Authentication', 'Medium');
        }
        
        update interfacesOutput;
        
        
    }    
    
    public void finish(Database.BatchableContext bc)
    {
        
        AsyncApexJob jobInfo = [SELECT Id, JobItemsProcessed, TotalJobItems, NumberOfErrors, ParentJobId, ExtendedStatus FROM AsyncApexJob where Id=:bc.getJobId()];
        
        if(jobInfo.NumberOfErrors > 0||Test.isRunningTest())
        {
            ATOCT_Utility.errorLogCreationFromBatch (jobInfo.JobItemsProcessed, jobInfo.TotalJobItems, jobInfo.NumberOfErrors, 'ATOCT_Talon_Callouts_Batch', 'Execute', 'ERROR', system.now(), jobInfo.ExtendedStatus, 'Apex', 'Medium');
        }
    } 
    public class interfaceRequestWrapper
    {
        public String memberNumber;
        public Id atoWorkItemId;
        public Id sfResultId;
        
        public interfaceRequestWrapper (String memberNumber, Id atoWorkItemId,Id sfResultId){
            this.memberNumber = memberNumber;
            this.atoWorkItemId = atoWorkItemId;
            this.sfResultId = sfResultId;
        }
        
    }
 
}