public with sharing class searchAccountController {
 
 @AuraEnabled
 public static List < account > fetchAccount(String searchKeyWord, String searchType, String searchIndustry, String searchPhone) {
  List < Account > returnList = new List < Account > ();
  List < Account > lstOfAccount = new List < Account > ();  
     system.debug('searchKeyWord '+ searchKeyWord);
     system.debug('searchType '+ searchType);
     system.debug('searchIndustry '+ searchIndustry);
     system.debug('searchPhone '+ searchPhone);
  String searchKey = searchKeyWord + '%';
  string typeString = searchType + '%';   
  string IndustryString = searchIndustry + '%'; 
  string PhoneString = searchPhone + '%'; 
        
  string sQuery = 'select id, Name, Type, Industry, Phone, Fax From Account Where Id!=null';
  
     if(searchKeyWord!=null && String.IsNotBlank(searchKeyWord)){
         sQuery +=' and (Name LIKE: searchKey)';
     }
     
     if(searchIndustry!=null && String.IsNotBlank(searchIndustry)){
         sQuery +=' and (Industry LIKE: IndustryString)';
     }
     
     if(searchType!=null && String.IsNotBlank(searchType)){
         sQuery +=' and (Type LIKE: typeString)';
     }
     
     if(searchPhone!=null && String.IsNotBlank(searchPhone)){
         sQuery +=' and (Phone LIKE: PhoneString)';
     }
 
  sQuery +=' order by Name ASC LIMIT 500';
  
     system.debug('sQuery>> '+ sQuery);
     lstOfAccount = Database.query(sQuery);
 
  for (Account acc: lstOfAccount) {
   returnList.add(acc);
  }
  return returnList;
 }
 
 /*@AuraEnabled
public static Account getAcc(String ID){
return[select name,Phone,Description from account where id= :ID];
}*/
}