public with sharing class AccountsExt { 
public list<Account> accountsList {get;set;}
public Integer lstsize {get;set;}
public map<Integer,String> groupListJsonMap{get;set;}
public list<Nomination_Record> listofNominatin{get;set;}
public String serializelistofNominatin{get;set;}

    public AccountsExt(ApexPages.StandardSetController controller) {
        groupListJsonMap  = new map<Integer,String>();
listofNominatin = new list<Nomination_Record>();
    accountsList =[select id,name,AccountNumber,(select id,name from contacts) from account];
    for(account a : accountsList){
    Nomination_Record nom = new Nomination_Record();
    nom.name = a.Name;
    nom.AccountNumber = a.AccountNumber;

    listofNominatin.add(nom);
    
    }
lstsize = accountsList.size()-1; 
serializelistofNominatin = JSON.serialize(listofNominatin);
    }
       public AccountsExt() {
        groupListJsonMap  = new map<Integer,String>();

    accountsList =[select Id,Name,AccountNumber,(select id,name from contacts) from account];
    for(account a : accountsList){
    Nomination_Record nom = new Nomination_Record();
    nom.Name = a.Name;
    nom.AccountNumber = a.AccountNumber;
    nom.Id = a.Id;

    listofNominatin.add(nom);
    
    }
lstsize = accountsList.size()-1; 
serializelistofNominatin = JSON.serialize(listofNominatin);
    }
public Class Nomination_Record{ 
public String Name ='';  
public String AccountNumber='';
public String Id='';
}

}