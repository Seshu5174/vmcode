public class PicklistSelectOption {
    
    public List<selectoption> myoptions{get;set;}
    
     public PicklistSelectOption(){
     
        selectoption  s = new selectoption ('A', 'ALL');
        selectoption  s1 = new selectoption ('one', 'Jan');
        selectoption  s2 = new selectoption ('two', 'Feb');
        
        myoptions = new List<selectoption>();
        myoptions.add(s);
        myoptions.add(s1);
        myoptions.add(s2);
        
        myoptions.add(new selectoption(' three', 'Mar'));
        myoptions.add(new selectoption('four', 'Apr'));
        
    }
    

}