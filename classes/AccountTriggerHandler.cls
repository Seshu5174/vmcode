public class AccountTriggerHandler {
    
    public void onAfterUpdate (Map<Id,Account> newAccount, Map<Id, Account> oldAccountMap){
        
        
        List<Recharge_and_Refocus__c> rrList = new List<Recharge_and_Refocus__c>([Select id, Employee_Name_Lkp__c from Recharge_and_Refocus__c where Employee_Name_Lkp__c in: newAccount.keyset()]); 
        map<ID, Recharge_and_Refocus__c> updateMap = new map<ID, Recharge_and_Refocus__c>();
        
        
        for(Recharge_and_Refocus__c rr: rrList){
            			
            Account old  = oldAccountMap.get(rr.Employee_Name_Lkp__c);
            Account ac  = newAccount.get(rr.Employee_Name_Lkp__c);
            
            string directManager;
            if(ac.Manager_Full_Name__c !=old.Manager_Full_Name__c || ac.Manager_Email_Address__c!=old.Manager_Email_Address__c || ac.Continuous_Service_Date__c!=old.Continuous_Service_Date__c|| ac.Level_1_LOB__c!=old.Level_1_LOB__c|| ac.Level_2_LOB__c!=old.Level_2_LOB__c|| ac.Division_Name__c!=old.Division_Name__c){
				Recharge_and_Refocus__c updateRR = new Recharge_and_Refocus__c(Id= rr.Id);
                
                if(ac.Manager_Full_Name__c !=old.Manager_Full_Name__c){
                    if(ac.Preferred_Manager_First_Name__c!=null){
                        directManager=ac.Preferred_Manager_First_Name__c+ ' '+ac.Preferred_Manager_Last_Name__c;
                    }
                    else{
                        directManager =  ac.Manager_Full_Name__c;
                    }
                    
                    updateRR.Direct_Manager__c = directManager;
                    
                }
                
                if(ac.Manager_Email_Address__c!=old.Manager_Email_Address__c)
                {
                    updateRR.Direct_Manager_Email__c = ac.Manager_Email_Address__c;
                    
                }
                if(ac.Continuous_Service_Date__c!=old.Continuous_Service_Date__c){
                    updateRR.Continuous_Service_Date__c= ac.Continuous_Service_Date__c.substring(0,10);
                    
                }
                
                if(ac.Level_1_LOB__c!=old.Level_1_LOB__c){
                    updateRR.LOB__c= ac.Level_1_LOB__c;
                    
                }
                if(ac.Level_2_LOB__c!=old.Level_2_LOB__c){
                    updateRR.SUB_LOB__c= ac.Level_2_LOB__c;
                    
                }
                if(ac.Division_Name__c!=old.Division_Name__c){
                    updateRR.Division__c= ac.Division_Name__c;
                    system.debug('Inside of division Name');
                }                    
                
                updateMap.put(updateRR.ID, updateRR);
            }
            
            
        }
        
        update updateMap.values();
        
    }
}