// Copyright 2016, Sales Engineering, Salesforce.com Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// - Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// - Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// - Neither the name of the salesforce.com nor the names of its contributors
//   may be used to endorse or promote products derived from this software
//   without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
({
    //Component Name: News_Announcements display name "Recent News"
    //Author : Raj Rao, Principal Solution Engineer, Salesforce.com
    //Date Released : March 25, 2016
    doInit : function(component, event, helper) {
        helper.loadNewsChannelNames(component);
        helper.loadnumDocsByChannel(component);//get number of records per News Channel
        //if more link has been clicked check for channel name in the URL
        var newsURL = window.location.href;
      	if (newsURL.indexOf('channel/') != -1) {
            var selectedChannel = decodeURI(newsURL.substr(newsURL.indexOf('channel/') + 8));
            component.set("v.selectedNewsChannel",selectedChannel);
            helper.loadNewsChannelRecordsByChannel(component, event);
        }else{
            //get news records by first news channel name
            helper.loadFirstNewsChannelRecords(component, event);
        }
    },   
    displayNewsChannelRecords: function(component, event, helper){
        //changed from event.target.value to event.target.getAttribute('data-value') to render with Locker Service Enabled
        component.set("v.selectedNewsChannel",event.target.getAttribute('data-value'));
        component.set("v.encodedSelectedNewsChannel",encodeURIComponent(event.target.getAttribute('data-value')));
        helper.loadNewsChannelRecordsByChannel(component); // get news records by selected news channel name
    },
 	showHideMenu : function(component, event) {
    	var cmpTarget = component.find('newsMenu');
       	var menuState = component.get('v.menuState');
        if(menuState === "Collapsed"){
            $A.util.addClass(cmpTarget, 'slds-is-open');
            component.set('v.menuState','Expanded');
        }else{
            $A.util.removeClass(cmpTarget, 'slds-is-open');
			component.set('v.menuState','Collapsed');
        } 
    },
    /** formController.js **/
    waiting : function(component, event, helper) {
    	component.set("v.wait", "updating...");
	},
	doneWaiting : function(component, event, helper) {
    	component.set("v.wait", "");
	},
})