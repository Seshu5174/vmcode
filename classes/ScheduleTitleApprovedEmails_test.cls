@istest

public class ScheduleTitleApprovedEmails_test{
    public testMethod static void testSchedule(){
    
 Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='ERUser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='ERUser@testorg.com');
        insert u;

        System.runAs(u) {
Test.StartTest();
ScheduleTitleApprovedEmails sh1 = new ScheduleTitleApprovedEmails();      
 String sch = '0 0 23 * * ?';
        system.schedule('Test check', sch, sh1);
Test.stopTest();
}
}
}