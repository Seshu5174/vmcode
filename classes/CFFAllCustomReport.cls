public with sharing class CFFAllCustomReport{

   
public List<SelectOption> repOptions{get;set;}
public List<SelectOption> lobOptions{get;set;}
public List<SelectOption> cffOptions{get;set;}
Public String selReport{get; set;}
Public String selLOB{get; set;}

public List<SelectOption> pdfOptions{get;set;}
Public String selPdfRep{get; set;}

Public String selCFF{get; set;}

public List<SelectOption> resultOptions{get;set;}
Public String selResultRep{get; set;}
Public boolean isCFAdmin{get;set;}

public CFFAllCustomReport()
 {
     selReport = 'None';
     selPdfRep ='None';
     selLOB = 'None';
        selCFF ='None';
        selResultRep='None';
    isCFAdmin = true;
    //Get User Profile and check if it CF Admin
     if([SELECT count() from User where Id =:Userinfo.getUserId()] > 0)
    {
     User pName = [SELECT Profile.Name from User where Id =:Userinfo.getUserId()];
    if(pName.Profile.Name.equals('CF Admin - force.com'))
    {
        isCFAdmin = false;
    }
    
    }    
    resultOptions = new List<SelectOption>();
    resultOptions.add(new SelectOption('None','None'));
    resultOptions.add(new SelectOption('CFF_RespSummaryReport','CFF_RespSummaryReport'));
    resultOptions.add(new SelectOption('CFF_StatusByLOB','CFF_StatusByLOB'));
    
    //Enterpriselevel report only to super users
    if(isCFAdmin)
    {
    resultOptions.add(new SelectOption('CFF_EnterpriseLevelByLOB', 'CFF_EnterpriseLevelByLOB'));
    }
     repOptions = new List<SelectOption>();
 //repOptions.add(new SelectOption('None','None'));
 repOptions.add(new SelectOption('CFF_StatusByFunction','CFF_StatusByFunction'));
    
  pdfOptions = new List<SelectOption>();
  pdfOptions.add(new SelectOption('None','None'));
  pdfOptions.add(new SelectOption('CFFMSWordPage','CFF MSWord Page'));
  pdfOptions.add(new SelectOption('CFFPDFPage','CFF PDF Page'));


cffOptions = new List<SelectOption>();
cffOptions.add(new SelectOption('None','None'));
cffOptions.add(new SelectOption('Corporate Audit','Corporate Audit'));
cffOptions.add(new SelectOption('CFO Group','CFO Group'));
cffOptions.add(new SelectOption('Global Compliance','Global Compliance'));
cffOptions.add(new SelectOption('Global Human Resources','Global Human Resources'));
cffOptions.add(new SelectOption('Legal','Legal'));
cffOptions.add(new SelectOption('Global Risk','Global Risk'));

    lobOptions = new List<SelectOption>();
     lobOptions.add(new SelectOption('None','None'));
     lobOptions.add(new SelectOption('CAO','CAO'));
     lobOptions.add(new SelectOption('CEO','CEO'));
     lobOptions.add(new SelectOption('CFO','CFO'));
  lobOptions.add(new SelectOption('Compliance','Compliance'));
  lobOptions.add(new SelectOption('Consumer','Consumer'));
  lobOptions.add(new SelectOption('GBAM','GBAM'));
  lobOptions.add(new SelectOption('GHR','GHR'));
  lobOptions.add(new SelectOption('GMCA','GMCA'));
    lobOptions.add(new SelectOption('GT_O', 'GT&O'));
lobOptions.add(new SelectOption('GWIM', 'GWIM'));
    lobOptions.add(new SelectOption('Legal','Legal'));
     lobOptions.add(new SelectOption('Risk','Risk'));


 }
 
public PageReference goToReport() {
      
       PageReference pageRef = new PageReference('/apex/CFF_RespSummaryReport');
       pageRef.setRedirect(true);
       return pageRef;

    }
    
    public pagereference generatePDFZip(){
        pagereference pg = new pagereference('/apex/'+selPdfRep+'?LOBName='+selLOB);
        pg.setredirect(true);
        return pg;
    }
    
     public PageReference passValueToCtl() {
     //selReport ='CFF_RespSummaryReport';
    return null;
            }


}