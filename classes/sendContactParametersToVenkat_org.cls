global class sendContactParametersToVenkat_org {
    
    public void sendContact(string fname, string lname, string email, string title, id id1){
        sendContactInfo(id1);
    }
    
    @future (callout=true)
    public static void sendContactInfo(id id2){
        contact condata = [select Firstname, lastname, Email, Title From contact where id =:id2];
        string fname = condata.Firstname;
        string lname = condata.LastName;
        string email = condata.Email;
        string title = condata.Title;
        
        partnerSoapSforceCom.Soap mypartnersoap = new partnerSoapSforceCom.Soap();  // Main class:partnerSoapSforceCom  Sublcass:Soap which contains login method
        
        partnerSoapSforceCom.LoginResult partnerloginresult = mypartnersoap.login('venkat.makke@sf.com', 'Naidu174NKFpSLDFwC4gsq9KdDqAtnszf' ); //  Return type of login method is "partnerSoapSforceCom.LoginResult"
        
         system.debug ('Generated session id is: '+ partnerloginresult.sessionId); // Generating SessionID
        
         string sessionidfetch =  partnerloginresult.sessionId;    // storing sessionID
        
        soapSforceComSchemasClassReceivepar.sessionHeader_element webservicesessionheader = new soapSforceComSchemasClassReceivepar.sessionHeader_element(); 
        
        webservicesessionheader.sessionId = sessionidfetch; // passing session id to session header element
        
        soapSforceComSchemasClassReceivepar.receiveParametersFromSalesforceDevorg  obj = new soapSforceComSchemasClassReceivepar.receiveParametersFromSalesforceDevorg();
        
        obj.sessionHeader = webservicesessionheader; //passing session header
        
        string status = obj.createContact(fname, lname, email, title);
        system.debug('status of task: '+ status);
        
    }

}