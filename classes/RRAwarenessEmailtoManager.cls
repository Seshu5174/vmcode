global class RRAwarenessEmailtoManager implements Database.Batchable<sObject>,Database.stateful, Schedulable
{
    Boolean isHistoryObject = false;
    
    public void execute(SchedulableContext sc) {
        Database.executeBatch(this, 200);
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        
        
        RR_Setting__mdt[] RR_settings = [SELECT Eligibility_Year__c,DeveloperName FROM RR_Setting__mdt];
        map<String,String> year_map = new map<String,String>();
        for(RR_Setting__mdt year_data: RR_settings){
            year_map.put(year_data.DeveloperName, year_data.Eligibility_Year__c);
        }
        
        string  currentYear = year_map.get('Current_Year');
        string todayDate = date.today().format();
        String query;
        
        if(todayDate=='7/1/2020'){
            isHistoryObject = false;
            query = 'SELECT Id, Name, Direct_Manager__c, Direct_Manager_Email__c,  Employee_Full_Name__c, Eligibility_Year__c, Continuous_Service_Date__c, How_Many_Weeks_Eligible__c FROM Recharge_and_Refocus__c where Eligibility_Year__c=:currentYear and Direct_Manager_Email__c!=\'\' order by Direct_Manager_Email__c';     
        }
        
        else {
            isHistoryObject = true;
            query = 'SELECT Id, IsDeleted, ParentId, CreatedById, CreatedDate, Field, OldValue, NewValue FROM Recharge_and_Refocus__History where (Field=\'Direct_Manager_Email__c\' or Field=\'Direct_Manager__c\') and CreatedDate=Today';
            
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Sobject> scope) 
    {
        
        RR_Setting__mdt[] RR_settings = [SELECT Eligibility_Year__c,DeveloperName FROM RR_Setting__mdt];
        map<String,String> year_map = new map<String,String>();
        for(RR_Setting__mdt year_data: RR_settings){
            year_map.put(year_data.DeveloperName, year_data.Eligibility_Year__c);
        }
        
        
        string  currentYear = year_map.get('Current_Year');
        string todayDate = date.today().format();
        system.debug('isHistoryObject:'+isHistoryObject);
        List<Recharge_and_Refocus__c> ManagerList = new List<Recharge_and_Refocus__c>();
        if(isHistoryObject){
            List<Id> RRIds = new List<Id>();
            Set<Id> RRIdsSet = new Set<Id>();
            List<Recharge_and_Refocus__History> ManagerHistoryList = new List<Recharge_and_Refocus__History>();
            ManagerHistoryList  = (List<Recharge_and_Refocus__History>) scope;
            for(Recharge_and_Refocus__History RRHistory: ManagerHistoryList){
                RRIdsSet.add(RRHistory.ParentId);
            }
            system.debug('RRIdsSet >> '+ RRIdsSet);
            system.debug('RRIdsSet Size>> '+ RRIdsSet.size());
            
            if(!RRIdsSet.isEmpty()){
                RRIds.addAll(RRIdsSet);
            }
            ManagerList = database.query('SELECT Id, Name, Direct_Manager__c, Direct_Manager_Email__c,  Employee_Full_Name__c, Eligibility_Year__c, Continuous_Service_Date__c, How_Many_Weeks_Eligible__c FROM Recharge_and_Refocus__c where Eligibility_Year__c=:currentYear and Id IN: RRIds and Direct_Manager_Email__c!=\'\' order by Direct_Manager_Email__c');
            
        }
        else{
            ManagerList = (List<Recharge_and_Refocus__c> ) scope;
            
        }
        system.debug('ManagerList>> '+ManagerList);
        system.debug('ManagerList Size>> '+ManagerList.size());
        Id orgWidAddrId;
        for(OrgWideEmailAddress owa : [select id, Address, DisplayName from OrgWideEmailAddress]) 
        {
            if(owa.DisplayName.contains('Recharge'))
                orgWidAddrId = owa.Id;
        } 
        
        List<string> mailbox = new List<string>{'ghr.mis1@bankofamerica2.com'};
            
            Boolean isAdded = false;
        Map<String,List<Recharge_and_Refocus__c>> ManagerToEmployees = new Map<String,List<Recharge_and_Refocus__c>>();
        Map<String,List<String>> ManagerToEmail = new Map<String,List<String>>();
        
        for(Recharge_and_Refocus__c  randr:  ManagerList)
        {
            // = (Recharge_and_Refocus__c) genricInfo;
            String emailInfo = randr.Direct_Manager_Email__c.toLowerCase();
            String emailReplace = emailInfo.replace('bankofamerica.com','bofa.com');
            
            if(ManagerToEmployees.containskey(emailReplace)){
                ManagerToEmployees.get(emailReplace).add(randr);
                ManagerToEmail.get(emailReplace).add(emailReplace); 
            }
            else{
                ManagerToEmployees.put(emailReplace,new List<Recharge_and_Refocus__c>{randr});
                ManagerToEmail.put(emailReplace,new List<String>{emailReplace});
                
            }
        }
        
        List<Messaging.SingleEmailMessage>  listEmail = new  List<Messaging.SingleEmailMessage> ();
        
        for (string ManagerEmail: ManagerToEmployees.keyset())
        {
            String htmlBody = '';
            Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage(); 
            
            String emailBody = '<div style="width:850px;margin:0 auto;"><div style="text-align: right;color:black;">'+system.label.Email_Logo+'</div>';  
            
            emailBody+='<div style="font-size:24px;font-family:calibri;padding-top:25px;color:black;">'+'Employee(s) on your team are eligible to participate in the Global Recharge & Refocus Program ' +'</div>' ;
            
            emailBody+='<div style="font-size:18px;font-family:calibri;padding-top:20px;color:black;">'+'As part of our commitment to support our teammates’ emotional wellness and in celebration of long-tenured careers, employee(s) on your team are now eligible to participate in the Global  ' + '<a href="'+system.Label.RR_HR_Connect+'" style="text-decoration:none;color:blue;">Recharge & Refocus program</a>'+'.'+'</div>' ;
            
            
            htmlBody += '<table border="1" style="border-collapse: collapse;margin-top:15px;margin-bottom:25px;border:1px solid black;width:90%;font-size:15px;font-family:calibri;"><tr><th style="width:33%;color:white;font-weight:100;background-color:dimgray;font-weight:600;">Eligible employee name</th><th style="width:33%;color:white;font-weight:100;background-color:dimgray;font-weight:600;">Continuous service date</th><th style="width:33%;color:white;font-weight:100;background-color:dimgray;font-weight:600;">Number of weeks eligible</th></tr>';
            
            for(Recharge_and_Refocus__c rrdetails: ManagerToEmployees.get(ManagerEmail)){
                
                htmlBody += '<tr><td style="text-align:center;">' + rrdetails.Employee_Full_Name__c + '</td><td style="text-align:center;">' + rrdetails.Continuous_Service_Date__c + '</td><td style="text-align:center;">' + rrdetails.How_Many_Weeks_Eligible__c + '</td></tr>';
            }
            
            htmlBody += '</table>';
            emailBody +=htmlBody;
            emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:5px;color:black;background-color:#EFEFEF;padding-left:10px;margin-right:20px;"><b>'+'What’s next?' +'</b>'+'</div>';
            emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:10px;display:flex;color:black;background-color:#EFEFEF;margin-right:20px;padding-left:10px;padding-right:15px;"><div >'+'• '+ '</div><div style="padding-left:15px;margin-right:20px;"><b>'+ ' Your employee(s) will receive notification that they’re eligible to participate, ' +'</b>'+' along with resources to assist their decision to request time off or decline to participate, including the '+'<a href="'+system.Label.RR_Leave_Guide+'" style="text-decoration:none;color:blue;">Recharge & Refocus guide</a>'+ ' which outlines how the program works.'+'</div></div>';
            emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:5px;display:flex;color:black;background-color:#EFEFEF;margin-right:20px;padding-left:10px;padding-right:15px;"><div>'+'• '+ '</div><div style="padding-left:15px;margin-right:20px;"><b>'+' Eligible employees will be asked to schedule a discussion with you '+'</b>'+ 'you if they want to participate, including how much time off they’d like to take (based on the total number of weeks eligible), when they’d like to take time off and any other important details to note. Employees also can choose not to participate. '+'</div></div>';
            emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:5px;padding-left:45px;display:flex;color:black;background-color:#EFEFEF;margin-right:20px;padding-right:15px;"><div>'+'o'+'</div><div style="padding-left:10px;margin-right:20px;"> '+'    Please note: Employees will need your verbal approval prior to submitting a request for time off.'+'</div></div>';
            emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:5px;display:flex;color:black;background-color:#EFEFEF;margin-right:20px;padding-bottom:10px;padding-left:10px;"><div>'+'• '+ '</div><div style="padding-left:15px;margin-right:20px;padding-right:15px;"><b>'+ ' Once your employee submits a request for time off, or declines to participate, you’ll receive a notice with next steps to take action.' +'</b>'+' Requests should be approved at least 60 days prior to the first day of the requested time off. You can check the status of your team’s requests at any time by visiting the '+'<a href="'+system.Label.RR_Request_Site_Link+'" style="text-decoration:none;color:blue;">Recharge & Refocus request site.</a></div></div>';
            emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:20px;color:black;"><b>'+'Questions?' +'</b>'+'</div>';
            emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:2px;color:black;">'+'Visit '+'<a href="'+system.Label.RR_HR_Connect+'" style="text-decoration:none;color:blue;">HR Connect > XXX</a>'+', read our '+ '<a href="'+system.Label.RR_FAQs_for_managers+'" style="text-decoration:none;color:blue;">frequently asked questions for managers</a>'+' or '+ '<a href="'+system.Label.RR_contact_us+'" style="text-decoration:none;color:blue;">contact us </a>'+'for more information.'+'</div>';
            emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:40px;color:black;">'+'Visit '+'<a href="'+system.Label.RR_HR_Connect+'" style="text-decoration:none;color:blue;">HR Connect</a>'+' for more information about available benefits. '+ '<a href="'+system.Label.RR_Review+'" style="text-decoration:none;color:blue;">Review </a>'+' important '+ '<a href="'+system.Label.RR_Legal_Disclaimer+'" style="text-decoration:none;color:blue;">legal disclaimer</a>'+'.'+'</div>';
            emailBody +='<div style="font-size:15px;font-family:calibri;color:black;padding-top:15px;">'+'Bank of America N.A. Member FDIC © 2020 Bank of America Corporation. All rights reserved.' +'</div> </div>';
            
            semail.setToAddresses(ManagerToEmail.get(ManagerEmail));
            semail.setOrgWideEmailAddressId(orgWidAddrId);  
            
            semail.setSubject('Employee(s) on your team are eligible to participate in the Global Recharge & Refocus Program');
            semail.setHTMLBody(emailBody);
            listEmail.add(semail);
        }
        
        try{
            Messaging.sendEmail(listEmail);
        }
        catch(EmailException emailExp){
            system.debug('Exception: '+emailExp);
            
        }
        
    }
    global void finish(Database.BatchableContext BC) 
    {
    }
}