public class importDataFromCSVController {
public Blob csvFileBody{get;set;}
public string csvAsString{get;set;}
public String[] csvFileLines{get;set;}
public List<Market_President__c> acclist{get;set;}
  public importDataFromCSVController(){
    csvFileLines = new String[]{};
    acclist = New List<Market_President__c>(); 
  }
  
  public void importCSVFile(){
       try{
           csvAsString = csvFileBody.toString();
           csvFileLines = csvAsString.split('\n'); 
            
           for(Integer i=1;i<csvFileLines.size();i++){
               Market_President__c accObj = new Market_President__c() ;
               string[] csvRecordData = csvFileLines[i].split(',');
               accObj.Name__c= csvRecordData[0] ;             
               accObj.Market_Role__c = csvRecordData[1];
               accObj.LOB__c = csvRecordData[2];
               accObj.Sub_LOB__c = csvRecordData[3];                                                                             
               acclist.add(accObj);   
           }
        insert acclist;
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importin data Please make sure input csv file is correct');
            ApexPages.addMessage(errorMessage);
        }  
  }
}