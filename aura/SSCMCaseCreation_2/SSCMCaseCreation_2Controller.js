({
handleLoad  : function(component, event, helper) {

}, 
secondryfieldChanged : function(component, event, helper) {
helper.primaryOrSecondryFieldChange(component,event,helper);

},
primaryfieldChanged : function(component, event, helper) {
helper.primaryOrSecondryFieldChange(component,event,helper);

var primary = component.find('Inquiry_Summary_Topic_fld').get("v.value");

if(!$A.util.isEmpty(primary)){
var action = component.get("c.retrieveInquirySummaryTopic");

action.setParams({
'topicId': primary

});
action.setCallback(this, function(response) {
// hide spinner when response coming from server 
var state = response.getState();

if (state === "SUCCESS") {
var storeResponse = response.getReturnValue();
console.log('storeResponse:'); 
console.log(storeResponse);
var caseInfo = storeResponse;
component.find("Inquiry_Category_1__fld").set("v.disabled",false);
if(!$A.util.isEmpty(caseInfo.Inquiry_Category_1__c)) {                       

component.find("Inquiry_Category_1__fld").set("v.value",caseInfo.Inquiry_Category_1__c);
component.find("Inquiry_Category_1__fld").set("v.disabled",true);

}
else{
component.find("Inquiry_Category_1__fld").set("v.value",'');

}
// component.find("Inquiry_Category_1__fld").set("v.value",caseInfo.Inquiry_Category_1__c);
// component.set("v.setField3Info",false);

component.set("v.setField2Info", !component.get("v.setField2Info"));

//  component.set("v.setField2Info",true);
//   alert("setting fld render");
if(component.get("v.setField2Info")){
if((component.find("Inquiry_Category_2__flhd")!=undefined)&&(component.find("Inquiry_Category_2__flhd")!=null)){
console.log("Inquiry_Category_2__flhd");

console.log(component.find("Inquiry_Category_2__flhd"));
//    alert(component.find("Inquiry_Category_2__flhd"));
//  alert( $A.util.isEmpty(component.find("Inquiry_Category_2__flhd")));


component.find("Inquiry_Category_2__flhd").set("v.disabled",false);
//   alert("disabled false");

if(!$A.util.isEmpty(caseInfo.Inquiry_Category_2__c)){
//   alert("value is not empty");

component.find("Inquiry_Category_2__flhd").set("v.value",caseInfo.Inquiry_Category_2__c);
component.find("Inquiry_Category_2__flhd").set("v.disabled",true);
//   alert("setting disabled3");

}
else
component.find("Inquiry_Category_2__flhd").set("v.value",'');

}

}
else{
if((component.find("Inquiry_Category_2__fld")!=undefined)&&(component.find("Inquiry_Category_2__fld")!=null)){
console.log("Inquiry_Category_2__fld");

console.log(component.find("Inquiry_Category_2__fld"));
//     alert(component.find("Inquiry_Category_2__fld"));
//   alert( $A.util.isEmpty(component.find("Inquiry_Category_2__fld")));


component.find("Inquiry_Category_2__fld").set("v.disabled",false);
//  alert("disabled false");

if(!$A.util.isEmpty(caseInfo.Inquiry_Category_2__c)){
//   alert("value is not empty");

component.find("Inquiry_Category_2__fld").set("v.value",caseInfo.Inquiry_Category_2__c);
component.find("Inquiry_Category_2__fld").set("v.disabled",true);
//  alert("setting disabled3");

}
else
component.find("Inquiry_Category_2__fld").set("v.value",'');

}

}
//  alert("setting value");

component.set("v.setField3Info",!component.get("v.setField3Info"));
if(component.get("v.setField3Info")){
component.find("Inquiry_Category_3__flhd").set("v.disabled",false);

if(!$A.util.isEmpty(caseInfo.Inquiry_Category_3__c)) {                       

component.find("Inquiry_Category_3__flhd").set("v.value",caseInfo.Inquiry_Category_3__c);
component.find("Inquiry_Category_3__flhd").set("v.disabled",true);

}
else
component.find("Inquiry_Category_3__flhd").set("v.value",'');
}
else{
component.find("Inquiry_Category_3__fld").set("v.disabled",false);

if(!$A.util.isEmpty(caseInfo.Inquiry_Category_3__c)) {                       

component.find("Inquiry_Category_3__fld").set("v.value",caseInfo.Inquiry_Category_3__c);
component.find("Inquiry_Category_3__fld").set("v.disabled",true);

}
else
component.find("Inquiry_Category_3__fld").set("v.value",'');

}
component.set("v.relatedCase",storeResponse);
}
});

$A.enqueueAction(action);


}
else{
console.log("primary topic is empty");
component.find("Inquiry_Category_1__fld").set("v.disabled",false);


component.find("Inquiry_Category_1__fld").set("v.value",'');
if(component.find("Inquiry_Category_2__fld")!=undefined&&component.find("Inquiry_Category_2__fld")!=null){
component.find("Inquiry_Category_2__fld").set("v.value",'');
component.find("Inquiry_Category_2__fld").set("v.disabled",false);

}

if(component.find("Inquiry_Category_2__flhd")!=undefined&&component.find("Inquiry_Category_2__flhd")!=null){
component.find("Inquiry_Category_2__flhd").set("v.value",'');
component.find("Inquiry_Category_2__flhd").set("v.disabled",false);
}
if(component.find("Inquiry_Category_3__fld")!=undefined&&component.find("Inquiry_Category_3__fld")!=null){
component.find("Inquiry_Category_3__fld").set("v.value",'');
component.find("Inquiry_Category_3__fld").set("v.disabled",false);

}
if(component.find("Inquiry_Category_3__flhd")!=undefined&&component.find("Inquiry_Category_3__flhd")!=null){
component.find("Inquiry_Category_3__flhd").set("v.value",'');
component.find("Inquiry_Category_3__flhd").set("v.disabled",false);

}

component.set("v.setField3Info",!component.get("v.setField3Info"));
component.set("v.setField2Info", !component.get("v.setField2Info"));


}


},
addSubject : function(component, event, helper) {
console.log("info");
component.set("v.isCancelEnabled",true);
console.log(component.find("isSubjectInquirer").get("v.value"));
var isRepPrepopulated = component.get("v.isSCRepresetative");
if(component.find("isSubjectInquirer").get("v.value")){
component.set("v.subjectInfo",component.get("v.accountName").Name);
if(!isRepPrepopulated)
component.find("Inquirer_Relationship_to_Subject__fld").set("v.value","Self");
component.set("v.isAddSubject",false);
}
else{
component.set("v.subjectInfo","");   
component.set("v.isAddSubject",true);
if(!isRepPrepopulated)
component.find("Inquirer_Relationship_to_Subject__fld").set("v.value","");

}

helper.displaySCRepresentativeError(component,event,helper);
helper.checkingRequiredFieldsFilledhelper(component,event,helper); 

},	isQCReqInfo: function(component, event, helper) {

console.log("info");
console.log(component.find("isQCReqField").get("v.value"));
component.set("v.isQCRequired",component.find("isQCReqField").get("v.value"));        
helper.checkingRequiredFieldsFilledhelper(component,event,helper);
component.set("v.isCancelEnabled",true);
if(!component.find("isQCReqField").get("v.value")){
component.set("v.QCReason","");
}
},
startprocess: function(component, event, helper) {
console.log('startprocess==>')
component.set("v.isSpinner",true);
},
handleSubmit: function(component, event, helper) {
component.set("v.isSpinner",true);
console.log('handleSubmit:isAccountFilled'+component.get("v.recordId"));
console.log('handleSubmit:isAccountFilled'+component.get("v.subjectsInfo"));
component.set("v.isButtonClicked",true);
component.find("contactRecordCreator").getNewRecord(
"SSCM_Subject__c", // sObject type (entityAPIName)
null,      // recordTypeId
false,     // skip cache?
$A.getCallback(function() {
var rec = component.get("v.newSSCMSUb");
var error = component.get("v.newSSCMSUbError");
if(error || (rec === null)) {
console.log("Error initializing record template: " + error);
}
else {
console.log("Record template initialized: " + rec.apiName);
}
})
);
event.preventDefault();
var eventFields = event.getParam("fields");
console.log(eventFields);
//console.log();
var subjectInfoListInfo = component.get("v.subjectsInfo");
console.log('subjectInfoListInfo==>');
console.log(subjectInfoListInfo);
var caseSubject = '';
//component.get("v.subjectInfo");
if(!$A.util.isEmpty(subjectInfoListInfo)){
caseSubject = subjectInfoListInfo.join();
console.log(caseSubject);
}
var emailInfoList = component.get("v.emailsInfo");
var namesList = component.get("v.namesList");
console.log('namesList==>');
console.log(namesList);
var emailsInfoIds = component.get("v.emailsInfoIds");

console.log('subjectInfoListInfo==>');
console.log(emailInfoList);
var replyToEmailInfo = '';
var replyToNameInfo = '';
//component.get("v.subjectInfo");
if(!$A.util.isEmpty(emailInfoList)){
replyToEmailInfo =emailsInfoIds.join(';');
replyToNameInfo = namesList.join(';');
}
eventFields["QC_Reason__c"] = component.get("v.QCReason");
if($A.util.isEmpty(component.get("v.recordId"))&&$A.util.isEmpty(component.get("v.anonymousEnquirerInfo"))){
eventFields["Status"] = 'Closed';   
}
eventFields["Subjects__c"] = caseSubject;
eventFields["Reply_To_Email_Address__c"] = replyToEmailInfo;
eventFields["Reply_To_Names__c"] = replyToNameInfo;

eventFields["AccountId"] = component.get("v.recordId");
eventFields["RecordTypeId"] = component.get("v.recordTypeIdInfo");
eventFields["Inquiry_Summary_Description__c"] = component.get("v.Description");
eventFields["Type"] = 'SSCM';

//eventFields["Inquirer_Name__c"] = component.get("v.anonymousEnquirerInfo");
//eventFields["Inquirer_Person_Number__c"] = component.get("v.anonymousEnquirerInfo");
if($A.util.isEmpty(component.get("v.recordId"))&&!$A.util.isEmpty(component.get("v.anonymousEnquirerInfo"))){
eventFields["Inquirer_Name__c"] ='Third Party';
eventFields["Inquirer_Person_Number__c"] ='Third Party';
}
console.log("Inquirer_Name__c Ends:"+eventFields["Inquirer_Name__c"]);

event.setParam("fields", eventFields);
component.find('caseSubmisstionId').submit(eventFields);

},
toggleFull: function(component, event, helper) {
component.set("v.isFull",!component.get("v.isFull"));
},
removeName : function(component, event, helper) {
component.set("v.subjectInfo",'');
component.find("isSubjectInquirer").set("v.value",false);
component.find("Inquirer_Relationship_to_Subject__fld").set("v.value","");
component.set("v.isAddSubject",true);
helper.checkingRequiredFieldsFilledhelper(component,event,helper); 
},
handleError: function(cmp, event, helper) {
window.scroll(0,0);
cmp.set("v.isSpinner",false);
cmp.set("v.isButtonClicked",false);
},
handleSuccess: function(cmp, event, helper) {
var toastEvent = $A.get("e.force:showToast");
toastEvent.setParams({
"type":"success",
"title": "Success!",
"message": "The Case has been created successfully."
});
toastEvent.fire();
var params = event.getParams();
cmp.set('v.caserecordId', params.response.id);
cmp.set("v.isButtonClicked",false);
cmp.set("v.simpleNewSSCM.Case_Id__c",params.response.id);



console.log('caseID######'+params.response.id);
console.log('caseID######'+cmp.get("v.caserecordId"));

var subjectsInfoAcId = cmp.get("v.subjectInfo");
var isAccountFilled = false;
console.log('isAccountFilled'+cmp.get("v.recordId"));
console.log('isAccountFilled'+subjectsInfoAcId);
if((subjectsInfoAcId!=undefined)&&(subjectsInfoAcId!=null)&&(subjectsInfoAcId!='')){
isAccountFilled = true;
cmp.set("v.simpleNewSSCM.Employee__c", cmp.get("v.recordId"));
console.log('subjectsInfo==>isAccountFilled');
}
var subjsscm = []; 
var csID=params.response.id;
console.log('Line179#######caseID' +csID);
var subjectsInfoAccountIdsInfo = cmp.get("v.subjectsInfoAccountIds");
if(!isAccountFilled&&(subjectsInfoAccountIdsInfo!=undefined)&&(subjectsInfoAccountIdsInfo!=null)&&(subjectsInfoAccountIdsInfo.length>0)){
cmp.set("v.simpleNewSSCM.Employee__c", subjectsInfoAccountIdsInfo[0]);
if(subjectsInfoAccountIdsInfo.length >0){
var i=0;
console.log('lengthbefore###'+subjectsInfoAccountIdsInfo.length);
while( i  <  subjectsInfoAccountIdsInfo.length){
console.log('lengthbefore###'+subjectsInfoAccountIdsInfo.length);
console.log('Ivalue##'+i);
subjsscm.push({'sobjectType':'SSCM_Subject__c','Employee__c':subjectsInfoAccountIdsInfo[i],'Case_Id__c':csID});
console.log('############caseID'+csID);
console.log('######EmployeeIds'+subjectsInfoAccountIdsInfo[i]);
i++;
}
console.log('######EmployeeIds'+subjsscm.length);    
subjsscm.shift();
console.log('######EmployeeIds'+subjsscm.length);
cmp.set("v.Subjects",subjsscm);

console.log('see2'+subjsscm);

var action = cmp.get("c.insertSubjects");
console.log('############insertStarted1');
action.setParams({
"sub":cmp.get("v.Subjects")});
console.log('############insertStarted2');
action.setCallback(this, function(response) {
//store state of response
console.log('############insertStarted3');
var state = response.getState();
if (state === "SUCCESS") {

console.log('response : ' + response.getReturnValue());
console.log('############insertStarted4');

}

else if (state === "INCOMPLETE") {
alert('Response is Incompleted');
}else if (state === "ERROR") {
var errors = response.getError();
if (errors) {
if (errors[0] && errors[0].message) {
alert("Error message: " + 
errors[0].message);
}
} else {
alert("Unknown error");
}
}
});
$A.enqueueAction(action);
}


cmp.set("v.emailsInfo", []);
cmp.set("v.emailsInfoIds", []);
cmp.set("v.namesList", []);
cmp.set("v.EmailsInfoAccountIds", []);
cmp.set("v.pageResult", []);
cmp.set("v.subjectInfoList", []);
cmp.set("v.subjectsInfoIds", []);
cmp.set("v.subjectsInfoAccountIds", []);
cmp.set("v.subjectsInfo", []);





isAccountFilled = true;
}
console.log('isAccountFilled:'+isAccountFilled);
console.log('isAccountFilled:'+cmp.get("v.simpleNewSSCM.Employee__c"));
if(isAccountFilled)
{
cmp.find("contactRecordCreator").saveRecord(function(saveResult) {
console.log('saveResult.state');

console.log(saveResult.state);
if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
helper.toggleClass(cmp,'backdrop','slds-backdrop--');
helper.toggleClass(cmp,'modaldialoginvoiceEntry','slds-fade-in-');
cmp.set("v.isSpinner",false);
console.log('v.caserecordId');
console.log( cmp.get('v.caserecordId'));
console.log( cmp.get('v.recordId'));


// record is saved successfully
var resultsToast = $A.get("e.force:showToast");
resultsToast.setParams({
"title": "Saved",
//"message": "The record was saved."
});
resultsToast.setParams({
"type":"success",
"title": "Success!",
//"message": "SSCM Subject record has been created successfully."
});

resultsToast.fire();
} else if (saveResult.state === "INCOMPLETE") {
// handle the incomplete state
console.log("User is offline, device doesn't support drafts.");
} else if (saveResult.state === "ERROR") {
// handle the error state
console.log('Problem saving contact, error: ' + 
JSON.stringify(saveResult.error));

helper.toggleClass(cmp,'backdrop','slds-backdrop--');
helper.toggleClass(cmp,'modaldialoginvoiceEntry','slds-fade-in-');
cmp.set("v.isSpinner",false);
} 
else {
console.log('Unknown problem, state: ' + saveResult.state +
', error: ' + JSON.stringify(saveResult.error));
}
});
}        
else{
helper.toggleClass(cmp,'backdrop','slds-backdrop--');
helper.toggleClass(cmp,'modaldialoginvoiceEntry','slds-fade-in-');
}

//cmp.set('v.showSpinner', false);
//  cmp.set("v.isSpinner",false);

},
closeModel : function(component,event,helper){

helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
helper.toggleClassInverse(component,'modaldialoginvoiceEntry','slds-fade-in-');
component.set("v.isCaseCreationPage",false);   

component.set("v.isAnonymousPage",false);   
component.set("v.anonymousEnquirerInfo","");   

//       component.set("v.isBack",true);

},
goHome : function(component,event,helper){
component.set("v.isCaseCreationPage",false);        
},
checkingRequiredFieldsFilledText: function(component,event,helper){
component.set("v.isNotOnload",true);
helper.checkingRequiredFieldsFilledhelper(component,event,helper); 
helper.displaySCRepresentativeError(component,event,helper);
},
checkingRequiredFieldsFilled : function(component,event,helper){
component.set("v.isNotOnload",true);
helper.checkingRequiredFieldsFilledhelper(component,event,helper);            


console.log("v.isNotOnload");
helper.displaySCRepresentativeError(component,event,helper);


helper.enableCancelHelper(component,event,helper);

}, 
enableCancel : function(component,event,helper){
helper.enableCancelHelper(component,event,helper);
helper.checkPrimaryHelper(component,event,helper);
},    
resetRelSubject : function(component,event,helper){
component.find("Inquirer_Relationship_to_Subject__fld").set("v.value","");
},
resetForm : function(component,event,helper){

component.find("Inquirer_Relationship_to_Subject__fld").set("v.value","");

component.find("Inquiry_Method__fld").set("v.value","");

component.find("Inquiry_Category_1__fld").set("v.value","");
if(component.find("Inquiry_Category_2__fld")!=undefined&&component.find("Inquiry_Category_2__fld")!=null)
component.find("Inquiry_Category_2__fld").set("v.value",'');
if(component.find("Inquiry_Category_2__flhd")!=undefined&&component.find("Inquiry_Category_2__flhd")!=null)
component.find("Inquiry_Category_2__flhd").set("v.value",'');
if(component.find("Inquiry_Category_3__fld")!=undefined&&component.find("Inquiry_Category_3__fld")!=null)
component.find("Inquiry_Category_3__fld").set("v.value","");
if(component.find("Inquiry_Category_3__flhd")!=undefined&&component.find("Inquiry_Category_3__flhd")!=null)
component.find("Inquiry_Category_3__flhd").set("v.value","");

component.find("Inquiry_Summary_Topic__fld").set("v.value","");
component.find("Inquiry_Summary_Topic_Secondary__fld").set("v.value","");
component.find("Inquiry_Summary_Description__fld").set("v.value","");
component.find("SSCM_Priority__fld").set("v.value","Non-Critical");
component.find("isQCReqField").set("v.value",false);
component.find("isSubjectInquirer").set("v.value",false);
component.set("v.isAddSubject",true);
component.set("v.subjectInfo","");
if(component.find("QC_Reason_fld")!=undefined){
//component.find("QC_Reason_fld").set("v.value","");
component.set("v.QCReason","");
}

},
isClicked: function(component,event,helper){
console.log("is clicked");
component.set("v.isNotOnload",true);
},
addSubjectFromEmployees: function(component,event,helper){
console.log("addSubjectFromEmployees");
component.set("v.enablePicking",true);

helper.toggleClass(component,'backdrop','slds-backdrop--');
helper.toggleClass(component,'modaldialogSubjectSelection','slds-fade-in-');
//document.body.setAttribute('style', 'overflow: hidden;');
//document.getElementById("Subjectpopupfocus").focus();

component.set("v.searchFirstName", "");
component.set("v.searchLastName", "");
component.set("v.searchInquirerEmail", "");

component.set("v.searchStandardID", "");
component.set("v.searchInquirerEmail", "");
component.set("v.searchManagerPersonNumber", "");
component.set("v.searchCostCenter", "");
component.set("v.searchManagerID", "");
component.set("v.searchInquirerPhone", "");
component.set("v.searchPreferredName", "");
component.set("v.searchDoB", "");
component.set("v.employeeStatus", "");
component.set("v.searchResult", "");
component.set("v.searchResult", "");
component.set("v.searchResult", "");
component.set("v.searchResult", "");
component.set("v.searchResult", "");


}, 

addReplyToAddresses: function(component,event,helper){
component.set("v.searchFirstName", "");
component.set("v.searchLastName", "");
component.set("v.searchInquirerEmail", "");

component.set("v.searchStandardID", "");

console.log("addReplyToAddresses");
component.set("v.replyTo",true);

helper.toggleClass(component,'backdrop','slds-backdrop--');
helper.toggleClass(component,'modaldialogSubjectSelection','slds-fade-in-');
}, 
showTopics: function(component,event,helper){
component.set("v.showTopics",true);

helper.toggleClass(component,'backdrop','slds-backdrop--');
helper.toggleClass(component,'modaldialogSubjectSelection','slds-fade-in-');
},
closeAddSubjectModel: function(component,event,helper){

helper.toggleClass(component,'backdrop','slds-backdrop--');
helper.toggleClass(component,'modaldialogSubjectSelection','slds-fade-in-');
component.set("v.enablePicking",false);
component.set("v.focusSubjects",true);
},
closeReplyToModel: function(component,event,helper){

helper.toggleClass(component,'backdrop','slds-backdrop--');
helper.toggleClass(component,'modaldialogSubjectSelection','slds-fade-in-');
component.set("v.replyTo",false);
component.set("v.focusReplyTo",true);
},
closeTopicModel: function(component,event,helper){

helper.toggleClass(component,'backdrop','slds-backdrop--');
helper.toggleClass(component,'modaldialogSubjectSelection','slds-fade-in-');
component.set("v.showTopics",false);
component.set("v.focusTopics",true);

},
addtopic: function(component,event,helper){

if(component.get("v.isAddtopicButtonVisible"))
{
component.set("v.isAddtopicButtonVisible",false);
component.set("v.isSecondaryTopicVisible",true);
}

},

removeNameFromIteration : function(component,event,helper){
console.log('event.getSource().get("v.name")');
console.log(event.getSource().get("v.name"));
var indexInfo = event.getSource().get("v.name");
var subjectInfoListInfo = component.get("v.subjectsInfo");
subjectInfoListInfo.splice(indexInfo,1);    
component.set("v.subjectsInfo",subjectInfoListInfo);
var subjectsInfoIdsListInfo = component.get("v.subjectsInfoIds");
subjectsInfoIdsListInfo.splice(indexInfo,1);    
component.set("v.subjectsInfoIds",subjectsInfoIdsListInfo);

var subjectsInfoAccountIdsInfo = component.get("v.subjectsInfoAccountIds");
subjectsInfoAccountIdsInfo.splice(indexInfo,1);    
component.set("v.subjectsInfoAccountIds",subjectsInfoAccountIdsInfo);

//subjectInfoList
},
addSubjectFromSelectedEmployees : function(component,event,helper){
var subjectInfoListInfo = component.get("v.subjectInfoList");
// var subjectInfoListInfo = component.get("v.subjectsInfo");

component.set("v.subjectsInfo",subjectInfoListInfo);
helper.toggleClass(component,'backdrop','slds-backdrop--');
helper.toggleClass(component,'modaldialogSubjectSelection','slds-fade-in-');
component.set("v.enablePicking",false);

},


removeEmailFromIteration : function(component,event,helper){
console.log('event.getSource().get("v.name")');
console.log(event.getSource().get("v.name"));
var indexInfo = event.getSource().get("v.name");
var emailsInfoList = component.get("v.emailsInfo");
emailsInfoList.splice(indexInfo,1);    
component.set("v.emailsInfo",emailsInfoList); 
var emailInfoListVl = component.get("v.emailInfoList");
emailInfoListVl.splice(indexInfo,1);    
component.set("v.emailInfoList",emailInfoListVl); 
var namesList = component.get("v.namesList");
console.log('namesList==>');
console.log(namesList);
namesList.splice(indexInfo,1);    
component.set("v.namesList",namesList); 
var emailsInfoIds = component.get("v.emailsInfoIds");
emailsInfoIds.splice(indexInfo,1);    
component.set("v.emailsInfoIds",emailsInfoIds); 

},
addEmailsFromSelectedEmployees : function(component,event,helper){
var emailInfoListInfo = component.get("v.emailInfoList");
// var subjectInfoListInfo = component.get("v.subjectsInfo");

component.set("v.emailsInfo",emailInfoListInfo);
helper.toggleClass(component,'backdrop','slds-backdrop--');
helper.toggleClass(component,'modaldialogSubjectSelection','slds-fade-in-');
component.set("v.replyTo",false);

},
showCardView : function(component,event,helper){
console.log(event.target.name);
component.set("v.showMiniviewInquirer",false);
console.log('event.currentTarget.offsetLeft');
console.log(event.currentTarget.offsetLeft);
component.set("v.showMiniview",true);
component.set("v.selectedRow",event.target.name);
var index = event.target.name;
var subIds = component.get("v.subjectsInfoIds");
var subAccountIds = component.get("v.subjectsInfoAccountIds");
component.set("v.selectedrecordId",component.get("v.recordId"));
if(event.target.name!=undefined&&subIds[event.target.name]!=undefined){
console.log('subAccountIds[event.target.name]');
console.log(subAccountIds[event.target.name]);
component.set("v.selectedrecordId",subAccountIds[event.target.name]);
var text= document.getElementById(index); 
if(text) {
console.log('event.currentTarget.offsetLeft==>');
console.log(event.currentTarget.offsetLeft);
text.style.left=event.currentTarget.offsetLeft+'px';
//text.style.top=(event.clientY-200)+'px';
}
}
/*console.log(document.getElementById(event.target.name).style);
console.log(document.getElementById(event.target.name).style.left);
var el_up = document.getElementById(event.target.name); 
var n = el_up.style.left; 
console.log(n);

var increate= parseInt(n)-10;
el_up.style.left = increate.toString()+'%';*/

//var stl = document.getElementById(event.target.name).style;
//var leftinfo= +document.getElementById(event.target.name).style.left-30%;
////document.getElementById(event.target.name).style = stl+'left:'+leftinfo.toString();
},
showInquirerMiniView : function(component,event,helper){
component.set("v.showMiniview",false);
component.set("v.showMiniviewInquirer",true);
var text= document.getElementById("showMiniviewInquirer"); 
if(text) {
console.log('event.currentTarget.offsetLeft==>');
console.log(event.currentTarget.offsetLeft);
text.style.left=event.currentTarget.offsetLeft+'px';
//text.style.top=(event.clientY-200)+'px'; 
}
},
closePopover : function(component,event,helper){
console.log('closePopover sscm');
component.set("v.showMiniview",false);

component.set("v.showMiniviewInquirer",false);
},
retrieveDependentFieldsInfo : function(component,event,helper){
console.log("Inquiry_Summary_Topic_fld");

helper.enableCancelHelper(component,event,helper);


},
setField3 : function(component,event,helper){
}

})