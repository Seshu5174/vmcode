Public class TitleNominationAssignment{
 
 public Static List<Nomination_Assignment__c> AssignApprover(Nomination__c nomTitleRec,  Nomsite_Security_Roles__c RepRec) 

 {
 Nomination_Assignment__c assignMgr;
 Nomination_Assignment__c assignRep;

 List<Nomination_Assignment__c> assignList = new List<Nomination_Assignment__c>();
  if(nomTitleRec.LOB__c == 'GLOBAL TECHNOLOGY AND OPERATIONS' || nomTitleRec.LOB__c == 'GLOBAL HUMAN RESOURCES' ||nomTitleRec.LOB__c=='GLOBAL RISK' || nomTitleRec.LOB__c=='THE CFO GROUP' ||
  nomTitleRec.LOB__c == 'LEGAL' || nomTitleRec.LOB__c=='LIQUIDATING BUSINESS / OTHER' || nomTitleRec.LOB__c=='CORPORATE AUDIT' || nomTitleRec.LOB__c=='OFFICE OF CAO' || nomTitleRec.LOB__c=='OFFICE OF CEO' ||
   nomTitleRec.LOB__c == 'ESG CAPITAL DEPLOYMENT AND PUBLIC POLICY' || nomTitleRec.LOB__c == 'GLOBAL CORPORATE PLANNING AND STRATEGY')
 {
String ApprMgrNM = nomTitleRec.Created_By_Lkp__r.Manager_Full_Name__c;
ID ApprMgrId = nomTitleRec.Created_By_Lkp__r.Manager__c;
String ApprMgrBand = nomTitleRec.Created_By_Lkp__r.Manager_Band__c;
String apprvMgrEmail = nomTitleRec.Created_By_Lkp__r.Manager_Email_Address__c;
 //HR Rep -- No Action Required
 
 //1st Assignment 
 if(nomTitleRec.Proposed_Title__c=='Vice President' || nomTitleRec.Proposed_Title__c=='Assistant Vice President' || nomTitleRec.Proposed_Title__c =='Officer'  )
 {
 //Insert Approving Manager
  assignMgr= new Nomination_Assignment__c();
  assignMgr.Maanger_Representative_Name__c =ApprMgrNM ;
  assignMgr.Approving_Manager_Email__c = apprvMgrEmail;
  assignMgr.Approving_Manager_Band__c =ApprMgrBand;
  assignMgr.Assignment_Type__c = 'Manager of Nominator';
  assignMgr.Assignment_Level__c = 1;
 assignMgr.Approving_Manager__c = ApprMgrId;
  assignMgr.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
  assignMgr.Nomination__c = nomTitleRec.Id;
  assignList.add(assignMgr);
     //Insert HR Rep
     
  assignRep = new Nomination_Assignment__c();

 //assignRep.Representative__c=RepRec.Id;
 if(RepRec != null)
 {
  assignRep.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;
  assignRep.Representative_Role__c= RepRec.Role_Type__c;
 }
 else assignRep.Maanger_Representative_Name__c= 'No Representative Available';

 assignRep.Assignment_Level__c = 2;
 assignRep.Assignment_Type__c = 'Representative';
 assignRep.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
 assignRep.Nomination__c = nomTitleRec.Id;
 assignList.add(assignRep);

 }
 else if(nomTitleRec.Proposed_Title__c == 'Senior Vice President') {
 //Assign Manager of Manager 
   assignMgr= new Nomination_Assignment__c();
   assignMgr.Maanger_Representative_Name__c =ApprMgrNM ;
   assignMgr.Approving_Manager_Email__c = apprvMgrEmail;
   assignMgr.Approving_Manager_Band__c =ApprMgrBand;
   assignMgr.Assignment_Type__c = 'Manager of Nominator';
   assignMgr.Assignment_Level__c = 2;
   assignMgr.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
   assignMgr.Nomination__c = nomTitleRec.Id;
   assignList.add(assignMgr);
  
  //Representative 1
  assignRep = new Nomination_Assignment__c();
 if(RepRec != null){
   assignRep.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;  
   assignRep.Representative_Role__c= RepRec.Role_Type__c;
 }
 else assignRep.Maanger_Representative_Name__c = 'No Representative Available';
  assignRep.Assignment_Level__c = 1;
  assignRep.Assignment_Type__c = 'Representative';
  assignRep.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
  assignRep.Nomination__c = nomTitleRec.Id;
  assignList.add(assignRep);

 //Assign Level 4 Manager
 Nomination_Assignment__c assignMgr4= new Nomination_Assignment__c();
 assignMgr4.Maanger_Representative_Name__c = nomTitleRec.Nominee_Lkp__r.manager_name_4__c;
 String Level4MgrId = nomTitleRec.Nominee_Lkp__r.Manager_Level_4_PN__c;
 Account [] Lvl4Account ;
 if(Level4MgrId != null)
 {
  Lvl4Account= new List<Account>([SELECT Band__c, Work_Email_Address__c from Account where person_Id__c =:Level4MgrId LIMIT 1]);
 }
 if(Lvl4Account != null)
 {
 assignMgr4.Approving_Manager_Email__c = Lvl4Account[0].Work_Email_Address__c ;
 assignMgr4.Approving_Manager_Band__c =Lvl4Account[0].Band__c;
 }
 assignMgr4.Assignment_Type__c = 'Level 4 Manager';
 assignMgr4.Assignment_Level__c = 3;
 assignMgr4.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
 assignMgr4.Approving_Manager__c = ApprMgrId;
 assignMgr4.Nomination__c = nomTitleRec.Id;

  assignList.add(assignMgr4);

  //Assign Same Rep as 4th Assignment
  Nomination_Assignment__c assignRep2 = new Nomination_Assignment__c();

 if(RepRec != null){
   assignRep2.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;
   assignRep2.Representative_Role__c= RepRec.Role_Type__c;
 }
 else assignRep2.Maanger_Representative_Name__c = 'No Representative Available';
  assignRep2.Assignment_Level__c = 4;
  assignRep2.Assignment_Type__c = 'Representative';
  assignRep2.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
  assignRep2.Nomination__c = nomTitleRec.Id;
  assignList.add(assignRep2);

 }

 
 }
 //======Private Bank and GS&EP Assignment============// 
  
  if(nomTitleRec.LOB__c == 'PRIVATE BANK' || nomTitleRec.LOB__c == 'GLOBAL STRATEGY & ENTERPRISE PLATFORMS') 
 {
   String ApprMgrNM = nomTitleRec.Created_By_Lkp__r.Manager_Full_Name__c;
   ID ApprMgrId = nomTitleRec.Created_By_Lkp__r.Manager__c;
   String ApprMgrBand = nomTitleRec.Created_By_Lkp__r.Manager_Band__c;
   String apprvMgrEmail = nomTitleRec.Created_By_Lkp__r.Manager_Email_Address__c;
 
  // VP below with no flags - 1st Assignment Level4 Manager
 if ((nomTitleRec.Flagged_for_HR_Review__c =='No') && (nomTitleRec.Proposed_Title__c=='Vice President' || nomTitleRec.Proposed_Title__c=='Assistant Vice President' || nomTitleRec.Proposed_Title__c =='Officer' ) )
 {
   Nomination_Assignment__c assignMgr4= new Nomination_Assignment__c();
   assignMgr4.Maanger_Representative_Name__c = nomTitleRec.Nominee_Lkp__r.manager_name_4__c;
   String Level4MgrId = nomTitleRec.Nominee_Lkp__r.Manager_Level_4_PN__c;
   Account [] Lvl4Account ;
   if(Level4MgrId != null)
 {
    Lvl4Account= new List<Account>([SELECT Band__c, Work_Email_Address__c from Account where person_Id__c =:Level4MgrId LIMIT 1]);
 }
 if(Lvl4Account != null)
 {
   assignMgr4.Approving_Manager_Email__c = Lvl4Account[0].Work_Email_Address__c ;
   assignMgr4.Approving_Manager_Band__c =Lvl4Account[0].Band__c;
 }
   assignMgr4.Assignment_Type__c = 'Level 4 Manager';
   assignMgr4.Assignment_Level__c = 1;
   assignMgr4.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
   assignMgr4.Nomination__c = nomTitleRec.Id;
   assignList.add(assignMgr4);
    
   //VP below with no flags 2nd Assignment HR Rep
     
   assignRep = new Nomination_Assignment__c();
   //assignRep.Representative__c=RepRec.Id;
 if(RepRec != null)
 {
   assignRep.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;
   assignRep.Representative_Role__c= RepRec.Role_Type__c;
 }
 else assignRep.Maanger_Representative_Name__c= 'No Representative Available';
   assignRep.Assignment_Level__c = 2;
   assignRep.Assignment_Type__c = 'Representative';
   assignRep.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
   assignRep.Nomination__c = nomTitleRec.Id;
   assignList.add(assignRep);
  
 }
  
 // VP below with flags - 1st Assignment HR Rep
 else if((nomTitleRec.Flagged_for_HR_Review__c =='Yes') && (nomTitleRec.Proposed_Title__c=='Vice President' || nomTitleRec.Proposed_Title__c=='Assistant Vice President' || nomTitleRec.Proposed_Title__c =='Officer' ) )
 {
     assignRep = new Nomination_Assignment__c();
 if(RepRec != null)
 {
    assignRep.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;
    assignRep.Representative_Role__c= RepRec.Role_Type__c;
 }
 else
   assignRep.Maanger_Representative_Name__c= 'No Representative Available';
   assignRep.Assignment_Level__c = 1;
   assignRep.Assignment_Type__c = 'Representative';
   assignRep.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
   assignRep.Nomination__c = nomTitleRec.Id;
   assignList.add(assignRep);
 
  // 2nd Assignment Level4 Manager 
    Nomination_Assignment__c assignMgr4= new Nomination_Assignment__c();
    assignMgr4.Maanger_Representative_Name__c = nomTitleRec.Nominee_Lkp__r.manager_name_4__c;
    String Level4MgrId = nomTitleRec.Nominee_Lkp__r.Manager_Level_4_PN__c;
    Account [] Lvl4Account ;
  if(Level4MgrId != null)
  {
    Lvl4Account= new List<Account>([SELECT Band__c, Work_Email_Address__c from Account where person_Id__c =:Level4MgrId LIMIT 1]);
  }
  if(Lvl4Account != null)
  {
     assignMgr4.Approving_Manager_Email__c = Lvl4Account[0].Work_Email_Address__c ;
     assignMgr4.Approving_Manager_Band__c =Lvl4Account[0].Band__c;
  }
     assignMgr4.Assignment_Type__c = 'Level 4 Manager';
     assignMgr4.Assignment_Level__c = 2;
     assignMgr4.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
     assignMgr4.Nomination__c = nomTitleRec.Id;
     assignList.add(assignMgr4);
   
   //Assign Same HR Rep as 3rd Assignment
   
    Nomination_Assignment__c assignRep2 = new Nomination_Assignment__c();
   if(RepRec != null){
     assignRep2.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;
     assignRep2.Representative_Role__c= RepRec.Role_Type__c;
 }
 else 
    assignRep2.Maanger_Representative_Name__c = 'No Representative Available';
    assignRep2.Assignment_Level__c = 3;
    assignRep2.Assignment_Type__c = 'Representative';
    assignRep2.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
    assignRep2.Nomination__c = nomTitleRec.Id;
    assignList.add(assignRep2);
 
 }
 
  if (((nomTitleRec.LOB__c == 'GLOBAL STRATEGY & ENTERPRISE PLATFORMS') && (nomTitleRec.Proposed_Title__c == 'Director' || nomTitleRec.Proposed_Title__c == 'Senior Vice President')) ||  (nomTitleRec.LOB__c == 'PRIVATE BANK' && nomTitleRec.Proposed_Title__c == 'Senior Vice President') ) {
  //HR Rep
  assignRep = new Nomination_Assignment__c();
  if(RepRec != null){
    assignRep.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;
    assignRep.Representative_Role__c= RepRec.Role_Type__c;
 }
 else assignRep.Maanger_Representative_Name__c = 'No Representative Available';
    assignRep.Assignment_Level__c = 1;
    assignRep.Assignment_Type__c = 'Representative';
    assignRep.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
    assignRep.Nomination__c = nomTitleRec.Id;
    assignList.add(assignRep);
}
  
} 
// For MERRILL LYNCH and CONSUMER LOBs

 if(nomTitleRec.LOB__c == 'MERRILL LYNCH' || nomTitleRec.LOB__c == 'CONSUMER & SMALL BUSINESS')
 {
  String ApprMgrNM = nomTitleRec.Created_By_Lkp__r.Manager_Full_Name__c;
  ID ApprMgrId = nomTitleRec.Created_By_Lkp__r.Manager__c;
  String ApprMgrBand = nomTitleRec.Created_By_Lkp__r.Manager_Band__c;
  String apprvMgrEmail = nomTitleRec.Created_By_Lkp__r.Manager_Email_Address__c;
 
 if ((nomTitleRec.Flagged_for_HR_Review__c =='No') && (nomTitleRec.Proposed_Title__c=='Vice President' || nomTitleRec.Proposed_Title__c=='Assistant Vice President' || nomTitleRec.Proposed_Title__c =='Officer' ) )
 {
  //Ist Assignment Nominator's mgr
  assignMgr= new Nomination_Assignment__c();
  assignMgr.Maanger_Representative_Name__c =ApprMgrNM ;
  assignMgr.Approving_Manager_Email__c = apprvMgrEmail;
  assignMgr.Approving_Manager_Band__c =ApprMgrBand;
  assignMgr.Assignment_Type__c = 'Manager of Nominator';
  assignMgr.Assignment_Level__c = 1;
  assignMgr.Approving_Manager__c = ApprMgrId;
  assignMgr.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
  assignMgr.Nomination__c = nomTitleRec.Id;
  assignList.add(assignMgr);
   
     //2nd Assignment HR Rep
     
  assignRep = new Nomination_Assignment__c();
  if(RepRec != null)
  {
    assignRep.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;
    assignRep.Representative_Role__c= RepRec.Role_Type__c;
  }
 else assignRep.Maanger_Representative_Name__c= 'No Representative Available';
      assignRep.Assignment_Level__c = 2;
      assignRep.Assignment_Type__c = 'Representative';
      assignRep.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
      assignRep.Nomination__c = nomTitleRec.Id;
      assignList.add(assignRep);

 }
 
 // VP and below with Flags
 else if((nomTitleRec.Flagged_for_HR_Review__c =='Yes') && (nomTitleRec.Proposed_Title__c=='Vice President' || nomTitleRec.Proposed_Title__c=='Assistant Vice President' || nomTitleRec.Proposed_Title__c =='Officer' ) )
 {
  //  1st Assignment HR Rep
    assignRep = new Nomination_Assignment__c();
 if(RepRec != null){
    assignRep.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;  
    assignRep.Representative_Role__c= RepRec.Role_Type__c;
  }
 else assignRep.Maanger_Representative_Name__c = 'No Representative Available';
      assignRep.Assignment_Level__c = 1;
      assignRep.Assignment_Type__c = 'Representative';
      assignRep.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
      assignRep.Nomination__c = nomTitleRec.Id;
      assignList.add(assignRep);
 
   // 2nd Assignment Nominator's mgr
  
      assignMgr= new Nomination_Assignment__c();
      assignMgr.Maanger_Representative_Name__c =ApprMgrNM ;
      assignMgr.Approving_Manager_Email__c = apprvMgrEmail;
      assignMgr.Approving_Manager_Band__c =ApprMgrBand;
      assignMgr.Assignment_Type__c = 'Manager of Nominator';
      assignMgr.Assignment_Level__c = 2;
      assignMgr.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
      assignMgr.Nomination__c = nomTitleRec.Id;
      assignList.add(assignMgr);
  
  //3rd Assignment HR Rep
  Nomination_Assignment__c assignRep2 = new Nomination_Assignment__c();

 if(RepRec != null){
     assignRep2.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;
     assignRep2.Representative_Role__c= RepRec.Role_Type__c;
 }
 else assignRep2.Maanger_Representative_Name__c = 'No Representative Available';
     assignRep2.Assignment_Level__c = 3;
     assignRep2.Assignment_Type__c = 'Representative';
     assignRep2.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
     assignRep2.Nomination__c = nomTitleRec.Id;
     assignList.add(assignRep2);

 }
 else if(nomTitleRec.Proposed_Title__c == 'Senior Vice President' || nomTitleRec.Proposed_Title__c == 'Director') {
   
  //1st Assignment HR Rep
    assignRep = new Nomination_Assignment__c();
 if(RepRec != null){
    assignRep.Maanger_Representative_Name__c= RepRec.User_Name__r.Name;  
    assignRep.Representative_Role__c= RepRec.Role_Type__c;
 }
 else assignRep.Maanger_Representative_Name__c = 'No Representative Available';
    assignRep.Assignment_Level__c = 1;
    assignRep.Assignment_Type__c = 'Representative';
    assignRep.Approver_Deadline_Date__c = nomTitleRec.Approver_Deadline_Date__c;
    assignRep.Nomination__c = nomTitleRec.Id;
    assignList.add(assignRep);

 }
}
    return assignList;
 }

}