public class parentToChild {
    
    public List<Account> acc {get;set;}
    
    public List<Teacher__c> teac {get;set;}
    
    public PageReference ShowAccts (){
        acc = [Select Name, Industry, (Select FirstName, LastName, Email From Contacts) From Account];
        return null;
            }
        
    public PageReference ShowTeachers (){
        teac = [Select Name, course__c, (Select Name, Gender__c, Class__c  From students__r) From Teacher__c];
        return null;
    }
}