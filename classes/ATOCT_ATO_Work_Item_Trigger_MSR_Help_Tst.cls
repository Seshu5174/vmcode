@isTest
public class ATOCT_ATO_Work_Item_Trigger_MSR_Help_Tst {
    
    static testMethod void beforeInsertTest()
    {
        ATOCT_ATO_Work_Item__c testWI1 = new ATOCT_ATO_Work_Item__c();
        testWI1.Name = 'Test1';
        testWI1.ATOCT_MSR_Id__c='12345';
        insert testWI1;
        
        ATOCT_ATO_Work_Item__c testWI2 = new ATOCT_ATO_Work_Item__c();
        testWI2.Name = 'Test2';
        testWI2.ATOCT_MSR_Id__c='0N970';
        insert testWI2;
        
        ATOCT_ATO_Work_Item__c testWI3 = new ATOCT_ATO_Work_Item__c();
        testWI3.Name = 'Test3';
        testWI3.ATOCT_MSR_Id__c='0D589';
        insert testWI3;
     }
    
    
    static testMethod void beforeUpdateTest()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        User u1 = new User(Alias = 'standt2',Country='United Kingdom',Email='demo2@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='bertdemo2@camfed.org', EmployeeNumber='0D589');
        insert u1;
        
        
        User u2 = new User(Alias = 'standt2',Country='United Kingdom',Email='demo22@randomdemodomain2.com',EmailEncodingKey='UTF-8', LastName='Test2', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='bertdemo22@camfed.org', EmployeeNumber='0N970');
        insert u2;
        
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'ATOCT_MSR'];
		insert new PermissionSetAssignment(AssigneeId = u2.id, PermissionSetId = ps.Id);
        
        Map<id,ATOCT_ATO_Work_Item__c> oldmap = new Map<id,ATOCT_ATO_Work_Item__c>();
		Map<id,ATOCT_ATO_Work_Item__c> newmap = new Map<id,ATOCT_ATO_Work_Item__c>();
        
        system.runAs(u1)
        {
        ATOCT_ATO_Work_Item__c testWI1 = new ATOCT_ATO_Work_Item__c();
        testWI1.Name = 'Test1';
        testWI1.ownerID = u1.ID;
        testWI1.ATOCT_MSR_Id__c='12345';
        insert testWI1;
        
        oldmap.put(testWI1.id, testWI1);
            
        Test.startTest();
		testWI1.ownerID = [select id, name from user where name='Test2'].ID;
		update testWI1;
		
        newmap.put(testWI1.id, testWI1);
		
        Test.stopTest();
        }
    
    }
    
    static testMethod void afterInsertTest()
    {
       
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        User u1 = new User(Alias = 'standt2',Country='United Kingdom',Email='demo2@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='bertdemo2@camfed.org', EmployeeNumber='0D589');
        insert u1;
        
        ATOCT_ATO_Work_Item__c atw = new ATOCT_ATO_Work_Item__c();
        atw.Name = 'test';
        atw.ownerID = u1.ID;
        insert atw;
        List<ATOCT_ATO_Interface_Result__c> interfaceList = new List<ATOCT_ATO_Interface_Result__c>();
        ATOCT_ATO_Interface_Result__c atI= new ATOCT_ATO_Interface_Result__c();
        ati.ATOCT_Record_Id__c = atw.Id;
        ati.ATOCT_USAA_Number__c = '12345';
        ati.ATOCT_Counter__c = 0;
        ati.ATOCT_Status__c = 'Ready to Send';
        insert interfaceList;
    }

}