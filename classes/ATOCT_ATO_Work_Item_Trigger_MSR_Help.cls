public with sharing class ATOCT_ATO_Work_Item_Trigger_MSR_Help
{
    
    public static void onBeforeInsert(List<ATOCT_ATO_Work_Item__c> atoAlertList)
    {
        
        try
        {
            Set<String> msrIdSet = new Set<String>();
            
            for(ATOCT_ATO_Work_Item__c alert: atoAlertList)
            {
                if(alert.ATOCT_MSR_Id__c!=null)
                {
                    msrIdSet.add(alert.ATOCT_MSR_Id__c);
                }
                
            }
            
            system.debug('@@MSR ID Set >>> '+ msrIdSet);
            
            if(!msrIdSet.isEmpty())
            {
                List<User> activeUserList = [Select Id,Name,EmployeeNumber,Email,Manager.Email,isActive From User Where EmployeeNumber IN: msrIdSet and isActive=true];
                system.debug('@@@activeUserList >> '+activeUserList);
                
                List<PermissionSetAssignment> PSetAssignmentList = new List<PermissionSetAssignment>([Select Id,
                                                                                                      PermissionSet.Name,
                                                                                                      AssigneeId From permissionSetAssignment
                                                                                                      Where PermissionSet.Name = 'ATOCT_MSR'
                                                                                                      And Assigneeid IN :activeUserList]);
                system.debug('Before Insert ---> List of users who are active and have the permission set '+PSetAssignmentList);
                
                Map<Id,String> userMap = new Map<Id,String>();
                set<Id> PSetAssignmentSet = new set<Id>();
                
                Map<String,User> activeUserWithPermissionsMap = new Map<String,User>();
                for(PermissionSetAssignment ps : PSetAssignmentList)
                {
                    PSetAssignmentSet.add(ps.AssigneeId);
                }
                for(User usr : activeUserList)
                {
                    
                    if(PSetAssignmentSet.contains(usr.Id))
                    {
                        userMap.put(usr.Id,usr.EmployeeNumber);
                        activeUserWithPermissionsMap.put(usr.EmployeeNumber,usr);
                    }
                    
                }
                
                system.debug('final user list for which assignment can be done==='+userMap);
                Integer aactiveUserWithPermissionsMapSize = activeUserWithPermissionsMap.size();
                for(ATOCT_ATO_Work_Item__c alertList : atoAlertList)
                {
                    if(alertList.ATOCT_MSR_Id__c != null && aactiveUserWithPermissionsMapSize>0 && activeUserWithPermissionsMap.containsKey(alertList.ATOCT_MSR_Id__c))
                    {
                        User usrMSR = activeUserWithPermissionsMap.get(alertList.ATOCT_MSR_Id__c);
                        alertList.OwnerId = usrMSR.Id;
                        alertList.ATOCT_Manager_Email__c = usrMSR.Manager.email;
                    }
                }
            }
        }
        catch(Exception ex)
        {
            system.debug('Error Occured '+ ex.getMessage());
        }
        
    }
    
    
    public static void onBeforeUpdate (Map<Id, ATOCT_ATO_Work_Item__c> oldWorkItemMap, Map<Id, ATOCT_ATO_Work_Item__c> newWorkItemMap)
    {
        try
        {
            Set<Id> ownerIdSet = new Set<Id>();
            
            for(ATOCT_ATO_Work_Item__c ato : newWorkItemMap.values())
            {
                
                ATOCT_ATO_Work_Item__c old  = oldWorkItemMap.get(ato.Id);
                
                system.debug('new owner id >>>'+ ato.OwnerId);
                system.debug('Old owner id >>>'+ old.OwnerId);
                
                if(ato.OwnerId != old.OwnerId)
                {
                    ownerIdSet.add(ato.ownerId);
                }
            }
            system.debug('@@OwnerIdSet >> '+ ownerIdSet);
            
            if(!ownerIdSet.isEmpty())
            {
                List<User> activeUserList = [Select Id,Name,EmployeeNumber,Email,Manager.Email,isActive From User Where Id IN: ownerIdSet and isActive=true];
                
                system.debug('@@@User ID '+activeUserList);
                
                List<PermissionSetAssignment> PSetAssignmentList = new List<PermissionSetAssignment>([Select Id,
                                                                                                      PermissionSet.Name,
                                                                                                      AssigneeId From permissionSetAssignment
                                                                                                      Where PermissionSet.Name = 'ATOCT_MSR'
                                                                                                      And Assigneeid IN :activeUserList]);
                
                system.debug('Before Update ---> Active Users with Permision Set >> '+PSetAssignmentList);
                
                Map<Id,String> userMap1 = new Map<Id,String>();
                Map<Id,User> activeUserWithPermissionsMap = new Map<Id,User>();
                
                if(!PSetAssignmentList.isEmpty())
                {
                    Set<ID> PSetAssignmentSet = new Set<ID>();
                    for(PermissionSetAssignment ps: PSetAssignmentList)
                    {
                        PSetAssignmentSet.add(ps.AssigneeId);
                    }
                    for(User usr1: activeUserList)
                    {
                        
                        if(PSetAssignmentSet.contains(usr1.Id))
                        {
                            userMap1.put(usr1.Id, usr1.EmployeeNumber);
                            activeUserWithPermissionsMap.put(usr1.Id, usr1);
                        }
                        
                    }
                    
                    Boolean isNotEmpty = !activeUserWithPermissionsMap.isEmpty() && !userMap1.isEmpty();
                    for(ATOCT_ATO_Work_Item__c alert: newWorkItemMap.values())
                    {
                        if(isNotEmpty)
                        {
                            User user = activeUserWithPermissionsMap.get(alert.OwnerId);
                            alert.ATOCT_Manager_Email__c = user.Manager.email;
                            alert.ATOCT_MSR_Id__c = user.EmployeeNumber;
                        }
                    }
                }
                else
                {
                    if(newWorkItemMap.size()>0){
                        List<ATOCT_ATO_Work_Item__c> atw = newWorkItemMap.values();
                        atw[0].addError('ATOCT MSR Permission Set is not assigned to Selected User. Please choose different user');
                    }
                }
            }
            
            
        }
        catch (Exception e)
        {
            system.debug('Exception >>'+ e.getMessage());
        }
    }
    
    
    // This Method creates Interface Results from work items with Owner ATOETL user
    public static void createInterfaceResults(Map<Id, ATOCT_ATO_Work_Item__c> atoMap)
    {
        
        ID etlUserID = [select id, name from user where name='Ravi Teja'].ID;
        
        List <ATOCT_ATO_Work_Item__c> etlOwnerWorkItemsList = [SELECT Id, Name, Member_Last_Name__c, USAA_Number__c, Work_Item_Date__c, Work_Item_Type__c
                                                               FROM ATOCT_ATO_Work_Item__c WHERE Id IN: atoMap.KeySet() and OwnerId=: etlUserID];  
        
        system.debug('*** etlOwnerWorkItemsList *** '+ etlOwnerWorkItemsList.size());
        
        if(!etlOwnerWorkItemsList.isEmpty())
        {
            
            List<ATOCT_ATO_Interface_Result__c> interfaceInsertList = new List<ATOCT_ATO_Interface_Result__c>();
            
            for(ATOCT_ATO_Work_Item__c atos : etlOwnerWorkItemsList)
            {
                ATOCT_ATO_Interface_Result__c interfaceResult = new ATOCT_ATO_Interface_Result__c();
                interfaceResult.ATOCT_Member_First_Name__c = atos.Name;
                interfaceResult.ATOCT_Member_Last_Name__c = atos.Member_Last_Name__c;
                interfaceResult.ATOCT_USAA_Number__c = atos.USAA_Number__c;
                interfaceResult.ATOCT_Record_Id__c = atos.ID;
                interfaceResult.ATOCT_Source__c = 'SALESFORCE';
                interfaceResult.ATOCT_Target__c = 'TALON';
                interfaceResult.ATOCT_Integration_Type__c = 'Talon';
                interfaceInsertList.add(interfaceResult); 
            }
            System.debug('interfaceInsertList >> '+ interfaceInsertList.size());
            insert interfaceInsertList;
        } 
        
    }
}