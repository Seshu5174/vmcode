trigger sendEmailtoTeacher on Teacher__c (after Insert, after Update) 
{
    string nominatorEmail;
    
    if(trigger.isInsert)
    {
             for (Teacher__c  tea: trigger.new)
             {
                 if(tea.Status__c=='Submitted'){
                         nominatorEmail = tea.Email__c;
                         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                        String[] toAddresses = new String[] {nominatorEmail}; 
                        mail.setToAddresses(toAddresses); 
                        mail.setSubject('You have a project assigned' + ' So#- '); 
                        String body = 'Project Assigned' ; 
                        mail.setPlainTextBody('test'); 
                        Messaging.sendEmail(new Messaging.SingleEMailMessage[]{mail});
                  }
             }
       
    }
}