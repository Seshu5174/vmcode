@isTest
public class ATOCT_TalonTestClass {
    
    public testMethod static void ATOCT_Talon_Callouts_BatchTest(){
        ATOCT_ATO_Work_Item__c atw = new ATOCT_ATO_Work_Item__c();
        atw.Name = 'test';
        atw.ownerID = [select id, name from user where name='Ravi Teja'].ID;
        insert atw;
        List<ATOCT_ATO_Interface_Result__c> interfaceList = new List<ATOCT_ATO_Interface_Result__c>();
        ATOCT_ATO_Interface_Result__c atI= new ATOCT_ATO_Interface_Result__c();
        ati.ATOCT_Record_Id__c = atw.Id;
        ati.ATOCT_USAA_Number__c = '12345';
        ati.ATOCT_Counter__c = 0;
        ati.ATOCT_Status__c = 'Ready to Send';
        ATOCT_ATO_Interface_Result__c atI1= new ATOCT_ATO_Interface_Result__c();
        ati1.ATOCT_Record_Id__c = atw.Id;
        ati1.ATOCT_USAA_Number__c = '12345';
        ati1.ATOCT_Counter__c = 1;
        ati1.ATOCT_Status__c = 'failed';
        interfaceList.add(ati1);
        
        ATOCT_ATO_Interface_Result__c atI2= new ATOCT_ATO_Interface_Result__c();
        ati2.ATOCT_Record_Id__c = atw.Id;
        ati2.ATOCT_USAA_Number__c = '12345';
        ati2.ATOCT_Counter__c = 2;
        ati2.ATOCT_Status__c = 'failed';
        interfaceList.add(ati2);
        insert interfaceList;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'Ok', '[{"memberNumber":"12345","recordId":'+ati.ATOCT_Record_Id__c+'"interfaceId":'+ati.Id+'}]', new Map<String, String>()));
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(200, 'Ok', '{"access_token":"access_token"}', new Map<String, String>()));
        
        database.executeBatch(new ATOCT_Talon_Callouts_Batch());
        Test.stoptest();        
    }
    public testMethod static void ATOCT_Talon_Callouts_BatchTestNegative(){
        ATOCT_ATO_Work_Item__c atw = new ATOCT_ATO_Work_Item__c();
        atw.Name = 'test';
        atw.ownerID = [select id, name from user where name='Ravi Teja'].ID;
        insert atw;
        List<ATOCT_ATO_Interface_Result__c> interfaceList = new List<ATOCT_ATO_Interface_Result__c>();
        ATOCT_ATO_Interface_Result__c atI= new ATOCT_ATO_Interface_Result__c();
        ati.ATOCT_Record_Id__c = atw.Id;
        ati.ATOCT_USAA_Number__c = '12345';
        ati.ATOCT_Counter__c = 0;
        ati.ATOCT_Status__c = 'Ready to Send';
        ATOCT_ATO_Interface_Result__c atI1= new ATOCT_ATO_Interface_Result__c();
        ati1.ATOCT_Record_Id__c = atw.Id;
        ati1.ATOCT_USAA_Number__c = '12345';
        ati1.ATOCT_Counter__c = 1;
        ati1.ATOCT_Status__c = 'failed';
        interfaceList.add(ati1);
        
        ATOCT_ATO_Interface_Result__c atI2= new ATOCT_ATO_Interface_Result__c();
        ati2.ATOCT_Record_Id__c = atw.Id;
        ati2.ATOCT_USAA_Number__c = '12345';
        ati2.ATOCT_Counter__c = 2;
        ati2.ATOCT_Status__c = 'failed';
        interfaceList.add(ati2);
        insert interfaceList;
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(400, 'Ok', '[{"memberNumber":"12345","recordId":'+ati.ATOCT_Record_Id__c+'"interfaceId":'+ati.Id+'}]', new Map<String, String>()));
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(400, 'Ok', '{"access_token":"access_token"}', new Map<String, String>()));
        
        database.executeBatch(new ATOCT_Talon_Callouts_Batch());
        Test.stoptest();        
    }
    
}