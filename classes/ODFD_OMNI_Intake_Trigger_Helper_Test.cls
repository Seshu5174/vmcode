@isTest
public with sharing class ODFD_OMNI_Intake_Trigger_Helper_Test {
    

    public testMethod static void testMethod1(){
        
        
        OMNI_Intake__c intake = new OMNI_Intake__c();
        intake.Effort_Name__c  = 'test effort Name';
        intake.Primary_Business_POC__c = '00341000009bCLl';
        intake.Omni_Delivery_Agent__c  = '00341000009bCLl';
        intake.Status__c='Triage';
        intake.Initial_Channel_Impact_Date__c= System.today();
        insert intake;
        
        intake.Status__c='Development';
        update intake;
        
        List<OMNI_Assignment__c> assignments = [SELECT Id, Name, Assigned_User_Email__c FROM OMNI_Assignment__c WHERE OMNI_Intake__c=: intake.Id];
              
    }
    
    
    public testMethod static void testMethod2(){
        
        
        OMNI_Intake__c intake = new OMNI_Intake__c();
        intake.Effort_Name__c  = 'test effort Name';
        intake.Primary_Business_POC__c = '00341000009bCLl';
        intake.Omni_Delivery_Agent__c  = '00341000009bCLl';
        intake.Status__c='Triage';
        intake.Initial_Channel_Impact_Date__c= System.today();
        insert intake;
        
        
        
        intake.Initial_Channel_Impact_Date__c=system.today()+1;
        update intake;
        
        List<OMNI_Assignment__c> assignments = [SELECT Id, Name, Assigned_User_Email__c FROM OMNI_Assignment__c WHERE OMNI_Intake__c=: intake.Id];
              
    }
    
    public testMethod static void testMethod3(){
        
        
                
        OMNI_Intake__c intake = new OMNI_Intake__c();
        intake.Effort_Name__c  = 'test effort Name';
        intake.Primary_Business_POC__c = '00341000009bCLl';
        intake.Omni_Delivery_Agent__c  = '00341000009bCLl';
        intake.Omni_Delivery_Agent_Email__c='testemail@gmail.com';
        intake.Experience_Owner_1_Email__c='testemail@gmail.com';
        intake.Experience_Owner_2_Email__c='testemail@gmail.com';
        intake.Process_Owner_1_Email__c='testemail@gmail.com';
        intake.Process_Owner_2_Email__c='testemail@gmail.com';
        intake.Project_Manager_1_Email__c='testemail@gmail.com';
        intake.Project_Manager_2_Email__c='testemail@gmail.com';
        intake.Status__c='Triage';
        intake.Initial_Channel_Impact_Date__c= System.today();
        insert intake;
        
       
        
        intake.Omni_Delivery_Agent_Email__c='test_eamil@gmail.com';
        intake.Experience_Owner_1_Email__c='test_eamil@gmail.com';
        intake.Experience_Owner_2_Email__c='test_eamil@gmail.com';
        intake.Process_Owner_1_Email__c='test_eamil@gmail.com';
        intake.Process_Owner_2_Email__c='test_eamil@gmail.com';
        intake.Project_Manager_1_Email__c='test_eamil@gmail.com';
        intake.Project_Manager_2_Email__c='test_eamil@gmail.com';
        update intake;
        
        List<OMNI_Assignment__c> assignments = [SELECT Id, Name, Assigned_User_Email__c FROM OMNI_Assignment__c WHERE OMNI_Intake__c=: intake.Id];
              
    }
}