public with sharing class SSCMsearchEmployeeController {

static Boolean isFetched = false;
@AuraEnabled
public static list<EmployeeInformationWraper> fetchEmployee (String searchStandardID, String searchPersonNumber, String searchDoB, String searchFirstName, String searchLastName,  String searchPreferredName, String searchPhone, String searchEmail, string searchEmpLevel, string searchManagerID, string searchManagerPersonNumber,  string searchCostCenter) {

List <Employee_Name__c> returnList = new List < Employee_Name__c> ();
List < Employee_Name__c > lstOfEmployees = new List < Employee_Name__c > (); 

string standardID = searchStandardID + '%';
string personNumber = searchPersonNumber + '%'; 
string doB = searchDoB + '%'; 
string firstName = searchFirstName + '%';
string lastName = searchLastName + '%';
string preferredName = searchPreferredName + '%'; 
string phone = searchPhone + '%'; 
string email = searchEmail + '%'; 
string level = searchEmpLevel + '%';     
string managerID = searchManagerID + '%';    
string managerPerNumber = searchManagerPersonNumber + '%';    
string costCenter = searchCostCenter + '%';    

string sQuery = 'select id,Name, FirstName__c, LastName__c, Preferred_Full_Name__c, Date_of_Birth__c, Person_Number__c, Employee__r.Cost_Center__c, Standard_ID__c, Email__c, Employee__r.Country_code__c,Phone__c, Employee__r.Employment_Status__c, Employee__r.Employee_Most_Recent_Hire_Date__c, Employee__r.Manager_Full_Name__c, Employee__r.manager_name_2__c,Manager_Person_Number__c, Employee__r.Manager_Standard_ID__c from Employee_Name__c Where Id!=null';

if(searchStandardID!=null && String.IsNotBlank(searchStandardID)){
sQuery +=' and (Standard_ID__c LIKE: standardID)';
}

if(searchPersonNumber!=null && String.IsNotBlank(searchPersonNumber)){
sQuery +=' and (Person_Number__c LIKE: personNumber)';
}

if(searchDoB!=null && String.IsNotBlank(searchDoB)){
sQuery +=' and (Date_of_Birth__c LIKE: doB)';
}

if(searchFirstName!=null && String.IsNotBlank(searchFirstName)){
sQuery +=' and (FirstName__c LIKE: firstName)';
}

if(searchLastName!=null && String.IsNotBlank(searchLastName)){
sQuery +=' and (LastName__c LIKE: lastName)';
}

if(searchPreferredName!=null && String.IsNotBlank(searchPreferredName)){
sQuery +=' and (Preferred_Full_Name__c LIKE: preferredName)';
}

if(searchPhone!=null && String.IsNotBlank(searchPhone)){
sQuery +=' and (Phone__c LIKE: phone)';
}

if(searchEmail!=null && String.IsNotBlank(searchEmail)){
sQuery +=' and (Email__c LIKE: email)';
}

if(searchEmpLevel!=null && String.IsNotBlank(searchEmpLevel)){
//sQuery +=' and (Employee__r.Employment_Status__pc LIKE: level)';
sQuery +=' and (Employee__r.Employment_Status__c LIKE: level)';
}

if(searchManagerID!=null && String.IsNotBlank(searchManagerID)){
//sQuery +=' and (Employee__r.Manager_Standard_ID__pc LIKE: managerID)';
sQuery +=' and (Employee__r.Manager_Standard_ID__c LIKE: managerID)';
}
if(searchManagerPersonNumber!=null && String.IsNotBlank(searchManagerPersonNumber)){
sQuery +=' and (Manager_Person_Number__c LIKE: managerPerNumber)';
}    

if(searchCostCenter!=null && String.IsNotBlank(searchCostCenter)){
//sQuery +=' and (Employee__r.Company_Cost_Center_Code__pc LIKE: costCenter)';
sQuery +=' and (Employee__r.Company_Cost_Center_Code__c LIKE: costCenter)';
}        
sQuery +=' order by LastName__c ASC LIMIT 500';

lstOfEmployees  = Database.query(sQuery);
list<EmployeeInformationWraper> ens = new list<EmployeeInformationWraper>();
for (Employee_Name__c emp: lstOfEmployees) {
//returnList.add(emp);
ens.add(new EmployeeInformationWraper(emp));
}
return ens;
}
@AuraEnabled
public static Case CreateCase(String accountID){

Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('SSCM').getRecordTypeId();
Case cs = new Case();
cs.AccountID = accountID;

cs.RecordTypeId =recordTypeId;
    if(test.isRunningTest()){
      cs.Subjects__c  = 'test obct';
    }
insert cs;
return cs;
}

// To Check Inquirer is the user or not
@AuraEnabled
public static boolean searchUser (String personNumber){
boolean IsCurrentUser = false;
system.debug('personNumber: '+ personNumber);
List<user> userInfo=[select Name, IsActive, FederationIdentifier, Profile.Name from user where IsActive=true and Profile.Name LIKE 'SSCM%' and FederationIdentifier=:personNumber];
if(UserInfo.size()>0){
IsCurrentUser=true;
}
system.debug('IsCurrentUser:: '+ IsCurrentUser);
return IsCurrentUser;
}

@AuraEnabled
public static CaseInformationWraper fetchAccountDetails(String recordId){
CaseInformationWraper cs =new CaseInformationWraper();
Account ac = new Account();

Boolean isSCRepresetative = false;
String loggedInUserId = UserInfo.getUserId();
if(String.isNotBlank(recordId)){
Employee_Name__c emp = new Employee_Name__c();
List<Employee_Name__c> emps =[Select Id,Person_Number__c,Employee__c from Employee_Name__c where id=:recordId];
if(emps!=null&&emps.size()>0){
emp = emps[0];
List<Account> acs =[Select Id,Name from Account where id=:emp.Employee__c];
if(acs!=null&&acs.size()>0){
ac = acs[0];

}
if(String.isNotBlank(emp.Person_Number__c)){
List<user> userInfo=[select Name, IsActive, FederationIdentifier, Profile.Name from user where IsActive=true and Profile.Name LIKE 'SSCM%' and FederationIdentifier=:emp.Person_Number__c and ID=:loggedInUserId];


if(UserInfo.size()>0){
isSCRepresetative=true;
}
}
else{
isSCRepresetative = false;      
}
}

}
system.debug('isSCRepresetative'+isSCRepresetative);
cs.isSCRepresetative = isSCRepresetative;
cs.ac = ac;
cs.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('SSCM').getRecordTypeId();
return cs;

}
@AuraEnabled
public static Account retrieveEmployeeNamenfo(String recordId){
Account en = new Account();
    List<Account> acs = [Select Id,Name,eLeader_CC_Dept__c,Employee_Most_Recent_Hire_Date__c, PAN_No__c, Continuous_Service_Date__c, Work_Email_Address__c,Phone, AADHAR_No__c, Manager_Full_Name__c,Manager__r.Manager_Full_Name__c,HR_Manager_Name__c,Blood_Group__c, Work_Location__c  from Account where Id=:recordId];
    if(acs.size()>0){
        en = acs[0];
    }
return en;
}
@AuraEnabled
public static Case retrieveInquirySummaryTopic(String topicId){
Case cs = new Case();
List<Inquiry_Summary_Topic__c> isps = [Select Id,Name,Inquiry_Category_1__c,Inquiry_Category_2__c,Inquiry_Category_3__c from Inquiry_Summary_Topic__c where ID=:topicId];
if(isps.size()>0){
Inquiry_Summary_Topic__c isp = isps[0];
cs.Inquiry_Category_1__c = isp.Inquiry_Category_1__c;
cs.Inquiry_Category_2__c = isp.Inquiry_Category_2__c;
cs.Inquiry_Category_3__c = isp.Inquiry_Category_3__c;
    
/*Map<Object,List<String>> ct2 = getDependentPicklistValues(Case.Inquiry_Category_2__c,isp.Inquiry_Category_1__c);
if((ct2.containskey(isp.Inquiry_Category_1__c))&&(ct2.get(isp.Inquiry_Category_1__c).size()==1)){
cs.Inquiry_Category_2__c    = ct2.get(isp.Inquiry_Category_1__c)[0];
}
Map<Object,List<String>> ct3  = getDependentPicklistValues(Case.Inquiry_Category_3__c,isp.Inquiry_Category_1__c);
if((ct3.containskey(isp.Inquiry_Category_1__c))&&(ct2.get(isp.Inquiry_Category_1__c).size()==1)){
cs.Inquiry_Category_3__c    = ct3.get(isp.Inquiry_Category_3__c)[0];
}*/
}
return cs;
}
@AuraEnabled
public static String  insertSubjects( List<SSCM_Subject__c> sub){

String rtnValue;


try{
if(sub.size() > 0)
insert sub;
rtnValue = 'success';
}

catch(DMLException ex){
    system.debug('ex.getMessage()'+ex.getMessage());
AuraHandledException e = new AuraHandledException(ex.getMessage());
throw e;

}

return rtnValue ;  

}
public class CaseInformationWraper{
@AuraEnabled
public Account ac {get;set;}
@AuraEnabled
public String recordTypeId {get;set;}
@AuraEnabled
public Boolean isSCRepresetative {get;set;}
}
public class EmployeeInformationWraper{
@AuraEnabled
public Employee_Name__c ename {get;set;}
@AuraEnabled
public Boolean isSelected {get;set;} 
public EmployeeInformationWraper(Employee_Name__c ename){
this.ename = ename;
this.isSelected = false;

}
}
public static Map<Object,List<String>> getDependentPicklistValues( Schema.sObjectField dependToken,String controllingvl )
{
Schema.DescribeFieldResult depend = dependToken.getDescribe();
Schema.sObjectField controlToken = depend.getController();
//system.debug('controlEntries'+controlToken);
//system.debug('depend'+depend);

if ( controlToken == null ) return null;
Schema.DescribeFieldResult control = controlToken.getDescribe();
List<Schema.PicklistEntry> controlEntries =
(   control.getType() == Schema.DisplayType.Boolean
?   null
:   control.getPicklistValues()
);
//system.debug('controlEntries'+controlEntries);
String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
Map<Object,List<String>> dependentPicklistValues = new Map<Object,List<String>>();
//system.debug(' entry.getLabel()'+ base64map);

for ( Schema.PicklistEntry entry : depend.getPicklistValues() ) if ( entry.isActive() )
{
List<String> base64chars =
String.valueOf
(   ((Map<String,Object>) JSON.deserializeUntyped( JSON.serialize( entry ) )).get( 'validFor' )
).split( '' );
for ( Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++ )
{
Object controlValue =
(   controlEntries == null
?   (Object) (index == 1)
:   (Object) (controlEntries[ index ].isActive() ? controlEntries[ index ].getLabel() : null)
);
Integer bitIndex = index / 6, bitShift = 5 - Math.mod( index, 6 );
//  system.debug(' entry.getLabel()'+ bitIndex);

if  (   controlValue == null
||  (base64chars!=null&&base64chars.size()>bitIndex)&&(base64map.indexOf( base64chars[ bitIndex ] ) & (1 << bitShift)) == 0
) continue;
if ( !dependentPicklistValues.containsKey( controlValue ) )
{
dependentPicklistValues.put( controlValue, new List<String>() );
//        system.debug(' entry.getLabel()'+ entry.getLabel());
}
dependentPicklistValues.get( controlValue ).add( entry.getLabel() );
//      system.debug('dependentPicklistValues:'+dependentPicklistValues);

}
}
//system.debug('dependentPicklistValues:'+dependentPicklistValues);
return dependentPicklistValues;
}

Public Class DisplayWraper{
@AuraEnabled
public Integer totalPages {get;set;}
@AuraEnabled
public list<EmployeeInformationWraper> employees{get;set;}
}
}