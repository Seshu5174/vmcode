// Copyright 2016, Sales Engineering, Salesforce.com Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// - Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// - Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// - Neither the name of the salesforce.com nor the names of its contributors
//   may be used to endorse or promote products derived from this software
//   without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
({
 	loadNewsChannelNames : function(cmp) {
        // get NewsChannelNames
        var action = cmp.get("c.getNewsChannelNames");
        //display spinner
        cmp.set("v.displayLoader",true); 
		action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                cmp.set("v.newsChannelNames", response.getReturnValue());
                if(response.getReturnValue() != null){  
                    var newsURL = window.location.href;
			      	if (newsURL.indexOf('channel/') != -1) {
            			var channel = decodeURI(newsURL.substr(newsURL.indexOf('channel/') + 8));
                        cmp.set("v.selectedNewsChannel",channel);
                    }else{
                    	// set first newsChannelName
                    	cmp.set("v.selectedNewsChannel", cmp.get("v.newsChannelNames")[0]);
                        cmp.set("v.encodedSelectedNewsChannel", encodeURIComponent(cmp.get("v.newsChannelNames")[0]));
                    }    
                }
            }
        });    
        $A.enqueueAction(action);
    },
    loadnumDocsByChannel : function(cmp) {
		var action = cmp.get("c.getnumDocsByChannel");
        action.setCallback(this, function(response) {
        	var state = response.getState();
        	if (cmp.isValid() && state === "SUCCESS") {
        		cmp.set("v.numDocsByChannel", response.getReturnValue());
        	}
    	});    
        $A.enqueueAction(action);
    },
    loadFirstNewsChannelRecords : function(cmp) {
    // Load firstNewsChannel records
    	var action = cmp.get("c.getFirstNewsChannelRecords");
        //pass number of news articles to display to apex method
        action.setParams({
            "numDocs" : cmp.get("v.numbberOfArticles")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                cmp.set("v.newsDocs", response.getReturnValue());
                var isEmpty = $A.util.isEmpty(cmp.get("v.newsDocs"));
                var error = cmp.find("errorMsg");
                if(isEmpty){ 
                    $A.util.removeClass(error, 'hide');
                    $A.util.addClass(error, 'show');
                    cmp.set("v.displayLoader",false);
                }else{
    				$A.util.removeClass(error, 'show');
                    $A.util.addClass(error, 'hide');
				}
                //display More... link
                var cmpTarget = cmp.find("more"); 
                var selectedChannel = cmp.get("v.selectedNewsChannel");
                if(cmp.get("v.numDocsByChannel")[selectedChannel] > cmp.get("v.numbberOfArticles")){
                    $A.util.removeClass(cmpTarget, 'hide');
                    $A.util.addClass(cmpTarget, 'show');
                }else{
                    $A.util.removeClass(cmpTarget, 'show');
                    $A.util.addClass(cmpTarget, 'hide');
                }
             	//hide spinner
                cmp.set("v.displayLoader",false); 
            }
        });    
        $A.enqueueAction(action);
    },
    loadNewsChannelRecordsByChannel: function(cmp) {
        var newsURL = window.location.href;
        if (newsURL.indexOf('channel/') != -1) {
            var selectedChannel = decodeURI(newsURL.substr(newsURL.indexOf('channel/') + 8));
            this.navigateToChannel(cmp);
        }
        // get NewsChannel Records by Channel
        var action = cmp.get("c.getNewsChannelRecordsByChannel");
        //pass slected News Channel and number of news articles to display to apex method
        action.setParams({
            "newsChannel": cmp.get("v.selectedNewsChannel"),
            "numDocs" : cmp.get("v.numbberOfArticles")
        })
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                cmp.set("v.newsDocs", response.getReturnValue());
                //if no records are found then display error message
                var isEmpty = $A.util.isEmpty(cmp.get("v.newsDocs"));
                var error = cmp.find("errorMsg");
                if(isEmpty){ 
                    $A.util.removeClass(error, 'hide');
                    $A.util.addClass(error, 'show');
                    cmp.set("v.displayLoader",false);
                }else{
    				$A.util.removeClass(error, 'show');
                    $A.util.addClass(error, 'hide');
				}
                //display More... link
                var cmpTarget = cmp.find("more"); 
                var selectedChannel = cmp.get("v.selectedNewsChannel");
                if(cmp.get("v.numDocsByChannel")[selectedChannel] > cmp.get("v.numbberOfArticles")){
                    $A.util.removeClass(cmpTarget, 'hide');
                    $A.util.addClass(cmpTarget, 'show');
                }else{
                    $A.util.removeClass(cmpTarget, 'show');
                    $A.util.addClass(cmpTarget, 'hide');
                }
                //hide spinner
                cmp.set("v.displayLoader",false); 
            }
        });    
        $A.enqueueAction(action);
    },
    navigateToChannel: function(cmp) {
        var address = "/recentnews#channel/"+encodeURIComponent(cmp.get("v.selectedNewsChannel"));
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": address 
        });
        urlEvent.fire();
    },
})