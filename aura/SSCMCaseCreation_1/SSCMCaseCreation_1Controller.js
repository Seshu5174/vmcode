({
//doInit : function(component, event, helper) {
//if(component.get("v.isFromAddSubject"))
//helper.SearchHelper(component, event, helper);
//},

doInit : function(component, event, helper) {
var browserType = navigator.sayswho= (function(){
var ua= navigator.userAgent, tem,
M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
console.log('browserType'+M[1]);
if(/trident/i.test(M[1])){
tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
return 'IE '+(tem[1] || '');
}
if(M[1]=== 'Chrome'){
tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
}
M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);{
console.log('browserType'+M) ;

return M.join(' ');
}
})();

console.log('browserType'+browserType) ;	
if (browserType.startsWith("IE")) {
component.set("v.isIE", true);

}
},

openInChrome: function(component, event, helper){
var shell = new ActiveXObject("WScript.Shell");
//shell.run("chrome www.google.com");
shell.run("chrome " + window.location.href);
},

Search: function(component, event, helper) {


helper.SearchHelper(component, event, helper);

},
showDetails    : function(component, event, helper) {

helper.showDetailsHelper(component, event);
},
renderPage: function(component, event, helper) {
helper.renderPage(component, component.get("v.recordsPerPage"));
},
resetOnBack : function(component, event, helper) {

if(component.get("v.isBack")){
helper.resetSearchFilterHelper(component, event, helper);
component.set("v.isSearch",false);        
component.set("v.isDetail",false);   
}
},
resetSearch : function(component, event, helper) {
helper.resetSearchFilterHelper(component, event, helper);
},
selectAllRecords : function(component, event, helper) {
console.log(component.find("selectAll").get("v.value"));
console.log(component.find("selectAll").get("v.checked"));
var pagesMap = component.get("v.pagesMap");

pagesMap[component.get("v.pageNumber")]=component.find("selectAll").get("v.checked");
component.set("v.pagesMap",pagesMap);
//if(component.find("selectAll").get("v.checked"))
{
var subList = component.get("v.subjectInfoList");
var subListIds= [];
subListIds = component.get("v.subjectsInfoIds");
var subAccountIds= [];
subAccountIds = component.get("v.subjectsInfoAccountIds");

helper.maintainSelectedList(component, event, helper);
}
},
addSubjects : function(component, event, helper) {
var subList = component.get("v.subjectInfoList");
var subListIds= [];
subListIds = component.get("v.subjectsInfoIds");
var subAccountIds= [];
subAccountIds = component.get("v.subjectsInfoAccountIds");
var emailInfoList = component.get("v.emailInfoList");
var namesList = component.get("v.namesList");
var emailsInfoIds = component.get("v.emailsInfoIds");
console.log(event.target);
console.log(event);
console.log(event.getSource().get("v.checked"));
console.log(event.getSource().get("v.name"));
console.log(event.getSource().get("v.title"));
var accountInfo = event.getSource().get("v.name");
    var records = component.get("v.pageResult");
    console.log('index'+accountInfo);
    console.log('index'+records[accountInfo]);
    var rec = records[accountInfo];
/*var IdInfo = accountInfo.split('-');
var emailInfo = IdInfo[2];
var emNameArray = emailInfo.split(':');
console.log('emailInfo');
console.log(emailInfo);
    
          
var NameInfo = emNameArray[0];
var emailInform = emNameArray[1];
var enameId = IdInfo[0];
var acId = IdInfo[1];*/   
    
    
var emailInfo =  rec.ename.Name+': '+rec.ename.Email__c;          
var NameInfo = rec.ename.Name;
var emailInform = rec.ename.Email__c;
var enameId = rec.ename.Id;
var acId = rec.ename.Employee__c;
    
if(event.getSource().get("v.checked")){
if(subList.indexOf(event.getSource().get("v.title"))==-1){
subList.push(event.getSource().get("v.title"));    
subListIds.push(enameId);    
subAccountIds.push(acId); 
if(emailInform!=undefined){
emailInfoList.push(emailInfo);
namesList.push(NameInfo);
emailsInfoIds.push(emailInform);
}
}
}
else{
subList.splice(subList.indexOf(event.getSource().get("v.title")),1);    
subListIds.splice(enameId,1);    
subAccountIds.splice(acId,1);
emailInfoList.splice(emailInfo,1);

namesList.splice(NameInfo,1);
emailsInfoIds.splice(emailInform,1);

}
component.set("v.subjectInfoList",subList); 
component.set("v.subjectsInfoIds",subListIds); 
component.set("v.subjectsInfoAccountIds",subAccountIds);
component.set("v.emailInfoList",emailInfoList); 
component.set("v.namesList",namesList); 
component.set("v.emailsInfoIds",emailsInfoIds); 

},

addAddresses : function(component, event, helper) {
var emailList = component.get("v.EmailInfoList");
var emailListIds= [];
emailListIds = component.get("v.EmailsInfoIds");
var emailAccountIds= [];
emailAccountIds = component.get("v.EmailsInfoAccountIds");

console.log(event.target);
console.log(event);
console.log(event.getSource().get("v.checked"));
console.log(event.getSource().get("v.name"));
console.log(event.getSource().get("v.title"));
var accountInfo = event.getSource().get("v.name");
var IdInfo = accountInfo.split('-');
var enameId = IdInfo[0];
var acId = IdInfo[1];
if(event.getSource().get("v.checked")){
if(emailList.indexOf(event.getSource().get("v.title"))==-1){
emailList.push(event.getSource().get("v.title"));    
emailListIds.push(enameId);    
emailAccountIds.push(acId);    
}
}
else{
emailList.splice(emailList.indexOf(event.getSource().get("v.title")),1);    
emailListIds.splice(enameId,1);    
emailAccountIds.splice(acId,1);    
}
component.set("v.EmailInfoList",emailList); 
component.set("v.EmailsInfoIds",emailListIds); 
component.set("v.EmailsInfoAccountIds",emailAccountIds); 
},
gotoAnonymousPage : function(component, event, helper) {
component.set("v.isAnonymousPage",true);
},
goBacktoSearchPage : function(component, event, helper) {
component.set("v.isAnonymousPage",false);
component.set("v.Employee_Name","");
helper.SearchHelper(component, event, helper,component.get("v.pageNumber"),  component.get("v.currentPagesCount"),true);
},


gotoNextPage : function(component, event, helper) {
component.set("v.isCaseCreationPage",true);
helper.retrieveAccountInfo(component, event);
}    



})