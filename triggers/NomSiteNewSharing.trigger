trigger NomSiteNewSharing on Nomination__c (after insert, after update, after delete) {
 Profile p = [select id,Name from Profile where id=:Userinfo.getProfileid()];
 List<ID> newIds = new List<ID>();
 if(trigger.isInsert) {
 for(Nomination__c neRec: trigger.new)
 {
  newIds.add (neRec.Id);
  NomUITriggerHelper.updateNomSharing(newIds);
    }
 }
if(trigger.isInsert || trigger.isUpdate  ) 
{
if(trigger.isUpdate){
if(NomUITriggerHelper.runOnce()){
    if(p.Name  != 'NOMSITE - API User' )
    {
        NomUITriggerHelper.captureHistory(trigger.new, trigger.OldMap);
    }
}
}

if(NomUITriggerHelper.bMultiNom)  {return;}

if(p.Name  == 'NOMSITE - API User')
{
NomUITriggerHelper.bMultiNom = true; //lock it
NomUITriggerHelper.NumOfNominationsPerNominee(trigger.new);
NomUITriggerHelper.updateNomiIDwithAlphaChr(trigger.new);

NomUITriggerHelper.bMultiNom = false;

}
}
if(trigger.isInsert || trigger.isUpdate  )
{
//call team upd fuction
if(NomUITriggerHelper.addTeam)
{
return;
}
if(p.Name  == 'NOMSITE - API User'){
NomUITriggerHelper.addTeam = true; //lock 
NomUITriggerHelper.AddTeamNomination(trigger.new, trigger.oldMap, trigger.isUpdate);
NomUITriggerHelper.addTeam = false;
}
}
if(trigger.isDelete) 
{
if(NomUITriggerHelper.bMultiNom)  {return;}
if(NomUITriggerHelper.addTeam)  {return;}
if( p.Name  == 'NOMSITE - API User')
{
NomUITriggerHelper.bMultiNom = true; //lock it
NomUITriggerHelper.NumOfNominationsPerNominee(trigger.old);
NomUITriggerHelper.updateNomiIDwithAlphaChr(trigger.old);
NomUITriggerHelper.bMultiNom = false;
}
}

}