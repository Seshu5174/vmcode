public with sharing class ODFD_EmailHandler 
{
 	public static void sendEmail(List<String> emailIds, String recordId, String userId, String templateId)
    {
        Id orgWidAddrId;
        for(OrgWideEmailAddress owa : [SELECT id, Address, DisplayName from OrgWideEmailAddress]) 
        {
            if(owa.DisplayName.contains('Salesforce'))
                orgWidAddrId = owa.Id;
        }
        
        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(templateId, userId, recordId);
            
			email.setTargetObjectId(userId);
        	email.setUseSignature(false);
        	email.setSaveAsActivity(false);
        	email.setToAddresses(emailIds);
            email.setOrgWideEmailAddressId(orgWidAddrId);  
          
        if(!Test.isRunningTest()){
            Messaging.SendEmailResult[] r= Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }
        
    }
   
}