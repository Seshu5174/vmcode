public class COntrollpiiclistInfo {
    public static Map<Object,List<String>> getDependentPicklistValues( Schema.sObjectField dependToken )
    {
        Schema.DescribeFieldResult depend = dependToken.getDescribe();
        Schema.sObjectField controlToken = depend.getController();
                system.debug('controlEntries'+controlToken);
                system.debug('depend'+depend);

        if ( controlToken == null ) return null;
        Schema.DescribeFieldResult control = controlToken.getDescribe();
        List<Schema.PicklistEntry> controlEntries =
            (   control.getType() == Schema.DisplayType.Boolean
             ?   null
             :   control.getPicklistValues()
            );
        system.debug('controlEntries'+controlEntries);
        String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        Map<Object,List<String>> dependentPicklistValues = new Map<Object,List<String>>();
        system.debug(' entry.getLabel()'+ base64map);
        
        for ( Schema.PicklistEntry entry : depend.getPicklistValues() ) if ( entry.isActive() )
        {
            List<String> base64chars =
                String.valueOf
                (   ((Map<String,Object>) JSON.deserializeUntyped( JSON.serialize( entry ) )).get( 'validFor' )
                ).split( '' );
            for ( Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++ )
            {
                Object controlValue =
                    (   controlEntries == null
                     ?   (Object) (index == 1)
                     :   (Object) (controlEntries[ index ].isActive() ? controlEntries[ index ].getLabel() : null)
                    );
                Integer bitIndex = index / 6, bitShift = 5 - Math.mod( index, 6 );
                system.debug(' entry.getLabel()'+ bitIndex);
                
                if  (   controlValue == null
                     ||  (base64chars!=null&&base64chars.size()>bitIndex)&&(base64map.indexOf( base64chars[ bitIndex ] ) & (1 << bitShift)) == 0
                    ) continue;
                if ( !dependentPicklistValues.containsKey( controlValue ) )
                {
                    dependentPicklistValues.put( controlValue, new List<String>() );
                    system.debug(' entry.getLabel()'+ entry.getLabel());
                }
                dependentPicklistValues.get( controlValue ).add( entry.getLabel() );
                        system.debug('dependentPicklistValues:'+dependentPicklistValues);

            }
        }
        system.debug('dependentPicklistValues:'+dependentPicklistValues);
        return dependentPicklistValues;
    }
    
}