trigger AddToPublicGroup on User (after insert, after update) {
    Id groupId    ;    // CF Manager Profider Public group
    Id groupIdSU1 ; //CF Admins CFO Group
    id groupIdSU2 ; // CF Admins Corp Audit
    Id groupIdSU3 ; //CF Admins Global Compliance
    Id groupIdSU4 ; //CF Admins Global Human Resources
    Id groupIdSU5 ; //CF Admins Global Risk
    Id groupIdSU6 ; //CF Admins Legal
    Id groupIdSU7;  //CF Super Users
    Id groupIdSU8; //remove group CF Profider
    Id groupIdSU9; //remove group cf manager group is being removed 
        
    Id profileId1 ; // CF Manager Provider
    id profileId2 ;//CF Super User 
    
String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm();
system.debug( 'baseUrl ='+ baseURL);
system.debug('baseUrlcode='+ baseURL.contains('dev'));
if (baseURL.contains('DEV'))
{    

     groupId = '00GQ0000002P1FYMA0';    // CF Manager Profider Public group
     groupIdSU1 = '00GQ0000002P1FSMA0'; //CF Admins CFO Group
     groupIdSU2 = '00GQ0000002P1FTMA0'; // CF Admins Corp Audit
     groupIdSU3 = '00GQ0000002P1FUMA0'; //CF Admins Global Compliance
     groupIdSU4 = '00GQ0000002P1FVMA0'; //CF Admins Global Human Resources
     groupIdSU5 = '00GQ0000002P1FWMA0'; //CF Admins Global Risk
     groupIdSU6 = '00GQ0000002P1FXMA0'; //CF Admins Legal
     groupIdSU7 = '00GQ0000002PAMvMAO'; // CF Super Users
     groupIdSU8 = '00GQ0000002P1FZMA0'; // CF Provider remove group per claudia
     groupIdSU9 = '00GQ0000002PAMqMAO'; // CF manager remove group per clauda 2 groups going away
           
     profileId1 = '00eQ0000000MCI8IAO'; // CF Manager Provider
     profileId2 = '00eQ0000000MCFWIA4';//CF Super User 
                   
                   
                         
 }
 else {
     groupId = '00G30000004AuXYEA0';    // CF Manager Profider Public group production
     groupIdSU1 = '00G30000004AuXREA0'; //CF Admins CFO Group
     groupIdSU2 = '00G30000004AuXSEA0'; // CF Admins Corp Audit
     groupIdSU3 = '00G30000004AuXTEA0'; //CF Admins Global Compliance
     groupIdSU4 = '00G30000004AuXUEA0'; //CF Admins Global Human Resources
     groupIdSU5 = '00G30000004AuXVEA0'; //CF Admins Global Risk
     groupIdSU6 = '00G30000004AuXWEA0'; //CF Admins Legal
     groupIdSU7 = '00G30000004AuXbEAK'; // CF Super Users
     groupIdSU8 = '00G30000004AuXZEA0'; // CF Provider remove group per claudia
     groupIdSU9 = '00G30000004AuXXEA0'; // CF manager remove group per clauda 2 groups going away
         
     profileId1 = '00e30000001qNftAAE'; // CF Manager Provider
     profileId2 = '00e30000001qNfvAAE';//CF Super User 
 }

 
    
    Set<Id> allGroups = new set<Id>(); 
    allGroups.add(groupId);
    allGroups.add(groupIdSU1);
    allGroups.add(groupIdSU2);
    allGroups.add(groupIdSU3);
    allGroups.add(groupIdSU4);
    allGroups.add(groupIdSU5);
    allGroups.add(groupIdSU6);
    allGroups.add(groupIdSU7);
    allGroups.add(groupIdSU8);
    allGroups.add(groupIdSU9);
    
    
    
    
    List <GroupMember> GMlist = new List <GroupMember>();
    Set<Id> userIdSet = new Set<id>(); 
    
    for (User a : trigger.new) {
        system.debug('profile='+ a.ProfileId);   //|| a.ProfileId != profileId2        
        if(a.profileid != profileId1  || a.ProfileId != profileId2   ) { 
           
            userIdSet.add(a.Id);
             system.debug('userIdSetvalues='+ userIdSet);
        }
       system.debug('CurrentProfileId = '+ a.ProfileId);
       system.debug('ProfileId1='+profileId1);
       system.debug('ProfileId2='+profileId2);
        if (a.profileid == profileId1) {
            GroupMember GM = new GroupMember();
            GM.GroupId = groupId; 
            GM.UserOrGroupId = a.Id; 
            GMlist.add(GM);
            
            allGroups.add(groupId);
        }
         
        if (a.profileid == profileId2) {
              //CF Admins CFO Group
            GroupMember GM = new GroupMember();
            GM.GroupId = groupIdSU1;
            GM.UserOrGroupId = a.Id; 
            GMlist.add(GM);
             allGroups.add(groupIdSU1);
             
             //CF Admins Corp Audit   
            GroupMember GM2 = new GroupMember();
            GM2.GroupId = groupIdSU2;
            GM2.UserOrGroupId = a.Id; 
            GMlist.add(GM2);
             allGroups.add(groupIdSU2);
             //CF Admins Global Compliance  
            GroupMember GM3 = new GroupMember();
            GM3.GroupId = groupIdSU3;
            GM3.UserOrGroupId = a.Id; 
            GMlist.add(GM3);
             allGroups.add(groupIdSU3); 
            
             //  CF Admins Global Human Resources
            GroupMember GM4 = new GroupMember();
            GM4.GroupId = groupIdSU4;
            GM4.UserOrGroupId = a.Id; 
            GMlist.add(GM4);
            allGroups.add(groupIdSU4); 
            //CF Admins Global Risk
            GroupMember GM5 = new GroupMember();
            GM5.GroupId = groupIdSU5;
            GM5.UserOrGroupId = a.Id; 
            GMlist.add(GM5);
            allGroups.add(groupIdSU5);  
                    
             //CF Admins Legal
        
            GroupMember GM6 = new GroupMember();
            GM6.GroupId = groupIdSU6;
            GM6.UserOrGroupId = a.Id; 
            GMlist.add(GM6);
            allGroups.add(groupIdSU6);
            
              //CF super User
            GroupMember GM7 = new GroupMember();
            GM7.GroupId = groupIdSU7;
            GM7.UserOrGroupId = a.Id; 
            GMlist.add(GM7);
            allGroups.add(groupIdSU7);
         }
        
             
         
    
    }
    
   //List<GroupMember> grpList = [Select Id from GroupMember where GroupId =: groupId
     //                         and UserOrGroupId in : userIdSet ];  
  //                             
    system.debug('allgroups='+ allGroups);
    system.debug('allUsers='+ userIdSet);
    List<GroupMember> grpList = [Select Id from GroupMember where GroupId =: allGroups and
                                  UserOrGroupId in : userIdSet ];
    
    delete grpList;
    system.Debug('GMlist Size='+ GMlist.size());
    if(!Test.isRunningTest())
    if(GMlist.size() > 0) insert gmList; 

}