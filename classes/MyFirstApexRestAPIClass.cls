@RestResource(urlMapping='/AccountDetails/*')
global with sharing class MyFirstApexRestAPIClass
{
    @HttpGet
    global static List<Account> doGet() 
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String AccNumber = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        List<Account> result = [SELECT Id, Name, AccountNumber, Type FROM Account];
        //List<Account> result = [SELECT Id, Name, AccountNumber, Type FROM Account WHERE AccountNumber = :AccNumber ];
        return result;
    }

    @HttpDelete
    global static void doDelete() 
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String AccNumber = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        List<Account> result = [SELECT Id, Name, Phone, Website FROM Account WHERE AccountNumber = :AccNumber ];
        delete result;
    }

    @HttpPost
    global static String doPost(String name,String phone,String AccountNumber ) 
    {
        Account acc = new Account();
        acc.name= name;
        acc.phone=phone;
        acc.AccountNumber =AccountNumber ;
        insert acc;
        
        return acc.id;
    }

}