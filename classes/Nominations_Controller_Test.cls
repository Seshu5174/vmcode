@isTest
public class Nominations_Controller_Test {
    public testMethod static void testNominationPage(){
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'BAC MIS System Admin'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE D&I GDIO Admin');
        insert role1;
        UserRole role2 = new UserRole(Name = 'NOMSITE D&I Team Reviewer');
        insert role2;
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        User usr2 = new User(  firstname = fName,
                             lastName = lName,
                             email = uniqueName + '@test' + orgId + '.org',
                             Username = uniqueName + '@test' + orgId + '.org',
                             EmailEncodingKey = 'ISO-8859-1',
                             Alias = uniqueName.substring(18, 23),
                             TimeZoneSidKey = 'America/Los_Angeles',
                             LocaleSidKey = 'en_US',
                             LanguageLocaleKey = 'en_US',
                             ProfileId = pf.Id,
                             UserRoleId = role2.Id);
        
        //Nomination_Status__c
        
        System.runAs(usr2){
            Nomination__c nom = new Nomination__c(Nomination_Type__c='Individual',LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='FIXED INCOME SALES',Nomination_Status__c='Submitted', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO');
            insert nom;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_Type__c='LOB Admin', Access_Level__c='Edit',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', LOB_Name__c='GLOBAL BANKING AND MARKETS',Record_Type__c='DI', Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', User_Name__c=usr2.Id,Nomination_Status__c='Ineligible');
            insert nsr;
            Nominations_Controller ns = new Nominations_Controller(new ApexPages.StandardController(new Nomination__c()));
            ns.getNominationTypes();
            
            ns.cancel();
            
            ns.refreshRecords();
            ns.initialize();
            //ns.generatepdf();
            ns.refreshNominationTypeAction();
            ns.dummyAction();
            ns.dummy();
            ns.openAsExcel();
            ns.openAsPdf();
            ns.navToAdminPage();
            ns.refreshNominations();
            ns.BackNumOfNominations();
            ns.getNumberOfNominations();
            ns.getListSUBLOBVal();
            ns.getAllNominations();
            Nominations_Controller.refreshLOBSUBLOBS(true);
            
            //ns.retrieveAllNominationsPDF();
            ns.retrieveAllNominations();
            ns.resetfilteraction();
            ns.assignSelectInputAction();
            ns.nomDetObj = [SELECT Id, Assignee_NM__c,format(Reviewer_Deadline_Date__c), Status__c,Reviewer_Comments__c,Winner_s_Summary__c, Final_Nomination_Verbiage__c,Nomination_Number__c, LOB__c,LOB_Short_Name__c,SUB_LOB_Short_Name__c, Nominee_FN__c,Nomination_Type__c,  
                            Created_By__c,Person_Number__c, Market__c, Manager_Name__c,Manager_E_mail__c,format(CreatedDate),LastModifiedBy.Name,format(LastModifiedDate), No_of_Nominations__c,SUB_LOB__c,Job_Name__c,Geographic_Location__c, Reason_For_Withdrawal__c, Region__c, Band__c, 
                            Division__c,DI_Priority__c,Accomplishments__c,Role_of_Nominee_in_Effort__c,Impacted_External_Organizations__c,Employee_Network_Involvement__c,Impacted_Audience__c,Inclusive_Leader_Attributes__c,Transparency__c,Trust__c,Investment__c,Purpose__c
                            from Nomination__c LIMIT 1];
            ns.saveReview();
            PageReference myVfPage = Page.NominationsHistoryPage;
            Test.setCurrentPage(myVfPage);
            Nomination__c nomNumber = [Select Id,Nomination_Number__c FROM Nomination__c where Id=:nom.Id];
            // Put Id into the current page Parameters
            ApexPages.currentPage().getParameters().put('nomId',nomNumber.Nomination_Number__c);
            ns.getnomiHistoryLists();
            PageReference myVfPage2 = Page.NominationReviewPage;
            Test.setCurrentPage(myVfPage2);
            
            // Put Id into the current page Parameters
            ApexPages.currentPage().getParameters().put('nomId',nomNumber.Nomination_Number__c);
            ns.getNominationDetails();
            
            
            //pass  nomId=nom.id
            //ns.getNominationDetails();
            
            Nominations_Controller.getAllNominationsInfo(new Nomination__c());
        }
        System.runAs(usr){
            Nomination__c nom = new Nomination__c();
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_Type__c='LOB Admin',Record_Type__c='DI', Access_Level__c='Edit',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', User_Name__c=usr.Id);
            insert nsr;
            Nominations_Controller ns = new Nominations_Controller(new ApexPages.StandardController(new Nomination__c()));
            ns.getNominationTypes();
            
            ns.cancel();
            
            ns.refreshRecords();
            ns.initialize();
            // ns.generatepdf();
            ns.refreshNominationTypeAction();
            ns.dummyAction();
            ns.dummy();
            ns.openAsExcel();
            ns.openAsPdf();
            ns.navToAdminPage();
            ns.refreshNominations();
            ns.BackNumOfNominations();
            ns.getNumberOfNominations();
            ns.getListSUBLOBVal();
            ns.LOBDrDn ='GLOBAL BANKING AND MARKETS';
            ns.getListSUBLOBVal();
            ns.getAllNominations();
            
            
            ns.resetfilteraction();
            ns.assignSelectInputAction();
            
            
        }
        
    }
    
    
    public testMethod static void testNominationPage_nominationPage(){
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'BAC MIS System Admin'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE D&I GDIO Admin');
        insert role1;
        UserRole role2 = new UserRole(Name = 'NOMSITE D&I Team Reviewer');
        insert role2;
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        User usr2 = new User(  firstname = fName,
                             lastName = lName,
                             email = uniqueName + '@test' + orgId + '.org',
                             Username = uniqueName + '@test' + orgId + '.org',
                             EmailEncodingKey = 'ISO-8859-1',
                             Alias = uniqueName.substring(18, 23),
                             TimeZoneSidKey = 'America/Los_Angeles',
                             LocaleSidKey = 'en_US',
                             LanguageLocaleKey = 'en_US',
                             ProfileId = pf.Id,
                             UserRoleId = role2.Id);
        
        //Nomination_Status__c
        
        System.runAs(usr){
            Account ac = new Account();
            //    ac.FirstName='TestAccount';
            //     ac.LastName='TestAccount'; 
            ac.Name='TestAccount';
            insert ac;
            
            Nomination__c nom2 = new Nomination__c(Nomination_Type__c='Individual',Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Status__c='Winner');
            insert nom2;
            Nomination__c nom3 = new Nomination__c(Nomination_Type__c='Individual',Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Status__c='Recommended Winner');
            insert nom3;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO');
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_Type__c='LOB Admin', Access_Level__c='Edit',Record_Type__c='DI',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', User_Name__c=usr.Id);
            insert nsr;
            Nominations_Controller ns = new Nominations_Controller(new ApexPages.StandardController(new Nomination__c()));
            
            Nomination__c searchInfo = new Nomination__c();
            searchInfo.Nomination_Type__c='All';
            Nominations_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
            //  ns.retrieveAllNominationsPDF();
            searchInfo.Nomination_Type__c='Individual';
            Nominations_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
            //  ns.retrieveAllNominationsPDF();
            
            searchInfo.Nomination_Type__c='Individual';
            searchInfo.LOB__c='GLOBAL BANKING AND MARKETS';
            searchInfo.LOB_Short_Name__c='GBAM';
            searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO';  
            searchInfo.SUB_LOB__c ='FIXED INCOME SALES';            
            searchInfo.Region__c ='AMRS';
            searchInfo.Status__c ='Winner';
            searchInfo.Nomination_Status__c ='Submitted';
            Nominations_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
            //   ns.retrieveAllNominationsPDF();
            
            
        }
        
    }
    
    
    
    public testMethod static void testNominationPage_nominationPageTeam(){
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'BAC MIS System Admin'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE D&I GDIO Admin');
        insert role1;
        UserRole role2 = new UserRole(Name = 'NOMSITE D&I Team Reviewer');
        insert role2;
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        User usr2 = new User(  firstname = fName,
                             lastName = lName,
                             email = uniqueName + '@test' + orgId + '.org',
                             Username = uniqueName + '@test' + orgId + '.org',
                             EmailEncodingKey = 'ISO-8859-1',
                             Alias = uniqueName.substring(18, 23),
                             TimeZoneSidKey = 'America/Los_Angeles',
                             LocaleSidKey = 'en_US',
                             LanguageLocaleKey = 'en_US',
                             ProfileId = pf.Id,
                             UserRoleId = role2.Id);
        
        //Nomination_Status__c
        
        System.runAs(usr){
            Account ac = new Account();
            //  ac.FirstName='TestAccount';
            // ac.LastName='TestAccount'; 
            ac.Name='TestAccount';
            insert ac;
            Nomination__c nom = new Nomination__c(Nomination_Type__c='Team',Team_Nominee_Lkp__c=ac.Id);
            insert nom;
            
            Nomination__c nom2 = new Nomination__c(Nomination_Type__c='Team',Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Status__c='Winner',Team_Nominee_Lkp__c=ac.Id,Team_Nomination__c=nom.Id);
            insert nom2;
            Nomination__c nom3 = new Nomination__c(Nomination_Type__c='Team',Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Status__c='Recommended Winner',Team_Nominee_Lkp__c=ac.Id,Team_Nomination__c=nom.Id);
            insert nom3;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_Type__c='LOB Admin', Access_Level__c='Edit',Record_Type__c='DI',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', User_Name__c=usr.Id);
            insert nsr;
            Nominations_Controller ns = new Nominations_Controller(new ApexPages.StandardController(new Nomination__c()));
            
            Nomination__c searchInfo = new Nomination__c();
            searchInfo.Nomination_Type__c='All';
            searchInfo.LOB__c='GLOBAL BANKING AND MARKETS';
            searchInfo.SUB_LOB__c ='FIXED INCOME SALES'; 
            searchInfo.LOB_Short_Name__c='GBAM';
            searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO';             
            searchInfo.Region__c ='AMRS';
            searchInfo.Status__c ='Winner';
            searchInfo.Nomination_Status__c ='Winner';
            Nominations_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
            //       ns.retrieveAllNominationsPDF();
            
            searchInfo.Nomination_Type__c='Team';
            searchInfo.LOB__c='GLOBAL BANKING AND MARKETS';
            searchInfo.SUB_LOB__c ='FIXED INCOME SALES'; 
            searchInfo.LOB_Short_Name__c='GBAM';
            searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO'; 
            searchInfo.Region__c ='AMRS';
            searchInfo.Status__c ='Winner';
            searchInfo.Nomination_Status__c ='Submitted';
            Nominations_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
            //        ns.retrieveAllNominationsPDF();
            ns.nomineetType = searchInfo.Nomination_Type__c;
            ns.getAllNominations();
            
        }
        
    }
    
    
    public testMethod static void testNominationPage_viceChair(){
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'BAC MIS System Admin'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE D&I GDIC Vice Chair');
        insert role1;
        UserRole role2 = new UserRole(Name = 'NOMSITE D&I LOB/Regl Admin');
        insert role2;
        UserRole role3 = new UserRole(Name = 'NOMSITE - D&I - Admin');
        insert role3;
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        User usr2 = new User(  firstname = fName+'12',
                             lastName = lName+'12',
                             email = uniqueName + '@test12' + orgId + '.org',
                             Username = uniqueName + '@test12' + orgId + '.org',
                             EmailEncodingKey = 'ISO-8859-1',
                             Alias = uniqueName.substring(18, 23),
                             TimeZoneSidKey = 'America/Los_Angeles',
                             LocaleSidKey = 'en_US',
                             LanguageLocaleKey = 'en_US',
                             ProfileId = pf.Id,
                             UserRoleId = role2.Id);
        User usr3 = new User(  firstname = fName+'123',
                             lastName = lName+'123',
                             email = uniqueName + '@test123' + orgId + '.org',
                             Username = uniqueName + '@test123' + orgId + '.org',
                             EmailEncodingKey = 'ISO-8859-1',
                             Alias = uniqueName.substring(18, 23),
                             TimeZoneSidKey = 'America/Los_Angeles',
                             LocaleSidKey = 'en_US',
                             LanguageLocaleKey = 'en_US',
                             ProfileId = pf.Id,
                             UserRoleId = role3.Id);                         
        
        //Nomination_Status__c
        
        System.runAs(usr){
            Nominations_Controller ns2 = new Nominations_Controller(new ApexPages.StandardController(new Nomination__c()));
            
            ns2.getListSUBLOBVal();
            
            
            Nomination__c nom = new Nomination__c(Nomination_Type__c='Individual',LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='FIXED INCOME SALES',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', Nomination_Status__c='Submitted',Status__c='Recommended Winner');
            insert nom;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_Type__c='LOB Admin', Access_Level__c='Edit',Record_Type__c='DI',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', SUB_LOB_Name__c='FIXED INCOME SALES', User_Name__c=usr.Id);
            insert nsr;
            Nominations_Controller ns = new Nominations_Controller(new ApexPages.StandardController(new Nomination__c()));
            ns.getNominationTypes();
            
            
            ns.refreshNominations();
            
            ns.getListSUBLOBVal();
            
            ns.LOBDrDn ='GLOBAL BANKING AND MARKETS';
            ns.getListSUBLOBVal();
            ns.getAllNominations();
            
            //    ns.retrieveAllNominationsPDF();
            ns.retrieveAllNominations();
            
        }
        
        System.runAs(usr2){
            Nominations_Controller ns2 = new Nominations_Controller(new ApexPages.StandardController(new Nomination__c()));
            ns2.getListSUBLOBVal();
            Nomination__c nom = new Nomination__c(Nomination_Type__c='Individual',LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='FIXED INCOME SALES',Nomination_Status__c='Submitted',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', Status__c='Recommended Winner');
            insert nom;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_Type__c='LOB Admin', Access_Level__c='Edit',Record_Type__c='DI',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', User_Name__c=usr2.Id);
            insert nsr;
            Nominations_Controller ns = new Nominations_Controller(new ApexPages.StandardController(new Nomination__c()));
            ns.getNominationTypes();
            
            
            ns.refreshNominations();
            
            ns.getListSUBLOBVal();
            ns.getAllNominations();
            
            //     ns.retrieveAllNominationsPDF();
            ns.nomDetObj = nom;
            ns.isSelectAll = false;
                            ns.actionType='add';
            ns.Region_Info = 'AMRS';
            ns.LOB_Info = 'CORPORATE AUDIT';
            ns.Nomination_Type = 'Individual';
            ns.recordId = nom.Id;
            ns.nomDetObj.Status__c = 'Finalist';
            ns.currentStatusInfo = nom.Status__c;
            ns.retrieveAllNominations();
            ns.rerenderisNewtrue();
            ns.validateSaveReview();
            
            ns.refreshfilter();
            ns.renderFilterScreenValue();
            ns.addParamMetersToPage();
            ns.navigateToReports();
            ns.modifyListInfo();
            
            ns.retrieveNominationAdmins();
            ns.clearLists();
            ns.approveRecords();
                            ns.actionType='remove';

            ns.modifyListInfo();
            ns.isSelectAll = true;
            List<String> lobList = new List<String>{'CORPORATE AUDIT'};
                List<String> regionsList = new List<String>{'CAMRS'};
                    List<String> nominationTypesList = new List<String>{'Individual'};
                                        List<String> recordIdList = new List<String>{nom.Id};

                        ns.LOB_Info = Json.serialize(lobList);
            ns.Region_Info = Json.serialize(regionsList);
            
            ns.Nomination_Type = Json.serialize(nominationTypesList);
            ns.recordId = Json.serialize(recordIdList);
            ns.modifyListInfo();
        }
        System.runAs(usr3){
            Nominations_Controller ns2 = new Nominations_Controller(new ApexPages.StandardController(new Nomination__c()));
        } 
    }
    
    
    
    
    
}