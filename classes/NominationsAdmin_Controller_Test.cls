@isTest
public class NominationsAdmin_Controller_Test {
public testMethod static void testNominationAdminPage(){

Profile pf = [SELECT Id FROM Profile WHERE Name = 'BAC MIS System Admin'];

UserRole role1 = new UserRole(Name = 'NOMSITE D&I GDIO Admin');
insert role1;
UserRole role2 = new UserRole(Name = 'NOMSITE D&I Team Reviewer');
insert role2;
String orgId = UserInfo.getOrganizationId();
String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');

Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
String uniqueName = orgId + dateString + randomInt;
String fName='fntest';
String lName='lntest';
User usr = new User(  firstname = fName+'1',
lastName = lName+'1',
email = uniqueName + '@test1' + orgId + '.org',
Username = uniqueName + '@test1' + orgId + '.org',
EmailEncodingKey = 'ISO-8859-1',
Alias = uniqueName.substring(18, 23),
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = pf.Id,
UserRoleId = role1.Id);
User usr2 = new User(  firstname = fName,
lastName = lName,
email = uniqueName + '@test' + orgId + '.org',
Username = uniqueName + '@test' + orgId + '.org',
EmailEncodingKey = 'ISO-8859-1',
Alias = uniqueName.substring(18, 23),
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = pf.Id,
UserRoleId = role2.Id);

//Nomination_Status__c

System.runAs(usr2){
Nomination__c nom = new Nomination__c(Nomination_Type__c='Individual',LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='FIXED INCOME SALES',Nomination_Status__c='Submitted', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO');
insert nom;

SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
insert sub; 

Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='Team Reviewer', Access_Level__c='Edit',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', LOB_Name__c='GLOBAL BANKING AND MARKETS',Record_Type__c='DI', Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', User_Name__c=usr2.Id,Nomination_Status__c='Ineligible');
insert nsr;
NominationsAdmin_Controller ns1 = new NominationsAdmin_Controller(new ApexPages.StandardController(new Nomination__c()));
ns1.getNominationTypes();

ns1.cancel();

ns1.refreshRecords();
ns1.initialize();
ns1.generatepdf();
ns1.refreshNominationTypeAction();
ns1.dummyAction();
ns1.dummy();
ns1.openAsExcel();

ns1.navToAssginReviewrsPage();
ns1.navToDataReportsPage();
ns1.navToMgtPage();
ns1.getAssigneeFilters();
ns1.getALLRegions();
ns1.getNominationsPerGroup();

ns1.resetpdffilters();
ns1.openAsPdf();
ns1.refreshNominations();
ns1.BackNumOfNominations();
ns1.getNumberOfNominations();
ns1.getListSUBLOBVal();
ns1.getAllNominations();
NominationsAdmin_Controller.refreshLOBSUBLOBS(true);

ns1.retrieveAllNominationsPDF();
ns1.retrieveAllNominations();
ns1.resetfilteraction();
ns1.assignSelectInputAction();
ns1.nomDetObj = [SELECT Id, Assignee_NM__c,format(Reviewer_Deadline_Date__c), Status__c,Reviewer_Comments__c,Winner_s_Summary__c, Final_Nomination_Verbiage__c,Nomination_Number__c, LOB__c,LOB_Short_Name__c,SUB_LOB_Short_Name__c, Nominee_FN__c,Nomination_Type__c,  
Created_By__c,Person_Number__c, Market__c, Manager_Name__c,Manager_E_mail__c,format(CreatedDate),LastModifiedBy.Name,format(LastModifiedDate), No_of_Nominations__c,SUB_LOB__c,Job_Name__c,Geographic_Location__c, Reason_For_Withdrawal__c, Region__c, Band__c, 
Division__c,DI_Priority__c,Accomplishments__c,Role_of_Nominee_in_Effort__c,Impacted_External_Organizations__c,Employee_Network_Involvement__c,Impacted_Audience__c,Inclusive_Leader_Attributes__c,Transparency__c,Trust__c,Investment__c,Purpose__c
from Nomination__c LIMIT 1];
ns1.saveReview();
PageReference myVfPage = Page.NominationsHistoryPage;
Test.setCurrentPage(myVfPage);

// Put Id into the current page Parameters
ApexPages.currentPage().getParameters().put('nomId',nom.Nomination_Number__c);
ns1.getnomiHistoryLists();
PageReference myVfPage2 = Page.NominationReviewPage;
Test.setCurrentPage(myVfPage2);

// Put Id into the current page Parameters
ApexPages.currentPage().getParameters().put('nomId',nom.Nomination_Number__c);
ns1.getNominationDetails();


//pass  nomId=nom.id
//ns1.getNominationDetails();

NominationsAdmin_Controller.getAllNominationsInfo(new Nomination__c());
}
System.runAs(usr){
Nomination__c nom = new Nomination__c();
SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
insert sub; 

Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='GDIO Admin',Record_Type__c='DI', Access_Level__c='Edit',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', User_Name__c=usr.Id);
insert nsr;

Nomination_Admin__c dates= new Nomination_Admin__c (Awardees_Publish_Date__c=date.parse('12/08/2019'),Record_Type__c='DI', Site_Lockdown_date__c=date.parse('09/23/2019'), Type__c='Deadline Dates');
insert dates;

NominationsAdmin_Controller ns1 = new NominationsAdmin_Controller(new ApexPages.StandardController(new Nomination__c()));
ns1.getNominationTypes();

ns1.cancel();

ns1.refreshRecords();
ns1.initialize();
ns1.generatepdf();
ns1.refreshNominationTypeAction();
ns1.dummyAction();
ns1.dummy();
ns1.openAsExcel();

ns1.navToAssginReviewrsPage();
ns1.navToDataReportsPage();
ns1.navToMgtPage();
ns1.getAssigneeFilters();
ns1.getALLRegions();
ns1.getNominationsPerGroup();
ns1.resetpdffilters();
ns1.openAsPdf();
ns1.refreshNominations();
ns1.BackNumOfNominations();
ns1.getNumberOfNominations();
ns1.getListSUBLOBVal();
ns1.LOBDrDn ='GLOBAL BANKING AND MARKETS';
ns1.getListSUBLOBVal();
ns1.getAllNominations();


ns1.resetfilteraction();
ns1.assignSelectInputAction();


}

}


public testMethod static void testNominationPage_nominationAdminPage(){

Profile pf = [SELECT Id FROM Profile WHERE Name = 'BAC MIS System Admin'];

UserRole role1 = new UserRole(Name = 'NOMSITE D&I GDIO Admin');
insert role1;
UserRole role2 = new UserRole(Name = 'NOMSITE D&I Team Reviewer');
insert role2;
String orgId = UserInfo.getOrganizationId();
String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');

Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
String uniqueName = orgId + dateString + randomInt;
String fName='fntest';
String lName='lntest';
User usr = new User(  firstname = fName+'1',
lastName = lName+'1',
email = uniqueName + '@test1' + orgId + '.org',
Username = uniqueName + '@test1' + orgId + '.org',
EmailEncodingKey = 'ISO-8859-1',
Alias = uniqueName.substring(18, 23),
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = pf.Id,
UserRoleId = role1.Id);
User usr2 = new User(  firstname = fName,
lastName = lName,
email = uniqueName + '@test' + orgId + '.org',
Username = uniqueName + '@test' + orgId + '.org',
EmailEncodingKey = 'ISO-8859-1',
Alias = uniqueName.substring(18, 23),
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = pf.Id,
UserRoleId = role2.Id);

//Nomination_Status__c

System.runAs(usr){
Account ac = new Account();
//ac.FirstName='TestAccount';
//ac.LastName='TestAccount'; 
  ac.Name='TestAccount';
insert ac;

Nomination__c nom2 = new Nomination__c(Nomination_Type__c='Individual',Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Status__c='Winner');
insert nom2;
Nomination__c nom3 = new Nomination__c(Nomination_Type__c='Individual',Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Status__c='Recommended Winner');
insert nom3;
SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO');
insert sub; 
Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='GDIO Admin', Access_Level__c='Edit',Record_Type__c='DI',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', User_Name__c=usr.Id);
insert nsr;
NominationsAdmin_Controller ns = new NominationsAdmin_Controller(new ApexPages.StandardController(new Nomination__c()));

Nomination__c searchInfo = new Nomination__c();
searchInfo.Nomination_Type__c='All';
NominationsAdmin_Controller.getAllNominationsInfo(searchInfo);
ns.searchInput = searchInfo;
ns.retrieveAllNominations();
ns.retrieveAllNominationsPDF();
searchInfo.Nomination_Type__c='Individual';
NominationsAdmin_Controller.getAllNominationsInfo(searchInfo);
ns.searchInput = searchInfo;
ns.retrieveAllNominations();
ns.retrieveAllNominationsPDF();

searchInfo.Nomination_Type__c='Individual';
searchInfo.LOB__c='GLOBAL BANKING AND MARKETS';
searchInfo.LOB_Short_Name__c='GBAM';
searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO';  
searchInfo.SUB_LOB__c ='FIXED INCOME SALES';            
searchInfo.Region__c ='AMRS';
searchInfo.Status__c ='Winner';
searchInfo.Nomination_Status__c ='Submitted';
NominationsAdmin_Controller.getAllNominationsInfo(searchInfo);
ns.searchInput = searchInfo;
ns.retrieveAllNominations();
ns.retrieveAllNominationsPDF();



}

}


public testMethod static void testNominationPage_nominationPageAdminTeam(){

Profile pf = [SELECT Id FROM Profile WHERE Name = 'BAC MIS System Admin'];

UserRole role1 = new UserRole(Name = 'NOMSITE D&I GDIO Admin');
insert role1;
UserRole role2 = new UserRole(Name = 'NOMSITE D&I Team Reviewer');
insert role2;
String orgId = UserInfo.getOrganizationId();
String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');

Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
String uniqueName = orgId + dateString + randomInt;
String fName='fntest';
String lName='lntest';
User usr = new User(  firstname = fName+'1',
lastName = lName+'1',
email = uniqueName + '@test1' + orgId + '.org',
Username = uniqueName + '@test1' + orgId + '.org',
EmailEncodingKey = 'ISO-8859-1',
Alias = uniqueName.substring(18, 23),
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = pf.Id,
UserRoleId = role1.Id);
User usr2 = new User(  firstname = fName,
lastName = lName,
email = uniqueName + '@test' + orgId + '.org',
Username = uniqueName + '@test' + orgId + '.org',
EmailEncodingKey = 'ISO-8859-1',
Alias = uniqueName.substring(18, 23),
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = pf.Id,
UserRoleId = role2.Id);

//Nomination_Status__c

System.runAs(usr){
Account ac = new Account();
//ac.FirstName='TestAccount';
//ac.LastName='TestAccount'; 
 ac.Name='TestAccount';
insert ac;
Nomination__c nom = new Nomination__c(Nomination_Type__c='Team',Team_Nominee_Lkp__c=ac.Id);
insert nom;

Nomination__c nom2 = new Nomination__c(Nomination_Type__c='Team',Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Status__c='Winner',Team_Nominee_Lkp__c=ac.Id,Team_Nomination__c=nom.Id);
insert nom2;
Nomination__c nom3 = new Nomination__c(Nomination_Type__c='Team',Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Status__c='Recommended Winner',Team_Nominee_Lkp__c=ac.Id,Team_Nomination__c=nom.Id);
insert nom3;
SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
insert sub; 
Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_Type__c='GDIO Admin', Access_Level__c='Edit',Record_Type__c='DI',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', User_Name__c=usr.Id);
insert nsr;
NominationsAdmin_Controller ns = new NominationsAdmin_Controller(new ApexPages.StandardController(new Nomination__c()));

Nomination__c searchInfo = new Nomination__c();
searchInfo.Nomination_Type__c='All';
searchInfo.LOB__c='GLOBAL BANKING AND MARKETS';
searchInfo.SUB_LOB__c ='FIXED INCOME SALES'; 
searchInfo.LOB_Short_Name__c='GBAM';
searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO';             
searchInfo.Region__c ='AMRS';
searchInfo.Status__c ='Winner';
searchInfo.Nomination_Status__c ='Winner';
NominationsAdmin_Controller.getAllNominationsInfo(searchInfo);
ns.searchInput = searchInfo;
ns.retrieveAllNominations();
ns.retrieveAllNominationsPDF();

searchInfo.Nomination_Type__c='Team';
searchInfo.LOB__c='GLOBAL BANKING AND MARKETS';
searchInfo.SUB_LOB__c ='FIXED INCOME SALES'; 
searchInfo.LOB_Short_Name__c='GBAM';
searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO'; 
searchInfo.Region__c ='AMRS';
searchInfo.Status__c ='Winner';
searchInfo.Nomination_Status__c ='Submitted';
NominationsAdmin_Controller.getAllNominationsInfo(searchInfo);
ns.searchInput = searchInfo;
ns.retrieveAllNominations();
ns.retrieveAllNominationsPDF();
ns.nomineetType = searchInfo.Nomination_Type__c;
ns.getAllNominations();

}

}

public testMethod static void testGetReportData(){

Profile pf = [SELECT Id FROM Profile WHERE Name = 'BAC MIS System Admin'];
String currentYear = string.ValueOf(System.today().year());

UserRole role1 = new UserRole(Name = 'NOMSITE D&I GDIO Admin');
insert role1;
//RecordType D_IRecordType = [select id,name from RecordType where RecordType.DeveloperName='D_I' and Name='NOMSITE DI'];   
//  string recordtype='NOMSITE DI';
RecordType D_IRecordType = [select id,name from RecordType where Name='NOMSITE DI'];
// RecordType D_IRecordType = new RecordType(Name='NOMSITE DI');
String orgId = UserInfo.getOrganizationId();
String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');

Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
String uniqueName = orgId + dateString + randomInt;
String fName='firstName';
String lName='lastName';
User usr = new User(  firstname = fName+'1',
lastName = lName+'1',
email = uniqueName + '@test1' + orgId + '.org',
Username = uniqueName + '@test1' + orgId + '.org',
EmailEncodingKey = 'ISO-8859-1',
Alias = uniqueName.substring(18, 23),
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = pf.Id,
UserRoleId = role1.Id);
//insert usr;

System.runAs(usr){
Account ac = new Account();
//ac.FirstName='TestAccount';
//ac.LastName='TestAccount1'; 
ac.Name='TestAccount1'; 
insert ac;


Nomination__c nomInd1 = new Nomination__c(Nomination_Type__c='Individual',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO',Region__c='EMEA',Status__c='Pending Review',Nomination_Year__c='2019',RecordTypeId=D_IRecordType.Id);
insert nomInd1;
Nomination__c nomInd2 = new Nomination__c(Nomination_Type__c='Individual',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO',Region__c='AMRS',Status__c='Pending Review',Nomination_Year__c='2019',RecordTypeId=D_IRecordType.Id);
insert nomInd2;
// Nomination__c nomInd3 = new Nomination__c(Nomination_Type__c='Individual',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO',Region__c='AMRS',Status__c='Pending Review',Nomination_Year__c='2019',RecordType=D_IRecordType);
//insert nomInd3;
Nomination__c nomInd4 = new Nomination__c(Nomination_Type__c='Individual',LOB_Short_Name__c='GTO', SUB_LOB_Short_Name__c='GDCE',Region__c='APAC',Status__c='Pending Review',Nomination_Year__c='2019',RecordTypeId=D_IRecordType.Id);
insert nomInd4;
Nomination__c nomInd5 = new Nomination__c(Nomination_Type__c='Individual',LOB_Short_Name__c='GTO',SUB_LOB_Short_Name__c='GLOBAL STAFFING',Region__c='APAC',Status__c='Pending Review', Nomination_Year__c='2019',RecordTypeId=D_IRecordType.Id);
insert nomInd5;
//   Nomination__c nomInd6 = new Nomination__c(Nomination_Type__c='Individual',Status__c='Pending Review',LOB_Short_Name__c='GTO',Region__c='APAC', SUB_LOB_Short_Name__c='GLOBAL STAFFING',Nomination_Year__c='2019',RecordType=D_IRecordType);
//    insert nomInd6;


Nomination__c nomMgr1 = new Nomination__c(Nomination_Type__c='Manager',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO',Status__c='Pending Review',Nomination_Year__c='2019',Region__c='AMRS',RecordTypeId=D_IRecordType.Id);
insert nomMgr1;
//   Nomination__c nomMgr2 = new Nomination__c(Nomination_Type__c='Manager',Nominee_Lkp__c=ac1.Id,LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO',Status__c='Pending Review',Nomination_Year__c='2019',Region__c='EMEA',RecordType=D_IRecordType);
// insert nomMgr2;
Nomination__c nomMgr3 = new Nomination__c(Nomination_Type__c='Manager',Status__c='Pending Review',LOB_Short_Name__c='GTO', SUB_LOB_Short_Name__c='GDCE',Nomination_Year__c='2019',Region__c='APAC',RecordTypeId=D_IRecordType.Id);

Nomination__c nomMgr4 = new Nomination__c(Nomination_Type__c='Manager',Status__c='Pending Review',LOB_Short_Name__c='GTO', SUB_LOB_Short_Name__c='GLOBAL STAFFING',Nomination_Year__c='2019',Region__c='APAC',RecordTypeId=D_IRecordType.Id);
insert nomMgr4;

Nomination__c nomTeamParent = new Nomination__c(Nomination_Type__c='Team',Team_Nominee_Lkp__c=ac.Id);
insert nomTeamParent;
Nomination__c nomTeam = new Nomination__c(Nomination_Type__c='Team',Status__c='Pending Review', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO',Nomination_Year__c='2019',Team_Nomination__c=nomTeamParent.Id,Region__c='AMRS',RecordTypeId=D_IRecordType.Id);
insert nomTeam;
Nomination__c nomTeam2= new Nomination__c(Nomination_Type__c='Team',Status__c='Pending Review', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO',Nomination_Year__c='2019',Team_Nomination__c=nomTeamParent.Id,Region__c='EMEA',RecordTypeId=D_IRecordType.Id);
List<String> statusesList=new List<string>{'Pending Review','Recommended Winner','Winner'};

NominationsAdmin_Controller ns1 = new NominationsAdmin_Controller(new ApexPages.StandardController(new Nomination__c()));
Nomination__c searchInfo = new Nomination__c();
// ns1.searchInput = new Nomination__c();

SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
insert sub; 
Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_Type__c='GDIO Admin', Access_Level__c='Edit',Record_Type__c='DI',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', User_Name__c=usr.Id);
insert nsr;

searchInfo.Nomination_Type__c='Individual';
searchInfo.LOB_Short_Name__c='GBAM';
searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO';            
searchInfo.Region__c ='EMEA';
searchInfo.Status__c ='Pending Review';
//  searchInfo.RecordType.name = 'NOMSITE DI';
searchInfo.RecordTypeId=D_IRecordType.Id;
searchInfo.Nomination_Year__c=currentYear;
//ns1.searchInput = searchInfo;
//   ns1.selectedstatuses='Pending Review';
// ns1.searchInput.RecordTypeId = D_IRecordType.id;
ns1.getReportRegionsList();
ns1.refreshAggregateResults();
ns1.getnomiReportData();
ns1.calculateSummaries();

searchInfo.Nomination_Type__c='Individual';
searchInfo.LOB_Short_Name__c='GTO';
searchInfo.SUB_LOB_Short_Name__c='GDCE';            
searchInfo.Region__c ='APAC';
searchInfo.Status__c ='Pending Review';
//   searchInfo.RecordTypeId = D_IRecordType.id;
searchInfo.Nomination_Year__c='2019';
//  ns1.searchInput = searchInfo;
//   ns1.selectedstatuses='Pending Review';

searchInfo.Nomination_Type__c='Individual';
searchInfo.LOB_Short_Name__c='GTO';
searchInfo.SUB_LOB_Short_Name__c ='GLOBAL STAFFING';            
searchInfo.Region__c ='APAC';
searchInfo.Status__c ='Pending Review';
//   searchInfo.RecordTypeId = D_IRecordType.id;
searchInfo.Nomination_Year__c=currentYear;
//   ns1.searchInput = searchInfo;
//   ns1.selectedstatuses='Pending Review';
//  ns1.getnomiReportData();
//  ns1.calculateSummaries(); 

searchInfo.Nomination_Type__c='Individual';
searchInfo.LOB_Short_Name__c='GBAM';
searchInfo.SUB_LOB_Short_Name__c='FIXED INCO';            
// searchInfo.Region__c ='AMRS';
searchInfo.Status__c ='Pending Review';
//  searchInfo.RecordTypeId = D_IRecordType.id;
//ns1.selectedstatuses='Pending Review';
ns1.selectedRegion='AMRS';
searchInfo.Nomination_Year__c=currentYear;
//   ns1.searchInput = searchInfo;

//  ns1.getReportRegionsList();
ns1.getnomiReportData();
ns1.calculateSummaries();

searchInfo.Nomination_Type__c='Manager';
searchInfo.LOB_Short_Name__c='GBAM';
searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO';            
searchInfo.Region__c ='AMRS';
searchInfo.Status__c ='Pending Review';
// searchInfo.RecordTypeId = D_IRecordType.id;
searchInfo.Nomination_Year__c=currentYear;
//ns1.searchInput = searchInfo;
// ns1.selectedstatuses='Pending Review';
ns1.getnomiReportData();
// ns1.calculateSummaries();

searchInfo.Nomination_Type__c='Manager';
searchInfo.LOB_Short_Name__c='GBAM';
searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO';            
searchInfo.Region__c ='EMEA';
searchInfo.Status__c ='Pending Review';
// searchInfo.RecordTypeId = D_IRecordType.id;
searchInfo.Nomination_Year__c=currentYear;
// ns1.searchInput = searchInfo;
//ns1.selectedstatuses='Pending Review';
//  ns1.getnomiReportData();
ns1.calculateSummaries();


searchInfo.Nomination_Type__c='Team';
searchInfo.LOB_Short_Name__c='GBAM';
searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO';            
searchInfo.Region__c ='AMRS';
searchInfo.Status__c ='Pending Review';
//searchInfo.RecordTypeId = D_IRecordType.id;
searchInfo.Nomination_Year__c=currentYear;
// ns1.searchInput = searchInfo;
// ns1.selectedstatuses='Pending Review';
ns1.getnomiReportData();
// ns1.calculateSummaries();

searchInfo.Nomination_Type__c='Team';
searchInfo.LOB_Short_Name__c='GBAM';            
searchInfo.SUB_LOB_Short_Name__c ='FIXED INCO';            
searchInfo.Region__c ='EMEA';
searchInfo.Status__c ='Pending Review';
// searchInfo.RecordTypeId = D_IRecordType.id;
searchInfo.Nomination_Year__c=currentYear;
//  ns1.searchInput = searchInfo;

//  ns1.getReportRegionsList();

ns1.selectedstatuses='Pending Review';
ns1.getnomiReportData();
//    ns1.calculateSummaries();
searchInfo.Nomination_Type__c='Team';
searchInfo.LOB_Short_Name__c='GTO';
searchInfo.SUB_LOB_Short_Name__c ='GDCE';            
searchInfo.Region__c ='APAC';
searchInfo.Status__c ='Pending Review';
searchInfo.RecordTypeId = D_IRecordType.id;
searchInfo.Nomination_Year__c=currentYear;
// ns1.searchInput = searchInfo;

//  ns1.selectedstatuses='Pending Review';
//   ns1.getnomiReportData();
ns1.calculateSummaries();       

searchInfo.Nomination_Type__c='Manager';
searchInfo.LOB_Short_Name__c='GTO';
searchInfo.SUB_LOB_Short_Name__c ='GDCE';            
searchInfo.Region__c ='APAC';
searchInfo.Status__c ='Pending Review';
searchInfo.RecordTypeId = D_IRecordType.id;
searchInfo.Nomination_Year__c=currentYear;
//  ns1.searchInput = searchInfo;
ns1.calculateSummaries();
ns1.renderFilterScreenValue();
ns1.resetpdffilters();
ns1.refreshfilter();

ns1.navToManageDatesPages();
ns1.navToAdminPage();
ns1.SecRoleId = [SELECT Id FROM Nomsite_Security_Roles__c  ].Id;

ns1.CloseAssignmentReviewer();
ns1.refreshfilter();
ns1.getDates();
ns1.saveDates();
ns1.Dates.Site_Lockdown_date__c = System.Today().addDays(-5);
ns1.saveDates();
ns1.Dates.Awardees_Publish_Date__c = System.Today().addDays(-5);
ns1.Dates.Site_Lockdown_date__c = System.Today().addDays(-5);
ns1.saveDates();
ns1.Dates.Site_Lockdown_date__c = System.Today().addDays(-5);
ns1.Dates.Awardees_Publish_Date__c = ns1.Dates.Site_Lockdown_date__c;
ns1.saveDates();
ns1.renderReportPanels();
ns1.resetReviewerDeadlineDate();
ns1.searchInput = new Nomination__c();

ns1.retrieveAllNominationsPDF();  

}

}   


public testMethod static void testCreateGroup()
{

Profile p = [SELECT Id FROM Profile WHERE Name='BAC MIS System Admin']; 
//User u = new User(Alias = 'standt', Email='SysMgr@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='SysMgr@testorg.com');
UserRole role1 = new UserRole(Name = 'NOMSITE D&I GDIO Admin');
insert role1;
User u = new User(  firstname = 'fName'+'1',
lastName = 'lName'+'1',
email = 'SysMgr@testorg.com',
Username = 'SysMgr@testorg.com',
EmailEncodingKey = 'UTF-8',
Alias = 'standt',
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = p.Id,
UserRoleId = role1.Id);

insert u;

UserRole role2 = new UserRole(Name = 'NOMSITE D&I LOB Reviewer');
insert role2;
User usr2 = new User(  firstname = 'fName2',
lastName = 'lName2',
email = 'fName2lName2@example.org',
Username = 'fName2lName2@example.org.qa',
EmailEncodingKey = 'UTF-8',
Alias ='fl123',
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = p.Id,
UserRoleId = role2.Id);
insert usr2;                 



System.runAs(u) {


Account ac = new Account();
//ac.FirstName='TestAccount';
//ac.LastName='TestAccount'; 
ac.Name='Test';
ac.Region__c='AMRS';
ac.Level_1_LOB__c='GLOBAL BANKING AND MARKETS';
ac.Level_2_LOB__c = 'FIXED INCOME SALES';
ac.LOB_Short_Name__c = 'GBAM';
ac.Sub_LOB_Short_Name__c = 'FIXED INCO';
insert ac;



Nomination_Admin__c dates= new Nomination_Admin__c (Awardees_Publish_Date__c=date.parse('12/08/2019'),Record_Type__c='DI', Site_Lockdown_date__c=date.parse('09/23/2019'), Type__c='Deadline Dates');
insert dates;
RecordType D_IRecordType = [select id,name from RecordType where Name='NOMSITE DI'];


Nomination__c nom = new Nomination__c(
Nomination_Type__c='Individual',
LOB__c='GLOBAL BANKING AND MARKETS',
SUB_LOB__c='FIXED INCOME SALES',
Nomination_Status__c='Submitted',
LOB_Short_Name__c='GBAM',
Final_Nomination_Verbiage__c='Testing Please Ignore', 
SUB_LOB_Short_Name__c='FIXED INCO',
RecordTypeId=D_IRecordType.Id,
Nomination_Year__c='2019',
Assignee_NM__c = 'Test Assignee',
Nomination_Number__c = 'I-Test1',
Region__c='EMEA',
Status__c='Pending Review',
Team_Nomination__c=null,
Nominee_Lkp__c=ac.Id);
insert nom;

Nomination__c nomMgr = new Nomination__c(Nomination_Type__c='Manager',Final_Nomination_Verbiage__c='Testing Please Ignore', 
Region__c='AMRS',LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='FIXED INCOME SALES',
Nomination_Status__c='Submitted', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO',
RecordTypeId=D_IRecordType.Id,Nomination_Year__c='2019',Assignee_NM__c = 'Test Assignee',Nomination_Number__c = 'M-Test1',
Status__c='Pending Review',Team_Nomination__c=null,Nominee_Lkp__c=ac.Id);
insert nomMgr;

Nomination__c nomTeamParent = new Nomination__c(Nomination_Type__c='Team',Status__c='Pending Review', Team_Nomination__c=null,Nomination_Number__c = 'T-Test1',Assignee_NM__c = 'Test Assignee',Final_Nomination_Verbiage__c='Testing Please Ignore',Nomination_status__c='Submitted',  RecordTypeId=D_IRecordType.Id,Nomination_Year__c='2019'); //parent
insert nomTeamParent;

Nomination__c nomTeam = new Nomination__c(Nomination_Type__c='Team', Team_Nomination__c=nomTeamParent.Id,
LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO',RecordTypeId=D_IRecordType.Id,Nomination_Year__c='2019');
insert nomTeam;

SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
insert sub; 
Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='Team Reviewer', Access_Level__c='Edit',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', LOB_Name__c='GLOBAL BANKING AND MARKETS',Record_Type__c='DI', Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', User_Name__c=usr2.Id);
insert nsr;
Nomsite_Security_Roles__c nsrReg= new Nomsite_Security_Roles__c(Role_type__c='Regional Reviewer', Access_Level__c='Edit',Record_Type__c='DI', Region__c='EMEA', User_Name__c=usr2.Id);
insert nsrReg;
Nomsite_Security_Roles__c nsrLOB= new Nomsite_Security_Roles__c(Role_type__c='LOB Reviewer',LOB_Short_Name__c='GBAM',LOB_Name__c='GLOBAL BANKING AND MARKETS', Access_Level__c='Edit',Record_Type__c='DI', Region__c='AMRS', User_Name__c=usr2.Id);
insert nsrLOB;
Nomsite_Security_Roles__c nSubLOB= new Nomsite_Security_Roles__c(Role_type__c='SUB LOB Reviewer', Access_Level__c='Edit',LOB_Short_Name__c='GBAM', Select_Sub_LOB__c=sub.Id ,SUB_LOB_Short_Name__c='FIXED INCO', LOB_Name__c='GLOBAL BANKING AND MARKETS',Record_Type__c='DI', Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', User_Name__c=usr2.Id);
insert nSubLOB;



test.startTest();
//ns1.GroupAdmMap.put(
NominationsAdmin_Controller ns1 = new NominationsAdmin_Controller(new ApexPages.StandardController(new Nomination__c()));
ns1.searchInput = new Nomination__c();
ns1.NominationsPerGroupInfo = 5;
ns1.NumOfGroups=1 ; 
ns1.grpOption='Manual';
ns1.searchInput.Status__c ='Pending Review';
ns1.searchNomination.Region__c='EMEA';

ns1.createGroups();
Nomination_Admin__c [] cGroups = [SELECT Id from Nomination_Admin__c where Type__c='Assignment'];





Nomination_Admin__c grp = new Nomination_Admin__c ();
grp.Assign_Sequence_Num__c = 1;
grp.Group_Name__c='Group - '+1;
grp.Type__c='Assignment';
grp.record_Type__c = 'DI';
grp.Assignment_Option__c='Manual';

grp.Nomination_Type__c = 'Team';
grp.region_Text__c= 'EMEA';
grp.Reviewer_Assign_LOB__c = 'GBAM';
//if(String.isNotBlank(searchInput.SUB_LOB_Short_Name__c)){grp.Reviewer_Assign_Sub_LOB__c =searchInput.SUB_LOB_Short_Name__c; }

insert grp;
nomTeamParent.groups__c = grp.Id;
 nomMgr.groups__c= grp.Id;
 nom.groups__c = grp.Id;
update nomTeamParent;
update nomMgr;
update nom;
ns1.InitActiveGroups();

ns1.groupIdPDF = grp.Id;
ns1.groupNomTypePDF = 'Individual';
ns1.assignPdfGenerate();

ns1.groupNomTypePDF = 'Manager';
ns1.assignPdfGenerate();

ns1.groupNomTypePDF = 'Team';
ns1.assignPdfGenerate();

//Team Assignment
ns1.NominationsPerGroupInfo = 5;
ns1.NumOfGroups= 1 ; 
ns1.grpOption='Automatic';
ns1.searchInput.Status__c ='Pending Review';
ns1.searchNomination.Region__c='EMEA';
ns1.searchInput.Nomination_Type__c ='Team';
ns1.createGroups();


Nomination_Admin__c [] groupId = [SELECT Id,Region_Text__c,Nomination_Type__c, Status_Text__c,Reviewer_Assign_LOB__c,Reviewer_Assign_Sub_LOB__c from  Nomination_Admin__c where Type__c='Assignment'];
List<Nomination__c> adminList = new List<Nomination__c>();
adminList.add(nom);
ns1.GroupAdmMap.put(groupId[0].Id, groupId[0]);
ns1.groupIdPDF =grp.Id;
ns1.SelectAssignee();

//Region Assignment
ns1.SecRoleId=nsrReg.Id;
ns1.AssignSecRoleUser();
ns1.NominationsPerGroupInfo = 5;
ns1.NumOfGroups= 1 ; 
ns1.grpOption='Automatic';
ns1.searchInput.Status__c ='Pending Review';
ns1.searchNomination.Region__c='EMEA';
ns1.searchInput.Nomination_Type__c ='Individual';
ns1.createGroups();

//LOB Assignment
ns1.NominationsPerGroupInfo = 5;
ns1.NumOfGroups= 1 ; 
ns1.grpOption='Automatic';
ns1.searchInput.Status__c ='Pending Review';
ns1.searchNomination.Region__c='AMRS';
ns1.searchInput.LOB_Short_Name__c='GBAM';
ns1.searchInput.Nomination_Type__c ='Manager';
ns1.createGroups();

Nomsite_Security_Roles__c nsrSubLOB= new Nomsite_Security_Roles__c(Role_type__c='SUB LOB Reviewer',LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO',Access_Level__c='Edit',Record_Type__c='DI', Region__c='AMRS', User_Name__c=usr2.Id);
insert nsrSubLOB;
//SubLOB Assignment
ns1.NominationsPerGroupInfo = 5;
ns1.NumOfGroups= 1 ; 
ns1.grpOption='Automatic';
ns1.searchInput.Status__c ='Pending Review';
ns1.searchInput.Region__c='AMRS';
ns1.searchInput.LOB_Short_Name__c='GBAM';
ns1.searchInput.SUB_LOB_Short_Name__c='FIXED INCO' ;

ns1.searchInput.Nomination_Type__c ='Manager';
ns1.createGroups();
test.stopTest();
}
}

public testMethod static void testNominationPage_LOBAdmin(){

Profile pf = [SELECT Id FROM Profile WHERE Name = 'BAC MIS System Admin'];

UserRole role2 = new UserRole(Name = 'NOMSITE D&I LOB/Regl Admin');
insert role2;
UserRole role3 = new UserRole(Name = 'NOMSITE - D&I - Admin');
insert role3;
String orgId = UserInfo.getOrganizationId();
String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');

Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
String uniqueName = orgId + dateString + randomInt;
String fName='fntest';
String lName='lntest';

User usr2 = new User(  firstname = fName+'12',
lastName = lName+'12',
email = uniqueName + '@test12' + orgId + '.org',
Username = uniqueName + '@test12' + orgId + '.org',
EmailEncodingKey = 'ISO-8859-1',
Alias = uniqueName.substring(18, 23),
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = pf.Id,
UserRoleId = role2.Id);
User usr3 = new User(  firstname = fName+'123',
lastName = lName+'123',
email = uniqueName + '@test123' + orgId + '.org',
Username = uniqueName + '@test123' + orgId + '.org',
EmailEncodingKey = 'ISO-8859-1',
Alias = uniqueName.substring(18, 23),
TimeZoneSidKey = 'America/Los_Angeles',
LocaleSidKey = 'en_US',
LanguageLocaleKey = 'en_US',
ProfileId = pf.Id,
UserRoleId = role3.Id);                         

//Nomination_Status__c


System.runAs(usr2){
NominationsAdmin_Controller ns2 = new NominationsAdmin_Controller(new ApexPages.StandardController(new Nomination__c()));
ns2.getListSUBLOBVal();
Nomination__c nom = new Nomination__c(Nomination_Type__c='Individual',LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='FIXED INCOME SALES',Nomination_Status__c='Submitted',LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', Status__c='Recommended Winner');
insert nom;
SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
insert sub; 
Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_Type__c='Team Reviewer', Access_Level__c='Edit',Record_Type__c='DI',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='FIXED INCOME SALES', LOB_Short_Name__c='GBAM', SUB_LOB_Short_Name__c='FIXED INCO', User_Name__c=usr2.Id);
insert nsr;
NominationsAdmin_Controller ns = new NominationsAdmin_Controller(new ApexPages.StandardController(new Nomination__c()));
ns.getNominationTypes();


ns.refreshNominations();

ns.getListSUBLOBVal();
ns.getAllNominations();

//     ns.retrieveAllNominationsPDF();
ns.retrieveAllNominations();

}
System.runAs(usr3){
NominationsAdmin_Controller ns2 = new NominationsAdmin_Controller(new ApexPages.StandardController(new Nomination__c()));
} 
}

}