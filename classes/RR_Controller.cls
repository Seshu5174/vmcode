public class RR_Controller {

    	@AuraEnabled
    public static List<Recharge_and_Refocus__c> getRRrecords(){
        return [select Id, Name, Employee_Full_Name__c, Eligibility_Year__c, Person_Number__c, YOS__c, Continuous_Service_Date__c, How_Many_Weeks_Eligible__c From Recharge_and_Refocus__c];
    }
}