trigger NomsiteRoles on Nomsite_Security_Roles__c (after insert, after update) {
Map<ID,ID> oldReviewer1IdMap = new Map<ID,ID> ();
Map<String,ID> subLOBmap = new Map<String,ID> ();
Map<ID,ID> nomMap = new Map<ID,ID> ();
Set<ID> userIdSet = new Set<ID>();
Map<String,ID> OldsubLOBmap = new Map<String,ID> ();
MAP<ID, ID> oldDelMap =  new Map<ID,ID> ();

List<sObject> NOMshareToInsert = new List<sObject>(); 
String OldUserId = null;
 String OldLOB = null;
 String OldSubLOB = null;
 Boolean isShare = false;
for (Nomsite_Security_Roles__c sbNom:trigger.new) {
        if(trigger.isUpdate){
                 OldUserId= trigger.OldMap.get(sbNom.id).User_Name__c ;
                 OldLOB= trigger.OldMap.get(sbNom.id).LOB_Name__c ;
             //    OldSubLOB= trigger.OldMap.get(sbNom.id).Sub_Lob_Name_Formula__c;
                 System.debug('SUBLOBTest'+ OldSubLOB);
              //  if(OldUserId != sbNom.User_Name__c || OldSubLOB != SbNom.Sub_Lob_Name_Formula__c || OldLOB != sbNom.LOB_Name__c)
                {
                    userIdSet.add(OldUserId);
                    OldsubLOBmap.put(OldLOB+OldSubLOB, OldUserId );
                    isShare = true;
                }
                       
                }        
        if(trigger.isInsert || isShare==true){
        //Get LOB and SUB LOB Names
        //compare with Nomination record LOB and Sub LOB
     //   subLOBmap.put(sbNom.LOB_Name__c+sbNom.Sub_Lob_Name_Formula__c, sbNom.User_Name__c);
       // System.debug('LG:'+ sbNom.LOB_Name__c+sbNom.Sub_Lob_Name_Formula__c);
             } 
                   }//end of for loop

           List<Nomination__c> NominationList = new  List<Nomination__c>([SELECT Id, LOB__c, Sub_LOB__c FROM Nomination__c]);
      for (Nomination__c nomRec: NominationList)
      {
          String UserID = subLOBmap.get(nomRec.LOB__c + nomRec.Sub_LOB__c);
          if( UserID  != null)
          {
              nomMap.put(nomRec.Id, UserID);
          }
        String oldUserId = OldsubLOBMap.get(nomRec.LOB__c + nomRec.Sub_LOB__c);
        if(oldUserId != null)
        {
            oldDelMap.put(nomRec.Id, UserID);
        }
      }
      //get existing mappings
      
       List<Nomination__share> delExitsingList = new List<Nomination__share> ([SELECT Id from Nomination__Share where UserOrGroupId IN: oldDelMap.KeySet() AND RowCause ='SUB_LOB_Reviewer__c' ]);
     
               
    //iterate Share object
    for (ID parentId:nomMap.keyset() )
    {
        Nomination__Share SubLobRev = new  Nomination__Share();
        SubLobRev.ParentId =  parentId;
        SubLobRev.RowCause = 'SUB_LOB_Reviewer__c';
        SubLobRev.AccessLevel = 'Edit';
        SubLobRev.UserOrGroupId =nomMap.get(parentId); 
         NOMshareToInsert.add(SubLobRev );
     }
 if(NOMshareToInsert.size() > 0)
 {
     insert NOMshareToInsert;
 }
 if(delExitsingList.size() > 0)
 {
     delete delExitsingList;
 }



}