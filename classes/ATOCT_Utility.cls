public with sharing class ATOCT_Utility
{
    public static void errorLogCreationFromBatch(integer jobsProcessed, integer TotalJobItems, integer NumberOfErrors, String className, String method, string errType, Datetime recDate, string message, string transType, string priority)
    {
           
        if(Schema.sobjectType.ATOCT_ATO_Error_Log__c.isAccessible() && Schema.sObjectType.ATOCT_ATO_Error_Log__c.isCreateable())
        {
            ATOCT_ATO_Error_Log__c errorLog = new ATOCT_ATO_Error_Log__c();
                errorLog.ATOCT_Batches_Processed__c=jobsProcessed;
                errorLog.ATOCT_Total_Batches__c = TotalJobItems;
                errorLog.ATOCT_Failures__c = NumberOfErrors;
                errorLog.ATOCT_Class__c = className;
                errorLog.ATOCT_Method__c = method;
                errorLog.ATOCT_Type__c = errType;
                errorLog.ATOCT_Date_Time__c = recDate;
                errorLog.ATOCT_Message__c = message;
                errorLog.ATOCT_Transaction_Type__c = transType;
                errorLog.ATOCT_Priority__c = priority;
            insert errorLog;    
        }
            
    }
    
  
}