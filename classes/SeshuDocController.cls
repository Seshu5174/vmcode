public class SeshuDocController{
	
	public List<Wclass> GroupEmpList{get;set;}
	public SeshuDocController(){
		
	}
	
	public class Wclass{
		public string FullName;
		public string LOBName;
		public List<EmployeeInfo> cffRecs;
	}
	
	public class EmployeeInfo{
		public string Audit_Type;
		public string CFF_Rating;
		public string CF_Commentary;
	}
	
	
}