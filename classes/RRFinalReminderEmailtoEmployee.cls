public class RRFinalReminderEmailtoEmployee{
    
    @InvocableMethod
    public static void RRFinalReminder(List<ID> RecordID)
    {
        Id orgWidAddrId;
        for(OrgWideEmailAddress owa : [SELECT id, Address, DisplayName from OrgWideEmailAddress]) 
        {
            if(owa.DisplayName.contains('Recharge'))
                orgWidAddrId = owa.Id;
        } 
        
        
        List<Messaging.SingleEmailMessage>  listEmail = new  List<Messaging.SingleEmailMessage> ();
        List <Recharge_and_Refocus__c> rrRecords = [SELECT Id, Direct_Manager_Email__c, Request_Status__c, Election_Options__c, Employee_Email__c FROM Recharge_and_Refocus__c WHERE id IN: RecordID];
        
        for (Recharge_and_Refocus__c rrRecord: rrRecords)
        {
            
            string emailofEmployee =rrRecord.Employee_Email__c;
            string emailofManager = rrRecord.Direct_Manager_Email__c;
            string requestStatus = rrRecord.Request_Status__c;
            
            if (emailofEmployee !=null && emailofManager !=null && (requestStatus ==null || requestStatus == 'Deferred'))
            {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                
                String[] toAddresses = new String[] {emailofEmployee};
                String[] ccAddresses = new String[] {emailofManager};
                        
                        
                String emailBody ='<div style="margin:0 auto;width:850px;"><div style="text-align: right;color:black;">'+system.label.Email_Logo+'</div>';  
                emailBody +='<div style="font-size:24px;font-family:calibri;padding-top:25px;color:black;padding-bottom:15px;">'+'Final reminder: Take action for the Global Recharge & Refocus Program ' +'</div>' ;
                emailBody +='<div style="font-size:18px;font-family:calibri;color:black;padding-bottom:15px;">'+'Based on our records, you have not requested or declined to participate in the Global Recharge & Refocus Program ' +'</div>' ;
                emailBody +='<div style="font-size:15px;font-family:calibri;color:black;padding-bottom:15px;font-weight:bold;">'+'We need you to take action. Here’s how:' +'</div>' ;
                
                emailBody += '<div style="display:flex;font-size:14px;"><div style="width:450px;"><div style="background-color:#7d7d7d;padding-left:90px;padding-top:3px; padding-bottom:3px;color:white;font-weight:bold;">'+ 'If you want to participate: '+'</div><div style="display:flex;padding-left:10px;"><div>'+' • '+ '</div><div style="padding-left:10px;padding-right:10px;">'+ 'Schedule a discussion with your manager to discuss your desire to participate, including how much time off you’d like to take (based on the total number of weeks you’re eligible to take), when you’d like to take time off and any other important details to note. '+'</div></div><div style="display:flex;padding-top:5px;padding-left:10px;"> <div style="padding-left:40px;">'+' • '+'</div>    <div style="padding-left:10px;padding-right:10px;">'+ 'Your manager has to provide their verbal approval before you submit your request. '+'</div></div><div style="display:flex;padding-top:5px;padding-left:10px;"> <div>'+'•'+'</div>    <div style="padding-left:10px;padding-right:10px;">'+'Submit your request for time off through the '+'<a href="'+system.Label.RR_Request_Site_Link+'" style="text-decoration:none;color:blue;">Recharge & Refocus request site</a>'+'. Submitted requests should be approved at least 60 days prior to the first day of your requested time off. You can visit the Recharge & Refocus request site to check the status of or make changes to your request. '+'</div></div></div><div style="border:0.5px solid #7d7d7d;background-color:#7d7d7d;"></div><div style="width:450px;"><div style="background-color:#7d7d7d; padding-left:85px;padding-top:3px;padding-bottom:3px;color:white;font-weight:bold;">'+'If you don’t want to participate: '+'</div> <div style="display:flex; padding-left:10px;"> <div>'+' • '+'</div><div style="padding-left:10px;padding-right:10px;">'+'If you don’t wish to participate in the Recharge & Refocus Program during your milestone year, visit the '+'<a href="'+system.Label.RR_Request_Site_Link+'" style="text-decoration:none;color:blue;">Recharge & Refocus request site</a>'+', then choose “Decline to participate” from the request screen and click submit. This will notify your manager of your decision. '+'</div></div><div style="display:flex;padding-top:5px;padding-left:10px;"> <div>'+'•'+'</div><div style="padding-left:10px;padding-right:10px;">'+' As a reminder, any unused time away allotted under the Recharge & Refocus Leave program will be forfeited. '+'</div></div></div></div>';
                
                
                emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:25px;color:black;"><b>'+'What’s next?' +'</b>'+'</div>';
                emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:5px;color:black;">'+'Your manager will receive notification once you take action. If you’ve requested time off, you’ll receive a notice when they decision the request.' +'</div>'; 
                
                emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:15px;color:black;"><b>'+'Questions?' +'</b>'+'</div>';
                emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:5px;color:black;">'+'Visit '+'<a href="'+system.Label.RR_HR_Connect+'" style="text-decoration:none;color:blue;">HR Connect > XXX</a>'+', read our '+ '<a href="'+system.Label.RR_FAQs+'" style="text-decoration:none;color:blue;">frequently asked questions</a>'+' or '+ '<a href="'+system.Label.RR_contact_us+'" style="text-decoration:none;color:blue;">contact us </a>'+' for more information.'+'</div>';
                emailBody +='<div style="font-size:15px;font-family:calibri;padding-top:35px;color:black;">'+'Visit '+'<a href="'+system.Label.RR_HR_Connect+'" style="text-decoration:none;color:blue;">HR Connect</a>'+' for more information about available benefits. '+ '<a href="'+system.Label.RR_Review+'" style="text-decoration:none;color:blue;">Review </a>'+' important '+ '<a href="'+system.Label.RR_Legal_Disclaimer+'" style="text-decoration:none;color:blue;">legal disclaimer</a>'+'.'+'</div>';
                emailBody +='<div style="font-size:15px;font-family:calibri;color:black;padding-top:15px;">'+'Bank of America N.A. Member FDIC © 2020 Bank of America Corporation. All rights reserved.' +'</div></div>'; 
                
                
                mail.setToAddresses(toAddresses);
                mail.setOrgWideEmailAddressId(orgWidAddrId);  
                mail.setCcAddresses(ccAddresses);
                mail.setSubject('Final reminder to take action for Global Recharge & Refocus Program');
                mail.setHTMLBody(emailBody);
                listEmail.add(mail);
            }
        }
        Messaging.sendEmail(listEmail);
    }
}