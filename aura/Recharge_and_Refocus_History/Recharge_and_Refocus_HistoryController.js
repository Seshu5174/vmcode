({
    doInit : function(component, event, helper) {
        console.log('doInit');
        helper.SearchHelper(component, event, helper,component.get("v.pageNumber"),  component.get("v.currentPagesCount"),true);
    },    
    //pagination related methods
    
    pageChange: function(component, event, helper) {
        var pageNumber = event.getParam("pageNumber");
        var currnetPagesCount = event.getParam("currnetPagesCount");
        console.log('pageChange');
        component.set("v.pageNumber", pageNumber);
        component.set("v.currentPagesCount", currnetPagesCount);
        
        //helper.getAllSobjectRecords(component, pageNumber, currnetPagesCount);
        helper.SearchHelper(component, event, helper,component.get("v.pageNumber"),  component.get("v.currentPagesCount"),false);
        
    },
    
    recordCounterChange : function(component, event, helper){
        console.log('recordCounterChange');
        var currnetPagesCount = event.getParam("currnetPagesCount");
        component.set("v.pageNumber", '1');
        component.set("v.currentPagesCount", currnetPagesCount);
        //helper.getAllSobjectRecords(component,'1', currnetPagesCount);
        helper.SearchHelper(component, event, helper,component.get("v.pageNumber"),  component.get("v.currentPagesCount"),false);
    },
    
    /* Get the changed record count pass the count to the event Fire the event */
    changeRecordNumber : function(component, event, helper) {
        var currentPagesCount  = component.find("selectItem").get("v.value");
        var myEvent = $A.get("e.c:PageTotalChange");
        myEvent.setParams({ "currnetPagesCount": currentPagesCount});
        myEvent.fire();
        
    }
})