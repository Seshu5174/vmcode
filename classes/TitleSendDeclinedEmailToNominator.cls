global class TitleSendDeclinedEmailToNominator implements Database.Batchable<sObject>
{


global Database.QueryLocator start(Database.BatchableContext BC) 
{
  
string RecordType = 'NOMSITE TITLE';

NOMSITE_Setting__mdt[] NOMSITE_settings = [SELECT Nomination_Year__c,DeveloperName FROM NOMSITE_Setting__mdt];
map<String,String> year_map = new map<String,String>();
for(NOMSITE_Setting__mdt year_data: NOMSITE_settings){
year_map.put(year_data.DeveloperName, year_data.Nomination_Year__c);
} 
    string  currentYear = Test.isRunningTest()?'2019': year_map.get('Current_Year');

String query = 'SELECT Id, Name, Band__c, Created_By_Lkp__c,Nominator_E_mail__c,Proposed_Title__c,Nominee_FN__c,Proxy_Email__c, Nomination_Status__c FROM Nomination__c where Recordtype.name=:RecordType and Nomination_year__c=:currentYear and (Nomination_Status__c = \'Declined By HR\' OR Nomination_Status__c = \'Declined By Manager\' OR Nomination_Status__c = \'Declined By HR for Manager\') and Nominator_E_mail__c!=\'\' and (Nominator_Band__c!=\'H1\' and Nominator_Band__c!=\'H0\')';        
return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List<Nomination__c> NominationsList) 
{

Id orgWidAddrId;
//Get Organization Wide Email Address id
for(OrgWideEmailAddress owa : [select id, Address, DisplayName from OrgWideEmailAddress]) 
{
if(owa.DisplayName.contains('Corporate Title'))
orgWidAddrId = owa.Id;
} 



Boolean isAdded = false;
Map<String,List<Nomination__c>> creatorToNomination = new Map<String,List<Nomination__c>>();
Map<String,List<String>> creatorToNominationeeEmail = new Map<String,List<String>>();

for(Nomination__c nom : NominationsList)
{

if(creatorToNomination.containskey(nom.Created_By_Lkp__c)){
creatorToNomination.get(nom.Created_By_Lkp__c).add(nom);
creatorToNominationeeEmail.get(nom.Created_By_Lkp__c).add(nom.Nominator_E_mail__c);
}
else{
creatorToNomination.put(nom.Created_By_Lkp__c,new List<Nomination__c>{nom});
creatorToNominationeeEmail.put(nom.Created_By_Lkp__c,new List<String>{nom.Nominator_E_mail__c});
}           
}


List<Messaging.SingleEmailMessage>  listEmail = new  List<Messaging.SingleEmailMessage> ();

for(String cretedby : creatorToNomination.keyset()){
String nominations_List = '';
Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage(); 
   
String emailBody = '<div style="text-align: right;">'+system.label.Email_Logo+'</div>';  
emailBody+='<html><body>'+ '<p style="font-size:16.5px;font-family:calibri;padding-top:10px;">'+'Global Human Resources'+'</p>' ;
emailBody+='<p style="font-size:22px;font-family:calibri;"><b >'+'We have an update on your corporate title promotion nominations'+'</b></p>' ;
emailBody+='<p style="font-size:15px;font-family:calibri;padding-bottom:10px;">'+'Thank you for submitting your corporate title promotion nominations. After careful review by your line of business, the nomination you submitted for the following employee(s) has been declined.'+'<b>'+' You should not communicate the declined status to your employee(s).'+'</b>'+'</p>' ;

for(Nomination__c nomi: creatorToNomination.get(cretedby)){
nominations_List +='<p style="font-size:15px;font-family:calibri;">'+ nomi.Nominee_FN__c+' - Proposed Title: '+nomi.Proposed_Title__c+'</p>';
}
emailBody +=nominations_List;
emailBody +='<p style="font-size:15px;font-family:calibri;padding-top:10px;">'+'If you have submitted additional nominations, you will receive updates on those submissions separately. You can view all of your nominations via the ' + '<a href="https://google.com">Corporate Title Promotions Nominations Dashboard</a>'+ '.' +'</p>';
emailBody +='<p style="font-size:15px;font-family:calibri;padding-top:10px;">'+'Note: This email address is not monitored for replies. If you have any questions, please contact your Global Human Resources or Learning & Leadership Development partner.'+'</p>';
emailBody +='<p style="font-size:15px;font-family:calibri;padding-top:10px;">'+'Thank you for helping make Bank of America a great place to work for all of our teammates.'+'</p>';

emailBody +='<p style="font-size:15px;font-family:calibri;padding-top:10px;">'+'The Corporate Title Nominations Team'+'</p>';

emailBody +='<br/><br/>';
emailBody +='<p style="font-size:15px;font-family:calibri;padding-top:10px;">'+'Bank of America N.A. Member FDIC © 2019 Bank of America Corporation. All rights reserved.'+'</p>'+'<br/>'+'</body></html>';                      

semail.setToAddresses(creatorToNominationeeEmail.get(cretedby));
semail.setOrgWideEmailAddressId(orgWidAddrId);  

semail.setSubject('We have an update on your corporate title promotion nominations');
semail.setHTMLBody(emailBody);
listEmail.add(semail);
}
Messaging.sendEmail(listEmail);
}
global void finish(Database.BatchableContext BC) 
{
}
}