public class MyBatchJob implements Database.Batchable<SObject>, Schedulable {

       public final string query;

       public MyBatchJob() {

            query = 'Select Id, Name From Account where lastmodifiedDate=today';

       }    

       public void execute(SchedulableContext sc) {
            Database.executeBatch(this, 100);
       }

       public Database.QueryLocator start(Database.BatchableContext bc) {
            return Database.getQueryLocator(query);
       }

       public void execute(Database.BatchableContext bc, List<sObject> scope) {

           list< Employee_Name__c > newObjects = new list< Employee_Name__c >();

           for(Sobject s : scope){
               Account obj = (Account) s;
               newObjects.add(new Employee_Name__c(
                  Employee__c =obj.Id,
                  Name= obj.Name
          ));
           }
           insert newObjects;
        

       }

       public void finish(Database.BatchableContext bc) {
          system.debug('JOB IS FINISHED');
       }

}