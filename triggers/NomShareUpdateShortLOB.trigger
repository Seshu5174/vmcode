trigger NomShareUpdateShortLOB on Nomsite_Security_Roles__c (before insert, before update) {
for(Nomsite_Security_Roles__c shRec: trigger.new)
{
if(shRec.LOB_Name__c != null && shRec.LOB_Name__c != '')
{
          
       List<SUB_LOB_Name__c> getLOBNM = [SELECT LOB_Short_Name__c from SUB_LOB_Name__c where LOB_Name__c =:shRec.LOB_Name__c LIMIT 1];
         for(SUB_LOB_Name__c gLB:getLOBNM)
         {
         
             shRec.LOB_Short_Name__c = gLB.LOB_Short_Name__c; 
             }                      
      }
  }    

}