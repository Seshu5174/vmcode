trigger ATOCT_ATO_Work_Item_Trigger on ATOCT_ATO_Work_Item__c (before insert,before update,after insert) {
    
    
    // Update Owner from MSR Id in file
    if(Trigger.isInsert && Trigger.isBefore)
    {
        ATOCT_ATO_Work_Item_Trigger_MSR_Help.onBeforeInsert(Trigger.new);
    }
    
    // Populate MSR ID based on Owner change
    if(Trigger.isUpdate && Trigger.isBefore)
    {
        ATOCT_ATO_Work_Item_Trigger_MSR_Help.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }
    
    if(Trigger.isInsert && Trigger.isAfter)
    {
        ATOCT_ATO_Work_Item_Trigger_MSR_Help.createInterfaceResults(Trigger.newMap);
    }
    
 
}