global class UpdateStudent implements schedulable{

    global void execute (schedulableContext sc){
        
          List<students__c>    stus = [Select id, Gender__c, Name from students__c];
      	  for (students__c a: stus){
            If(a.Gender__c=='Male')
            a.Name = 'Mr.'+a.Name;
            If(a.Gender__c=='Female')
                 a.Name = 'Mrs.'+a.Name;
        }
        update stus;
    }
}