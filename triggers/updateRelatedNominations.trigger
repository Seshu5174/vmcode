trigger updateRelatedNominations on Nomination__c(before update, before insert, after update, after insert) {
    if (trigger.isAfter) {
        system.debug('trigger firing');
        set < ID > MangerRecordIds = new set < ID > ();

        set < ID > IndividualRecordIds = new set < ID > ();

        Map < ID, Nomination__c > MangerMapIds = new Map < ID, Nomination__c > ();
        Map < ID, Nomination__c > IndividualMapIds = new Map < ID, Nomination__c > ();

        Schema.DescribeFieldResult sublobfieldResult = Nomination__c.Sub_LOB__c.getDescribe();
        List < Schema.PicklistEntry > subLOBList = sublobfieldResult.getPicklistValues();
        set < String > subLobpicks = new set < String > ();

        for (Schema.PicklistEntry f: subLOBList) {
            subLobpicks.add(f.getValue().toLowerCase());
        }

        Schema.DescribeFieldResult fieldResult = Nomination__c.LOB__c.getDescribe();
        List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
        set < String > lobList = new set < String > ();

        for (Schema.PicklistEntry f: ple) {
            lobList.add(f.getValue().toLowerCase());
        }

        Integer count = 0;
        for (Nomination__c gnm: trigger.new) {
            Nomination__c oldNomination = new Nomination__c();
            if (trigger.old != null) 
                oldNomination = trigger.old[count];

            if (String.isNotBlank(gnm.LOB__c) && (oldNomination == null || (oldNomination != null && (String.isBlank(oldNomination.LOB__c) || (String.isNotBlank(oldNomination.LOB__c) && (oldNomination.LOB__c.toLowercase() != gnm.LOB__c.toLowercase()))))) && (!lobList.contains(gnm.LOB__c.toLowercase()))) {

                UpdatePickList.updateLOB(gnm.LOB__c);

            }
            else if (String.isNotBlank(gnm.Sub_LOB__c) && (oldNomination == null || (oldNomination != null && String.isBlank(oldNomination.Sub_LOB__c) || (String.isNotBlank(oldNomination.Sub_LOB__c) && (oldNomination.Sub_LOB__c.toLowercase() != gnm.Sub_LOB__c.toLowercase())))) && (!subLobpicks.contains(gnm.Sub_LOB__c.toLowercase()))) {
                UpdatePickList.updateSubLOB(gnm.Sub_LOB__c, gnm.LOB__c);
            }
            if (String.isNotBlank(gnm.status__c) && (oldNomination.status__c != gnm.status__c)) {
                if (gnm.Nomination_Type__c == 'Individual') {
                    IndividualMapIds.put(gnm.Nominee_Lkp__c, gnm);
                    IndividualRecordIds.add(gnm.Id);

                }
                else if (gnm.Nomination_Type__c == 'Manager') {
                    MangerMapIds.put(gnm.Nominee_Lkp__c, gnm);
                    MangerRecordIds.add(gnm.Id);

                }
            }
            count++;
        }
        /*  for(Nomination__c gnm :trigger.new){
                Nomination__c oldNomination = new Nomination__c ();
                if(trigger.old!=null)
                oldNomination  = trigger.old[count];
                if((String.isBlank(oldNomination.LOB__c)||(String.isNotBlank(oldNomination.LOB__c)&&(oldNomination.LOB__c.toLowercase()!=gnm.LOB__c.toLowercase())))&&(!lobList.contains(gnm.LOB__c.toLowercase()))){
                UpdatePickList.updateLOB(gnm.LOB__c);

                }
                else  if((String.isBlank(oldNomination.Sub_LOB__c)||(String.isNotBlank(oldNomination.Sub_LOB__c)&&(oldNomination.Sub_LOB__c.toLowercase()!=gnm.Sub_LOB__c.toLowercase())))&&(!subLobpicks.contains(gnm.Sub_LOB__c.toLowercase()))){
                UpdatePickList.updateSubLOB(gnm.Sub_LOB__c,gnm.LOB__c);
                }
                if(String.isNotBlank(gnm.status__c)&&(oldNomination.status__c!=gnm.status__c)){
                if(gnm.Nomination_Type__c=='Individual'){
                IndividualMapIds.put(gnm.Nominee_Lkp__c,gnm);
                IndividualRecordIds.add(gnm.Id);

                }
                else if(gnm.Nomination_Type__c=='Manager'){
                MangerMapIds.put(gnm.Nominee_Lkp__c,gnm);
                MangerRecordIds.add(gnm.Id);

                }
                }
                count++;
                }*/
        system.debug(IndividualRecordIds);
        list < Nomination__c > individualNominationsList = [select id, Status__c, Nominee_Lkp__c from Nomination__c where Nomination_Type__c = 'Individual'
        and Nominee_Lkp__c in :IndividualMapIds.keySet() and id not in :IndividualRecordIds];
        system.debug(individualNominationsList.size());
        list < Nomination__c > updateIndividualNominations = new list < Nomination__c > ();
        for (Nomination__c relatedNomination: individualNominationsList) {
            if (relatedNomination.Status__c != IndividualMapIds.get(relatedNomination.Nominee_Lkp__c).Status__c) {
                relatedNomination.Status__c = IndividualMapIds.get(relatedNomination.Nominee_Lkp__c).Status__c;
                updateIndividualNominations.add(relatedNomination);
            }
        }

     //   update updateIndividualNominations;

        list < Nomination__c > manageNominationsList = [select id, Status__c, Nominee_Lkp__c from Nomination__c where Nomination_Type__c = 'Manager'
        and Nominee_Lkp__c in :MangerMapIds.keySet() and Id not in :MangerRecordIds];

        list < Nomination__c > updateMangerNominations = new list < Nomination__c > ();
        for (Nomination__c relatedNomination: manageNominationsList) {
            if (relatedNomination.Status__c != MangerMapIds.get(relatedNomination.Nominee_Lkp__c).Status__c) {

                relatedNomination.Status__c = MangerMapIds.get(relatedNomination.Nominee_Lkp__c).Status__c;
                updateMangerNominations.add(relatedNomination);
            }
        }

     //   update updateMangerNominations;
    }
    else {
        set < Id > idList = new set < Id > ();
        set < Id > gdiNominationIdList = new set < Id > ();
        list < Nomination__c > nominationsList = new list < Nomination__c > ();
        Integer count = 0;
        for (Nomination__c gnm: trigger.new) {
            Nomination__c oldNomination = new Nomination__c();
            if (trigger.old != null) oldNomination = trigger.old[count];
            if (gnm.Nominee_Lkp__c != null && ((oldNomination.Nominee_Lkp__c != null && oldNomination.Nominee_Lkp__c != gnm.Nominee_Lkp__c) || oldNomination.Nominee_Lkp__c == null)) {

                idList.add(gnm.Nominee_Lkp__c);
                gdiNominationIdList.add(gnm.Id);
                nominationsList.add(gnm);
            }
        }
        list < Account > employeeList = [select id, Level_1_LOB__c, Level_2_LOB__c from Account where id in :idList];
        Map < Id, Account > employeeToLOb = new Map < Id, Account > ();
        for (Account acInput: employeeList) {
            if (String.isNotBlank(acInput.Level_1_LOB__c)) employeeToLob.put(acInput.Id, acInput);
        }
        system.debug(nominationsList.size());
        for (Nomination__c nomineeinput: nominationsList) {

            if (nomineeinput.Nominee_Lkp__c != null && employeeToLob.containskey(nomineeinput.Nominee_Lkp__c)) {
                Account accountInput = employeeToLob.get(nomineeinput.Nominee_Lkp__c);
                nomineeinput.LOB__c = accountInput.Level_1_LOB__c;
                nomineeinput.Sub_LOB__c = accountInput.Level_2_LOB__c;

            }
        }
    }
}