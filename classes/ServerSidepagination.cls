public class ServerSidepagination{
    Public Integer size{get;set;} 
    Public Integer noOfRecords{get; set;} 
    Public String optyName{get; set;} 
    public Opportunity searchOpp{get;set;}
    public List<SelectOption> paginationSizeOptions{get;set;}
         
    public ServerSidepagination(){
        size=10;
        searchOpp = new Opportunity();
        optyName = '';
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
    }
     
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {                
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [select id,Name,AccountId,Account.name,Amount,StageName,CloseDate,LastModifiedDate from Opportunity]));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
            }            
            return setCon;
        }
        set;
    }
     
    //Changes the size of pagination
    public PageReference refreshPageSize() {
         setCon.setPageSize(size);
         return null;
    }
 
    // Initialize setCon and return a list of record    
     
    public List<Opportunity> getOpportunities() {
         return (List<Opportunity>) setCon.getRecords();
    }
    public void doSearch(){
             Opportunity inputInfo = new Opportunity(Name=optyName);
            getOpportunities(searchOpp);
       
        
    }
     public List<Opportunity> getOpportunities(Opportunity searchInput) {
system.debug('searchInput'+searchInput.Name)         ;
         List<String> searchQuery =new List<String>();
         if(String.isNotBlank(searchInput.Name)){
             String nameInfo ='%'+searchInput.Name+'%';
             searchQuery.add('name like :nameInfo');
         }
         if(String.isNotBlank(searchInput.Account.Name)){
             String nameInfo ='%'+searchInput.Account.Name+'%';
             searchQuery.add('Account.Name like :nameInfo');
         }
              if(String.isNotBlank(searchInput.StageName)){
             String nameInfo ='%'+searchInput.StageName+'%';
             searchQuery.add('StageName like :nameInfo');
         }
        
             setCon = new ApexPages.StandardSetController(Database.getQueryLocator('select id,Name,AccountId,Account.name,Amount,StageName,CloseDate,LastModifiedDate from Opportunity'));
         
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
         return (List<Opportunity>) setCon.getRecords();
    }
}