global with sharing class DraftRecordsInfo{
    
     global String accountId{get;set;}
  
    
    
    global List<Nomination__c> Opps{
        get{         
return [SELECT Id,Name FROM Nomination__c WHERE Nominee_Lkp__c=:accountId and Nomination_Type__c='Individual'];
            
         }
        set;
    }
    
    
    
    
}