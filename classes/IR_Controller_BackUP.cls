public with sharing class IR_Controller_BackUP {

    public String idTable{get;set;}  
    public String currentYear;
    public String camelCasing{get;set;}
    public String previousYear;
    public String urlForReports;
    public boolean subLOBsPopulated{get;set;}
    public boolean isSwitch{get;set;}

    public boolean isValidated{get;set;}
        public boolean ispopup{get;set;}
public  String  FieldtobeRendered  {get;set;}
public  String  FieldtobeRenderedinPopup  {get;set;}
    transient List<Incentive_Rationale__c> feedback{get;set;}
    Set<String> tempShareSetForLOBAdminOptions = new Set<String>();
    public Set<String> currentFeedbackIds = new Set<String>(); 
    public String whereClause = '';
    public Set<String> LOBAdminFilter= new Set<String>();
    public String controlFunction;
    public transient List<Incentive_Rationale__c> myTeamFeedback{get;set;}
    public List<String> selectedSlobs;
    List<SelectOption> dependentVals;
    List<Incentive_Rationale__c> sublobs;
    public transient List<Incentive_Rationale__c> currentFeedbackIdList{get;set;}
    public String queryString{get;set;}
    public String loggedInUserId{get;set;}
    public Boolean isPopUpOpened{get;set;}
    public Boolean filtersApplied{get;set;}
    public Boolean isLOBSelected{get;set;}
    public transient List<Incentive_Rationale__c> myLOBFeedback{get;set;} 
    public List<Incentive_Rationale__share> tempShareListForLOBAdmin{get;set;}
    public transient List<Incentive_Rationale__share> groupShareInfo{get;set;}
    public Incentive_Rationale__c feedbackDtl{get;set;}
    public List<String> selectedLOBInTheFilter{get;set;}
    public String selectedLOBs{get;set;}
    public String selectedSLOBSInTheFilter{get;set;}
    public String LOBAdmin{get;set;}
    public String selectedTab{get;set;} 
    public String defaultTab{get;set;}
    public List<Profile> profile{get;set;}
    public String myProfileName{get;set;} 
    public Map<String,Integer> feedbackStatus{get;set;}
    public Map<String,Boolean> profileIndicator{get;set;}
    public Map<String,Integer> classification{get;set;} 
    public String recordId{get;set;}
    public transient String tabClicked{get;set;}
    public boolean isIncentive {get;set;}
        public String errormsg{get;set;}


    public IR_Controller_BackUP  (apexpages.standardcontroller controller) {
ispopup= false;
isSwitch = false;
 List < PermissionSetAssignment > lstcurrentUserPerSet = [SELECT Id, PermissionSet.Name, AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :Userinfo.getUserId() and (PermissionSet.Name='IR_LOB_Admin'  OR PermissionSet.Name='CF_Manager_Provider')];
               if(lstcurrentUserPerSet.size()> 0){
                   isSwitch = true;
               }
        isValidated=true;
        errormsg ='';
        FieldtobeRendered = '';
        FieldtobeRenderedinPopup = '';
        system.debug('########################## IN CONSTRUCTOR #####################');
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=EDGE');
        queryString = 'select LOB__c,SUB_LOB__c from Incentive_Rationale__c where ownerid=:loggedInUserId';
        currentYear = String.valueOf(Date.Today().Year());
        previousYear = String.valueOf(Date.Today().Year() - 1);
        selectedLOBInTheFilter = new List < String > ();
        loggedInUserId = UserInfo.getUserId();
        system.debug('logged in user' + loggedInUserId);
        subLOBsPopulated = true;
        isLOBSelected = false;
        isPopUpOpened = false;
        filtersApplied = false;
        idTable = '';
        Boolean isPermissionSetPresent = false;
        feedbackStatus = new Map < String, Integer > ();
        classification = new Map < String, Integer > ();
        profileIndicator = new Map < String, Boolean > ();
        isIncentive = false;

        profile = [Select Name from Profile where Id = :UserInfo.getProfileId() LIMIT 1];
        myProfileName = profile[0].Name;
        defaultTab = '1';
        selectedTab = '1';
              

        profileIndicator.put('provider', false);
        profileIndicator.put('Reviewer', false);
        profileIndicator.put('SuperUser', false);
        profileIndicator.put('LOBAdmin', false);
        profileIndicator.put('configLOBAdmin', false);
        profileIndicator.put('configCFAdmin', false);
        profileIndicator.put('configManager', false);
        profileIndicator.put('Provider', false);
        profileIndicator.put('configProviderHistory', false);
        profileIndicator.put('configCFAdminHistory', false);
        profileIndicator.put('configLOBAdminHistory', false);
        profileIndicator.put('configManagerHistory', false);
        profileIndicator.put('ManagerProvider', false);

        defaultTab = '3';
        if (myProfileName.equalsIgnoreCase('IR Super User')) {
            selectedTab = '3';
            profileIndicator.put('SuperUser', true);
            profileIndicator.put('configLOBAdmin', false);
            profileIndicator.put('configCFAdmin', true);
            idTable = 'feedbackTable3';
            restoreDefaults();

        }

        else if (myProfileName.equalsIgnoreCase('IR LOB Admin') || myProfileName.equalsIgnoreCase('CF Super User') ) {
            defaultTab = '4';
            selectedTab = '4';
            profileIndicator.put('SuperUser', false);
            profileIndicator.put('configCFAdmin', false);
            profileIndicator.put('configLOBAdmin', true);
            profileIndicator.put('LOBAdmin', true);
            buildGroupSharedInfo();
            idTable = 'feedbackTable4';
            restoreDefaults();

        }

        else if (myProfileName.equalsIgnoreCase('IR Manager Provider') || myProfileName.equalsIgnoreCase('CF Manager Provider - force.com') ) {
            List < Incentive_Rationale__c > cffShare = [Select Id from Incentive_Rationale__c where (Reviewer_1__c = :UserInfo.getUserId() OR Reviewer_2__c = :UserInfo.getUserId() OR Reviewer_3__c = :UserInfo.getUserId()) LIMIT 5];
            List < Incentive_Rationale__c > cff = [Select Id from Incentive_Rationale__c where ownerid = :UserInfo.getUserId() LIMIT 5];
            String URL = 'https://' + ApexPages.currentPage().getHeaders().get('Host');
            system.debug('SHARE LIST SIZE-->' + cffShare.size() + 'STANDARD OBJECT LIST SIZE--->' + cff.size());

            try {
                if (cffShare.size() == 0 && cff.size() == 0) {
                    throw new CFFException();
                }
            } catch(Exception ex) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You do not have any covered employees to evaluate.Please contact your administrator'));

            }

            if (cffShare.size() > 0 && cff.size() > 0) {
                //urlForReports = URL+'/00O300000097ZMJ';
                system.debug('WHEN THE USER IS A MANAGER PROVIDER-->');
                defaultTab = '1';
                profileIndicator.put('ManagerProvider', true);
                profileIndicator.put('provider', true);
                profileIndicator.put('Reviewer', true);
                profileIndicator.put('configManager', false); //recheck itmust be false
                profileIndicator.put('Provider', true);
                selectedTab = '1';
                restoreDefaults();
                idTable = 'feedbackTable1';

            } else if (cffShare.size() > 0) {
                //urlForReports = URL+'/00O300000097ZMJ';
                system.debug('WHEN THE USER IS A MANAGER-->');
                defaultTab = '2';
                profileIndicator.put('ManagerProvider', false);
                profileIndicator.put('provider', false);
                profileIndicator.put('Reviewer', true);
                profileIndicator.put('configManager', true);
                profileIndicator.put('Provider', false);
                selectedTab = '2';
                restoreDefaults();
                idTable = 'feedbackTable2';

            } else if (cff.size() > 0) {
                //urlForReports = URL+'/00O300000097ZMJ';
                system.debug('WHEN THE USER IS A PROVIDER-->');
                defaultTab = '1';
                profileIndicator.put('ManagerProvider', false);
                profileIndicator.put('provider', true);
                profileIndicator.put('Reviewer', false);
                profileIndicator.put('configManager', false);
                profileIndicator.put('Provider', true);
                selectedTab = '1';
                restoreDefaults();
                idTable = 'feedbackTable1';
            }

        } //end of if

    }

    public List < String > convertStrToList(string str) {
        List < String > listToReturn = new List < String > ();
        if (String.isNotBlank(str)) {
            for (String eachStr: str.split(',')) {
                listToReturn.add(eachStr.trim());
                System.debug('DBG: eachStr: ' + eachStr);
            }
        }
        System.debug('DBG: listToReturn: ' + listToReturn);
        return listToReturn;
    }

    public String toProperCase(String value) {
        value = value.toLowerCase();
        List < String > pieces = new List < String > ();
        List < String > subString = value.split(',');
        Integer counter = 0;
        for (String s1: value.split(',')) {
            counter = counter + 1;
            for (String s2: s1.split(' ')) {
                if (!'and'.equalsIgnoreCase(s2)) {
                    s2 = s2.substring(0, 1).toUpperCase() + s2.substring(1, s2.length());
                }
                pieces.add(s2);
            }
            if (counter != subString.size()) {
                pieces.add(',');
            }
        }

        return String.join(pieces, ' ');
    }

    public void buildQuery() {
        selectedSlobs = new List < String > ();

        
        if (!String.isEmpty(selectedSLOBSInTheFilter) && selectedSLOBSInTheFilter != null) {
            system.debug('SELECTED SLOBS FOR THE LOB--->' + selectedSLOBSInTheFilter);
            camelCasing = toProperCase(selectedSLOBSInTheFilter);
            selectedSlobs = convertStrToList(selectedSLOBSInTheFilter);
        } else {
            camelCasing = '';
        }

        system.debug('AFTER CONVERTING STRING TO THE LIST-->' + selectedSlobs);

        if (selectedLOBInTheFilter.size() > 0 && selectedLOBInTheFilter != null) {
            system.debug('SELECTED LOBS IN THE FILTER SCREEN-----> ' + selectedLOBInTheFilter);

            whereClause = ' where LOB__c IN: selectedLOBInTheFilter';

        }
        if (!String.isEmpty(selectedSLOBSInTheFilter) && selectedSLOBSInTheFilter != null) {

            whereClause += ' and SUB_LOB__c IN: selectedSlobs';
        }

    }

    public List < Incentive_Rationale__c > getDataGrid() {

        if (filtersApplied) {
            system.debug('LOB---->' + selectedLOBInTheFilter + 'SLOB---->' + selectedSLOBSInTheFilter);

            String queryStringInProvider = 'select  Employee_Person_Number__c, Employee_Name_Lkp__c, LNFN__c, LOB__c, SUB_LOB__c, Category__c, Conduct__c, Rationale_Req__c, Rationale_Reason__c, Comp_Approach__c, Results_Rating__c, Behavior_Rating__c, YOY_Incentives__c, YOY_TComp__c, Pool_Funding__c, Rating_Rationale__c, Standard_Sentence__c, Performance_Commentary__c, Rationale__c, Provider_Name__c, Reviewer_1__c, Reviewer_1_PN__c, Reviewer_2__c, Reviewer_3__c, IR_Year__c from Incentive_Rationale__c' + whereClause + ' and ownerid =:loggedInUserId Order by LNFN__c ASC';

            system.debug('query string in filters for provider----> ' + queryStringInProvider);
            feedback = Database.query(queryStringInProvider);
            system.debug('FEEDBACK SIZE AFTER THE FILTERS ARE APPLIED-->' + feedback.size());
        } else {

            {
                feedback = [select id, Employee_Person_Number__c, Employee_Name_Lkp__c, LNFN__c, LOB__c, SUB_LOB__c, Category__c, Conduct__c, Rationale_Req__c, Rationale_Reason__c, Comp_Approach__c, Results_Rating__c, Behavior_Rating__c, YOY_Incentives__c, YOY_TComp__c, Pool_Funding__c, Rating_Rationale__c, Performance_Commentary__c, Standard_Sentence__c, Rationale__c, Provider_Name__c, Reviewer_1__c, Reviewer_1_PN__c, Reviewer_2__c, Reviewer_3__c, IR_Year__c from Incentive_Rationale__c where ownerid = :UserInfo.getUserId() Order by LNFN__c ASC];
            }
        }

        return feedback;
    }

    public List < Incentive_Rationale__c > getDirectReportsForManager() {

        if (filtersApplied) {

            system.debug('LOB---->' + selectedLOBInTheFilter + 'SLOB---->' + selectedSLOBSInTheFilter);
            ////String whereClause = buildQuery();
            String queryStringInManager = 'select  Name ,Employee_Person_Number__c, Employee_Name_Lkp__c, LNFN__c, LOB__c, SUB_LOB__c, Category__c, Conduct__c, Rationale_Req__c, Rationale_Reason__c, Comp_Approach__c, Results_Rating__c, Behavior_Rating__c, YOY_Incentives__c, YOY_TComp__c, Rating_Rationale__c, Standard_Sentence__c, Performance_Commentary__c, Pool_Funding__c, Rationale__c, Provider_Name__c, Reviewer_1__c, Reviewer_1_PN__c, Reviewer_2__c, Reviewer_3__c, IR_Year__c  from Incentive_Rationale__c' + whereClause + ' and (Reviewer_1__c=:loggedInUserId or Reviewer_2__c=:loggedInUserId or Reviewer_3__c=:loggedInUserId) Order by LNFN__c ASC';
            system.debug('query string in filters for manager----> ' + queryStringInManager);
            myTeamFeedback = Database.query(queryStringInManager);
            system.debug('FEEDBACK SIZE AFTER THE FILTERS ARE APPLIED-->' + myTeamFeedback.size());
        } else {
            myTeamFeedback = [select Name, Employee_Person_Number__c, Employee_Name_Lkp__c, LNFN__c, LOB__c, SUB_LOB__c, Category__c, Conduct__c, Rationale_Req__c, Rationale_Reason__c, Comp_Approach__c, Results_Rating__c, Behavior_Rating__c, YOY_Incentives__c, YOY_TComp__c, Pool_Funding__c, Rating_Rationale__c, Standard_Sentence__c, Performance_Commentary__c,  Rationale__c, Provider_Name__c, Reviewer_1__c, Reviewer_1_PN__c, Reviewer_2__c, Reviewer_3__c, IR_Year__c from Incentive_Rationale__c where (Reviewer_1__c = :UserInfo.getUserId() OR Reviewer_2__c = :UserInfo.getUserId() OR Reviewer_3__c = :UserInfo.getUserId()) Order by LNFN__c ASC];
        }
        return myTeamFeedback;
    }

public List < Incentive_Rationale__c > getSuperUserUserGrid() {
        if (filtersApplied) {

            system.debug('LOB---->' + selectedLOBInTheFilter + 'SLOB---->' + selectedSLOBSInTheFilter);
            ////String whereClause = buildQuery();
            String queryStringInManager = 'select  Name ,Employee_Person_Number__c, Employee_Name_Lkp__c, LNFN__c, LOB__c, SUB_LOB__c, Category__c, Conduct__c, Rationale_Req__c, Rationale_Reason__c, Comp_Approach__c, Results_Rating__c, Behavior_Rating__c, YOY_Incentives__c, YOY_TComp__c, Rating_Rationale__c, Standard_Sentence__c, Performance_Commentary__c, Pool_Funding__c, Rationale__c, Provider_Name__c, Reviewer_1__c, Reviewer_1_PN__c, Reviewer_2__c, Reviewer_3__c, IR_Year__c from Incentive_Rationale__c' + whereClause + '  Order by LNFN__c ASC';
            // removed Covered_Employee_Person_Number__c, to encrypt person_number__c 
            system.debug('query string in filters for manager----> ' + queryStringInManager);
            myTeamFeedback = Database.query(queryStringInManager);
            system.debug('FEEDBACK SIZE AFTER THE FILTERS ARE APPLIED-->' + myTeamFeedback.size());
        } else {
            myTeamFeedback = [select Name, Employee_Person_Number__c, Employee_Name_Lkp__c, LNFN__c, LOB__c, SUB_LOB__c, Category__c, Conduct__c, Rationale_Req__c, Rationale_Reason__c, Comp_Approach__c, Results_Rating__c, Behavior_Rating__c, YOY_Incentives__c, Rating_Rationale__c, Performance_Commentary__c, Standard_Sentence__c, YOY_TComp__c, Pool_Funding__c, Rationale__c, Provider_Name__c, Reviewer_1__c, Reviewer_1_PN__c, Reviewer_2__c, Reviewer_3__c, IR_Year__c from Incentive_Rationale__c  Order by LNFN__c ASC];
            // removed Covered_Employee_Person_Number__c, so we can encrypt person_number   
        }
        return myTeamFeedback;
    }
        
    
    public List < Incentive_Rationale__c > getLOBAdminGrid() {

        if (filtersApplied) {

            system.debug('LOB---->' + selectedLOBInTheFilter + 'SLOB---->' + selectedSLOBSInTheFilter);
            String queryStringInLOBAdmin = '';

            queryStringInLOBAdmin = 'select  Employee_Person_Number__c, Employee_Name_Lkp__c, LNFN__c, LOB__c, SUB_LOB__c, Category__c, Conduct__c, Rationale_Req__c, Rationale_Reason__c, Comp_Approach__c, Results_Rating__c, Behavior_Rating__c, YOY_Incentives__c, YOY_TComp__c,   Pool_Funding__c,  Rating_Rationale__c, Standard_Sentence__c, Performance_Commentary__c, Rationale__c, Provider_Name__c, Reviewer_1__c, Reviewer_1_PN__c, Reviewer_2__c, Reviewer_3__c, IR_Year__c from Incentive_Rationale__c' + whereClause + ' and ownerid !=:loggedInUserId and LOB__c =:tempShareSetForLOBAdminOptions  Order by LNFN__c ASC';

            system.debug('query string in filters----> ' + queryStringInLOBAdmin);
            myLOBFeedback = Database.query(queryStringInLOBAdmin);
            system.debug('FEEDBACK SIZE AFTER THE FILTERS ARE APPLIED-->' + myLOBFeedback.size());

        } else {

            if (null == LOBAdmin || LOBAdmin.equalsIgnoreCase('0')) {
                myLOBFeedback = new List < Incentive_Rationale__c > ();

            } else {
                myLOBFeedback = [select Employee_Person_Number__c, Employee_Name_Lkp__c, LNFN__c, LOB__c, SUB_LOB__c, Category__c, Conduct__c, Rationale_Req__c, Rationale_Reason__c, Comp_Approach__c, Results_Rating__c, Behavior_Rating__c, YOY_Incentives__c, YOY_TComp__c, Pool_Funding__c, Rating_Rationale__c, Standard_Sentence__c, Performance_Commentary__c, Rationale__c, Provider_Name__c, Reviewer_1__c, Reviewer_1_PN__c, Reviewer_2__c, Reviewer_3__c, IR_Year__c from Incentive_Rationale__c where ownerid != :UserInfo.getUserId() and LOB__c = :LOBAdmin  Order by LNFN__c ASC];
                
            }
        }
        
        Set < String > coveredEmployeeRecords = new Set < String > ();
        List < Incentive_Rationale__c > cffForDisplay = new List < Incentive_Rationale__c > ();

        for (Incentive_Rationale__c cff: myLOBFeedback) {
            if (!coveredEmployeeRecords.contains(cff.LNFN__c)) {
                cffForDisplay.add(cff);
                coveredEmployeeRecords.add(cff.LNFN__c);
            }
        }

        return cffForDisplay;
    }

    public void applyFilters() {
        system.debug('inside apply filters------>');
        filtersApplied = true;
        selectedLOBs = '';
        List < String > lobs = selectedLOBInTheFilter;
        Integer counter = 0;
        if (lobs.size() > 0) {
            for (String eachStr: lobs) {
                counter++;

                if (!String.isEmpty(eachStr) && null != eachStr) {
                    if (counter == 1) selectedLOBs = eachStr.trim();
                    else selectedLOBs += eachStr.trim();

                }
                if (counter != lobs.size()) {
                    selectedLOBs += ',';
                }

            }
            selectedLOBs = selectedLOBs.trim();
        }
        buildQuery();
    }

    public PageReference applyDefaultView() {
        filtersApplied = false;
        selectedLOBInTheFilter.clear();
        system.debug('inside apply DefaultView------>' + filtersApplied);
        return null;

    }

    public void buildGroupSharedInfo() {

        //List<Incentive_Rationale__share> groupShareInfo;
        tempShareListForLOBAdmin = new List < Incentive_Rationale__share > ();

        List < GroupMember > grpMember = [Select GroupId from GroupMember where UserOrGroupId = :Userinfo.getUserId()];
        Set < Id > groupIDSet = new Set < Id > ();
        for (GroupMember grp: grpMember) {
            groupIDSet.add(grp.GroupId);
        }
        List < Group > LOBAdmingrpIdList = [Select Id from Group where Id = :groupIDSet and name like 'IR LOB Admin%'];

        groupShareInfo = new List < Incentive_Rationale__share > ();
        groupShareInfo = [Select Id, parent.LOB__c from Incentive_Rationale__share where UserOrGroupId = :LOBAdmingrpIdList];
        tempShareListForLOBAdmin.addAll(groupShareInfo);

    }

    public List < SelectOption > getLOBAdminOptions() {
        //buildGroupSharedInfo();

        List < String > tempShareListForLOBAdminOptions = new List < String > ();

        List < SelectOption > options = new List < SelectOption > ();
        options.add(new SelectOption('0', '-- Select --'));
        if (tempShareListForLOBAdmin != null) {
            for (Incentive_Rationale__share fs: tempShareListForLOBAdmin) {

                tempShareSetForLOBAdminOptions.add(fs.parent.LOB__c);
            }
            tempShareListForLOBAdminOptions.addAll(tempShareSetForLOBAdminOptions);

            for (String LOBs: tempShareListForLOBAdminOptions) {
                options.add(new SelectOption(LOBs, LOBs));
            }
        }
        doSort(options);
        return options;

    }

    public void WhenPopUpClosed() {

        isPopUpOpened = false;
        isLOBSelected = false;

    }
    public PageReference dummy() {
        system.debug('INSIDE DUMMY FOR NORMAL RERENDER-->');

        return null;

    }

    public PageReference openFilters() {
        system.debug('INSIDE OPEN FILTERS -->');
        if (!filtersApplied) {
            selectedLOBInTheFilter.clear();
        }
        subLOBsPopulated = true;

        return null;

    }
    
    public PageReference openAsExcel() {
        system.debug('IN OPEN AS EXCEL METHOD-->');
        pageReference newPage = Page.IR_excel;
       return newPage.setRedirect(false);
    }

    public List < SelectOption > getLOBInfilter() {

        Set < String > elementsToBeAddedToTheSet = new Set < String > ();

        List < Incentive_Rationale__c > ListOfLOBs;
        List < CFF_Historical_Feedback__c > ListOfLOBsHistory;
        Set < String > uniqueSetForLOBs = new Set < String > ();

        //if('1'.equalsIgnoreCase(selectedTab) || '2'.equalsIgnoreCase(selectedTab) || '3'.equalsIgnoreCase(selectedTab) ||  '4'.equalsIgnoreCase(selectedTab))
        {
            if (selectedTab.equalsIgnoreCase('2')) queryString = 'select LOB__c,SUB_LOB__c from Incentive_Rationale__c where (Reviewer_1__c=:loggedInUserId or Reviewer_2__c=:loggedInUserId or Reviewer_3__c=:loggedInUserId)';
            system.debug('RETRIEVING LOBS WHEN THE CURRENT TABS ARE CHOSEN-->' + queryString);
            ListOfLOBs = Database.query(queryString);
            system.debug('ListOfLOBs-->' + ListOfLOBs);

            for (Incentive_Rationale__c cff: ListOfLOBs) {
                if (cff.LOB__c != null && !String.isEmpty(cff.LOB__c)) {
                    uniqueSetForLOBs.add(cff.LOB__c.trim());

                }
            }
        }
        List < SelectOption > controllingValues = new List < SelectOption > ();
        controllingValues.add(new SelectOption('0', '-- Select --'));

        for (String LOBs: uniqueSetForLOBs) {
            if (LOBs != null && !String.isEmpty(LOBs)) {
                controllingValues.add(new SelectOption(LOBs, LOBs));
            }
        }
        doSort(controllingValues);
        return controllingValues;

    }

    public List < SelectOption > getdependentValues() {

        Set < String > slobs = new Set < String > ();
        dependentVals = new List < SelectOption > ();
        dependentVals.add(new SelectOption('0', '-- Select --'));
        String queryStringForSubLOBs = '';
        system.debug('selectedLOBInTheFilter------>' + selectedLOBInTheFilter + ' IS LOB SELECTED-->' + isLOBSelected);

        if ('1'.equalsIgnoreCase(selectedTab) || '2'.equalsIgnoreCase(selectedTab) || '3'.equalsIgnoreCase(selectedTab) || '4'.equalsIgnoreCase(selectedTab)) {
            if ('3'.equalsIgnoreCase(selectedTab)) queryStringForSubLOBs = queryString + ' where LOB__c IN :selectedLOBInTheFilter ';
            else queryStringForSubLOBs = queryString + ' and LOB__c IN :selectedLOBInTheFilter ';

            system.debug('RETRIEVING SUB-LOBS WHEN THE CURRENT TABS ARE CHOSEN-->' + queryStringForSubLOBs);
            List < Incentive_Rationale__c > slobsForSelectedLOB = Database.query(queryStringForSubLOBs);
system.debug('slobsForSelectedLOB'+slobsForSelectedLOB);
            for (Incentive_Rationale__c cff: slobsForSelectedLOB) {
                system.debug('SLOBS------>' + cff.SUB_LOB__c);
                slobs.add(cff.SUB_LOB__c);
            }

        }

        for (String retrievedSlobs: slobs) {
            dependentVals.add(new SelectOption(retrievedSlobs, retrievedSlobs));
        }

        doSort(dependentVals);
        return dependentVals;
    }

    public PageReference enableFilterButton() {
        subLOBsPopulated = false;
        return null;
    }

    public pagereference emplInfo() {

        isPopUpOpened = true;
         isValidated =true;
        errormsg = '';
        
        system.debug('RECORD ID IN EMPL INFO METHOD--> ' + recordId + ' SELECTED TAB--> ' + selectedTab);
        if (selectedTab.equalsIgnoreCase('1') || selectedTab.equalsIgnoreCase('2') || selectedTab.equalsIgnoreCase('3') || selectedTab.equalsIgnoreCase('4')) {

            feedbackDtl = [select Employee_Person_Number__c, Employee_Name_Lkp__c, LNFN__c, LOB__c, SUB_LOB__c, Category__c, Conduct__c, Standard_Sentence__c, Rationale_Req__c, Rationale_Reason__c, Comp_Approach__c, Results_Rating__c, Behavior_Rating__c, YOY_Incentives__c, YOY_TComp__c, Rating_Rationale__c, Performance_Commentary__c, Pool_Funding__c, Rationale__c, Provider_Name__c, Reviewer_1__c, Reviewer_1_PN__c, Reviewer_2__c, Reviewer_3__c, IR_Year__c from Incentive_Rationale__c where id = :recordId];
            
            try {} catch(Exception ex) {}
        }
          
        getFieldsRenderedInfo();

        return null;
    }
    public void getFieldsRenderedInfo()
    {
     FieldtobeRendered = '';
        
        
        if ((feedbackDtl.Rationale_Req__c=='Yes - Outlier'||feedbackDtl.Rationale_Req__c=='Yes - Non Outlier' || feedbackDtl.Rationale_Req__c=='Yes - Negative') ||(feedbackDtl.Rationale_Reason__c=='Perf Comment, Rating Constraint and Comp Rationale')){
 FieldtobeRendered = FieldtobeRendered+'Rationale_pay__c';
 
    system.debug('===>FieldtobeRendered Rationale__c ');
       system.debug(FieldtobeRendered);
        }

        
        
        
        
       if(feedbackDtl.Behavior_Rating__c=='E'&& feedbackDtl.Rationale_Reason__c=='Perf Commentary and Rating Rationale')
        {
            
                FieldtobeRendered = FieldtobeRendered+'Rating_Rationale__c'; 
                    system.debug('===>FieldtobeRendered Rating_Rationale__c ');
       system.debug(FieldtobeRendered);
        }
        
if(((feedbackDtl.Rationale_Reason__c== 'Perf Commentary'||feedbackDtl.Rationale_Reason__c== 'Perf Commentary and Rating Rationale'))||((feedbackDtl.Rationale_Reason__c== 'Perf Commentary and Rating Constraint' || feedbackDtl.Rationale_Reason__c=='Perf Comment, Rating Constraint and Comp Rationale'))){

                    FieldtobeRendered = FieldtobeRendered+'Performance_Commentary__c'     ;   
                    system.debug('===>FieldtobeRendered Performance_Commentary__c ');
       system.debug(FieldtobeRendered);
            
        }
        system.debug('===>FieldtobeRendered');
       system.debug(FieldtobeRendered);
}
    public pagereference takeMeToReports() {

        PageReference reportPage;

        if (null != urlForReports && !String.isEmpty(urlForReports)) {
            reportPage = new Pagereference(urlForReports);
        } else {
            reportPage = new Pagereference('/00O/o');
        }
        reportPage.setRedirect(true);
        return reportPage;

    }

    public PageReference restoreDefaults() {
        system.debug('inside restore defaults-->');

        currentFeedbackIdList = new List < Incentive_Rationale__c > ();
        if ('true'.equalsIgnoreCase(tabClicked)) {

            filtersApplied = false;

        }
        List < AggregateResult > status = new List < AggregateResult > ();

        feedbackStatus = new Map < String,
        Integer > ();
        classification = new Map < String,
        Integer > ();
        Integer coveredEmpls = 0;

        if (selectedTab.equalsIgnoreCase('1')) {

            // query for populating filters

            /*
we are making use of queryString to populate the Filter screen
*/
            queryString = '';
            queryString = 'select LOB__c,SUB_LOB__c from Incentive_Rationale__c where ownerid =:loggedInUserId';

        }
        if (selectedTab.equalsIgnoreCase('2')) {
            //query for populating filters
            queryString = '';
            queryString = 'select LOB__c,SUB_LOB__c from Incentive_Rationale__c where ownerid !=:loggedInUserId';

        }

        if (selectedTab.equalsIgnoreCase('3')) {
            //query for populating filters                          

            queryString = 'select LOB__c,SUB_LOB__c from Incentive_Rationale__c ';

        }

        if (selectedTab.equalsIgnoreCase('4')) {
            //query for populating filters

            system.debug('lob admin lobs sub lobs in restore defaults' + LOBAdminFilter);

            if (LOBAdmin != null && !String.isEmpty(LOBAdmin) && !LOBAdmin.equalsIgnoreCase('0')) {
                queryString = 'select LOB__c,SUB_LOB__c from Incentive_Rationale__c where ownerid !=:loggedInUserId and LOB__c =:LOBAdmin';
            } else {

                for (Incentive_Rationale__share fs: tempShareListForLOBAdmin) {
                    LOBAdminFilter.add(fs.parent.LOB__c);
                }
                queryString = 'select LOB__c,SUB_LOB__c from Incentive_Rationale__c where ownerid !=:loggedInUserId and LOB__c=:LOBAdminFilter';
            }

        }

        if (selectedTab.equalsIgnoreCase('2') || selectedTab.equalsIgnoreCase('1')) {

            if (selectedTab.equalsIgnoreCase('2')) {
                system.debug('when selected tab is 2');
                profileIndicator.put('configManager', true);
                profileIndicator.put('Provider', false);
                profileIndicator.put('configProviderHistory', false);
                profileIndicator.put('configManagerHistory', false);

            }
            else if (selectedTab.equalsIgnoreCase('1')) {
                system.debug('when selected tab is 1');
                profileIndicator.put('configManager', false);
                profileIndicator.put('Provider', true);
                profileIndicator.put('configProviderHistory', false);
                profileIndicator.put('configManagerHistory', false);
                system.debug('filters applied-->' + filtersApplied);

            }

            classification.put('Completed', 0);
            classification.put('Pending', coveredEmpls);

        }
        if (selectedTab.equalsIgnoreCase('3')) {
            profileIndicator.put('configCFAdmin', true);
            profileIndicator.put('configLOBAdmin', false);
            profileIndicator.put('configCFAdminHistory', false);
            profileIndicator.put('configLOBAdminHistory', false);
        }

        if (selectedTab.equalsIgnoreCase('4')) {
            profileIndicator.put('configCFAdmin', false);
            profileIndicator.put('configLOBAdmin', true);
            profileIndicator.put('configCFAdminHistory', false);
            profileIndicator.put('configLOBAdminHistory', false);
        }

        return null;
    }

    public void updateIR() {
          isValidated=true; 
           errormsg= ''; 
Integer cnterror = 0;          
        if ((FieldtobeRendered.contains('Rationale_pay__c'))&&(String.isBlank(feedbackDtl.Rationale__c))){
        isValidated =false;
        errormsg='Incentive Pay Rationale ';
        cnterror = cnterror+1;
        }

        
        
        
        
        if((FieldtobeRendered.contains('Rating_Rationale__c'))&&String.isBlank(feedbackDtl.Rating_Rationale__c))
        {
            
                    isValidated =false;
        errormsg=((errormsg=='')?'Rating Rationale ':(errormsg+'and Rating Rationale are required'));
        cnterror = cnterror+1;

        }
        
if((FieldtobeRendered.contains('Performance_Commentary__c'))&&(string.isBlank(feedbackDtl.Performance_Commentary__c))){

                    isValidated =false;
        errormsg=((errormsg=='')?'Performance Commentary ':(errormsg+'and Performance Commentary are required'));  
cnterror = cnterror+1;      
            
        }
        if(!isValidated){
            if(cnterror==1)
        errormsg = errormsg+'is required';
        if(cnterror==3){
            errormsg = 'Incentive Pay Rationale, Rating Rationale and Performance Commentary are required';
        }
        }


if(isValidated){
    ispopup=false;
        update feedbackDtl;
}
    }

    public void populateStandard1stSentence() {

        

            if (feedbackDtl.Comp_Approach__c == 'Total Comp')

            feedbackDtl.Standard_Sentence__c = ' Overall SS TC pool spend is '+(feedbackDtl.Pool_Funding__c==0?'Flat ':(string.valueOf(feedbackDtl.Pool_Funding__c)+'% ')) +'and individual TC is '+(feedbackDtl.YOY_TComp__c=='0'?'Flat ':(string.valueOf(feedbackDtl.YOY_TComp__c)+'% '))+'YOY with a rating of '+feedbackDtl.Results_Rating__c+'/'+feedbackDtl.Behavior_Rating__c;
            
            else if (feedbackDtl.Comp_Approach__c == 'Total Incentives-1') {
                feedbackDtl.Standard_Sentence__c = ' Same store Incentive experience is '+(feedbackDtl.Pool_Funding__c==0?'Flat ':string.valueOf(feedbackDtl.Pool_Funding__c)+'% ')+'YOY and individual Incentive experience is '+(feedbackDtl.YOY_Incentives__c=='0'?'Flat ':(string.valueOf(feedbackDtl.YOY_Incentives__c)+'% '))+'with a rating of '+feedbackDtl.Results_Rating__c+'/'+feedbackDtl.Behavior_Rating__c;
            
            }
            
            else if (feedbackDtl.Comp_Approach__c == 'Total Incentives-2') {
                feedbackDtl.Standard_Sentence__c = ' Overall Total Incentives is '+(feedbackDtl.YOY_Incentives__c=='0'?'Flat ':(string.valueOf(feedbackDtl.YOY_Incentives__c)+'% '))+'YOY with a rating of '+feedbackDtl.Results_Rating__c+'/'+feedbackDtl.Behavior_Rating__c ; 
                
            }
        
    }

    public void doSort(list < selectOption > optns) {
        Map < String, Selectoption > mapping = new map < String, selectOption > ();
        List < String > sortLabels = new List < String > ();
        Integer i = 1;
        for (selectOption opt: optns) {
            mapping.put((opt.getLabel().toLowerCase() + i++), opt);
        }
        sortLabels.addAll(mapping.keySet());
        sortLabels.sort();
        optns.clear();
        for (String key: sortLabels) {
            optns.add(mapping.get(key));
        }
    }
    public void donothing(){
        system.debug('feedbackDtl.Rationale__c');

    system.debug(feedbackDtl.Rationale__c);
    update feedbackDtl;
    }
public void showpopup(){
ispopup=true;
}
public void Resetpopup(){
FieldtobeRenderedinPopup = '';
}
public pagereference switchaction(){
return new Pagereference('/apex/CFF');
}
}