trigger NomUpdateTeamNM on Nomination__c (before update) {
SET <ID> TeamParentId = new SET<ID>();
MAP<String, Nomination__c> NewRecMap  = new MAP<String, Nomination__c>();
if(trigger.isUpdate){
for (Nomination__c TeamRec:trigger.new) {
                                             //Add code here to update team name
   if(TeamRec.Nomination_Type__c == 'Team' && TeamRec.Nomination_Status__c == 'Submitted' && TeamRec.Team_Nomination__c == null)
       {                    //Add parent Id to SET  
               System.debug('LG: Team Parent'); 
               TeamParentId.add(TeamRec.id);  
               NewRecMap.put(TeamRec.Id, TeamRec);         
         } 
                
}

//Search for child records
List<Nomination__c> TeamChildList = new List<Nomination__c>([SELECT Id, Team_Nomination__c, Nominee_E_mail__c,HR_Full_Name__c  from Nomination__c where Nomination_Type__c='Team' AND Team_Nomination__c != null AND Team_Nomination__c IN:TeamParentId Order by Team_Nomination__c]);
List<Nomination__c> updateParentList = new List<Nomination__c>();
Nomination__c updParentRec;
String fullName = '';
String prevTeamId = null;
String NomineeEmails = '';
Integer childCount = 0;
for(Nomination__c childRec:TeamChildList)
{

if(prevTeamId == null || prevTeamId == childRec.Team_Nomination__c){
    if(prevTeamId == null)
    {
        fullName = childRec.HR_Full_Name__c;
        NomineeEmails = childRec.Nominee_E_mail__c;
    }
    else
    {
    fullName = fullName+';'+childRec.HR_Full_Name__c;
    NomineeEmails = NomineeEmails+';'+childRec.Nominee_E_mail__c;
    }
prevTeamId = childRec.Team_Nomination__c;
childCount = childCount+1;
}
else
{
       //add to list
   updParentRec = NewRecMap.get(prevTeamId);
   
   updParentRec.Team_Member_Names__c = fullName;
   updParentRec.Team_Member_Emails__c = NomineeEmails;
    fullName =childRec.HR_Full_Name__c;
   prevTeamId = childRec.Team_Nomination__c;
      
   }
}
//Updating last child record
if(childCount > 0)
{
updParentRec = NewRecMap.get(prevTeamId);
updParentRec.Team_Member_Names__c = fullName;
updParentRec.Team_Member_Emails__c = NomineeEmails;
}
}


}