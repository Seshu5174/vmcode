@isTest
public class IR_Controller_Test {
    
    public static testMethod  void  IR_ControllerIRSUperser(){
        Incentive_Rationale__c ir =new  Incentive_Rationale__c();
        ir.IR_Year__c = '2017';
        ir.LOB__c = 'GBAM';
        insert ir;
        Incentive_Rationale__c ir2 =new  Incentive_Rationale__c();
        ir2.IR_Year__c = '2017';
        ir2.LOB__c = 'GBAM';
        insert ir2;
        Profile p = [SELECT Id FROM Profile WHERE Name='IR Super User'];
        
        User u1 = new User(Alias = 'standt',Country='United Kingdom',Email='demo@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='dprobertdemo@camfed.org');
        insert u1;
        
        System.runAs(u1){
            ApexPages.StandardController sc = new ApexPages.StandardController(ir);
            IR_Controller irl = new IR_Controller(sc);
            irl.getSuperUserUserGrid();
            irl.openFilters();
            irl.feedbackDtl =ir;
            irl.donothing();
                        irl.showpopup();
            irl.Resetpopup();
            irl.applyFilters();
            irl.getLOBAdminOptions();
            irl.WhenPopUpClosed();
            irl.enableFilterButton();
            irl.selectedLOBInTheFilter =  new list<String>{'GBAM'};
                irl.applyFilters();
            irl.applyDefaultView();
            irl.filtersApplied = true;
            irl.getSuperUserUserGrid();
            irl.recordId =  ir.id;
            irl.emplInfo();
            irl.updateIR();
            irl.urlForReports = 'reports';
            irl.takeMeToReports();
            irl.dummy();
            irl.selectedTab = '1';
            irl.restoreDefaults();
            irl.selectedTab = '1';

            irl.LOBAdmin = '0';
            irl.restoreDefaults();
            irl.tabClicked = 'true';
              irl.restoreDefaults();
            irl.selectedSLOBSInTheFilter = 'cff';
            irl.buildQuery();
                    irl.selectedTab = '2';
            irl.restoreDefaults();
        }
        
    }    
    public static testMethod  void  IR_ControllerIRSUperser2(){
        Incentive_Rationale__c ir =new  Incentive_Rationale__c();
        ir.IR_Year__c = String.valueOf(Date.Today().Year());
        ir.LOB__c = 'GBAM';
        insert ir;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='IR Super User'];
        
        User u1 = new User(Alias = 'standt1',Country='United Kingdom',Email='demo1@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Test1', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='dprobert1@camfed.org');
        insert u1;
        Incentive_Rationale__share ishare= new   Incentive_Rationale__share();
        ishare.ParentId =ir.id;
        ishare.UserOrGroupId  = u1.id;
        ishare.AccessLevel  ='Read';
        insert ishare;
        
        System.runAs(u1){
            ApexPages.StandardController sc = new ApexPages.StandardController(ir);
            IR_Controller irl = new IR_Controller(sc);
            irl.getSuperUserUserGrid();
            irl.openFilters();
            irl.applyFilters();
            irl.getLOBAdminOptions();
            irl.WhenPopUpClosed();
            irl.enableFilterButton();
            irl.selectedLOBInTheFilter =  new list<String>{'GBAM'};
                irl.applyFilters();
            irl.applyDefaultView();
            irl.getSuperUserUserGrid();
            irl.recordId =  ir.id;
            irl.emplInfo();
            irl.updateIR();
            irl.takeMeToReports();
            irl.convertStrToList('GBAM,CAM');
            irl.toProperCase('GBAM,CAM');
        }
        
    }
    
    public static testMethod  void  IR_ControllerLOBadmin(){
        Incentive_Rationale__c ir =new  Incentive_Rationale__c();
        ir.IR_Year__c = String.valueOf(Date.Today().Year());
                ir.LOB__c = 'CFO';

        insert ir;
        Profile p = [SELECT Id FROM Profile WHERE Name='IR LOB Admin'];
        
        User u1 = new User(Alias = 'standt2',Country='United Kingdom',Email='demo2@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Test2', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='bertdemo2@camfed.org');
        insert u1;
        
        Incentive_Rationale__share ishare= new   Incentive_Rationale__share();
        ishare.ParentId =ir.id;
        ishare.UserOrGroupId  = u1.id;
        ishare.AccessLevel  ='Read';
        //ishare.parent.LOB__c ='CFO';
        insert ishare;
         
        
        System.runAs(u1){
            
            ApexPages.StandardController sc = new ApexPages.StandardController(ir);
            IR_Controller irl = new IR_Controller(sc);
            irl.selectedLOBInTheFilter =  new list<String>{'GBAM'};
                irl.getLOBAdminGrid();
                irl.LOBAdmin = 'GBAM';
            irl.getLOBAdminGrid();
            irl.selectedLOBInTheFilter =  new list<String>{'GBAM'};
                irl.applyFilters();
            irl.getLOBAdminGrid();
            irl.LOBAdmin ='GBAM';
                          irl.restoreDefaults();
              irl.LOBAdmin ='0';
                          irl.restoreDefaults();
            irl.filtersApplied =false;
            irl.LOBAdmin = 'CFO';
            irl.getLOBAdminGrid();
                
        }
        
    }      
    public static testMethod  void  IR_ControllerIRForce(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='IR Manager Provider'];
        
        User u1 = new User(Alias = 'standt3',Country='United Kingdom',Email='demo3@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Test3', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='dprobertdemo3@camfed.org');
        insert u1;
          User u2 = new User(Alias = 'standt2',Country='United Kingdom',Email='demo2@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Test2', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='testnow@camfed.org');
        insert u2;
        Incentive_Rationale__c ir =new  Incentive_Rationale__c();
       
        
        System.runAs(u1){
            
            ApexPages.StandardController sc = new ApexPages.StandardController(ir);
            delete [select id from Incentive_Rationale__c];
             IR_Controller irll = new IR_Controller(sc);
             ir.Reviewer_1__c = u1.Id;
        ir.IR_Year__c = String.valueOf(Date.Today().Year());
          ir.LOB__c = 'CFO';
ir.SUB_LOB__c = 'CFO-tests';
        insert ir;
         Incentive_Rationale__c ir2 =new  Incentive_Rationale__c();
        ir2.Reviewer_1__c = u1.Id;
                        ir2.LOB__c = 'CFO';
ir2.SUB_LOB__c = 'CFO-tests';
                ir2.Reviewer_1__c = u1.Id;
         ir2.Reviewer_2__c = u1.Id;
        ir2.IR_Year__c = String.valueOf(Date.Today().Year());
        insert ir2;
        ir2.Reviewer_1__c = u2.Id;
         ir2.Reviewer_2__c = u2.Id;
         ir2.Reviewer_3__c = u2.Id;
        update ir2;
            
            IR_Controller irl = new IR_Controller(sc);
            
            irl.getDataGrid();
            irl.getDirectReportsForManager();
            irl.selectedLOBInTheFilter =  new list<String>{'GBAM'};
                irl.applyFilters();
            irl.selectedTab = '1';
            irl.getLOBInfilter();
            irl.selectedLOBInTheFilter=  new list<String>{'CFO'};
            irl.getdependentValues();           
            irl.getLOBAdminGrid();
            irl.selectedLOBInTheFilter =  new list<String>{'GBAM'};
                irl.applyFilters();
            
            irl.getDataGrid();
            irl.getDirectReportsForManager();
            irl.openAsExcel();
            irl.recordId = [select id from Incentive_Rationale__c limit 1].id; 
               irl.emplInfo();
                  irl.feedbackDtl.Comp_Approach__c = 'Total Comp';

                         irl.populateStandard1stSentence();

           irl.feedbackDtl.Comp_Approach__c = 'Total Incentives-1';
                        irl.populateStandard1stSentence();

           irl.feedbackDtl.Comp_Approach__c = 'Total Incentives-2';
            irl.populateStandard1stSentence();
        }
        
    }    

     public static testMethod  void  getfiledrendinfotest(){

 Incentive_Rationale__c ir =new  Incentive_Rationale__c();
        ir.IR_Year__c = '2017';
        ir.LOB__c = 'GBAM';
		ir.Rationale_Req__c='Yes - Outlier';
        insert ir;
		 ApexPages.StandardController sc = new ApexPages.StandardController(ir);
            IR_Controller irl = new IR_Controller(sc);
			irl.feedbackDtl = ir;
		irl.getFieldsRenderedInfo();
		irl.updateIR();
        Incentive_Rationale__c ir2 =new  Incentive_Rationale__c();
        ir2.IR_Year__c = '2017';
        ir2.LOB__c = 'GBAM';
		ir2.Behavior_Rating__c='E';
		ir2.Rationale_Reason__c='Perf Commentary and Rating Rationale';
		
        insert ir2;
		irl.feedbackDtl = ir2;
			irl.getFieldsRenderedInfo();
		irl.updateIR();
		      Incentive_Rationale__c ir3 =new  Incentive_Rationale__c();
        ir3.IR_Year__c = '2017';
        ir3.LOB__c = 'GBAM';
		ir3.Rationale_Reason__c= 'Perf Commentary';
		insert ir3;
		irl.feedbackDtl = ir3;
		irl.getFieldsRenderedInfo();
		irl.updateIR();
		}	
    
}