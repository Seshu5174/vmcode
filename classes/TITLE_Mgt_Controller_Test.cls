@isTest
public class TITLE_Mgt_Controller_Test {
    public testMethod static void testNominationPage(){
        system.debug('line 1');
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE Title Global Admin');
        insert role1;
        UserRole role2 = new UserRole(Name = 'NOMSITE Title  LOB Admin');
        insert role2;
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        User usr2 = new User(  firstname = fName,
                             lastName = lName,
                             email = uniqueName + '@test' + orgId + '.org',
                             Username = uniqueName + '@test' + orgId + '.org',
                             EmailEncodingKey = 'ISO-8859-1',
                             Alias = uniqueName.substring(18, 23),
                             TimeZoneSidKey = 'America/Los_Angeles',
                             LocaleSidKey = 'en_US',
                             LanguageLocaleKey = 'en_US',
                             ProfileId = pf.Id,
                             UserRoleId = role2.Id);
        
        
        
        System.runAs(usr2){
            RecordType rt = [SELECT Id, Name, SobjectType FROM RecordType where SobjectType='Nomination__c' and Name='NOMSITE TITLE'];
            Nomination__c nom = new Nomination__c(RecordTypeID=rt.Id,Approver_Comments__c='test com',Nomination_Number__c='111',LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='GLOBAL FICC', Division__c='FICC TRADING', Nomination_Status__c='Submitted', Proposed_Title__c='Vice President');
            insert nom;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='GLOBAL FICC', LOB_Short_Name__c='GBAM', Sub_LOB_Short_Name__c='FIXED INCO' );
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='Global Admin', Access_Level__c='Edit',LOB_Name__c='GLOBAL BANKING AND MARKETS',Record_Type__c='Title', Region__c='AMRS',SUB_LOB_Name__c='GLOBAL FICC',Division_Name__c='FICC TRADING', User_Name__c=usr2.Id);
            insert nsr;
            TITLE_Mgt_Controller ns = new TITLE_Mgt_Controller(new ApexPages.StandardController(new Nomination__c()));
            system.debug('line 2');    
            ns.reset();
            system.debug('line 3');
            ns.approveRecords();
            system.debug('line 4');
            ns.refreshRecords();
            system.debug('line 5');
            ns.initialize();
            system.debug('line 6');
            ns.reformJSONAction();
            system.debug('line 7');
            ns.refreshNominationTypeAction();
            system.debug('line 8');
            ns.dummyAction();
            system.debug('line 9');
            ns.dummy();
            system.debug('line 10');
            ns.openAsExcel();
            system.debug('line 11');
            ns.openAsPdf();
            system.debug('line 12');
            ns.navToReportPage();
            system.debug('line 13');
            ns.navToMgtPage();
            system.debug('line 14');
            ns.refreshNominations();
            system.debug('line 15');
            ns.downalodReport();
            system.debug('line 16');
            ns.getListLOBVal();
            system.debug('line 17');
            ns.getListSUBLOBVal();
            system.debug('line 18');
            ns.addParamMetersToPage();
            system.debug('line 19');
            ns.getListDivisionVal();
            system.debug('line 20');
            ns.getAllNominations();
            system.debug('line 21');
            ns.renderFilterScreenValue();
            system.debug('line 122');
            ns.getReportTypes();
            system.debug('line 123');
            ns.getBands();
            system.debug('line 124');
            ns.refreshfilter();
            system.debug('line 125');
            TITLE_Mgt_Controller.refreshLOBSUBLOBS(true);
            system.debug('line 126');
            
            
            ns.retrieveAllNominations();
            system.debug('line 127');
            ns.resetfilteraction();
            system.debug('line 128');
            ns.rerenderisNewtrue();
            system.debug('line 129');
            
            
            ns.resetfilters();
            system.debug('line 1230');
            ns.getreviewPagestatusesList();
            system.debug('line 1231');
            ns.assignSelectInputAction();
            system.debug('line 1232');
            ns.nomDetObj = [SELECT Id,  Assignment_Status__c,Proposed_Title__c,Flagged_for_HR_Review__c,Approver_Deadline_Date__c,format(Reviewer_Deadline_Date__c), Approver_Comments__c,Approver_Name__c,Nomination_Status__c,Rejection_Reason__c, Standard_ID__c,Nomination_Number__c, LOB__c,Nominee_FN__c,Nomination_Type__c,  
                            Created_By__c,Person_Number__c, Time_in_Current_Corporate_Title__c,Time_in_Current_Job_Role__c,Worker_Type__c,Primary_Work_Geographic_Region__c,Nominee_E_mail__c,Mail_Code_consumer__c,Market__c, Manager_Name__c,Manager_E_mail__c,Manager_Phone_Number__c,Manager_Level_1__c,Manager_Level_2__c,Manager_Level_3__c,Manager_Level_4__c,Manager_Level_5__c,Manager_Level_6__c,Manager_Level_7__c,Manager_Level_8__c,format(CreatedDate),LastModifiedBy.Name,format(LastModifiedDate), No_of_Nominations__c,SUB_LOB__c,Job_Name__c,Geographic_Location__c, Reason_For_Withdrawal__c, Region__c, Band__c, 
                            Division__c,DI_Priority__c,Accomplishments__c,Role_of_Nominee_in_Effort__c,Corporate_Title__c,Hire_Date__c,Primary_Work_City__c,Primary_Work_Country__c,Primary_Work_State__c,Eligibility_Reasons__c,Employee_Relations_Case__c, Severance__c,Most_Recent_YE_Rating_How__c,Most_Recent_YE_Rating_What__c,Most_Recent_Mid_Year_Rating_How__c,Most_Recent_Mid_Year_Rating_What__c,One_Year_Previous_YE_Rating_How__c,One_Year_Previous_YE_Rating_What__c,Two_Year_Previous_YE_Rating_What__c,Two_Year_Previous_YE_Rating_How__c,
                            Rationale_for_Title_Promotion__c,Accountability_1__c,Accountability_2__c,Accountability_3__c,Accountability_4__c,Roles_and_responsibilities__c,Leadership_Behavior__c,Contribution_Impact_on_the_Business__c,Performance_Results_Accomplishments__c,Benchmark_Employee_Name_1__c,Benchmark_Employee_1_Job_Title__c,Benchmark_Employee_1_Division_Dept__c,Benchmark_Employee_1_Manager__c,Benchmark_Employee_Name_2__c,Benchmark_Employee_2_Job_Title__c,Benchmark_Employee_2_Division_Dept__c,Benchmark_Employee_2_Manager__c,Benchmark_Employee_Name_3__c,Benchmark_Employee_3_Job_Title__c,Benchmark_Employee_3_Division_Dept__c,Benchmark_Employee_3_Manager__c,
                            Due_Diligence_Employee_Name_1__c,Due_Diligence_Employee_1_Job_Title__c,Due_Diligence_Employee_1_LOB__c,Due_Diligence_Employee_1_LOB_ShortName__c,Due_Diligence_Employee_Name_2__c,Due_Diligence_Employee_2_Job_Title__c,Due_Diligence_Employee_2_LOB__c,Due_Diligence_Employee_2_LOB_ShortName__c,Due_Diligence_Employee_Name_3__c,Due_Diligence_Employee_3_Job_Title__c,Due_Diligence_Employee_3_LOB__c,Due_Diligence_Employee_3_LOB_ShortName__c from Nomination__c LIMIT 1];
            ns.saveReview();
            ns.showRejectReason();
            ns.getSecurityRoles();
            ns.CloseOverrideAssign();
            ns.OverrideSelectedUser();
            
            
                PageReference trackStatusPage = Page.TITLETrackStatusPage;
            system.debug('Nomination_Number__c==>'+ns.nomDetObj.Nomination_Number__c);
            trackStatusPage.getParameters().put('TitleName','111');
            Test.setCurrentPage(trackStatusPage);
            ns.getTrackStatus();
            PageReference myVfPage = Page.TITLEHistoryPage;
            Test.setCurrentPage(myVfPage);
            
            // Put Id into the current page Parameters
            Nomination__c nom2 = new Nomination__c(Recordtypeid=Schema.SObjectType.Nomination__c.getRecordTypeInfosByName().get('NOMSITE TITLE').getRecordTypeId(),Nomination_Number__c='111',LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='GLOBAL FICC', Division__c='FICC TRADING', Nomination_Status__c='Submitted', Proposed_Title__c='Vice President');
            insert nom2;
            NOmination__c nomq =[select Id,name,Nomination_Number__c from NOmination__c  where id=:nom2.Id];
            
            system.debug('ns.nomDetObj.Nomination_Number__c::'+[SELECT Nomination_Number__c, Team_Member_Names__c, Nominee_FN__c,Nomination_Type__c from Nomination__c where  Recordtype.Name='NOMSITE TITLE' LIMIT 1]);
            
            system.debug('ns.nomDetObj.Nomination_Number__c::'+nomq.Nomination_Number__c);
            system.debug('ns.nomDetObj.Nomination_Number__c::'+nomq.Nomination_Number__c);
            ApexPages.currentPage().getParameters().put('nomId',nomq.Nomination_Number__c);
            ApexPages.currentPage().getParameters().put('TitleName',nomq.Nomination_Number__c);
            ns.getTitleNomHistory();
            
            //      ns.getTitleNomHistory();
            PageReference myVfPage2 = Page.TITLEReviewPage;
            Test.setCurrentPage(myVfPage2);
            system.debug('line 1233');
            // Put Id into the current page Parameters
            ApexPages.currentPage().getParameters().put('nomId',nom.Nomination_Number__c);
            //     ns.getTitleNomDetails();
            
            
            //pass  nomId=nom.id
            //ns.getNominationDetails();
            
            //TITLE_Mgt_Controller.getAllNominationsInfo(new Nomination__c());
        }
    }
    
    public testMethod static void testNominationPageotherUser(){
        system.debug('line 1');
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE Title Global Admin');
        insert role1;
        UserRole role2 = new UserRole(Name = 'NOMSITE Title  LOB Admin');
        insert role2;
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        System.runAs(usr){
            Nomination__c nom = new Nomination__c();
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='GLOBAL FICC' );
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='LOB Admin',Record_Type__c='Title', Access_Level__c='Edit',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='GLOBAL FICC', User_Name__c=usr.Id);
            insert nsr;
            TITLE_Mgt_Controller ns = new TITLE_Mgt_Controller(new ApexPages.StandardController(new Nomination__c()));
            
            ns.reformJSONAction(); 
            ns.downalodReport();
            ns.approveRecords();
            ns.refreshRecords();
            ns.initialize();
            ns.reset();
            ns.refreshNominationTypeAction();
            ns.dummyAction();
            ns.dummy();
            ns.getReportTypes();
            ns.getBands();
            ns.openAsExcel();
            ns.openAsPdf();
            ns.getListDivisionVal();
            ns.navToReportPage();
            TITLE_Mgt_Controller.refreshLOBSUBLOBS(true);
            ns.refreshfilter();
            ns.rerenderisNewtrue();
            ns.addParamMetersToPage();
            ns.getreviewPagestatusesList();
            ns.navToMgtPage();
            ns.refreshNominations();
            
            ns.getListSUBLOBVal();
            
        }
        
    }
    public testMethod static void testNominationPageotherUser2(){
        system.debug('line 1');
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE Title Global Admin');
        insert role1;
        UserRole role2 = new UserRole(Name = 'NOMSITE Title  LOB Admin');
        insert role2;
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        System.runAs(usr){
            Nomination__c nom = new Nomination__c();
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='GLOBAL FICC' );
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='LOB Admin',Record_Type__c='Title', Access_Level__c='Edit',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='GLOBAL FICC', User_Name__c=usr.Id);
            insert nsr;
            TITLE_Mgt_Controller ns = new TITLE_Mgt_Controller(new ApexPages.StandardController(new Nomination__c()));
            ns.LOBDrDn ='GLOBAL BANKING AND MARKETS';
            
            
            ns.retrieveAllNominationsPDF();
            ns.reformJSONAction();  
            
            
        }
        
    }
      public testMethod static void testNominationPage_nominationPage(){
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE Title  LOB Admin');
        insert role1;
        
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        
        
        //Nomination_Status__c
        
        System.runAs(usr){
            Account ac = new Account();
            //       ac.FirstName='TestAccount';
            //        ac.LastName='TestAccount'; 
            ac.Name='TestAccount';
            insert ac;
            
            Nomination__c nom2 = new Nomination__c(Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted', Proposed_Title__c='Senior Vice President', Band__c='H3', LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c ='FIXED INCOME SALES', Division__c='FICC TRADING', Region__c='AMRS' );
            insert nom2;
            Nomination__c nom3 = new Nomination__c(Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Proposed_Title__c='Senior Vice President', Band__c='H3', LOB__c='GLOBAL BANKING AND MARKETS', SUB_LOB__c ='FIXED INCOME SALES', Division__c='FICC TRADING', Region__c='AMRS' );
            insert nom3;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='GLOBAL FICC', DIVISION_NAME__c = 'FICC TRADING');
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='LOB Admin', Access_Level__c='Edit',Record_Type__c='Title',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='GLOBAL FICC',  Division_Name__c='FICC TRADING',User_Name__c=usr.Id);
            insert nsr;
            TITLE_Mgt_Controller ns = new TITLE_Mgt_Controller(new ApexPages.StandardController(new Nomination__c()));
            
            Nomination__c searchInfo = new Nomination__c();
            
            TITLE_Mgt_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
            ns.retrieveAllNominationsPDF();
            TITLE_Mgt_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
            ns.getListDivisionVal();
            ns.retrieveAllNominationsPDF();
            TITLE_Mgt_Controller.refreshLOBSUBLOBS(true);
            ns.reset();
            ns.downalodReport();
            ns.approveRecords();
            
            
            searchInfo.LOB__c='GLOBAL BANKING AND MARKETS';
            searchInfo.SUB_LOB__c ='FIXED INCOME SALES';            
            searchInfo.Region__c ='AMRS';
            searchInfo.Proposed_Title__c='Senior Vice President';
            searchInfo.Band__c ='H3';
            searchInfo.Division__c ='FICC TRADING';
            searchInfo.Nomination_Status__c ='Approved By HR';
            searchInfo.Flagged_for_HR_Review__c ='Yes';
            TITLE_Mgt_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            //ns.retrieveAllNominations();
        }
        
    }
   
    public testMethod static void testNominationPage_nominationPage2(){
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE Title  LOB Admin');
        insert role1;
        
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        
        
        //Nomination_Status__c
        
        System.runAs(usr){
            Account ac = new Account();
            //       ac.FirstName='TestAccount';
            //        ac.LastName='TestAccount'; 
            ac.Name='TestAccount';
            insert ac;
            
            Nomination__c nom2 = new Nomination__c(Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted', Proposed_Title__c='Senior Vice President', Band__c='H3', LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c ='FIXED INCOME SALES', Division__c='FICC TRADING', Region__c='AMRS' );
            insert nom2;
            Nomination__c nom3 = new Nomination__c(Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Proposed_Title__c='Senior Vice President', Band__c='H3', LOB__c='GLOBAL BANKING AND MARKETS', SUB_LOB__c ='FIXED INCOME SALES', Division__c='FICC TRADING', Region__c='AMRS' );
            insert nom3;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='GLOBAL FICC', DIVISION_NAME__c = 'FICC TRADING');
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='LOB Admin', Access_Level__c='Edit',Record_Type__c='Title',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c='AMRS',SUB_LOB_Name__c='GLOBAL FICC',  Division_Name__c='FICC TRADING',User_Name__c=usr.Id);
            insert nsr;
            TITLE_Mgt_Controller ns = new TITLE_Mgt_Controller(new ApexPages.StandardController(new Nomination__c()));
            
            Nomination__c searchInfo = new Nomination__c();
            
            TITLE_Mgt_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            
            
            searchInfo.LOB__c='GLOBAL BANKING AND MARKETS';
            searchInfo.SUB_LOB__c ='FIXED INCOME SALES';            
            searchInfo.Region__c ='AMRS';
            searchInfo.Proposed_Title__c='Senior Vice President';
            searchInfo.Band__c ='H3';
            searchInfo.Division__c ='FICC TRADING';
            searchInfo.Nomination_Status__c ='Approved By HR';
            searchInfo.Flagged_for_HR_Review__c ='Yes';
             ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
        }
        
    }
    public testMethod static void testNominationTitlePage(){
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE Title Representative');
        insert role1;
        
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        
        
        //Nomination_Status__c
        
        System.runAs(usr){
            Account ac = new Account();
            //       ac.FirstName='TestAccount';
            //     ac.LastName='TestAccount'; 
            ac.Name='TestAccount';
            insert ac;
            
            Nomination__c nom2 = new Nomination__c(Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted', Proposed_Title__c='Senior Vice President', Band__c='H3', LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c ='FIXED INCOME SALES', Division__c='FICC TRADING', Flagged_for_HR_Review__c='Yes', Region__c='AMRS');
            insert nom2;
            Nomination__c nom3 = new Nomination__c(Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Proposed_Title__c='Senior Vice President', Band__c='H3', LOB__c='GLOBAL BANKING AND MARKETS', SUB_LOB__c ='FIXED INCOME SALES', Division__c='FICC TRADING', Flagged_for_HR_Review__c='Yes', Region__c='AMRS');
            insert nom3;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='GLOBAL FICC', DIVISION_NAME__c = 'FICC TRADING');
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='HR LOB Representative', Access_Level__c='Edit',Record_Type__c='Title',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c ='AMRS',SUB_LOB_Name__c='GLOBAL FICC',  Division_Name__c='FICC TRADING',User_Name__c=usr.Id);
            insert nsr;
            TITLE_Mgt_Controller ns = new TITLE_Mgt_Controller(new ApexPages.StandardController(new Nomination__c()));
            
            Nomination__c searchInfo = new Nomination__c();
            
            TITLE_Mgt_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
            ns.retrieveAllNominationsPDF();
            TITLE_Mgt_Controller.refreshLOBSUBLOBS(true);
            ns.getListDivisionVal();
            
            
            
        }
        
        
        
    }
    public testMethod static void testNominationTitlePage2(){
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE Title Representative');
        insert role1;
        
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        
        
        //Nomination_Status__c
        
        System.runAs(usr){
            Account ac = new Account();
            //       ac.FirstName='TestAccount';
            //     ac.LastName='TestAccount'; 
            ac.Name='TestAccount';
            insert ac;
            
            Nomination__c nom2 = new Nomination__c(Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted', Proposed_Title__c='Senior Vice President', Band__c='H3', LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c ='FIXED INCOME SALES', Division__c='FICC TRADING', Flagged_for_HR_Review__c='Yes', Region__c='AMRS');
            insert nom2;
            Nomination__c nom3 = new Nomination__c(Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Proposed_Title__c='Senior Vice President', Band__c='H3', LOB__c='GLOBAL BANKING AND MARKETS', SUB_LOB__c ='FIXED INCOME SALES', Division__c='FICC TRADING', Flagged_for_HR_Review__c='Yes', Region__c='AMRS');
            insert nom3;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='GLOBAL FICC', DIVISION_NAME__c = 'FICC TRADING');
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='HR LOB Representative', Access_Level__c='Edit',Record_Type__c='Title',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c ='AMRS',SUB_LOB_Name__c='GLOBAL FICC',  Division_Name__c='FICC TRADING',User_Name__c=usr.Id);
            insert nsr;
            TITLE_Mgt_Controller ns = new TITLE_Mgt_Controller(new ApexPages.StandardController(new Nomination__c()));
            
            Nomination__c searchInfo = new Nomination__c();
            
            
            
            TITLE_Mgt_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
            ns.retrieveAllNominationsPDF();
            ns.reset();
            ns.downalodReport();
            ns.approveRecords();
            searchInfo.LOB__c='GLOBAL BANKING AND MARKETS';
            searchInfo.SUB_LOB__c ='FIXED INCOME SALES';            
            searchInfo.Region__c ='AMRS';
            searchInfo.Proposed_Title__c='Senior Vice President';
            searchInfo.Band__c ='H3';
            searchInfo.Division__c ='FICC TRADING';
            searchInfo.Nomination_Status__c ='Approved By HR';
            searchInfo.Flagged_for_HR_Review__c='Yes';
            
            
            
            
        }
        
        
        
    }
    
    
    
    public testMethod static void testNominationTitlePage3(){
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        UserRole role1 = new UserRole(Name = 'NOMSITE Title Representative');
        insert role1;
        
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        String fName='fntest';
        String lName='lntest';
        User usr = new User(  firstname = fName+'1',
                            lastName = lName+'1',
                            email = uniqueName + '@test1' + orgId + '.org',
                            Username = uniqueName + '@test1' + orgId + '.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias = uniqueName.substring(18, 23),
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = pf.Id,
                            UserRoleId = role1.Id);
        
        
        //Nomination_Status__c
        
        System.runAs(usr){
            Account ac = new Account();
            //       ac.FirstName='TestAccount';
            //     ac.LastName='TestAccount'; 
            ac.Name='TestAccount';
            insert ac;
            
            Nomination__c nom2 = new Nomination__c(Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted', Proposed_Title__c='Senior Vice President', Band__c='H3', LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c ='FIXED INCOME SALES', Division__c='FICC TRADING', Flagged_for_HR_Review__c='Yes', Region__c='AMRS');
            insert nom2;
            Nomination__c nom3 = new Nomination__c(Nominee_Lkp__c=ac.Id,Nomination_Status__c='Submitted',Proposed_Title__c='Senior Vice President', Band__c='H3', LOB__c='GLOBAL BANKING AND MARKETS', SUB_LOB__c ='FIXED INCOME SALES', Division__c='FICC TRADING', Flagged_for_HR_Review__c='Yes', Region__c='AMRS');
            insert nom3;
            SUB_LOB_Name__c sub= new SUB_LOB_Name__c(LOB_Name__c='GLOBAL BANKING AND MARKETS', SUB_LOB_Name__c='GLOBAL FICC', DIVISION_NAME__c = 'FICC TRADING');
            insert sub; 
            Nomsite_Security_Roles__c nsr= new Nomsite_Security_Roles__c(Role_type__c='HR LOB Representative', Access_Level__c='Edit',Record_Type__c='Title',LOB_Name__c='GLOBAL BANKING AND MARKETS',Region__c ='AMRS',SUB_LOB_Name__c='GLOBAL FICC',  Division_Name__c='FICC TRADING',User_Name__c=usr.Id);
            insert nsr;
            TITLE_Mgt_Controller ns = new TITLE_Mgt_Controller(new ApexPages.StandardController(new Nomination__c()));
            
            Nomination__c searchInfo = new Nomination__c();
            
            
            TITLE_Mgt_Controller.getAllNominationsInfo(searchInfo);
            ns.searchInput = searchInfo;
            ns.retrieveAllNominations();
            ns.retrieveAllNominationsPDF();
            ns.reformJSONAction();  
            ns.getTitleNomDetails();
            
            
        }
        
        
        
    }
    
}