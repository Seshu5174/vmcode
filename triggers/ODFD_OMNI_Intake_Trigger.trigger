trigger ODFD_OMNI_Intake_Trigger on OMNI_Intake__c (after insert, after update) 
{
	//if(checkRecursive.runOnce())
    {
             if(Trigger.isAfter && Trigger.isInsert)
                { 
                      system.debug('after insert');
                      }
        
        
            if(Trigger.isAfter && Trigger.isUpdate)
                {
                    system.debug('after update');
                    ODFD_OMNI_Intake_Trigger_Helper.onAfterUpdate(Trigger.oldMap, Trigger.newMap);
                    
                }       
    }
    
    
    
}