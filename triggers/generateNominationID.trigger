trigger generateNominationID on Nomination__c (before insert,before update) { 
       Set<String> PersonNumberSet = new Set<String> ();
        Set<String> nomTypeSet= new Set<String> ();
        string sCount;
        string nomCode;
        string nomID;
        integer ARcount;
        public integer total_ExistingNomiCount;
        integer curYear = System.Today().year();
        String sCurYear = String.Valueof(curYear);
        list<AggregateResult> aggrResult= new list<AggregateResult>();                                      

       //   integer recCount = [select count() From Nomination__c where Nomination_Year__c = :sCurYear and Status__c='Submitted'  ];
        aggrResult=[SELECT Count(Id),Nomination_Type__c nm, Person_Number__c pn FROM Nomination__c WHERE (Nomination_Status__c = 'Submitted' AND Nomination_Status__c!=null and (Nomination_Type__c!=null AND Nomination_Type__c!='Team')AND  Nomination_Year__c!=null) GROUP BY Nomination_Type__c,Person_Number__c];
        IF(aggrResult.SIZE()>0){ 
         total_ExistingNomiCount= (Integer) aggrResult[0].get('expr0');
        }
        string recCount =String.valueOf(aggrResult.size());
        if(recCount == '0') sCount ='1'; else sCount =recCount;
      // if(recCount == 0) sCount ='1'; else sCount =String.valueOf(recCount);
        for(Nomination__c  nom:Trigger.new)
        {
          for (AggregateResult ar : aggrResult)  {
            system.debug(ar);
              if(String.ValueOf(ar.get('nm'))==nom.Nomination_Type__c&& (String.ValueOf(ar.get('pn'))==nom.Person_Number__c))
              ARcount =(Integer)ar.get('expr0');
              system.debug('ARcount'+ (Integer)ar.get('expr0'));
              PersonNumberSet.add(nom.Person_Number__c);
              nomTypeSet.add(nom.Nomination_Type__c);
              System.debug('PERSON NUMBER******'+nom.Person_Number__c);
              if (nom.Nomination_type__c  == 'Individual')  nomCode= 'I-';
              if (nom.Nomination_type__c  == 'Manager')  nomCode= 'M-'; 
           }
         }
        //get existing records
        List<Nomination__c > existingNomList = [Select ID,Person_Number__c,Nomination_Number__c,Nomination_Status__c,Nomination_Type__c 
                                               FROM Nomination__c 
                                               WHERE Person_Number__c IN :PersonNumberSet 
                                               AND Nomination_Type__c !='Team'
                                               AND Nomination_Year__c = :sCurYear 
                                               AND Nomination_Status__c='Submitted' 
                                              // ORDER BY Person_Number__c 
                                               ];
         Map<String,string> existingMap=  new  Map<String,string>();                                 
         for(Nomination__c nom: existingNomList)
          {
              String key = nom.Person_Number__c+ nom.Nomination_Type__c ;
              existingMap.put(key,nom.Nomination_Number__c);
          }
         for(Nomination__c nom : Trigger.new)
         {
              String key = nom.Person_Number__c+nom.Nomination_Type__c;
              string existingNom = existingMap.get(key);
              if(existingNom != null)
               {
               String s2= existingNom.Substring(0,existingNom.length()-1);
               integer ARcount1=ARcount+1;
               nom.Nomination_number__c= s2 +ARcount1;
             }
         else {
            if((sCount.length() == 2 )){
            //  nomID = nomCode + '10000' + sCount+'A';  
               nomID = nomCode + '10000' + sCount+'-'+1; 
               nom.Nomination_number__c =nomID;
               nomID = nomCode + '10000' + sCount;  
              }
          }
     }
}