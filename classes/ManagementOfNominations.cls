global  with sharing class ManagementOfNominations{
   public List<Nomination__c> NominationList{set;get;}
   public NominationHistory__c nomhis{set;get;}
    public ManagementOfNominations(ApexPages.StandardController controller) {
       nomhis=new NominationHistory__c();
       NominationList = new List<Nomination__c>();
    }
   public List<Nomination__c> getAllNominations() {
NominationList = [SELECT ID, Nominee_FN__c,Nomination_Number__c, Flagged_for_HR_Review__c,Corporate_Title__c,Proposed_Title__c,Time_in_Current_Corporate_Title__c,  Nomination_Status__c, Approver_Name__c, Rejection_Reason__c, Job_Name__c, Band__c,  LOB_Short_Name__c, SUB_LOB_Short_Name__c, Division__c, Region__c, Most_Recent_YE_Rating_What__c, Most_Recent_YE_Rating_How__c, Assignment_Status__c, Created_By__c from Nomination__c where Recordtype.Name='NOMSITE TITLE'];        
return NominationList; 

}

}