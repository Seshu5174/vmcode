@isTest(SeeAllData=true)

public class CFF_CustomReportTestClass{
    static testMethod void testReport() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='CF Super User']; 
        User u = new User(Alias = 'standt', Email='CFSuperUse@testorg.com',EmailEncodingKey='UTF-8', LastName='TestCFF',LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='CFSuperUser@testorg.com');
        insert u;
        
        System.runAs(u) {
            //Cff custom report landing page
            CFFAllCustomReport allRep = new CFFAllCustomReport();
            allRep.goToReport();
            allRep.generatePDFZip();
            allRep.passValueToCtl();
            
            CFFRespSummaryReport cffRep = new  CFFRespSummaryReport(); 
            cffRep.openRepAsExcel();
            
            Account ac = new Account();
         ac.name='Test Account';
        insert ac;
        Control_Function_Feedback__c cff= NEW Control_Function_Feedback__c();
        cff.LOB__c='GBAM';
        cff.Feedback_Year__c='2018';
        cff.cff_rating__c ='Meets';
        cff.Covered_Employee_Name__c =ac.id;
        cff.Category__c ='CAT 2' ;
         cff.Audit_Type__c = 'Global Compliance';

        cff.Feedback_Status__c='Required';
    
        insert cff;
        Control_Function_Feedback__c cff2= NEW Control_Function_Feedback__c();
        cff2.LOB__c='GBAM';
        cff2.Feedback_Year__c='2018';
        cff2.Covered_Employee_Name__c =ac.id;
        cff2.Feedback_Status__c='Required';
       cff2.Category__c ='CAT 2';
        cff2.cff_rating__c ='Exemplar';
        cff2.Audit_Type__c = 'Global Risk';

        insert cff2;
            
            Control_Function_Feedback__c cff3= NEW Control_Function_Feedback__c();
        cff3.LOB__c='GBAM';
        cff3.Feedback_Year__c='2018';
        cff3.Covered_Employee_Name__c =ac.id;
        cff3.Feedback_Status__c='Required';
           cff3.Category__c ='CAT 2';
        cff3.Audit_Type__c = 'Legal';

        cff3.cff_rating__c ='N/A-Sit in CF';
        insert cff3;
        
         Control_Function_Feedback__c cff4= NEW Control_Function_Feedback__c();
        cff4.LOB__c='GBAM';
        cff4.Feedback_Year__c='2018';
        cff4.Covered_Employee_Name__c =ac.id;
        cff4.Feedback_Status__c='Required';
        cff4.Category__c ='CAT 2';
        cff4.Audit_Type__c = 'Corporate Audit';
        cff4.cff_rating__c ='N/A-No Interaction' ;
        insert cff4;


                               
        Account ac2 = new Account();
     	ac2.name='Test Account';
        insert ac2;
        Control_Function_Feedback__c cf1= NEW Control_Function_Feedback__c();
        cf1.LOB__c='GBAM';
        cf1.Feedback_Year__c='2018';
        cf1.cff_rating__c ='Meets';
        cf1.Covered_Employee_Name__c =ac2.id;
         cf1.Category__c ='CAT 2' ;
        cf1.Feedback_Status__c='Required';
    
        insert cf1;
        Control_Function_Feedback__c cf2= NEW Control_Function_Feedback__c();
        cf2.LOB__c='GBAM';
        cf2.Feedback_Year__c='2018';
        cf2.Covered_Employee_Name__c =ac2.id;
        cf2.Feedback_Status__c='Required';
       cf2.Category__c ='CAT 2';
        cf2.cff_rating__c ='Negative';
        insert cf2;
            
            Control_Function_Feedback__c cf3= NEW Control_Function_Feedback__c();
        cf3.LOB__c='GBAM';
        cf3.Feedback_Year__c='2018';
        cf3.Covered_Employee_Name__c =ac2.id;
        cf3.Feedback_Status__c='Required';
           cf3.Category__c ='CAT 2';

        cf3.cff_rating__c ='N/A-Sit in CF';
        insert cf3;
      
     Account ac3 = new Account();
    ac3.name='Test Account3';
        insert ac3;
        Control_Function_Feedback__c cfna1= NEW Control_Function_Feedback__c();
        cfna1.LOB__c='GBAM';
        cfna1.Feedback_Year__c='2018';
        cfna1.cff_rating__c ='N/A-Sit in CF';
        cfna1.Covered_Employee_Name__c =ac2.id;
         cfna1.Category__c ='CAT 2' ;
        cfna1.Feedback_Status__c='Required';
    
        insert cfna1;
        Control_Function_Feedback__c cfna3= NEW Control_Function_Feedback__c();
        cfna3.LOB__c='GBAM';
        cfna3.Feedback_Year__c='2018';
        cfna3.Covered_Employee_Name__c =ac3.id;
        cfna3.Feedback_Status__c='Required';
           cfna3.Category__c ='CAT 2';

        cfna3.cff_rating__c ='N/A-Sit in CF';
        insert cfna3;
        
         Account ac4 = new Account();
      ac4.name='Test Account4';
        insert ac4;
        Control_Function_Feedback__c cfm1= NEW Control_Function_Feedback__c();
        cfm1.LOB__c='GBAM';
        cfm1.Feedback_Year__c='2018';
        cfm1.cff_rating__c ='Exemplar';
        cfm1.Covered_Employee_Name__c =ac4.id;
         cfm1.Category__c ='CAT 2' ;
        cfm1.Feedback_Status__c='Required';
         insert cfm1;
        Control_Function_Feedback__c cfm2= NEW Control_Function_Feedback__c();
        cfm2.LOB__c='GBAM';
        cfm2.Feedback_Year__c='2018';
        cfm2.cff_rating__c ='Exemplar';
        cfm2.Covered_Employee_Name__c =ac4.id;
         cfm2.Category__c ='CAT 2' ;
        cfm2.Feedback_Status__c='Required';
         insert cfm2;

        CFFResultsReport cffRes2 = new CFFResultsReport();
         cffRes2.openResultExcel();




                     //****Custom Martix report *****************
            PageReference pageRef = Page.CFF_StatusByFunction;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('CFFName', 'CFO Group');
            CFFCustomMatrixReport matrixRep = new  CFFCustomMatrixReport();
            matrixRep.callEnterpriseLevel();
            matrixRep.dispMap();
            matrixRep.openRepExcel();
			matrixRep.openEntRepExcel();
			matrixRep.callEnterpriseLevel();
            
            CFFPdfReport pdRep = new CFFPdfReport();
        }
    }
    static testMethod void testPdfReport() {
        // list<Control_Function_Feedback__c> allRecords = [SELECT Id,Covered_Employee_Name__c, Name, Covered_Employee_Person_Number__c, LNFN__c, LOB__c, Audit_Type__c, CFF_Rating__c,CF_Commentary__c  from Control_Function_Feedback__c where  LOB__c=:vLOB and Feedback_Year__c = '2018'  ORDER BY Covered_Employee_Name__c, Audit_Type__c ASC] ;
        Account ac = new Account();
       ac.name='Test Account';
        insert ac;
        Control_Function_Feedback__c cff= NEW Control_Function_Feedback__c();
        
        cff.LOB__c='GT&O';
        cff.Feedback_Year__c='2018';
        cff.Covered_Employee_Name__c =ac.id;
        insert cff;
        Control_Function_Feedback__c cff2= NEW Control_Function_Feedback__c();
        cff2.LOB__c='GT&O';
        cff2.Feedback_Year__c='2018';
        cff2.Covered_Employee_Name__c =ac.id;
        insert cff2;
        pagereference pf = page.CFFPDFPAGE;
        
        Test.setCurrentPage(pf);
        Apexpages.currentpage().getparameters().put('LOBName','GT_O');
        CFFPdfReport pdRep = new CFFPdfReport();
        pdRep.goBack();
    }
    
}