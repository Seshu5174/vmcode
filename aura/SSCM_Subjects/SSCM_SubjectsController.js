({
	myAction : function(component, event, helper) {
		
	},
    handleCaseData: function(component, event, helper){
        var disableFlg = component.get("v.caseRecFields").Inquirer_is_the_subject__c;
        if(disableFlg)
        {
            component.set("v.disableAddSubj", true);
        }
    },
    
//Add subjects button clicked from Main Page 
searchEmployee:function(component, event, helper) {
       component.set("v.searchStandardID",'');
	component.set("v.searchPersonNumber",'');
component.set("v.searchDoB",null);
component.set("v.searchLastName",'');
component.set("v.searchFirstName",'');
component.set("v.searchPreferredName",'');
component.set("v.searchInquirerPhone",''); 
component.set("v.searchManagerID",''); 
component.set("v.searchManagerPersonNumber",''); 
component.set("v.searchCostCenter",''); 
component.set("v.searchInquirerEmail");
    if(component.find('select')!=undefined)
component.find('select').set('v.value','');
component.set("v.pageResult",[]);
component.set("v.searchResult",[]);
component.set("v.TotalNumberOfRecord", 0);
component.set("v.recordsPerPage", 10);
component.set("v.maxPage", 1);
component.set("v.pageNumber",1)
component.set("v.isSearch",false);
    component.set("v.isAddSubject", !component.get("v.isAddSubject"));
   
},

 closeModel:function(component, event, helper){
  
        component.set("v.isAddSubject", !component.get("v.isAddSubject"));
    },
    
    
Search: function(component, event, helper) {
helper.SearchHelper(component, event, helper);
},
resetSearch : function(component, event, helper) {
helper.resetSearchFilterHelper(component, event, helper);
},
    //From Popup Window
addSubjects : function(component, event, helper) {
	var caseid =component.get("v.recordId");
    var empid = component.get("v.employeeID");
    console.log("caseid");
      
    console.log(caseid);
    var tempIDs=[];
    tempIDs = component.get("v.subjectsInfoAccountIds");
       console.log("After Add subject Button")
    console.log(tempIDs)
    var action = component.get("c.addSubjectEmps");
    action.setParams({'CaseId': component.get("v.recordId"),
                      'EmpId': component.get("v.employeeID"),
                      'EmpIdList': tempIDs
                      });
                      
        
        action.setCallback(this, function(response) {
		var state = response.getState();
       
            if(state=='SUCCESS')
            {
		     
             var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({

			message:'Subject added Successfully',
			duration:' 5000',
			key: 'info_alt',
			type: 'success',
			mode: 'pester'
			});
		toastEvent.fire();
              //  $A.get('e.force.refreshview').fire();
            }
            component.set("v.isAddSubject", !component.get("v.isAddSubject"));
            
            });
        	$A.enqueueAction(action);
    
    
},
    
    getSubjEmpId: function(component, event, helper) {
	var subAccountIds= [];
        var accountId = event.getSource().get("v.name");
       component.set("v.employeeID", accountId);
   
		subAccountIds = component.get("v.subjectsInfoAccountIds");
        if( event.getSource().get("v.checked"))
        {
        	console.log("Checked")
            subAccountIds.push(accountId);
        }
        else
        {
            console.log("Not Checked")
            var index = subAccountIds.indexOf(accountId);
            if(index > -1){
               subAccountIds.splice(index, 1) ;
            }
        }
	component.set("v.subjectsInfoAccountIds",subAccountIds);
        console.log(subAccountIds);
},
    
    refreshSubjects : function(component, event, helper) {
        var caseid =component.get("v.recordId");
        console.log("Subjects:");
        component.set("v.iCount", 0);
         component.set('v.columns', [
             {label: 'Subject Name', fieldName:'linkName' , sortable:'false', type: 'url',typeAttributes: {
                    label: {fieldName: 'Name' },target: '_blank'
                  }},
                {label: 'BAC Person Number', fieldName: 'BAC_Person_Number__c', sortable:'false', type: 'Text'},
             {label: 'BAC Standard ID', fieldName: 'BAC_Standard_ID__c', type: 'Text'}
            
                
            ]);
        var action = component.get("c.getSubjects");
        action.setParams({caseId : caseid});
        action.setCallback(this, function(response) {
		var state = response.getState();
            if(state=='SUCCESS')
            {
          // component.set("v.subjectEmps",response.getReturnValue()); 
           //var subjectList= [];
          // subjectList.push(response.getReturnValue()[0]);
             // component.set("v.iterSubjects",subjectList );  
             var records = response.getReturnValue();
            //component.set("v.data", response.getReturnValue());
                records.forEach(function(record){
                    record.linkName='/'+record.Id;
                });
                component.set("v.data", records);
           component.set("v.iLength1", response.getReturnValue().length) ;
            console.log(response.getReturnValue().length);
            }
            else
            {
                console.log('Error');
            }
		});
        	$A.enqueueAction(action);
    },
    
    doInit : function(component, event, helper) {
        
        
        var caseid =component.get("v.recordId");
        console.log("Subjects:");
        component.set("v.iCount", 0);
         component.set('v.columns', [
             {label: 'Subject Name', fieldName:'linkName' , sortable:false, type: 'url',typeAttributes: {
                    label: {fieldName: 'Name' },target: '_blank'
                  }},
                {label: 'BAC Person Number', fieldName: 'BAC_Person_Number__c', type: 'Text'},
             {label: 'BAC Standard ID', fieldName: 'BAC_Standard_ID__c', type: 'Text'}
            
                
            ]);
        var action = component.get("c.getSubjects");
        action.setParams({caseId : caseid});
        action.setCallback(this, function(response) {
		var state = response.getState();
            if(state=='SUCCESS')
            {
          // component.set("v.subjectEmps",response.getReturnValue()); 
           //var subjectList= [];
          // subjectList.push(response.getReturnValue()[0]);
             // component.set("v.iterSubjects",subjectList );  
             var records = response.getReturnValue();
            //component.set("v.data", response.getReturnValue());
                records.forEach(function(record){
                    record.linkName='/'+record.Id;
                });
                component.set("v.data", records);
           component.set("v.iLength1", response.getReturnValue().length) ;
            console.log(response.getReturnValue().length);
            }
            else
            {
                console.log('Error');
            }
		});
        	$A.enqueueAction(action);
    }
})