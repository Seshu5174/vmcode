public class NomiShareTriggerHelper {
  public static Map<String,ID> userNewMap = new Map<String,ID> ();
  public  static Map<ID,ID> nomMap = new Map<ID,ID> ();
  public static MAP<ID, ID> oldDelMap =  new Map<ID,ID> ();
  public  static MAP<String, String> AccessLevelMap = new MAP<String, String>();
  public  static MAP<String, String> ShareALMap = new MAP<String, String>(); //Used to set Edit /Read
  Public static MAP<ID, String> rowCauseMap = new MAP<ID, String>(); // Used to the row cause for inserts
  public static Map<String,ID>  OldUserMap = new Map<String,ID> ();
  public static MAP<string, string> ExcludeSubLOBMap = new MAP<string, string>();
  public static string shRecordType = null;


  
    public static void SecurityRoleShare( List<Nomsite_Security_Roles__c> newRoles, Map<Id, Nomsite_Security_Roles__c> oldRoles, Boolean isUpdate, Boolean isInsert, Boolean UI, List<ID> fromUI ) 
    {
       
          MAP<String, String> RoleTypeMap = new MAP<String, String>();
                        Set<String> rowCauseSet =  New Set<String>();
        rowCauseSet.add('LOB_Reviewer__c');
        rowCauseSet.add('SUB_LOB_Reviewer__c');
        rowCauseSet.add('Regional_Reviewer__c');
        rowCauseSet.add('Viewer__c');
        List<sObject> NOMshareToInsert = new List<sObject>(); 
         Boolean allRegions = false;
        String OldUserId = null;
         String OldLOB = null;
         String OldSubLOB = null;
         String OldRegion = null;
         String OldAL = null;
         string OldRole = null;
         Boolean OldAllRegions = false;
         Boolean excludeSubLOB = false;
         Boolean isShare = false;
         String NewLOB = null;
         String NewSubLOB = null;
         String NewRegion = null;
        Set <ID> OldUserSet = new Set <ID> ();
         Set<ID> LOBAdminSet = new Set<ID>();
                for (Nomsite_Security_Roles__c secRole:newRoles) {
                if(isUpdate){
                         OldUserId= oldRoles.get(secRole.id).User_Name__c ;
                         OldRole  = oldRoles.get(secRole.id).Role_Type__c;
                         if(OldRole == 'Team Reviewer')
                         {
                         deletFromTeamGroup(OldUserId, 'NOMSITE Team Reviewer');
                         }
                         
                         if(oldRoles.get(secRole.id).LOB_Name__c == null) OldLOB=''; else OldLOB =oldRoles.get(secRole.id).LOB_Name__c ;
                         if(oldRoles.get(secRole.id).SUB_LOB_Name__c == null) OldSubLOB = ''; else OldSubLOB =oldRoles.get(secRole.id).SUB_LOB_Name__c; 
                         if(oldRoles.get(secRole.id).Region__c == null)OldRegion= '' ; else OldRegion = oldRoles.get(secRole.id).Region__c;
                         
                          if(OldRole == 'HR Region Representatives' && OldRegion=='EMEA')
                         {
                         deletFromTeamGroup(OldUserId, 'NOMSITE Title Regional Rep EMEA');
                         }
                         if(OldRole == 'HR Region Representatives' && OldRegion=='APAC')
                         {
                         deletFromTeamGroup(OldUserId, 'NOMSITE Title Regional Rep APAC');
                         }
                        if(OldRole == 'HR Region Representatives' && OldRegion=='AMRS')
                         {
                         deletFromTeamGroup(OldUserId, 'NOMSITE Title Regional Rep AMRS');
                         }


                         system.debug('LG OldSublob:'+OldSubLOB);
                         OldAL = oldRoles.get(secRole.id).Access_Level__c;
                         excludeSubLOB  = oldRoles.get(secRole.id).Exc_Sub_LOB__c;
                         OldAllRegions = oldRoles.get(secRole.id).All_Regions__c;
                        // if(OldRegion  !=secRole.Region__c || OldUserId != secRole.User_Name__c || OldSubLOB != secRole.SUB_LOB_Name__c || OldLOB != secRole.LOB_Name__c || OldAL != secRole.Access_Level__c || excludeSubLOB != secRole.Exc_Sub_LOB__c )
                       // {
                              
                              if(excludeSubLOB == true && oldRoles.get(secRole.id).ALL_LOB__c == false)OldUserMap.put(OldRegion +OldLOB, OldUserId );
                                else if (excludeSubLOB == true && oldRoles.get(secRole.id).ALL_LOB__c == true)OldUserMap.put(OldRegion, OldUserId );

                                else OldUserMap.put(OldRegion +OldLOB+OldSubLOB, OldUserId );

                             System.debug('LG Old User'+ OldUserId);

                            isShare = true;
                        //}
                       }        
                               
                if(isInsert || isShare==true){
                //Get LOB and SUB LOB Names
                //compare with Nomination record LOB and Sub LOB
                shRecordType = secRole.Record_Type__c;
                if(secRole.LOB_Name__c == null) newLOB = ''; else newLOB =secRole.LOB_Name__c ;
                //if(secRole.Select_Sub_LOB__r.SUB_LOB_Name__c == null) newSubLOB = ''; else newSubLOB =secRole.Select_Sub_LOB__r.SUB_LOB_Name__c ;
               if(secRole.Select_Sub_LOB__c != null)
                {   
                List<SUB_LOB_Name__c> subLbName = new List<SUB_LOB_Name__c>([SELECT SUB_LOB_Name__c from SUB_LOB_Name__c where Id=:secRole.Select_Sub_LOB__c LIMIT 1]);
                for(SUB_LOB_Name__c subR:subLbName)
                {
                newSubLOB = subR.SUB_LOB_Name__c;
                }
                }
                else
                {
                newSubLOB = '';
                }
                if(secRole.Region__c == null) newRegion = ''; else newRegion =secRole.Region__c; 
                if(secRole.All_Regions__c ==true) newRegion = '';
                
                system.debug('LG SubLOB:'+newSubLOB);
                if(secRole.Exc_Sub_LOB__c == true && secRole.ALL_LOB__c == false) 
                {
                ExcludeSubLOBMap.put(newRegion+newLOB, newSubLOB);  
                userNewMap.put(newRegion+newLOB, secRole.User_Name__c);
                System.debug('LOB Reviewer:'+ newRegion+newLOB+newSubLOB+'Exclude');
                RoleTypeMap.put(newRegion+newLOB, secRole.Role_Type__c);
                AccessLevelMap.put(newRegion+newLOB, secRole.Access_Level__c);

                }  
                else if(secRole.Exc_Sub_LOB__c == true && secRole.ALL_LOB__c == true) //Regional Admin
                {
                ExcludeSubLOBMap.put(newRegion+newLOB+newSubLOB, newSubLOB);  
                userNewMap.put(newRegion, secRole.User_Name__c);
                System.debug('Sub LOB Eclude, Include all:'+ newRegion+newLOB+newSubLOB+'Exclude');
                RoleTypeMap.put(newRegion, secRole.Role_Type__c);
                AccessLevelMap.put(newRegion, secRole.Access_Level__c);

                }  

               else
               {
               userNewMap.put(newRegion+newLOB+newSubLOB, secRole.User_Name__c);
                System.debug('LOB Reviewer:'+ newRegion+newLOB+newSubLOB);
                RoleTypeMap.put(newRegion+newLOB+newSubLOB, secRole.Role_Type__c);
                AccessLevelMap.put(newRegion+newLOB+newSubLOB, secRole.Access_Level__c);
                 }
                if(secRole.Role_Type__c == 'Team Reviewer')
                {
                    //Add UserID to Team Reviewer Group
                    InsertUserToGroup(secRole.User_Name__c, 'NOMSITE Team Reviewer');
                             }
         
   if(secRole.Role_Type__c == 'HR Region Representatives' && newRegion=='EMEA')
                {
                    //Add UserID to Team Reviewer Group
                    InsertUserToGroup(secRole.User_Name__c, 'NOMSITE Title Regional Rep EMEA');
                             }
         

     if(secRole.Role_Type__c == 'HR Region Representatives' && newRegion=='APAC')
                {
                    //Add UserID to Team Reviewer Group
                    InsertUserToGroup(secRole.User_Name__c, 'NOMSITE Title Regional Rep APAC');
                             }

       if(secRole.Role_Type__c == 'HR Region Representatives' && newRegion=='AMRS')
                {
                    //Add UserID to Team Reviewer Group
                    InsertUserToGroup(secRole.User_Name__c, 'NOMSITE Title Regional Rep AMRS');
                             }

                     
                           
     }//end of for loop
      //Get Nomination Record Type
        String nomRecordTypeId=null;
        RecordType  secType=null;
       if(shRecordType == 'Title')
       {
        secType = [Select Id from RecordType where Name = 'NOMSITE TITLE'];
         if(secType.Id != null)
         {
          nomRecordTypeId = secType.Id ;
          system.debug('Title Id:'+nomRecordTypeId);
         }
       }
       else
       {
           secType = [Select Id from RecordType where Name = 'NOMSITE DI'];
         if(secType.Id != null)
         {
          nomRecordTypeId = secType.Id ;
         }

       }
      
        //Use this code for SUB LOB and LOB-----------Assignment
        String RoleType = null;
         Boolean bExclude = false;
         String  oldUserDelId  = null;
         List<Nomination__c> NominationList; 
         Integer curYear = System.Today().year();
         String scurYear = String.valueof(curYear);        
          if(UI == true){
          //NominationList = new list<Nomination__c>(fromUI);
       NominationList = new List<Nomination__c>([SELECT Id, LOB__c, Sub_LOB__c, Region__c, Nomination_Status__c,Nominee_Lkp__r.LOB_Short_Name__c, Status__c FROM Nomination__c where Id IN: fromUI AND Nomination_Status__c != 'Draft']);
         }
         else{
              NominationList = new  List<Nomination__c>([SELECT Id, LOB__c, Sub_LOB__c, Region__c, Nominee_Lkp__r.LOB_Short_Name__c, Nomination_Status__c, Status__c FROM Nomination__c where Nomination_Status__c != 'Draft' AND nomination_Year__c =:scurYear AND RecordTypeId=:nomRecordTypeId]);
             }
            
              for (Nomination__c nomRec: NominationList)
              {
                 system.debug('Inside Security Loop');
                 system.debug('LG LOB Name'+nomRec.Nominee_Lkp__r.LOB_Short_Name__c);
                   string excSubLob = null;
                 bExclude = false;
                    excSubLob = ExcludeSubLOBMap.get(nomRec.Region__c+nomRec.LOB__c+nomRec.Sub_LOB__c);
                    if(excSubLob != null)
                    {
                       bExclude = true;

                       /*if( excSubLob == nomRec.Sub_LOB__c)
                       {
                           bExclude = true;
                       }*/
                    }
                   if(newRegion == '')
                  {
                      //RoleType =RoleTypeMap.get('All Regions'); 
                     // SetSharingRecords(nomRec,  RoleType , 'All Regions', bExclude);
                     nomRec.Region__c = '';
                  }

                  if(RoleTypeMap.get(nomRec.Region__c+nomRec.LOB__c + nomRec.Sub_LOB__c) != null)
                  {
                      RoleType =RoleTypeMap.get(nomRec.Region__c+nomRec.LOB__c + nomRec.Sub_LOB__c); 
                      SetSharingRecords(nomRec,  RoleType , nomRec.Region__c+nomRec.LOB__c + nomRec.Sub_LOB__c, bExclude);
                     
                  }
                  else if(RoleTypeMap.get(nomRec.Region__c+nomRec.LOB__c ) != null)
                  {
                      RoleType =RoleTypeMap.get(nomRec.Region__c+nomRec.LOB__c);
                      SetSharingRecords(nomRec,  RoleType , nomRec.Region__c+nomRec.LOB__c,bExclude );
                      system.debug('LOB Reviewver1');
                                          }
                  else if(RoleTypeMap.get(nomRec.Region__c) != null)
                  {
                      RoleType =RoleTypeMap.get(nomRec.Region__c);
                      SetSharingRecords(nomRec, RoleType , nomRec.Region__c, bExclude);

                  }
                  if(OldUserMap.get(nomRec.Region__c+nomRec.LOB__c + nomRec.Sub_LOB__c) != null)
                {
                  oldUserDelId  = OldUserMap.get(nomRec.Region__c+nomRec.LOB__c + nomRec.Sub_LOB__c);
                  OldUserSet.add(oldUserDelId);
                   if(oldUserDelId != null )oldDelMap.put(nomRec.Id,  oldUserDelId);

                   
                }
                 else if( OldUserMap.get(nomRec.Region__c+nomRec.LOB__c )  != null)
                 {
                      oldUserDelId = OldUserMap.get(nomRec.Region__c+nomRec.LOB__c );
                         OldUserSet.add(oldUserDelId);
                        if(oldUserDelId != null )oldDelMap.put(nomRec.Id,  oldUserDelId);
                 }               
                   else if( OldUserMap.get(nomRec.Region__c)  != null)
                 {
                      oldUserDelId = OldUserMap.get(nomRec.Region__c);
                         OldUserSet.add(oldUserDelId);
                         if(oldUserDelId != null )oldDelMap.put(nomRec.Id,  oldUserDelId);


                 }
                           
                  if(oldUserDelId != null && bExclude == false )
                  {
                 // oldDelMap.put(nomRec.Id,  oldUserDelId);
                  }
                   if(RoleType == 'LOB Reviewer' || RoleType == 'HR LOB Representative' || RoleType == 'Commitee Member' || RoleType == 'HR LLD Partners')  rowCauseMap.put(nomRec.Id, 'LOB_Reviewer__c');
                    if(RoleType == 'SUB LOB Reviewer' || RoleType=='HR Sub-LOB Representatives')  rowCauseMap.put(nomRec.Id, 'SUB_LOB_Reviewer__c');
                    if(RoleType == 'Regional Reviewer' )  rowCauseMap.put(nomRec.Id, 'Regional_Reviewer__c');
                    if(RoleType == 'LOB/Reg Admin' || RoleType == 'LOB Admin')  
                     {
                     rowCauseMap.put(nomRec.Id, 'LOB_Reg_Admin__c');
                     //   LOBAdminSet.add(nomMap.get(nomRec.Id)); 
                    }
                             
                                  


                 
                
              }
              //bExclude = false;
                           //get existing mappings and delete
                           List<Nomination__share> delExitsingList;
                 if(UI == true){}
                 else
                 {            
                 delExitsingList = new List<Nomination__share> ([SELECT Id from Nomination__Share where parentId IN: oldDelMap.KeySet()AND UserOrGroupId IN: OldUserSet  AND RowCause IN:rowCauseSet]);
               }  
             //Get All Team Nominations
             if(LOBAdminSet.isEmpty() == false)
             {

             Set<Id> teamExcSet = nomMap.keyset();
             List<Nomination__c> TeamList = new List<Nomination__c> ([SELECT Id from Nomination__c where Nomination_Type__c='Team' AND Id NOT IN: teamExcSet ]);
                          List<ID> userIdList = new List<ID>();
             userIdList.addall(LOBAdminSet);
              for(Nomination__c teamRec:  TeamList)
               {
                for(ID sId:userIdList)
                { 
                
                if(sId != null)
                {
                System.debug('LG Test ID:' +sID);
                 Nomination__Share shTeam = new  Nomination__Share();
                shTeam.ParentId =  teamRec.Id;
                shTeam.RowCause = 'LOB_Reg_Admin__c';
                shTeam.AccessLevel = 'Read';
                 shTeam.UserOrGroupId = sId; 
                 NOMshareToInsert.add(shTeam );
                }
                }
               }
               }       
            //iterate Share object
            integer insertCount = 0;
            for (ID parentId:nomMap.keyset() )
            {
                Nomination__Share Reviewer = new  Nomination__Share();
                Reviewer.ParentId =  parentId;
                Reviewer.RowCause = rowCauseMap.get(parentId);
                Reviewer.AccessLevel = ShareALMap.get(parentId);
                Reviewer.UserOrGroupId =nomMap.get(parentId); 
                 NOMshareToInsert.add(Reviewer );
                                  system.debug('LOB Reviewver2');
             }
            if(UI == true){}
          else if(delExitsingList.size() > 0)
         {
             delete delExitsingList;
         }

         if(NOMshareToInsert.size() > 0)
         {
             insert NOMshareToInsert;
         }
       } 
          }    
        public static void SetSharingRecords(Nomination__c nmRec, String  RoleType, String mapStr, Boolean bExclude)
        {
             String NewUserId = null;
             String oldUserId = null;
             NewUserId = userNewMap.get(mapStr);
            system.debug('New User Id'+ NewUserId );
                  if( NewUserId  != null && bExclude == false && nmRec.Nomination_Status__c != 'Draft' && !(RoleType == 'Team Reviewer' ||  RoleType == 'GDIC_Vice_Chair__c' || RoleType == 'GDIO Admin' || RoleType== 'Viewer'  ))
                  {
                     if(! (RoleType == 'LOB Reviewer' || RoleType== 'SUB LOB Reviewer' || RoleType== 'Regional Reviewer' ))
                     {
                      nomMap.put(nmRec.Id, NewUserId);
                      ShareALMap.put(nmRec.Id, AccessLevelMap.get(mapStr));
                      }
                      else if(!(nmRec.Status__c == 'Ineligible' ||  nmRec.Nomination_Status__c == 'Withdrawn'))
                      {
                           nomMap.put(nmRec.Id, NewUserId);
                          ShareALMap.put(nmRec.Id, AccessLevelMap.get(mapStr));

                      }
                  }
         }

      @Future  
    public static void InsertUserToGroup(String UserId, String GroupName) 
    {
      List<GroupMember> GMlist = new List<GroupMember>();
        System.debug('Team Reviewver');
                 Group teamGroup = new Group();
                 if([select count() from Group where Name =: GroupName] > 0)
                  teamGroup= [select id from Group where Name =: GroupName]; 
                 if(teamGroup != null)
                 {
                 GroupMember gm = new GroupMember();
                gm.GroupId = teamGroup.Id;
                gm.UserOrGroupId = UserId;
             gmList.add(gm);

        if(gmList.size() > 0){
            insert gmList;
        }

    }    
    } 
  
    public static void deleteSharing(List<Nomsite_Security_Roles__c> delRoles)
    {            
    List<Nomination__c> delList = new List<Nomination__c>();
    Set<String> UserIdSet=new Set<String>();
    for (Nomsite_Security_Roles__c delRole:delRoles) 
                 {

                 if(delRole.Role_Type__c == 'Team Reviewer')
                 {
                   deletFromTeamGroup(delRole.User_Name__c,'NOMSITE Team Reviewer');           
                              
                  }
                  if(delRole.Role_Type__c == 'HR Region Representatives' && delRole.Region__c=='EMEA')
                 {
                   deletFromTeamGroup(delRole.User_Name__c,'NOMSITE Title Regional Rep EMEA');  
                   
                              
                  }
                  if(delRole.Role_Type__c == 'HR Region Representatives' && delRole.Region__c=='APAC')
                 {
                   
                   deletFromTeamGroup(delRole.User_Name__c,'NOMSITE Title Regional Rep APAC'); 
                    
                              
                  }
                   if(delRole.Role_Type__c == 'HR Region Representatives' && delRole.Region__c=='AMRS')
                 {
                   
                   deletFromTeamGroup(delRole.User_Name__c,'NOMSITE Title Regional Rep AMRS'); 
                    
                              
                  }



                  if(delRole.Role_Type__c == 'LOB/Reg Admin' || delRole.Role_Type__c == 'LOB Admin')
                  {
                      UserIdSet.add(delRole.User_Name__c);
                  delList = [SELECT Id from Nomination__c  where LOB__c=:delRole.LOB_Name__c];

                  }
                  if(delRole.Role_Type__c == 'LOB Reviewer' ||  delRole.Role_Type__c == 'HR LOB Representative' || delRole.Role_Type__c== 'Commitee Member' || delRole.Role_Type__c== 'HR LLD Partners')
                  {
                   UserIdSet.add(delRole.User_Name__c);
                  delList = [SELECT Id from Nomination__c  where LOB__c=:delRole.LOB_Name__c];
                  }
                  if(delRole.Role_Type__c == 'SUB LOB Reviewer' || delRole.Role_Type__c=='HR Sub-LOB Representatives')
                  {
                   UserIdSet.add(delRole.User_Name__c);
                  delList = [SELECT Id from Nomination__c  where LOB__c=:delRole.LOB_Name__c AND SUB_LOB__c =: delRole.SUB_LOB_Name__c];

                  }
                  if(delRole.Role_Type__c== 'Regional Reviewer' )
                  {
                  UserIdSet.add(delRole.User_Name__c);
                  delList = [SELECT Id from Nomination__c  where Region__c=:delRole.Region__c];
                  }
                  }
       List<Nomination__share> delShare= [SELECT Id from Nomination__share where ParentId IN:delList and UserOrGroupId=:UserIdSet];
          if(delShare.size() > 0)
          {
              delete delShare;
          }  

    }
   @Future
    public static void deletFromTeamGroup(String UserId, String GroupName)
    {
    
       List<GroupMember> GMlist = new List<GroupMember>();
        System.debug('Team Reviewver');
         Group teamGroup = new Group();
       if([select count() from Group where Name =: GroupName] > 0)
        teamGroup= [select id from Group where Name =:GroupName]; 
        if(teamGroup != null){
        GroupMember gm = [SELECT Id from GroupMember where GroupId =: teamGroup.Id AND UserOrGroupId =: UserId];
                                    gmList.add(gm);
        }
         if(gmList.size() > 0){
           delete gmList;
        }

    }
}