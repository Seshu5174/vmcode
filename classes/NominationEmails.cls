public class NominationEmails{

//@InvocableMethod(label='Send Email To Nominee' description='Send  emails to nominee after Open period closes' )
public static string SendDIEmails(List<id> nomId)
{
Set<id> nomIdset = new Set<Id>(nomId);
String nomineeEmail  = 'GHR.MIS1@bankofamerica2.com';
String managerEmail = 'GHR.MIS1@bankofamerica2.com';
Id orgWidAddrId;
Messaging.SingleEmailMessage[] eMails = new List<Messaging.SingleEmailMessage>();
String[] toAddress = new list<String>();
String[] ccAddress = new list<string>();
//Get Organization Wide Email Address id
for(OrgWideEmailAddress owa : [select id, Address, DisplayName from OrgWideEmailAddress]) {
if(owa.DisplayName.contains('Global Diversity'))

 orgWidAddrId = owa.Id;
  } 

//Get Nomination Records
List<Nomination__c> nomList = new List<Nomination__c> ([SELECT Id, Nomination_Year__c, Created_By__c, Nomination_Type__c,HR_Full_Name__c,Manager_E_mail__c,Nominee_E_mail__c,Nominator_E_mail__c,Team_Nomination__r.Team_Member_Names__c,Team_Nomination__r.Team_Member_Emails__c,DI_Priority__c,Accomplishments__c,Role_of_Nominee_in_Effort__c,Impacted_Audience__c,
Impacted_External_Organizations__c,Employee_Network_Involvement__c,Team_Nomination__r.DI_Priority__c,Team_Nomination__r.Accomplishments__c,Team_Nomination__r.Role_of_Nominee_in_Effort__c,Team_Nomination__r.Impacted_Audience__c,Team_Nomination__r.Impacted_External_Organizations__c,Team_Nomination__r.Employee_Network_Involvement__c,
Inclusive_Leader_Attributes__c,Transparency__c,Trust__c,Investment__c,Purpose__c from Nomination__c where Id IN:nomId AND  (Nomination_Type__c = 'Individual' OR Nomination_Type__c = 'Manager' OR(Nomination_Type__c = 'Team' AND Team_Nomination__c != null) )]);
//String[] toAddress = new String[] {NomineeEmail};


Messaging.SingleEmailMessage mail = null;
String attachmentStr = '';
String htmlBody = '';
String attHtmlStr = '';
 for(Nomination__c nomRec: nomList)
 {
mail = new Messaging.SingleEmailMessage();
toAddress.add(nomRec.Nominee_E_mail__c);
ccAddress.add(nomRec.Manager_E_mail__c);

mail.setToAddresses(toAddress);
mail.setCcAddresses (ccAddress);
mail.setOrgWideEmailAddressId(orgWidAddrId);
mail.setSubject('Global Diversity & Inclusion Award – You’ve been nominated!');
integer nextYear = Integer.valueof(NomRec.Nomination_Year__c) + 1;
if(nomRec.Nomination_Type__c == 'Manager' || nomRec.Nomination_Type__c == 'Individual')
{

htmlBody = '<html><body>'+ '<p>'+'You have been nominated for a '+nomRec.Nomination_Year__c+' Global Diversity & Inclusion Award by '+nomRec.Created_By__c+' in recognition of your commitment to creating a diverse and inclusive environment. A summary of your nomination is provided below. Your full nomination can be reviewed '
+'<a href="https://nominations.bankofamerica.com/">here</a>'+' until 08/15/'+nextYear+'</p>'
+'</br>'+'<p>'+'<b>Nomination Information:</b>'+'</br>'
+'Nomination type: '+nomRec.Nomination_Type__c+'</br>'
+'Nominee name(s): '+nomRec.HR_Full_Name__c+'</br>'
+'Nominee E-mail(s): '+nomRec.Nominee_E_mail__c +'</br>'+'</br>'
+'Thank you,'+'</br>'+'The Global Diversity & Inclusion Office'+'</p>'+'</body></html>';

mail.setPlainTextBody('You have been nominated for a '+nomRec.Nomination_Year__c+' Global Diversity & Inclusion Award by '+  nomRec.Created_By__c +' in recognition of your commitment to creating a diverse and inclusive environment. A summary of your nomination is provided below. Your full nomination can be reviewed here until 08/15/'+nextYear+'\n'
+'\n'+'Nomination Information:'+'\n'
+'Nomination type: '+nomRec.Nomination_Type__c+'\n'
+'Nominee name(s): '+nomRec.HR_Full_Name__c+'\n'
+'Nominee E-mail(s): '+nomRec.Nominee_E_mail__c +'\n'+'\n'
+'Thank you,'+'\n'+'The Global Diversity & Inclusion Office');
}
else if(nomRec.Nomination_Type__c == 'Team')
{
htmlBody = '<html><body>'+ '<p>'+'You have been nominated for a '+nomRec.Nomination_Year__c+' Global Diversity & Inclusion Award by '+nomRec.Created_By__c+' in recognition of your commitment to creating a diverse and inclusive environment. A summary of your nomination is provided below. Your full nomination can be reviewed '
+'<a href="https://nominations.bankofamerica.com/">here</a>'+' until 08/15/'+nextYear+'</p>'
+'</br>'+'<p>'+'<b>'+'Nomination Information:'+'</b>'+'</br>'
+'Nomination type: '+nomRec.Nomination_Type__c+'</br>'
+'Nominee name(s): '+nomRec.Team_Nomination__r.Team_Member_Names__c+'</br>'
+'Nominee E-mail(s): '+nomRec.Team_Nomination__r.Team_Member_Emails__c +'</br>'+'</br>'
+'Thank you,'+'</br>'+'The Global Diversity & Inclusion Office'+'</p>'+'</body></html>';

mail.setPlainTextBody('You have been nominated for a '+nomRec.Nomination_Year__c+' Global Diversity & Inclusion Award by '+  nomRec.Created_By__c +' in recognition of your commitment to creating a diverse and inclusive environment. A summary of your nomination is provided below. Your full nomination can be reviewed'+'\n'
+'\n'+'Nomination Information:'+'\n'
+'Nomination type: '+nomRec.Nomination_Type__c+'\n'
+'Nominee name(s): '+nomRec.Team_Nomination__r.Team_Member_Names__c+'\n'
+'Nominee E-mail(s): '+nomRec.Team_Nomination__r.Team_Member_Emails__c+'\n'+'\n'
+'Thank you,'+'\n'+'The Global Diversity & Inclusion Office');

}

mail.setHTMLBody(htmlBody);
/*mail.setHTMLBody('<p>'+'You have been nominated for a '+nomRec.Nomination_Year__c+' Global Diversity & Inclusion Award by '+nomRec.Created_By__c+' in recognition of your commitment to creating a diverse and inclusive environment. A summary of your nomination is provided below. Your full nomination can be reviewed'+'</p>'
+'</br>'+'<p>'+'Nomination Information:'+'</br>'
+'Nomination type:'+nomRec.Nomination_Type__c+'</br>'
+'Nominee name(s):'+nomRec.HR_Full_Name__c+'</br>'
+'Nominee E-mail(s):'+nomRec.Nominee_E_mail__c +'</br>'+'</br>'
+'Thank you,'+'</br>'+'The Global Diversity & Inclusion Office'+'</p>');*/

/***********Text file********************/
attachmentStr = 'Diversity and Inclusion Priority:'+nomRec.DI_Priority__c+'\n'
+'Accomplishments:'+nomRec.Accomplishments__c+'\n'+
+'Role of Nominee in Effort:'+nomRec.Role_of_Nominee_in_Effort__c+'\n'
+'Impacted Audience:'+nomRec.Impacted_Audience__c+'\n'
+'Impacted External Organizations:'+nomRec.Impacted_External_Organizations__c;

/****************Html File*****************/
if(nomRec.Nomination_Type__c == 'Individual')
{
attHtmlStr = '<!DOCTYPE html><html><body>'+
'<h2 align="left" color="#e51837">Nomination Information</h2><br></br>'+
'<div><p style="font-size:10" >Nominated for: '+nomRec.Nomination_Year__c+' Global Diversity & Inclusion Award '+'<br></br>'+
'Nominated by: '+nomRec.Created_By__c+'<br></br>'+
'Nomination type: '+nomRec.Nomination_Type__c+'<br></br>'+
'Nominee name(s): '+nomRec.HR_Full_Name__c+'<br></br>'+
'Nominee E-mail(s): '+nomRec.Nominee_E_mail__c +'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Diversity and Inclusion Priority:</b></p>'+'<p style="font-size:10" >'+nomRec.DI_Priority__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Accomplishments:</b></p>'+'<p style="font-size:10" >'+nomRec.Accomplishments__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Role of Nominee in Effort:</b></p>'+'<p style="font-size:10" >'+nomRec.Role_of_Nominee_in_Effort__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Impacted Audience:</b></p>'+'<p style="font-size:10" >'+nomRec.Impacted_Audience__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Impacted External Organizations:</b></p>'+'<p style="font-size:10" >'+nomRec.Impacted_External_Organizations__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Employee Network Involvement:</b></p>'+'<p style="font-size:10" >'+nomRec.Employee_Network_Involvement__c+'</p></div>'
+'</body></html>';
}
else if(nomRec.Nomination_Type__c == 'Manager')
{
attHtmlStr = '<!DOCTYPE html><html><body>'+
'<h2 align="left" color="#e51837">Nomination Information</h2><br></br>'+
'<div><p style="font-size:10" >Nominated for: '+nomRec.Nomination_Year__c+' Global Diversity & Inclusion Award '+'<br></br>'+
'Nominated by: '+nomRec.Created_By__c+'<br></br>'+
'Nomination type: '+nomRec.Nomination_Type__c+'<br></br>'+
'Nominee name(s): '+nomRec.HR_Full_Name__c+'<br></br>'+
'Nominee E-mail(s): '+nomRec.Nominee_E_mail__c +'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Inclusive Leader Attributes:</b></p>'+'<p style="font-size:10" >'+nomRec.Inclusive_Leader_Attributes__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Demonstrated Transparency:</b></p>'+'<p style="font-size:10" >'+nomRec.Transparency__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Trust in his/her teams:</b></p>'+'<p style="font-size:10" >'+nomRec.Trust__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>How has the manager invested in his/her team?:</b></p>'+'<p style="font-size:10" >'+nomRec.Investment__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>How has the manager established a sense of purpose for his/her team?:</b></p>'+'<p style="font-size:10" >'+nomRec.Purpose__c+'</p></div>'
+'</body></html>';
}
else if(nomRec.Nomination_Type__c == 'Team')
{
    attHtmlStr = '<!DOCTYPE html><html><body>'+
'<h2 align="left" color="#e51837">Nomination Information</h2><br></br>'+
'<div><p style="font-size:10" >Nominated for: '+nomRec.Nomination_Year__c+' Global Diversity & Inclusion Award '+'<br></br>'+
'Nominated by: '+nomRec.Created_By__c+'<br></br>'+
'Nomination type: '+nomRec.Nomination_Type__c+'<br></br>'+
'Nominee name(s): '+nomRec.Team_Nomination__r.Team_Member_Names__c+'<br></br>'+
'Nominee E-mail(s): '+nomRec.Team_Nomination__r.Team_Member_Emails__c +'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Diversity and Inclusion Priority:</b></p>'+'<p style="font-size:10" >'+nomRec.Team_Nomination__r.DI_Priority__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Accomplishments:</b></p>'+'<p style="font-size:10" >'+nomRec.Team_Nomination__r.Accomplishments__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Role of Nominee in Effort:</b></p>'+'<p style="font-size:10" >'+nomRec.Team_Nomination__r.Role_of_Nominee_in_Effort__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Impacted Audience:</b></p>'+'<p style="font-size:10" >'+nomRec.Team_Nomination__r.Impacted_Audience__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Impacted External Organizations:</b></p>'+'<p style="font-size:10" >'+nomRec.Team_Nomination__r.Impacted_External_Organizations__c+'</p></div><br></br>'+
'<div><p style="font-size:12" color="#009ACD"><b>Employee Network Involvement:</b></p>'+'<p style="font-size:10" >'+nomRec.Team_Nomination__r.Employee_Network_Involvement__c+'</p></div>'
+'</body></html>';
}
Messaging.Emailfileattachment eFileAtt = new Messaging.Emailfileattachment();
eFileAtt.setFileName('FullNominationDetails.pdf');
//eFileAtt.setBody(Blob.valueOf(attHtmlStr));
eFileAtt.setBody(Blob.toPDF(attHtmlStr));

mail.setFileAttachments(new Messaging.EmailFileAttachment[] {eFileAtt});

eMails.add(mail);
ccAddress.remove(0);
toAddress.remove(0);
}
Messaging.SendEmailResult [] results = Messaging.sendEmail(eMails);
String errorMsg = 'Email sent Successfully';
/*if (results[0]== 'success') { 
System.debug('The email was sent successfully.'); 
errorMsg = 'Email sent Successfully';
    // Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}
else errorMsg = 'Error in sending Email  Successfully';*/

return errorMsg;
}
}