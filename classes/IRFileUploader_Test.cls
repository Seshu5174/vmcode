@isTest
public class IRFileUploader_Test {
    public static testMethod  void  IRFileUploader(){
            
                Incentive_Rationale__c ir =new  Incentive_Rationale__c();
        ir.IR_Year__c = '2017';
        ir.LOB__c = 'GBAM';
        insert ir;
        Incentive_Rationale__c ir2 =new  Incentive_Rationale__c();
        ir2.IR_Year__c = '2017';
        ir2.LOB__c = 'GBAM';
        insert ir2;
         ApexPages.StandardController sc = new ApexPages.StandardController(ir);
       
       IRFileUploader irfl = new IRFileUploader(sc);
        irfl.contentFile=blob.valueOf(ir.id+',2,3,"new","new1","new1"\n'+ir2.id+',2,3,"new","new1","new1"');
        
        irfl.irReadFile();
    }
}