global class EmptyRecycleBin implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator('Select Id from Account');
    }
    
    global void execute(Database.BatchableContext BC, List<Account> Scope){
        delete scope;
    }
	
    global void finish(Database.BatchableContext BC){
        
    }
}