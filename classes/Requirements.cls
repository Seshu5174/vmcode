Public Class Requirements{

/*

CFF
•   June 30 - Rollover to 2018 and 2017 moved to Historical
o   Sentence Change for Historical View (Slide 1)
•   July 31 - Load CEE and Set Up Providers
•   July 31 Enhancement
o   Developmental Commentary Box (Slide 2)
•   August 31st – Cleanup
o   Special Character Upload Issue
Note: The Save as Draft Enhancement is no longer requested as a process workaround will prove more effective

IR – End of October
•   Clean up
o   Special Characters
o   Users to be able to move between CFF and IR Tool
•   Enhancements
o   Commentary box grayed out if not applicable
   Note: Logic Changes on this request from last conversation (Slide 3)
o   Expand size of commentary boxes (Slide 4)
o   Add Outcome impact to export of standard view (Slide 5)
o   Standard sentence – if YOY is zero the read ‘Overall Total Incentive is flat YOY’ vs ‘Overall Total Incentive is 0% YOY’ (Slide 6)
o   Super User ability to Upload ratings and YOY




Goal: To notify the providers of Incentive, Rating and Performance Commentary which commentary they need to provide
Commentary Requirements
Incentive Pay Rationale is required when: 
CEE Incentive Req Fields is: Y-Outlier, Y-Non Outlier or Y-Negative OR Conduct Outcome Impact Field is “Perf Comment, Rating Constraint and Comp Rationale”
Rating Rationale is Required when:
    Behavior Rating is E AND Conduct Outcome Impact is “Perf Commentary and Rating Rationale” 
Performance Commentary is required when: 
Conduct Outcome Impact Field is “Perf Commentary”, “Perf Commentary and Rating Rationale”, “Perf Commentary and Rating Constraint”, or “Perf Comment Rating Constraint and Comp Rationale”
Potential Solution:
Box will only appear when one or more requirements are satisfied – BEST Solution




Goal: Provide a larger viewing area for providers to see more of their comment for easier editing and review
Potential Solutions: 
1. Expand size of commentary boxes so more of the review can be seen without having to scroll
2. A pop-up for users to review the commentary – button on the side of the comment box with verbiage instructing the user to enter the details; save/cancel buttons; when saved, info will display in the main box




IR – Reflect 0% as Flat in 1st sentence
Goal: To give providers the standard verbiage when referring to Flat Changes YOY (0%)
Potential Solution: 
1. Change 1st sentence to reflect “Flat” in instances where Incentive, TC or Pool Funding is 0%




*/

}