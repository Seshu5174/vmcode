public class OpportunitiesPaginationController2{
    Public Integer size{get;set;} 
    Public Integer currentPageNumber{get;set;} 
    Public Integer noOfRecords{get; set;} 
    public List<SelectOption> paginationSizeOptions{get;set;}
    
     
    
    public List<Account> acclistInfo{get;set;}
    private integer totalRecs = 0;
    private integer OffsetSize = 0;
    private integer LimitSize= 10;
    public OpportunitiesPaginationController2(){
        Map<Integer,set<Id>> pageNumberidsList = new Map<Integer,set<Id>>();
        Map<Integer,List<Account>> pageNumberAccountsList  = new Map<Integer,List<Account>> ();
        List<Id> idsList = new  List<Id> ();
        size=10;
        pageNumberidsList  = new Map<Integer,set<Id>>();
     pageNumberAccountsList  = new Map<Integer,List<Account>>();
         totalRecs = [select count() from account];
        idsList = new List<Id>();
        Map<Id,Account> accountsMap = new Map<Id,Account>([SELECT Name, website, AnnualRevenue, description, Type FROM account  order by Name LIMIT :LimitSize OFFSET :OffsetSize]);
        
        idsList.addall(accountsMap.keyset());
                acclistInfo = accountsMap.values();
Cache.OrgPartition orgPart = Cache.Org.getPartition('local.TestPagination');
// Add cache value to the partition. Usually, the value is obtained from a 
// callout, but hardcoding it in this example for simplicity.
orgPart.put('idsList', idsList);
        pageNumberAccountsList.put(1,acclistInfo);
        orgPart.put('pageNumberAccountsList', pageNumberAccountsList);
        pageNumberidsList.put(1,accountsMap.keyset());
        orgPart.put('pageNumberidsList', pageNumberidsList);
        currentPageNumber = 1;
         paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
    }
    
    /*public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {                
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [select id,Name From Account limit 100 offset 100 ]));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
            }            
            return setCon;
        }
        set;
    }
    
    //Changes the size of pagination
    public PageReference refreshPageSize() {
        setCon.setPageSize(size);
        return null;
    }
    
    // Initialize setCon and return a list of record    
    
    public List<Account> getOpportunities() {
        return (List<Account>) setCon.getRecords();
    }
    */
    public List<account> getacclist()
    {
        List<account> acc = Database.Query('SELECT Name, website, AnnualRevenue, description, Type FROM account LIMIT :LimitSize OFFSET :OffsetSize');
        System.debug('Values are ' + acc);
        return acc;
    }
    public void FirstPage()
    {
        OffsetSize = 0;
    }
    public void previous()
    {
        OffsetSize = OffsetSize-LimitSize;
    }
    public void next()
    {
        OffsetSize = OffsetSize+LimitSize;
    }
    public void LastPage()
    {
        OffsetSize = totalrecs-math.mod(totalRecs,LimitSize);
    }
    public boolean getprev()
    {
        if(OffsetSize == 0)
            return true;
        else
            return false;
    }
    public boolean getnxt()
    {
        if((OffsetSize + LimitSize) > totalRecs)
            return true;
        else
            return false;
    }
    public void nextRecords(){
        
        Map<Integer,set<Id>> pageNumberidsList = new Map<Integer,set<Id>>();
        Map<Integer,List<Account>> pageNumberAccountsList  = new Map<Integer,List<Account>> ();
        List<Id> idsList = new  List<Id> ();
        Cache.OrgPartition orgPart = Cache.Org.getPartition('local.TestPagination');
         idsList  = (List<Id>)orgPart.get('idsList');
		system.debug('idsList==>'+idsList.size());
        
        pageNumberidsList = (Map<Integer,set<Id>> )orgPart.get('pageNumberidsList');
        pageNumberAccountsList =(Map<Integer,List<Account>>) orgPart.get('pageNumberAccountsList');
        system.debug('orgPart.getpageNumberidsList'+orgPart.get('pageNumberidsList'));
        try{
        currentPageNumber++;
        if(pageNumberidsList.containsKey(currentPageNumber)){
          acclistInfo =  pageNumberAccountsList.get(currentPageNumber);
        }
        else{
            List<String> idsInfo = new List<String>();
       //     for(set<String> s:pageNumberidsList.values())
       //     {
           // idsInfo.addall(s);
            //}
        
		 
         
        //idsList = new List<Id>();
           Map<Id,Account> accountsMapInfo = new Map<Id,Account>([SELECT Name, website, AnnualRevenue, description, Type FROM account where id not in:idsList order by Name limit 10]);
        
        idsList.addall(accountsMapInfo.keyset());
                acclistInfo = accountsMapInfo.values(); 
// Add cache value to the partition. Usually, the value is obtained from a 
// callout, but hardcoding it in this example for simplicity.
system.debug('ids list:: latest::idsList ll'+idsList.size());
Cache.OrgPartition orgPart2 = Cache.Org.getPartition('local.TestPagination');
            orgPart2.put('idsList', idsList);
            system.debug('ids list:: latest::'+((List<Id>)orgPart2.get('idsList')).size());
        pageNumberAccountsList.put(1,acclistInfo);
        orgPart2.put('pageNumberAccountsList', pageNumberAccountsList);
        pageNumberidsList.put(1,accountsMapInfo.keyset());
        orgPart2.put('pageNumberidsList', pageNumberidsList);

            
        }
        }
        Catch(Exception ex){
            system.debug('ex line'+ex.getStackTraceString());
            apexpages.addMessages(ex);
        }
    }
    public void prevRecords(){
      Map<Integer,set<Id>> pageNumberidsList = new Map<Integer,set<Id>>();
        Map<Integer,List<Account>> pageNumberAccountsList  = new Map<Integer,List<Account>> ();
        List<Id> idsList = new  List<Id> ();
        Cache.OrgPartition orgPart = Cache.Org.getPartition('local.TestPagination');
         idsList  = (List<Id>)orgPart.get('idsList');
		
        pageNumberAccountsList =(Map<Integer,List<Account>>) orgPart.get('pageNumberAccountsList');
        system.debug('pageNumberAccountsList size:'+pageNumberAccountsList.size());
        system.debug('pageNumberAccountsList size:'+pageNumberAccountsList.keyset());
        pageNumberidsList = (Map<Integer,set<Id>> )orgPart.get('pageNumberidsList');
        
        currentPageNumber--;
           
        
        if(pageNumberidsList.containsKey(currentPageNumber)){
          acclistInfo =  pageNumberAccountsList.get(currentPageNumber);
        }
       
    }
}