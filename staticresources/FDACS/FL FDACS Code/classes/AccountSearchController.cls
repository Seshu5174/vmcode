// Copyright 2016, Sales Engineering, Salesforce.com Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// - Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// - Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// - Neither the name of the salesforce.com nor the names of its contributors
//   may be used to endorse or promote products derived from this software
//   without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

public without sharing class AccountSearchController {
    @AuraEnabled
    public static AccountSearchParams initSearchParams() {
        return new AccountSearchParams();
    }

    @AuraEnabled
    public static List<String> getCities() {
        Set<String> citySet = new Set<String>();
        citySet.add('');
        for (Account a : [SELECT Id, Name, BillingCity FROM Account WHERE BillingState = 'FL']) {
            citySet.add(a.BillingCity);
        }

        List<String> l = new List<String>();
        l.addAll(citySet);
        l.sort();

        return l;
    }

    @AuraEnabled
    public static List<String> getSicDesc() {
        Set<String> vSet = new Set<String>();
        vSet.add('');
        for (Account a : [SELECT SicDesc FROM Account WHERE BillingState = 'FL']) {
            vSet.add(a.SicDesc);
        }

        List<String> l = new List<String>();
        l.addAll(vSet);
        l.sort();

        return l;
    }

    @AuraEnabled
    public static List<Account> getAccounts(String searchParams) {


        AccountSearchParams sParams = null;
        if (searchParams != null) {
            sParams = (AccountSearchParams)JSON.deserialize(searchParams, AccountSearchParams.class);
        }

        String queryStr = 'SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingLatitude, BillingLongitude, Phone, Website\n' +
                          '  FROM Account \n' +
                          ' WHERE BillingState = \'FL\'';
        if (sParams != null) {
            if (sParams.name != null && sParams.name.length() > 0) {
                queryStr += ' AND Name LIKE \'%' + sParams.name + '%\'';
            }

            if (sParams.city != null && sParams.city.length() > 0) {
                queryStr += ' AND BillingCity LIKE \'%' + sParams.city + '%\'';
            }

            if (sParams.sicDesc != null && sParams.sicDesc.length() > 0) {
                queryStr += ' AND sicDesc = \'' + sParams.sicDesc + '\'';
            }

        }

        queryStr += ' ORDER BY Name ASC';

        System.debug('QUERY > ' + queryStr);
        return Database.query(queryStr);
    }
}