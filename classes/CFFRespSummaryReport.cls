public with sharing class CFFRespSummaryReport{

Public Map<String, Integer> mLOBByBand{get;set;}
Public Map<String, Integer> hTotMap{get;set;}
Public Map<String, Integer> vTotMap{get;set;}
Public Map<String, Integer> nonReqMap{get;set;}
Public Map<String, Integer> noRespMap{get;set;}


Public List<String> lstLOB{get;set;}
Public List<String> lstBands{get;set;}

Public Integer[] vTotal{get;set;} 
Public Integer allTotal{get;set;}
Public Integer totalNonReq{get;set;}
Public Integer totalNoResp{get;set;}

public String previousYear;
public String currentYear;

//Declare lists -----

public CFFRespSummaryReport()
{
lstLOB = new list<String>();

lstLOB.add('GBAM');
lstLOB.add('GWIM');
lstLOB.add('Consumer');
lstLOB.add('GT&O');
lstLOB.add('CAO');
//lstLOB.add('CEO');
lstLOB.add('CFO');
lstLOB.add('Legal');
lstLOB.add('Compliance');
lstLOB.add('Risk');
lstLOB.add('GMCA');
lstLOB.add('GHR');

String varLOB;
Integer countMeets;

CFF_Setting__mdt[] cff_settings = [SELECT Feedaback_Year__c, DeveloperName FROM CFF_Setting__mdt];
map < String, String > cff_map = new map < String,  String > ();
for (CFF_Setting__mdt cff_data: cff_settings) {
    cff_map.put(cff_data.DeveloperName, cff_data.Feedaback_Year__c);
}

currentYear = cff_map.get('Current_Year'); //'2018'; //String.valueOf(Date.Today().Year());
previousYear = cff_map.get('Previous_Year'); //String.valueOf(Date.Today().Year()-1);


mLOBByBand = new Map<String, Integer>();
vTotMap =  new Map<String, Integer>();
hTotMap = new Map<String, Integer> ();
//Add Array to save Sum of bands
Integer[] hCount =new Integer[]{0,0,0,0,0,0,0};
vTotal= new Integer[]{0,0,0,0,0,0,0};
List<AggregateResult> allRecs;
Integer  cResult;

//Records for CFF_Status is not completed
noRespMap = new Map<String, Integer>();
Integer cRatings;
varLOB ='';
countMeets=0;
Map<Id, Sobject> otherResult= new Map<Id, Sobject>([select   Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' AND (CFF_RATING__c ='Negative' OR  CFF_RATING__c = 'Meets' OR  CFF_RATING__c = 'Exemplar')GROUP BY  Covered_Employee_Name__c ]);
    Set<Id> otherSet=new Set<id> (otherResult.Keyset());
//List<AggregateResult> noResp = [select  LOB__c, count(Id) cRate from Control_Function_Feedback__c   where  Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c = 'Required' AND CFF_Rating__c='No Rating' GROUP BY LOB__c  ];
List<AggregateResult> noResp = [SELECT LOB__c,  Covered_Employee_Name__c FROM Control_Function_Feedback__c where Feedback_Year__c =:currentYear and Category__c ='CAT 2' AND  Feedback_Status__c = 'Required' AND 
Covered_Employee_Name__c NOT IN:otherSet GROUP by LOB__c, Covered_Employee_Name__c];
for (AggregateResult gLOB:noResp)
{ 
    //cRatings = Integer.valueof(gLOB.get('cRate'));
    //noRespMap.put(String.ValueOf(gLOB.get('LOB__c')), cRatings);
    if(varLOB == string.valueof(gLOB.get('LOB__c')))
    {
        countMeets=countMeets+1;
    }
    else
     {
           if(varLOB =='')
           {
               countMeets=1;
               varLOB =string.valueof(gLOB.get('LOB__c'));
           }
           else
           {
               noRespMap.put(varLOB, countMeets); 
               varLOB =string.valueof(glOB.get('LOB__c'));
                countMeets=1;

           }
        }    
}
noRespMap.put(varLOB, countMeets); 

//Add LOB Name to map for 0 counts
totalNoResp=0;
for(String lobNm:lstLOB)
{
    if(noRespMap.ContainsKey(lobNm))
    {
       totalNoResp =  totalNoResp +noRespMap.get(lobNm);
    }
    else
    {
        noRespMap.put(lobNm, 0);
    }
}

//Calculate for Non Required total by LOB
nonReqMap =  new Map<String, Integer>();
varLOB ='';
countMeets=0;
//List<AggregateResult> nonReq = [SELECT LOB__c, count(Id) nReq FROM Control_Function_Feedback__c where Feedback_Year__c =:currentYear and Category__c ='CAT 2' AND  Feedback_Status__c = 'Non-Required' GROUP by LOB__c];

List<AggregateResult> nonReq = [SELECT LOB__c,  Covered_Employee_Name__c FROM Control_Function_Feedback__c where Feedback_Year__c =:currentYear and Category__c ='CAT 2' AND  Feedback_Status__c = 'Non-Required' GROUP by LOB__c, Covered_Employee_Name__c];
for (AggregateResult grpLob:nonReq)
{ 
    //nonReqMap.put(String.ValueOf(grpLOB.get('LOB__c')), Integer.ValueOf(grpLob.get('nReq')));
    if(varLOB == string.valueof(grpLob.get('LOB__c')))
    {
        countMeets=countMeets+1;
    }
    else
     {
           if(varLOB =='')
           {
               countMeets=1;
               varLOB =string.valueof(grpLob.get('LOB__c'));
           }
           else
           {
               nonReqMap.put(varLOB, countMeets); 
               varLOB =string.valueof(grpLob.get('LOB__c'));
                countMeets=1;

           }
        }      
}
nonReqMap.put(varLOB, countMeets); 

//Add LOB Name to map for 0 counts
totalNonReq=0;
for(String lobNm:lstLOB)
{
    if(nonReqMap.ContainsKey(lobNm))
    {
        totalNonReq =  totalNonReq +nonReqMap.get(lobNm);
    }
    else
    {
        nonReqMap.put(lobNm, 0);
    }
}

for(String lobName:lstLOB)
{
   //String lobName='GBAM';
    allRecs = [SELECT Covered_Employee_Name__c, count(Id) respCount FROM Control_Function_Feedback__c where Feedback_Year__c =:currentYear and Category__c ='CAT 2' and LOB__c=:lobName AND Feedback_Status__c = 'Required' AND (CFF_RATING__c ='Meets' OR CFF_RATING__c ='Exemplar' OR CFF_RATING__c ='Negative')GROUP by Covered_Employee_Name__c];
 
    for(AggregateResult b:allRecs)
    {
       cResult = Integer.ValueOf(b.get('respCount'));
        if(!Test.isRunningTest()){
       hCount[cResult]=hCount[cResult]+1;
      vTotal[cResult]= vTotal[cResult]+1;
        }
            }
    
   
    mLOBByBand.put(lobName+'Resp0', hCount[0]); //not used in Report 
    mLOBByBand.put(lobName+'Resp1', hCount[1]);
    mLOBByBand.put(lobName+'Resp2', hCount[2]);
    mLOBByBand.put(lobName+'Resp3', hCount[3]);
    mLOBByBand.put(lobName+'Resp4', hCount[4]);
    mLOBByBand.put(lobName+'Resp5', hCount[5]);
    mLOBByBand.put(lobName+'Resp6', hCount[6]);
   
   hTotMap.put(lobName,noRespMap.get(lobName)+hCount[1]+hCount[2]+hCount[3]+hCount[4]+hCount[5]+hCount[6]+nonReqMap.get(lobName)); 
   
   System.debug(lobName+'Resp1' +  mLOBByBand.get(lobName+'Resp1'));
    System.debug(lobName+'Resp2'+  mLOBByBand.get(lobName+'Resp2'));
    System.debug(lobName+'Resp3'+mLOBByBand.get(lobName+'Resp3'));
   System.debug(lobName+'Resp4'+mLOBByBand.get(lobName+'Resp4'));
   system.debug(lobName+'Resp5'+ mLOBByBand.get(lobName+'Resp5'));

System.debug('Totl Response 1:'+hCount[1]);
System.debug('Totl Response 2:'+hCount[2]);
System.debug('Totl Response 3:'+hCount[3]);
System.debug('Totl Response 4:'+hCount[4]);
System.debug('Totl Response 5:'+hCount[5]);
  hCount[0]=0;
hCount[1]=0;
hCount[2]=0;
hCount[3]=0;
hCount[4]=0;
hCount[5]=0;
hCount[6]=0;
System.debug('Totl Response 1:'+hCount[1]);
System.debug('Totl Response 2:'+hCount[2]);
System.debug('Totl Response 3:'+hCount[3]);
System.debug('Totl Response 4:'+hCount[4]);
System.debug('Totl Response 5:'+hCount[5]);
        
} //end of LOB loop
 //Keep in vTotMap
 
   
vTotMap.put('Resp0', vTotal[0]); //not used in report
vTotMap.put('Resp1', vTotal[1]);
vTotMap.put('Resp2', vTotal[2]);
vTotMap.put('Resp3', vTotal[3]);
vTotMap.put('Resp4', vTotal[4]);
vTotMap.put('Resp5', vTotal[5]);
vTotMap.put('Resp6', vTotal[6]);


allTotal = totalNoResp+vTotal[1]+vTotal[2]+vTotal[3]+vTotal[4]+vTotal[5]+vTotal[6]+totalNonReq;




} //end of constructor

public PageReference openRepAsExcel() {
system.debug('IN OPEN AS EXCEL METHOD-->');
PageReference newPage = Page.CFF_ResposeSummaryXLS;
return newPage.setRedirect(false);
}
}