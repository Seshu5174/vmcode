Public class b_class implements Database.Batchable <SObject> {
    String query;
    String Field;
    String Value;
    
    public b_class (String q, String f, String v){
    query = q;
    field = f;
    value = v;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        return database.getQueryLocator(query);
    }

    public void execute(Database.batchableContext bc, list<sObject> Scope){
     for (SObject s: scope){
        s.put(field, value);
        }
        update scope;
   }       
       
    public void finish(Database.batchableContext bc){
     }   
}