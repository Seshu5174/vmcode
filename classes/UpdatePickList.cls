public class UpdatePickList{  
    public static MetadataService.MetadataPort createService(){ 
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;     
    }
    @future(callout=true)  
    public static void updateSubLOB(String subLobValue,String lobValue){
        MetadataService.MetadataPort service = createService();             
        // Read Custom Field
        MetadataService.CustomField customField =
            (MetadataService.CustomField) service.readMetadata('CustomField',
                                                               new String[] { 'Nomination__c.SubLob__c' }).getRecords()[0];
        
        // Add pick list values
        customField.picklist.controllingField  = 'Lob__c';
        metadataservice.PicklistValue two = new metadataservice.PicklistValue();
        two.fullName= subLobValue;
        
        two.controllingFieldValues = new String[] { lobValue};
            two.default_x=false;
        
        customField.picklist.picklistValues.add(two);
        system.debug('customField================>');
        system.debug(customField);
        // Update Custom Field
        handleSaveResults(
            service.updateMetadata(
                new MetadataService.Metadata[] { customField })[0]);
    }
    @future(callout=true)
    public static void updateLOB(String lobValue){
        MetadataService.MetadataPort service = createService();             
        // Read Custom Field
        MetadataService.CustomField customField =
            (MetadataService.CustomField) service.readMetadata('CustomField',
                                                               new String[] { 'Nomination__c.Lob__c' }).getRecords()[0];
        
        // Add pick list values
        metadataservice.PicklistValue two = new metadataservice.PicklistValue();
        two.fullName= lobValue;
        
        
        two.default_x=false;
        
        customField.picklist.picklistValues.add(two);
        system.debug('customField================>');
        system.debug(customField);
        // Update Custom Field
        handleSaveResults(
            service.updateMetadata(
                new MetadataService.Metadata[] { customField })[0]);
    }
    
    public static void handleSaveResults(MetadataService.SaveResult saveResult)
    {
        // Nothing to see?
        if(saveResult==null || saveResult.success)
            return;
        // Construct error message and throw an exception
        if(saveResult.errors!=null)
        {
            List<String> messages = new List<String>();
            messages.add(
                (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') +
                'occured processing component ' + saveResult.fullName + '.');
            for(MetadataService.Error error : saveResult.errors)
                messages.add(
                    error.message + ' (' + error.statusCode + ').' +
                    ( error.fields!=null && error.fields.size()>0 ?
                     ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            /*if(messages.size()>0)
throw new MetadataServiceExamplesException(String.join(messages, ' '));*/
        }
        /*if(!saveResult.success)
throw new MetadataServiceExamplesException('Request failed with no specified error.');*/
    }
}