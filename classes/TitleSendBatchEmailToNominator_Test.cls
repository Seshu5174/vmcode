@isTest

public class TitleSendBatchEmailToNominator_Test
{
    public testMethod static void testTitleSendApprovedEmailToNominator()
    {
        List<Nomination__c> nomList = new List<Nomination__c>();
        
        for (integer i=0;i<40;i++){
        Nomination__c nom = new Nomination__c (Nominator_E_mail__c='test@nomination.com',Nominator_Band__c='H9',Nomination_year__c='2019',Recordtypeid=Schema.SObjectType.Nomination__c.getRecordTypeInfosByName().get('NOMSITE TITLE').getRecordTypeId(),Nomination_Number__c='111'+i,LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='GLOBAL FICC', Division__c='FICC TRADING', Nomination_Status__c='Approved By HR for Manager', Proposed_Title__c='Vice President');
        nomList.add(nom);
        }
        insert nomList;
        
        Test.startTest();
        TitleSendApprovedEmailToNominator ta = new TitleSendApprovedEmailToNominator();
        Database.executeBatch(ta);
        Test.stopTest();
    }
    public testMethod static void testTitleSendDeclinedEmailToNominator()
    {
        List<Nomination__c> nomList = new List<Nomination__c>();
        
        for (integer i=0;i<40;i++){
        Nomination__c nom = new Nomination__c (Nominator_E_mail__c='test@nomination.com',Nominator_Band__c='H9',Nomination_year__c='2019',Recordtypeid=Schema.SObjectType.Nomination__c.getRecordTypeInfosByName().get('NOMSITE TITLE').getRecordTypeId(),Nomination_Number__c='111'+i,LOB__c='GLOBAL BANKING AND MARKETS',SUB_LOB__c='GLOBAL FICC', Division__c='FICC TRADING', Nomination_Status__c='Declined By HR for Manager', Proposed_Title__c='Vice President');
        nomList.add(nom);
        }
        insert nomList;
        
        Test.startTest();
        TitleSendDeclinedEmailToNominator ta = new TitleSendDeclinedEmailToNominator();
        Database.executeBatch(ta);
        Test.stopTest();
    }
}