public class NominationServer{
    Public Integer size{get;set;} 
    Public Integer noOfRecords{get; set;} 
    Public String optyName{get; set;} 
    public DateTime createDateInfo {get;set;}
    public Date createDateDt {get;set;}
    public Nomination__c searchOpp{get;set;}
    public List<SelectOption> paginationSizeOptions{get;set;}
    public void eraseDateInput(){
        searchOpp.Created_Date__c = null;
    }
    public NominationServer(){
        size=10;
        searchOpp = new Nomination__c(Created_Date__c =null);
        optyName = '';
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
    }
     
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {                
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [select id,Name,Nomination_Number__c,Nominee_FN__c,Flagged_for_HR_Review__c,Proposed_Title__c,Corporate_Title__c,Time_in_Current_Corporate_Title__c,Nomination_Status__c,Approver_Name__c,Rejection_Reason__c,Job_Name__c,Band__c,LOB__c,SUB_LOB__c,Division__c,Primary_Work_Country__c,Region__c,Most_Recent_YE_Rating_What__c,Most_Recent_YE_Rating_How__c,Assignment_Status__c,Created_By__c,Created_Date__c from Nomination__c where  Nomination_Number__c!=null and Nomination_Number__c!='']));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
            }            
            return setCon;
        }
        set;
    }
     
    //Changes the size of pagination
    public PageReference refreshPageSize() {
         setCon.setPageSize(size);
         return null;
    }
 
    // Initialize setCon and return a list of record    
     
    public List<Nomination__c> getOpportunities() {
         return (List<Nomination__c>) setCon.getRecords();
    }
    public void doSearch(){
             Nomination__c inputInfo = new Nomination__c(Nomination_Number__c=optyName);
            getOpportunities(searchOpp);
       
        
    }
     public List<Nomination__c> getOpportunities(Nomination__c searchInput) {
system.debug('searchInput'+searchInput.Nomination_Number__c);
 List<String> searchQuery =new List<String>();
      
 
            if(String.isNotBlank(searchInput.Nomination_Number__c)){
             String nameInfo = ' Nomination_Number__c like \'%'+searchInput.Nomination_Number__c+'%\'';
             searchQuery.add(nameInfo);
              }
         if(String.isNotBlank(searchInput.Nominee_FN__c)){
             String nameInfo = ' Nominee_FN__c like \'%'+searchInput.Nominee_FN__c+'%\'';
             searchQuery.add(nameInfo);
              }
			    if(String.isNotBlank(searchInput.Flagged_for_HR_Review__c)){
                    String value=searchInput.Flagged_for_HR_Review__c;
             String nameInfo = ' Flagged_for_HR_Review__c =:value';
             searchQuery.add(nameInfo);
              }
         if(String.isNotBlank(searchInput.Proposed_Title__c)){
             String value=searchInput.Proposed_Title__c;
             String nameInfo = ' Proposed_Title__c =:value';
             searchQuery.add(nameInfo);
              }
			    if(String.isNotBlank(searchInput.Corporate_Title__c)){
             String nameInfo = ' Corporate_Title__c like \'%'+searchInput.Corporate_Title__c+'%\'';
             searchQuery.add(nameInfo);
              }
         if(String.isNotBlank(searchInput.Time_in_Current_Corporate_Title__c)){
             String nameInfo =' Time_in_Current_Corporate_Title__c like \'%'+searchInput.Time_in_Current_Corporate_Title__c+'%\'';
             searchQuery.add(nameInfo);
              }
			  
			    if(String.isNotBlank(searchInput.Nomination_Status__c)){
                    String value = searchInput.Nomination_Status__c;
             String nameInfo =' Nomination_Status__c =:value';
             searchQuery.add(nameInfo);
              }
         if(String.isNotBlank(searchInput.Approver_Name__c)){
             String nameInfo =' Approver_Name__c like \'%'+searchInput.Approver_Name__c+'%\'';
             searchQuery.add(nameInfo);
              }
			    if(String.isNotBlank(searchInput.Job_Name__c)){
             String nameInfo =' Job_Name__c like \'%'+searchInput.Job_Name__c+'%\'';
             searchQuery.add(nameInfo);
              }
         if(String.isNotBlank(searchInput.Band__c)){
             String nameInfo =' Band__c like \'%'+searchInput.Band__c+'%\'';
                 //'%'+searchInput.Band__c+'%\'';
             //like :nameInfo
             searchQuery.add(nameInfo);
              }
			    if(String.isNotBlank(searchInput.LOB__c)){
                    String value = searchInput.LOB__c;
             String nameInfo =' LOB__c =:value';
             searchQuery.add(nameInfo);
              }
         if(String.isNotBlank(searchInput.SUB_LOB__c)){
             String value = searchInput.SUB_LOB__c;
             String nameInfo =' SUB_LOB__c =:value';
             searchQuery.add(nameInfo);
              }
			    if(String.isNotBlank(searchInput.Division__c)){
             String nameInfo =' Division__c like \'%'+searchInput.Division__c+'%\'';
             searchQuery.add(nameInfo);
              }
         if(String.isNotBlank(searchInput.Primary_Work_Country__c)){
             String nameInfo =' Primary_Work_Country__c like \'%'+searchInput.Primary_Work_Country__c+'%\'';
             searchQuery.add(nameInfo);
              }

			  	    if(String.isNotBlank(searchInput.Region__c)){
                                     searchInput.Region__c ='EMEA';

             //String nameInfo ='%EMEA%';
                        system.debug('searchInput.Region__'+searchInput.Region__c);
                        String nameInfo =' Region__c like \'%'+searchInput.Region__c+'%\'';
             //searchQuery.add('Region__c like :nameInfo');
             searchQuery.add(nameInfo);
               //         Query += ' and Region__c like :nameInfo';
              }
         if(String.isNotBlank(searchInput.Most_Recent_YE_Rating_What__c)){
             String nameInfo =' Most_Recent_YE_Rating_What__c like \'%'+searchInput.Most_Recent_YE_Rating_What__c+'%\'';
             searchQuery.add(nameInfo);
              }
			    if(String.isNotBlank(searchInput.Most_Recent_YE_Rating_How__c)){
             String nameInfo =' Most_Recent_YE_Rating_How__c like \'%'+searchInput.Most_Recent_YE_Rating_How__c+'%\'';
             searchQuery.add(nameInfo);
              }
         if(String.isNotBlank(searchInput.Assignment_Status__c)){
             String nameInfo =' Assignment_Status__c like \'%'+searchInput.Assignment_Status__c+'%\'';
             searchQuery.add(nameInfo);
              }
			  
			    if(String.isNotBlank(searchInput.Created_By__c)){
             String nameInfo =' Created_By__c like \'%'+searchInput.Created_By__c+'%\'';
              searchQuery.add(nameInfo);
              }	       
         if((searchInput.Created_Date__c!=null)){
//             String nameInfo ='%'+searchInput.Created_Date__c+'%';
DateTime createdDateInfo = searchInput.Created_Date__c;
                          String nameInfo =' Created_Date__c =:createdDateInfo';
searchQuery.add(nameInfo);
             //searchQuery.add('Created_Date__c like :nameInfo');
              }
                 String sQuery ='select id,Name,Nomination_Number__c,Nominee_FN__c,Flagged_for_HR_Review__c,Proposed_Title__c,Corporate_Title__c,Time_in_Current_Corporate_Title__c,Nomination_Status__c,Approver_Name__c,Rejection_Reason__c,Job_Name__c,Band__c,LOB__c,SUB_LOB__c,Division__c,Primary_Work_Country__c,Region__c,Most_Recent_YE_Rating_What__c,Most_Recent_YE_Rating_How__c,Assignment_Status__c,Created_By__c,Created_Date__c from Nomination__c where Id!=null ';      
          if(searchQuery!=null&&searchQuery.size()>0){
              system.debug('where query:'+String.join(searchQuery, ' and '));
             sQuery += 'and '+String.join(searchQuery, ' and ');
              system.debug('searchInput.Region__'+searchInput.Region__c);
             
               
         }
 
 
        system.debug('searchInput.Region__sQuery:'+sQuery);
             setCon = new ApexPages.StandardSetController(Database.getQueryLocator(sQuery));
         
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
         system.debug('setCon.getRecords()'+setCon.getRecords());
         return (List<Nomination__c>) setCon.getRecords();
    }
}