public class OpportunitiesPaginationController{
    Public Integer size{get;set;} 
    Public Integer noOfRecords{get; set;} 
    public List<SelectOption> paginationSizeOptions{get;set;}
   //public Map<String,Opportunity> optyPagingMap {get;set;}
         
    public OpportunitiesPaginationController(){
        size=10;
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
        getOpportunities();
    }
     
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {                
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [select id,Name,AccountId,Account.name,Amount,StageName,CloseDate,LastModifiedDate from Opportunity]));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
                system.debug('noOfRecords setcon====>');                
                system.debug(noOfRecords);
            }            
            return setCon;
        }
        set;
    }
     
    //Changes the size of pagination
    public PageReference refreshPageSize() {
         setCon.setPageSize(size);
         return null;
    }
 
    // Initialize setCon and return a list of record    
     
    public List<Opportunity> getOpportunities() {
//        optyPagingMap = new          Map<String,Opportunity> ();

    system.debug('noOfRecords getOpportunities====>');                
                system.debug(noOfRecords);
                for(Opportunity opty:(List<Opportunity>) setCon.getRecords()){
               // optyPagingMap.put(opty.Id,opty);
                }
                    system.debug('noOfRecords getOpportunities map refreshing====>');                
                    //system.debug(optyPagingMap.values()[0].Id);

         return (List<Opportunity>) setCon.getRecords();
    }
   public  Map<String,Opportunity>  getoptyPagingMap (){
    Map<String,Opportunity> optyPagingMapInput = new          Map<String,Opportunity> ();
        for(Opportunity opty:(List<Opportunity>) setCon.getRecords()){
                optyPagingMapInput.put(opty.Id,opty);
                }
                return optyPagingMapInput;
    }
}