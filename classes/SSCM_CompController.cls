public class SSCM_CompController {

    @AuraEnabled
    public static Account getInquirer(Id accountId){
        return [SELECT Id, Name, FNLN__c, Person_Number__c, Standard_ID__c,Date_of_Birth__c,
    Job_Title__c, Worker_Type__c 
                  FROM Account Where Id=:accountId];
    }
    
    @AuraEnabled
    public static List<Case> getEmpSSCMCases(Id accountId){
        return [SELECT Id, CaseNumber,Inquirer_Name__c,Inquirer_Person_Number__c
                  FROM Case Where (AccountId=:accountId OR Select_Subject_Name__c =:accountId )AND RecordType.Name='SSCM' ];
    }

@AuraEnabled
    public static List<SSCM_Subject__c> getSubjects(Id caseId){
        return [SELECT Name,Id,Case_Id__r.Inquirer_is_the_subject__c,BAC_Person_Number__c,BAC_Standard_ID__c,Band__c,Cost_Center__c,Country__c,Continuous_Service_Date__c,eLedger_CC_Dept__c,Employment_Status__c,
        First_Name__c,Hire_Date__c,Last_Name__c,Location__c,Manager_Person_Number__c,Original_Hire_Date__c,PAN_No__c,Phone_Number__c,Preferred_Name__c,Sub_LOB__c,Termination_Date__c,
        Work_Email_Address__c,Worker_ID__c
                  FROM SSCM_Subject__c Where Case_Id__c=:caseId  ];
    }
    
    @AuraEnabled
public static list<EmployeeInformationWraper> fetchEmployee (String searchStandardID, String searchPersonNumber, String searchDoB, String searchFirstName, String searchLastName,  String searchPreferredName, String searchPhone, String searchEmail, string searchEmpLevel, string searchManagerID, string searchManagerPersonNumber,  string searchCostCenter) {

List <Employee_Name__c> returnList = new List < Employee_Name__c> ();
List < Employee_Name__c > lstOfEmployees = new List < Employee_Name__c > (); 

string standardID = searchStandardID + '%';
string personNumber = searchPersonNumber + '%'; 
string doB = searchDoB + '%'; 
string firstName = searchFirstName + '%';
string lastName = searchLastName + '%';
string preferredName = searchPreferredName + '%'; 
string phone = searchPhone + '%'; 
string email = searchEmail + '%'; 
string level = searchEmpLevel + '%';     
string managerID = searchManagerID + '%';    
string managerPerNumber = searchManagerPersonNumber + '%';    
string costCenter = searchCostCenter + '%';    

string sQuery = 'select id,Name, FirstName__c, LastName__c, Preferred_Full_Name__c, Date_of_Birth__c, Person_Number__c, Employee__r.Company_Cost_Center_Code__c, Standard_ID__c, Email__c, Employee__r.Country_code__c,Phone__c, Employee__r.Employment_Status__c, Employee__r.Employee_Most_Recent_Hire_Date__c, Employee__r.Manager_Full_Name__c, Employee__r.Manager__pr.Manager_Full_Name__c,Manager_Person_Number__c, Employee__r.Manager_Standard_ID__c,Employee__r.Termination_Date,Employee__r.Band__c,Employee__r.Level_2_LOB__c,Employee__r.Job_Title_pc from Employee_Name__c Where Id!=null';

if(searchStandardID!=null && String.IsNotBlank(searchStandardID)){
sQuery +=' and (Standard_ID__c LIKE: standardID)';
}

if(searchPersonNumber!=null && String.IsNotBlank(searchPersonNumber)){
sQuery +=' and (Person_Number__c LIKE: personNumber)';
}

if(searchDoB!=null && String.IsNotBlank(searchDoB)){
sQuery +=' and (Date_of_Birth__c LIKE: doB)';
}

if(searchFirstName!=null && String.IsNotBlank(searchFirstName)){
sQuery +=' and (FirstName__c LIKE: firstName)';
}

if(searchLastName!=null && String.IsNotBlank(searchLastName)){
sQuery +=' and (LastName__c LIKE: lastName)';
}

if(searchPreferredName!=null && String.IsNotBlank(searchPreferredName)){
sQuery +=' and (Preferred_Full_Name__c LIKE: preferredName)';
}

if(searchPhone!=null && String.IsNotBlank(searchPhone)){
sQuery +=' and (Phone__c LIKE: phone)';
}

if(searchEmail!=null && String.IsNotBlank(searchEmail)){
sQuery +=' and (Email__c LIKE: email)';
}

if(searchEmpLevel!=null && String.IsNotBlank(searchEmpLevel)){
sQuery +=' and (Employee__r.Employment_Status__c LIKE: level)';
}

if(searchManagerID!=null && String.IsNotBlank(searchManagerID)){
sQuery +=' and (Employee__r.Manager_Standard_ID__c LIKE: level)';
}
if(searchManagerPersonNumber!=null && String.IsNotBlank(searchManagerPersonNumber)){
sQuery +=' and (Manager_Person_Number__c LIKE: managerPerNumber)';
}    

if(searchCostCenter!=null && String.IsNotBlank(searchCostCenter)){
sQuery +=' and (Employee__r.Company_Cost_Center_Code__c LIKE: costCenter)';
}        
sQuery +=' order by LastName__c ASC LIMIT 500';

lstOfEmployees  = Database.query(sQuery);
list<EmployeeInformationWraper> ens = new list<EmployeeInformationWraper>();
for (Employee_Name__c emp: lstOfEmployees) {
//returnList.add(emp);
ens.add(new EmployeeInformationWraper(emp));
}
return ens;
}
public class EmployeeInformationWraper{
@AuraEnabled
public Employee_Name__c ename {get;set;}
@AuraEnabled
public Boolean isSelected {get;set;} 
public EmployeeInformationWraper(Employee_Name__c ename){
this.ename = ename;
this.isSelected = false;

}

}

@AuraEnabled
public static String addSubjectEmps(String CaseId, String EmpId,List<String> EmpIdList)
{


System.debug('CaseId'+CaseId);
System.debug('EmpId'+EmpId);
String rtnValue = 'success';



List<SSCM_Subject__c> sscmSubjs= new List<SSCM_Subject__c> ();



if(CaseId != null)
{

    for (String subjId:EmpIdList)
    
    {
   System.debug('EmpIdlist'+subjId);
   
    SSCM_Subject__c newSubj = new SSCM_Subject__c();
    newSubj.Employee__c = subjId;
    newSubj.Case_id__c = CaseId;
    sscmSubjs.add(newSubj);
    }
}
if(sscmSubjs.size() >0)
{

insert sscmSubjs;
}
return rtnValue;
}

@AuraEnabled
public static String  insertSubjects( List<sObject> sub){

String rtnValue;


try{
if(sub.size() > 0)
insert sub;
rtnValue = 'success';
}

catch(DMLException ex){
AuraHandledException e = new AuraHandledException(ex.getMessage());
throw e;

}

return rtnValue ;  

}
}