public with sharing class NomUITriggerHelper {
 public static Boolean bMultiNom = false;
 public static boolean addTeam = false;
 private static boolean run = true;
    public static boolean runOnce(){
    if(run){
     run=false;
     return true;
    }
    else
    {
        return run;
    }
 }
public static void GenerateNominationID(List<Nomination__c> newRecs, Boolean isInsert, Boolean isUpdate)
{
    //get all triiger.new records and update with ID
        //get query where nominator
    List<AggregateResult> aggrNm =new List<AggregateResult>([SELECT Count(Id),Nomination_Type__c nmType, Nominee_lkp__c NomineeId, Random_Number__c ranNum FROM Nomination__c WHERE Nomination_Type__c != 'Team' AND  Random_Number__c != null  GROUP BY Nomination_Type__c, Nominee_lkp__c,Random_Number__c ]); 
  
    MAP<String, integer> aggrResultMap = new MAP<String, integer>();
    MAP<String, String> NumberMap = new MAP<String, String>();

    String NomType = '';
   String NomineeId = '';
   integer iCount = 0;
   for (AggregateResult ar :aggrNm)  {
   NomType = String.ValueOf(ar.get('nmType'));
   NomineeId = String.ValueOf(ar.get('NomineeId'));
   iCount = (Integer)ar.get('expr0');
   aggrResultMap.put(NomType+ NomineeId ,iCount); 
   NumberMap.put(NomType+ NomineeId, String.ValueOf(ar.get('ranNum')));

          }
    integer numOfRecs = 0;
    
List<Integer> charArr= new Integer[]{65}; 
String convertedChar = String.fromCharArray(charArr); 
String  nmTypeChar=''; 
    for(Nomination__c updRec:newRecs)
    {
         String sRandNum = '';
         Boolean firstRecord = false;
         if(updRec.Nomination_Type__c == 'Individual') nmTypeChar = 'I-';
         else if(updRec.Nomination_Type__c == 'Manager') nmTypeChar = 'M-';
         else if(updRec.Nomination_Type__c == 'Team') nmTypeChar = 'T-';

        if(aggrResultMap.get(updRec.Nomination_Type__c + updRec.Nominee_Lkp__c) == null && (updRec.Nomination_Type__c=='Individual' ||updRec.Nomination_Type__c == 'Manager' ))
           {
             sRandNum = getRandomNumber();
             firstRecord = true;
            }
        //get count from 
        if( firstRecord == true && (updRec.Nomination_Status__c=='Submitted' || updRec.Nomination_Status__c == 'Draft' )&& (updRec.Nomination_Type__c=='Individual' ||updRec.Nomination_Type__c == 'Manager' ))
        {
                    
                           updRec.Random_Number__c = sRandNum ;
             updRec.Nomination_Number__c = nmTypeChar+sRandNum +convertedChar;
           }
           else if (firstRecord == false && (updRec.Nomination_status__c == 'Submitted' ||updRec.Nomination_Status__c == 'Draft') && (updRec.Nomination_Type__c=='Individual' ||updRec.Nomination_Type__c == 'Manager' ) && (updRec.Nomination_Number__c == null))
           {
               //get count
               numOfRecs = aggrResultMap.get(updRec.Nomination_Type__c + updRec.Nominee_Lkp__c);
               charArr= new Integer[]{65+numOfRecs}; 
               convertedChar = String.fromCharArray(charArr); 

               String sameNum= numberMap.get(updRec.Nomination_Type__c + updRec.Nominee_Lkp__c);
               updRec.Random_Number__c = sameNum ;
               updRec.Nomination_Number__c = nmTypeChar+sameNum+convertedChar ;

           
               
           }
           else if ((updRec.Nomination_Type__c=='Team' &&(updRec.Nomination_Status__c=='Submitted' ||updRec.Nomination_Status__c == 'Draft' )&& updRec.Nomination_Number__c == null && updRec.Team_Nomination__c == null))
           {
             sRandNum = getRandomNumber();
             updRec.Random_Number__c = sRandNum ;
             updRec.Nomination_Number__c = nmTypeChar+sRandNum;
                       }
    if((updRec.Nomination_status__c == 'Submitted' || updRec.Nomination_status__c == 'Draft') && (updRec.Nomination_Type__c=='Team' && updRec.Team_Nomination__c == null))
    {
         updateTeamNMEmails(newRecs, isUpdate);

    }
}
}

public static String getRandomNumber()
{
    
      Integer randomNum = Math.round(Math.Random() * 10000000);
      Integer iNum = String.valueof(randomNum).length();
      String sRandNum = String.valueof(randomNum);
      if(iNum < 7 && iNum == 6) sRandNum = String.valueof(randomNum)+'1';
      else  if(iNum < 7 && iNum == 5) sRandNum = String.valueof(randomNum)+'1'+'2';
      else if(iNum < 7 && iNum == 4)sRandNum= String.valueof(randomNum)+'1'+'2'+'3';
      else if(iNum < 7 && iNum == 3) sRandNum=String.valueof(randomNum)+'1'+'2'+'3'+'4';
      else if(iNum < 7 && iNum == 2) sRandNum=String.valueof(randomNum)+'1'+'2'+'3'+'4'+'5';
      else if(iNum < 7 && iNum == 1) sRandNum=String.valueof(randomNum)+'1'+'2'+'3'+'4'+'5'+'6';
      
      return sRandNum;

}

public static void updateTeamNMEmails(List<Nomination__c> TeamRecs, Boolean isUpdate)
{
SET <ID> TeamParentId = new SET<ID>();
MAP<String, Nomination__c> NewRecMap  = new MAP<String, Nomination__c>();
String fullName = '';
String prevTeamId = null;
String NomineeEmails = '';
String TeamMemLOB = '';
String TeamMemRegion = '';
integer counter = 0;
Set<String> TeamMemList = new Set<String>();
//if(isUpdate){
for (Nomination__c TeamRec:TeamRecs) {
                                             //Add code here to update team name
   if(TeamRec.Nomination_Type__c == 'Team' && TeamRec.Team_Nomination__c == null)
       {                    //Add parent Id to SET  
               System.debug('LG: Team Parent'); 
               TeamParentId.add(TeamRec.id);  
               NewRecMap.put(TeamRec.Id, TeamRec);   
               IF(!string.isBlank(TeamRec.Team_Nominee_Lkp__c)) TeamMemList.addAll(TeamRec.Team_Nominee_Lkp__c.split(';'));   
               if(TeamMemList.size() > 0)
               {
               List<Account> TeamMembers = new List<Account> ([SELECT Name,Work_Email_Address__c, Region__c,Level_1_LOB__c from Account WHERE Id IN: TeamMemList]);
               counter = 0;
               for(Account Acc1:TeamMembers)
               {
                if(counter == 0)
                {
                    fullName =  Acc1.Name ;
                    NomineeEmails = Acc1.Work_Email_Address__c;
                    TeamMemLOB = Acc1.Level_1_LOB__c;
                    TeamMemRegion = Acc1.Region__c;
                }
                else
                {fullName =  fullName +';'+Acc1.Name ;
                NomineeEmails = NomineeEmails+';'+Acc1.Work_Email_Address__c;
                TeamMemLOB = TeamMemLOB+';'+Acc1.Level_1_LOB__c ;
                TeamMemRegion = TeamMemRegion+';'+Acc1.Region__c;

                 }
                 counter=counter+1;

              }
             TeamRec.Team_Member_Names__c = fullName;
              TeamRec.Team_Member_Emails__c = NomineeEmails;
            TeamRec.Team_Member_LOB__c = TeamMemLOB;
            TeamRec.Team_Member_Region__c = TeamMemRegion;


            }
         } 
                
}
//Get records from Account
//List<Account> TeamMembers = new List<Account> [SELECT Name from Account WHERE Id IN: TeamMemSet];
//Search for child records
/*List<Nomination__c> TeamChildList = new List<Nomination__c>([SELECT Id, Team_Nomination__c, Nominee_E_mail__c,HR_Full_Name__c, LOB__c  from Nomination__c where Nomination_Type__c='Team' AND Team_Nomination__c != null AND Team_Nomination__c IN:TeamParentId Order by Team_Nomination__c]);
List<Nomination__c> updateParentList = new List<Nomination__c>();
Nomination__c updParentRec;
String fullName = '';
String prevTeamId = null;
String NomineeEmails = '';
String TeamMemLOB = '';
Integer childCount = 0;
for(Nomination__c childRec:TeamChildList)
{

if(prevTeamId == null || prevTeamId == childRec.Team_Nomination__c){
    if(prevTeamId == null)
    {
        fullName = childRec.HR_Full_Name__c;
        NomineeEmails = childRec.Nominee_E_mail__c;
         TeamMemLOB = childRec.LOB__c;
    }
    else
    {
    fullName = fullName+';'+childRec.HR_Full_Name__c;
    NomineeEmails = NomineeEmails+';'+childRec.Nominee_E_mail__c;
    TeamMemLOB = TeamMemLOB+';'+childRec.LOB__c;

    }
prevTeamId = childRec.Team_Nomination__c;
childCount = childCount+1;
}
else
{
       //add to list
   updParentRec = NewRecMap.get(prevTeamId);
   
   updParentRec.Team_Member_Names__c = fullName;
   updParentRec.Team_Member_Emails__c = NomineeEmails;
    fullName =childRec.HR_Full_Name__c;
      updParentRec.Team_Member_LOB__c = TeamMemLOB;
   prevTeamId = childRec.Team_Nomination__c;
      
   }
}
//Updating last child record
if(childCount > 0)
{
updParentRec = NewRecMap.get(prevTeamId);
updParentRec.Team_Member_Names__c = fullName;
updParentRec.Team_Member_Emails__c = NomineeEmails;
updParentRec.Team_Member_LOB__c = TeamMemLOB;

}*/
//}
}
//count number of nominations for same nominee across all types : only submitted
public static void NumOfNominationsPerNominee(List<Nomination__c> newList)
{

 Set<string> newNomineeIdSet = new Set<String>();
for(Nomination__c nRec: newList)
    {
    IF(nRec.Nomination_Status__c == 'Submitted' && nRec.Nominee_Lkp__c != null)
    {
        newNomineeIdSet.add(nRec.Nominee_Lkp__c);
    }
    }

MAP<String, Integer> arCountMap = new MAP<String, Integer>(); 
String NomineeId = '';
Integer iCount = 0;

List<AggregateResult> arCountList = new  List<AggregateResult>([SELECT  Count(Id) nomCount, Nominee_Lkp__c NomineeId from Nomination__c where Nominee_lkp__c IN: newNomineeIdSet AND ((Status__c != 'Ineligible' AND Status__c != 'Withdrawn' AND Nomination_Status__c != 'Draft') OR (Nomination_type__c ='Team' AND Status__c != 'Ineligible' AND Team_Nomination__r.Status__c != 'Withdrawn' AND Team_Nomination__r.Status__c != 'Draft'  ) ) AND Nominee_Lkp__c != null  GROUP BY Nominee_Lkp__c]);
for (AggregateResult ar : arCountList )  
{
   NomineeId = String.ValueOf(ar.get('NomineeId'));
   iCount = (Integer)ar.get('nomCount');
   arCountMap.put(NomineeId ,iCount);

   }
 //Update No_of_Nominations__c Records of existing records
List<Nomination__c> nmExistingList = new List<Nomination__c>([SELECT Id, Nominee_Lkp__c, No_of_Nominations__c from Nomination__c where (Status__c != 'Ineligible'AND Status__c != 'Withdrawn' AND Nomination_Status__c != 'Draft')  AND Nominee_Lkp__c IN:arCountMap.keyset()] );
List<Nomination__c> updExistingList = new List<Nomination__c>();
for(Nomination__c nmRec:  nmExistingList )
{
    Nomination__c updExsRec= new Nomination__c();
    updExsRec.id = nmRec.Id;
    updExsRec.No_of_Nominations__c = arCountMap.get(nmRec.Nominee_Lkp__c);
    updExistingList.add(updExsRec);

}
if(updExistingList.size() > 0)
{
  update updExistingList;
}

}
//Update NominaionID with A, B, C like that
public static void updateNomiIDwithAlphaChr(List<Nomination__c> newList)
{
    Set<string> newNomineeIdSet = new Set<String>();
    List<Integer> charArr= new Integer[]{65}; 
    String convertedChar = String.fromCharArray(charArr); 
    String typeChar='';
    for(Nomination__c nRec: newList)
    {
    IF(nRec.Nomination_Status__c == 'Submitted' && nRec.Nominee_Lkp__c != null)
    {
        newNomineeIdSet.add(nRec.Nominee_Lkp__c);
    }
    }
    List<Nomination__c> updNumberList = new List<Nomination__c> ([SELECT  Nomination_Type__c, Nominee_Lkp__c, Nomination_Status__c, Status__c, Random_Number__c from Nomination__c where (Nomination_Status__c != null) AND (Nomination_Type__c='Individual' OR Nomination_Type__c='Manager') AND (Nominee_Lkp__c != null AND Nominee_Lkp__c IN: newNomineeIdSet) order by Nomination_type__c, Nominee_Lkp__c,  Nomination_status__c DESC, Status__c DESC ]);
    string prevType='';
    string prevNomineeLkp = '';
    integer iChar = 0;
    Nomination__c  updIndMgrRec;
    list<Nomination__c> nmList = new list<Nomination__c>();
    for(Nomination__c nmRec: updNumberList)
    {
        if( nmRec.Nomination_Type__c == 'Individual') typeChar = 'I-';
        else if( nmRec.Nomination_Type__c == 'Manager') typeChar = 'M-';
        updIndMgrRec = new Nomination__c();
        if(prevType=='' && prevNomineeLkp  == '')
        {
        //Assign A to first number
        updIndMgrRec.Id = nmRec.Id;
        updIndMgrRec.Nomination_Number__c = typeChar+ nmRec.Random_Number__c + convertedChar;
        prevType = nmRec.Nomination_Type__c;
        prevNomineeLkp = nmRec.Nominee_Lkp__c;
        }
        else if(prevType==nmRec.Nomination_type__c &&  prevNomineeLkp == nmRec.Nominee_Lkp__c)
                {
            iChar = iChar+1;
            charArr= new Integer[]{65+iChar}; 
               convertedChar = String.fromCharArray(charArr); 
            //Add numbers B, C, D like
            updIndMgrRec.Id = nmRec.Id;
            updIndMgrRec.Nomination_Number__c = typeChar+ nmRec.Random_Number__c + convertedChar;
            prevType = nmRec.Nomination_Type__c;
        prevNomineeLkp = nmRec.Nominee_Lkp__c;

        }
        else if(prevType==nmRec.Nomination_type__c && prevNomineeLkp != nmRec.Nominee_Lkp__c)
        {
            //start from A
            //updatte prev-->nominee lkp
            iChar = 0;
            charArr= new Integer[]{65+iChar}; 
               convertedChar = String.fromCharArray(charArr); 
              updIndMgrRec.Id = nmRec.Id;

               updIndMgrRec.Nomination_Number__c = typeChar+ nmRec.Random_Number__c + convertedChar;
               prevType = nmRec.Nomination_Type__c;
        prevNomineeLkp = nmRec.Nominee_Lkp__c;


        }
        else if(prevType!= nmRec.Nomination_type__c )
        {
           iChar = 0;
            charArr= new Integer[]{65+iChar}; 
            convertedChar = String.fromCharArray(charArr); 
            updIndMgrRec.Id = nmRec.Id;

           updIndMgrRec.Nomination_Number__c = typeChar+ nmRec.Random_Number__c + convertedChar;
        prevType = nmRec.Nomination_Type__c;
        prevNomineeLkp = nmRec.Nominee_Lkp__c;

            //starts from A
        }
    nmList.add(updIndMgrRec);
    }
    if(nmList.size() > 0)
    {
        update nmList;
    }
}

public static void AddTeamNomination(List<Nomination__c> teamList, MAP<ID, Nomination__c> oldTeamMap, Boolean isUpdate)
{
 Nomination__c chTeamRec= null;
    List<Nomination__c>  updChildList = new List<Nomination__c>();
    List<Nomination__c>  delChildList = new List<Nomination__c>();
     List<Nomination__c>  addChildList = new List<Nomination__c>();
      List<Nomination__c>  updateParent = new List<Nomination__c>();

Nomination__c chDelRec= null;

String fullTeamNM = '';
 Nomination__c updateTeamRec = null;
for(Nomination__c teamRec:teamList)
{
       if(teamRec.Nomination_Type__c == 'Team' && isUpdate == false)
       { 
         if(teamRec.Team_Nominee_Lkp__c != null)
         {
         String [] nomineeLkp = teamRec.Team_Nominee_Lkp__c.split(';');
         //get old list of team nominee lookup
        for(integer j=0; j < nomineeLkp.size(); j++ )
        {
        //add each team record
        chTeamRec = new Nomination__c();
        chTeamRec.Nomination_Year__c = teamRec.Nomination_Year__c;
        chTeamRec.Nomination_Type__c = teamRec.Nomination_Type__c;
        chTeamRec.Nominee_Lkp__c = nomineeLkp[j];
        chTeamRec.Team_Nomination__c = teamRec.Id;
        chTeamRec.Created_By_Lkp__c = teamRec.Created_By_Lkp__c;
        updChildList.add(chTeamRec);
            }
            }
    } 
    else if(teamRec.Nomination_Type__c == 'Team' && isUpdate == true)
    {
          system.debug('LGUpdate');
        //adjust team members
         String [] nomineeLkpNew = new String[0];
         String [] nomineeLkpOld  = new String[0];
        if(teamRec.Team_Nominee_Lkp__c != null )
         nomineeLkpNew = teamRec.Team_Nominee_Lkp__c.split(';');
        
        if(oldTeamMap.get(teamRec.Id).Team_Nominee_Lkp__c != null )
         nomineeLkpOld = oldTeamMap.get(teamRec.Id).Team_Nominee_Lkp__c.split(';');
        Nomination__c nomChRec = null;
        for(integer k=0; k < nomineeLkpNew.size(); k++)
        {
            //add new record
           if(nomineeLkpOld.indexOf(nomineeLkpNew[k]) == -1)
           {
             chTeamRec = new Nomination__c();
             chTeamRec.Nomination_Year__c = teamRec.Nomination_Year__c;
        chTeamRec.Nomination_Type__c = teamRec.Nomination_Type__c;
        chTeamRec.Nominee_Lkp__c = nomineeLkpNew[k];
        chTeamRec.Team_Nomination__c = teamRec.Id;
        chTeamRec.Created_By_Lkp__c = teamRec.Created_By_Lkp__c;
        addChildList.add(chTeamRec);
  
           }
        }
        for(integer m=0; m < nomineeLkpOld.size(); m++)
        {
            
            //delete old record Comparing Old list with new 
            if(nomineeLkpNew.indexOf(nomineeLkpOld[m]) == -1)
            {
           system.debug('LG'+ 'Inside delete');
            chDelRec = new Nomination__c();
            if([SELECT count() from Nomination__c where Team_Nomination__c =:teamRec.Id AND  Nominee_Lkp__c=:nomineeLkpOld[m]] >0)
            {
             nomChRec = [SELECT Id from NOmination__c where Team_Nomination__c =:teamRec.Id AND  Nominee_Lkp__c=:nomineeLkpOld[m]];
            }
            if(nomChRec != null)
            {
           system.debug('LG2 Inside delete'+ nomChRec.Id);

            chDelRec.Id = nomChRec.Id;
            delChildList.add(chDelRec);
            }
            }
        }
        //add parent records to update
       if(delChildList.size() > 0 || addChildList.size() >0)
        updateTeamNMEmails(updateParent, true);
        //updateParent.add(teamRec);
    } 

}
if(updChildList.size() >0)
{
    insert updChildList;
}
if(addChildList.size() > 0)
{
    insert addChildList;
}
if(delChildList.size() > 0)
{
    delete delChildList;
}

if(updateParent.size() >0)
{
  //update updateParent;
}
}
@Future
public static void updateNomSharing(List<ID> newIdList)
{
   System.debug('Sharing new role');
    Id recordTypeIdDI = [Select Id From RecordType Where Name = 'NOMSITE DI'].Id;
    Nomination__c [] nomList = [SELECT RecordTypeId from Nomination__c where ID IN:newIdList];
    List<Nomsite_Security_Roles__c> shRole = new List<Nomsite_Security_Roles__c>();
  if( nomList[0].recordTypeId ==  recordTypeIdDI ){
    shRole = [SELECT Id,Record_Type__c,LOB_Name__c,User_Name__c,All_Regions__c,Select_Sub_LOB__r.SUB_LOB_Name__c,Region__c,SUB_LOB_Name__c,Role_Type__c,Access_Level__c,Exc_Sub_LOB__c,ALL_LOB__c  from Nomsite_Security_Roles__c where Role_Type__c='LOB Reviewer' OR Role_Type__c='SUB LOB Reviewer' OR Role_Type__c='Regional Reviewer' OR Role_Type__c ='LOB/Reg Admin'];
   }
   else
   {
       shRole = [SELECT Id,Record_Type__c,LOB_Name__c,User_Name__c,All_Regions__c,Select_Sub_LOB__r.SUB_LOB_Name__c,Region__c,SUB_LOB_Name__c,Role_Type__c,Access_Level__c,Exc_Sub_LOB__c,ALL_LOB__c  from Nomsite_Security_Roles__c where Role_Type__c='LOB Admin' OR Role_Type__c='Data Analytics Representatives'];
   }

   
   //List<Nomination__c> nomshList = [SELECT Id,LOB__c, Sub_LOB__c, Region__c from Nomination__c where Id IN: newList];
   MAP<ID, Nomsite_Security_Roles__c> oldRoleMap = new MAP<ID, Nomsite_Security_Roles__c>();
   List<Nomsite_Security_Roles__c> xyzRole;
   for(Nomsite_Security_Roles__c shRec:shRole)
   {
   xyzRole = new List<Nomsite_Security_Roles__c>();
   xyzRole.add(shRec);
 NomiShareTriggerHelper.SecurityRoleShare(xyzRole,oldRoleMap,false, true, true, newIdList);
 xyzRole.clear();
    }
   }
  public static void UpdateWinnerPubishDate(List<Nomination__c> newList)
   {
   String publishDateId=null;
List<Nomination_Admin__c> publishDate = new List<Nomination_Admin__c>([SELECT Id from Nomination_Admin__c where Record_Type__c='DI' AND Type__c ='Deadline Dates' LIMIT 1]);
    for(Nomination_Admin__c pd:publishDate)
    {
    publishDateId = pd.Id;
    }


    for (Nomination__c  newNomi : newList) 
          {
            
        if(newNomi.Status__c=='Winner')
        {
            newNomi.Publish_Date_Lkp__c = publishDateId;
        }
    }


       
   }  
 public static void captureHistory(List<Nomination__c> newList, MAP<ID, Nomination__c> oldMap)
 { 
       String[] nomiFields = new String[] {'Status__c','Reviewer_Comments__c','Final_Nomination_Verbiage__c','Winner_s_Summary__c','Assignee_NM__c'};

          Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
          Schema.SObjectType nominationSchema = schemaMap.get('Nomination__c');
          Map<String, Schema.SObjectField> fieldMap = nominationSchema.getDescribe().fields.getMap();
       List<NominationHistory__c> NomiHistoryLists = new List<NominationHistory__c>();  
          for (Nomination__c  newNomi : newList) 
          {
          Nomination__c  oldNomi = oldMap.get(newNomi.Id);
          for (String fieldName: nomiFields )  {
                    
             if (newNomi.get(fieldName ) != oldNomi.get(fieldName)){
                 NominationHistory__c nomiHistory = new NominationHistory__c();
                 nomiHistory.Field_Name__c = fieldMap.get(fieldName).getDescribe().getLabel();   
                 nomiHistory.New_Value__c  = String.valueOf(newNomi.get(fieldName));
                 nomiHistory.Old_Value__c  = String.valueOf(oldNomi.get(fieldName));
                 nomiHistory.Parent_Id__c  = newNomi.Id;
                 //nomiHistory.Modified_Date__c = system.today();
                 nomiHistory.Nomination_Number__c = newNomi.Nomination_Number__c;
                 NomiHistoryLists.add(nomiHistory); 
                   
            }  
               
       }     
    }  
    If(NomiHistoryLists.size()>0){
              insert NomiHistoryLists;  
           }

}
}