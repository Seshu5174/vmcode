public with sharing class halfseriALIZE  {

    public halfseriALIZE() {

    }

    public map<String,String> consLIst{get;set;}
    public   list<account> accountslist {get;set;}
    public   list<account> accountslistrefredh {get;set;}
 public list<acwraper> acwrpaerLIst {get;set;}
    public   list<account> GroupEmpList {get;set;}
    public list<conWraper> conslistwraper ;
    public halfseriALIZE (ApexPages.StandardSetController controller) {
        consLIst = new map<String,String> ();
        acwrpaerLIst = new list<acwraper>();
                   /* acwraper acn = new acwraper();
            acn.idval ='0';
            acn.nameval = 'test';
            acwrpaerLIst.add(acn);*/
    
                    accountslist = new list<Account>();
         GroupEmpList = [select id,name,AccountNumber , (select id,name,Description from Contacts) from Account];
         for(Account ac: GroupEmpList ){
            
            conslistwraper  = new list<conWraper>();
               /*                     conWraper conwrpvl = new conWraper();
 
       conwrpvl.Audit_Type= '';
                    conwrpvl.CFF_Rating= '2016 Rating' ;
                    conwrpvl.CF_Commentary= '2016 Control Function Feedback' ;
                    conslistwraper.add(conwrpvl);*/
consLIst.put('0',JSON.Serialize(conslistwraper));
            if(ac.Contacts.size()>0){
                list<Contact> namesListContact= new  list<Contact>();
                namesListContact.addall(ac.Contacts);
                for(Contact cons: namesListContact){
                    
                    conWraper conwrp = new conWraper();
                    conwrp.Audit_Type= cons.Name!=NULL?cons.Name:'';
                    conwrp.CFF_Rating= cons.Description!=NULL?cons.Description:'' ;
                    conwrp.CF_Commentary= cons.Description!=NULL?cons.Description:'' ;
                    
                    conslistwraper.add(conwrp);
                    
                    
                }
            }
            
            consLIst.put(ac.Id,JSON.Serialize(conslistwraper));
            
            accountslist.add(ac);
            acwraper acnvl = new acwraper();
            acnvl.idval = ac.ID;
            acnvl.nameval = ac.name;
            acwrpaerLIst.add(acnvl); 
            
        }
        
        
    }
    
    public String processString(String ss){
        Apexpages.addmessage(new Apexpages.Message(ApexPages.Severity.info,'Actual Str::'+ ss));       
        
        String result='';
        ss = ss.remove('"');
        list<String> jsonspli = ss.split(':');
        for(String s :jsonspli){
            
            if(s.contains(',')){
                
                s = (s.substringBefore(',').contains('}'))?s.replaceFirst(s.substringBefore('}'),':"'+s+'"'):s.replaceFirst(s.substringBefore(','),':"'+s+'"');
                Apexpages.addmessage(new Apexpages.Message(ApexPages.Severity.info,'inside Str processed::'+ s));       
            }
            result += s;
        }
        Apexpages.addmessage(new Apexpages.Message(ApexPages.Severity.info,'result Str::'+ result));       
        
        return result;
    }
    public Class conWraper{ 
        public String Audit_Type ='';  
        public String CFF_Rating='';
        public String CF_Commentary='';
    }
    public class acwraper{
    public String idval{get;set;}
    public String nameval{get;set;}
    }
}