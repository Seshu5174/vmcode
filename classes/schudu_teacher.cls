public class schudu_teacher implements schedulable{
    public void execute(SchedulableContext con){
        teacher__c  t = new teacher__c();
        t.name = 'Sarvam';
        t.salary__c = 2000;
        t.phone__c = '810 456 2159';
        t.email__c = 'sarvam@gmail.com';
        insert t;
    }
}