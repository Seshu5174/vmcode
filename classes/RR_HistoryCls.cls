public with sharing class RR_HistoryCls {

static Boolean isFetched = false;
@AuraEnabled
public static DisplayWraper fetchHistoryRecords(String pageNumber,String currnetPagesCount,String recordId) {
DisplayWraper dsp = new DisplayWraper();
	Recharge_and_Refocus__c rr = [select Id,Modified_By__c from Recharge_and_Refocus__c where Id=:recordId];
List <Recharge_and_Refocus__History> returnList = new List < Recharge_and_Refocus__History> ();
List < Recharge_and_Refocus__History > lstOfEmployees = new List < Recharge_and_Refocus__History > (); 

string sQuery = 'SELECT Id, IsDeleted, ParentId, CreatedById,CreatedBy.Name,CreatedDate, Field, OldValue, NewValue FROM Recharge_and_Refocus__History where ParentId=:recordId and (Field!=\'Direct_Manager__c\' or Field= \'Direct_Manager_Email__c\') ';
String WhereCondition = '';
String CountUery = 'Select COUNT() from Recharge_and_Refocus__History Where ParentId=:recordId and (Field!=\'Direct_Manager__c\' or Field= \'Direct_Manager_Email__c\') ';

sQuery +=WhereCondition+' order by CreatedDate DESC ';
CountUery +=WhereCondition;
currnetPagesCount = '10';
/*   if(String.isBlank(pageNumber) || Integer.valueOf(pageNumber) == 1){
sQuery = sQuery + ' 1';
}else{
sQuery = sQuery + String.valueOf((Integer.valueOf(pageNumber)-1)*Integer.valueOf(currnetPagesCount));
} */
Map<String,String> fieldLabelToAPIName = getFieldLabel();
Boolean isModifiedByChanged = false;
lstOfEmployees  = Database.query(sQuery);
system.debug('pageNumber::'+pageNumber+'::info::'+String.valueOf((Integer.valueOf(pageNumber)-1)*Integer.valueOf(currnetPagesCount))+'::size::'+lstOfEmployees.size());    
list<EmployeeInformationWraper> ens = new list<EmployeeInformationWraper>();
Integer cnt =0;
Map<String,EmployeeInformationWraper> mp = new Map<String,EmployeeInformationWraper>();
Boolean isChangedToBlank = false;
for (Recharge_and_Refocus__History emp: lstOfEmployees) {
system.debug('bfr emp.NewValue'+ emp.NewValue+'emp.Field'+emp.Field);
String modifedByInfo = '';

if(!(emp.NewValue==null&&emp.OldValue==null)&&emp.Field!='Direct_Manager__c'&&emp.Field!='Direct_Manager_Email__c'){
system.debug(' emp.NewValue'+ emp.NewValue+'emp.Field'+emp.Field);
String uniqueKey = String.Valueof(emp.CreatedDate)+emp.CreatedById;
//(isModifiedByChanged?emp.NewValue:emp.CreatedById);
String oldValue = emp.OldValue!=null?String.valueof(emp.OldValue):null;
String newValue = emp.NewValue!=null?String.valueof(emp.NewValue):null;

if(emp.Field=='Modified_By__c'){
isModifiedByChanged=true;
modifedByInfo = newValue;

}
else{
isModifiedByChanged=false;
modifedByInfo = '';

}

if(emp.Field=='Start_Date_of_Request__c'||emp.Field=='End_Date_of_Request__c'){
if(emp.OldValue!=null){
Date oldD= (Date) emp.OldValue;
oldValue = oldD.format();
}
if(emp.NewValue!=null){
Date  newD=(Date) emp.NewValue;

newValue = newD.format();
}

}
else if(emp.Field=='Submit_Date__c'){
if(emp.OldValue!=null){
DateTime oldD= (DateTime) emp.OldValue;
oldValue = oldD.format();
}
if(emp.NewValue!=null){
DateTime  newD=(DateTime) emp.NewValue;

newValue = newD.format();
}
}

//String currentInfo = (emp.NewValue==null&&emp.OldValue!=null)?'Deleted '+oldValue+' in <b>'+fieldLabelToAPIName.get(emp.Field)+'</b>':(emp.NewValue!=null&&emp.OldValue==null?'Changed to '+emp.NewValue:'Changed <b>'+fieldLabelToAPIName.get(emp.Field)+'</b> from '+emp.OldValue+' to <b>'+emp.NewValue+'</b>');
String fieldAPIName = fieldLabelToAPIName.get(emp.Field);
//  String currentInfo = (newValue==null&&oldValue!=null)?'Deleted '+oldValue+' in <b>'+fieldAPIName+'</b>':(newValue!=null&&oldValue==null?'Changed <b>'+fieldAPIName+'</b> to '+newValue:'Changed <b>'+fieldLabelToAPIName.get(emp.Field)+'</b> from '+oldValue+' to <b>'+newValue+'</b>');
String currentInfo = isModifiedByChanged?'':(newValue==null&&oldValue!=null)?'Deleted '+oldValue+' in <b>'+fieldAPIName+'</b>':(newValue!=null&&oldValue==null?'Changed <b>'+fieldAPIName+'</b> to <b>'+newValue+'</b>':'Changed <b>'+fieldAPIName+'</b> from '+oldValue+' to <b>'+newValue+'</b>');

EmployeeInformationWraper existingEmployeeInfo = new EmployeeInformationWraper();
if(mp.containsKey(uniqueKey)){
existingEmployeeInfo = mp.get(uniqueKey);
modifedByInfo = isChangedToBlank?emp.CreatedBy.Name:existingEmployeeInfo.modifiedBy;
currentInfo = (existingEmployeeInfo.historyInfo+((String.isNotBlank(currentInfo)&&String.isNotBlank(existingEmployeeInfo.historyInfo))?'<br/>':'')+currentInfo);
//(String historyInfo,String userName,String userId,String createdDate){
}
system.debug('currentInfo:'+currentInfo+':emp.Field:'+emp.Field);
	EmployeeInformationWraper currentEmployeeInfo = new EmployeeInformationWraper(currentInfo,modifedByInfo,String.valueof(emp.CreatedById),(emp.CreatedDate.format('MM/dd/yyyy hh:mm a','America/New_York')));
currentEmployeeInfo.uniqueKey= uniqueKey;
//(isModifiedByChanged?newValue:(existingEmployeeInfo.isModifedByChange?existingEmployeeInfo.alterUserId:emp.CreatedBy.Name))
//existingEmployeeInfo.isModifedByChange?existingEmployeeInfo.alterUserId:emp.CreatedBy.Name)
currentEmployeeInfo.modifiedBy = isModifiedByChanged?newValue:modifedByInfo;
//(isModifiedByChanged?newValue:(existingEmployeeInfo.isModifedByChange?existingEmployeeInfo.alterUserId:emp.CreatedBy.Name));
currentEmployeeInfo.createdBy = emp.CreatedBy.Name;


if(isModifiedByChanged){
currentEmployeeInfo.isModifedByChange = true;
currentEmployeeInfo.isChangedToBlank = String.isBlank(newValue)&&String.isNotBlank(oldValue);
isChangedToBlank = String.isBlank(newValue)&&String.isNotBlank(oldValue);
currentEmployeeInfo.alterUserId=newValue;
}
mp.put(uniqueKey,currentEmployeeInfo);
system.debug('modifedByInfo:'+modifedByInfo);
}
/*else if(emp.Field!='Direct_Manager__c'&&emp.Field!='Direct_Manager_Email__c') {
cnt++;  
}*/
}
List<EmployeeInformationWraper> allHistoryInfo = mp.values();
List<EmployeeInformationWraper> allHistory =new List<EmployeeInformationWraper>();
system.debug('empInfo'+allHistoryInfo.size());
String modifiedByValue = '';
for(Integer i=allHistoryInfo.size()-1;i>=0;i--){
system.debug('empInfo'+i);
EmployeeInformationWraper empInfo = allHistoryInfo[i];
modifiedByValue = String.isNotBlank(empInfo.modifiedBy)?empInfo.modifiedBy:modifiedByValue;
system.debug('modifiedByValue::'+modifiedByValue+':empInfo.modifiedBy:'+empInfo.modifiedBy+':alterUserId:'+empInfo.alterUserId);
empInfo.userName = ((empInfo.isModifedByChange&&String.isBlank(empInfo.modifiedBy))||String.isBlank(modifiedByValue))?(String.isBlank(rr.Modified_By__c)?empInfo.createdBy:rr.Modified_By__c):modifiedByValue;
system.debug('empInfo'+empInfo.userName );
mp.put(empInfo.uniqueKey,empInfo);
}
ens.addAll(mp.values());
dsp.employees = ens;
dsp.RecordsCount = Database.countQuery(CountUery); 
dsp.totalPages = (dsp.RecordsCount/10)+1;
return dsp;
}




public class EmployeeInformationWraper{
@AuraEnabled
public Recharge_and_Refocus__History ename {get;set;}
@AuraEnabled
public String historyInfo {get;set;}
@AuraEnabled
public Boolean isModifedByChange {get{return this.isModifedByChange !=null && this.isModifedByChange;}set;}
@AuraEnabled
public Boolean isChangedToBlank {get{return this.isChangedToBlank !=null && this.isChangedToBlank;}set;}
@AuraEnabled
public String userName {get;set;}
@AuraEnabled
public String createdBy {get;set;}
@AuraEnabled
public String modifiedBy {get;set;}
	@AuraEnabled
public String uniqueKey {get;set;}
@AuraEnabled
public String alterUserId {get;set;}
@AuraEnabled
public String userId {get;set;}
@AuraEnabled
public String createdDate {get;set;}
public EmployeeInformationWraper(){

}
public EmployeeInformationWraper(String historyInfo,String userName,String userId,String createdDate){
this.historyInfo = historyInfo;
//this.userName = userName;
this.userId = userId;
this.createdDate = createdDate;

}
}

public static Map<String,String> getFieldLabel(){
Map <String, Schema.SObjectField> fieldMap = schema.getGlobalDescribe().get('Recharge_and_Refocus__c').getDescribe().fields.getMap();
Map<String,String> APItoLabel = new Map<String,String>();
for(Schema.SObjectField sfield : fieldMap.Values())
{

schema.describefieldresult dfield = sfield.getDescribe();
APItoLabel.put(dfield.getname(),dfield.getLabel ());
}
return APItoLabel;
}
Public Class DisplayWraper{
@AuraEnabled
public Integer totalPages {get;set;}
@AuraEnabled
public Integer RecordsCount {get;set;}
@AuraEnabled
public list<EmployeeInformationWraper> employees{get;set;}
}

}