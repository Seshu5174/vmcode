trigger NomsiteRolesTrigger on Nomsite_Security_Roles__c (after insert,after update, after delete) {
    if(trigger.isInsert || trigger.isUpdate)
    {
    NomiShareTriggerHelper.SecurityRoleShare( Trigger.new, Trigger.oldMap, Trigger.isUpdate, Trigger.isInsert, false, null );
    }
    else if(trigger.isDelete)
    {
        NomiShareTriggerHelper.deleteSharing(trigger.old);
    }
}