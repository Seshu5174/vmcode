public with sharing class CFFResultsReport{
Public Map<String, Integer> mCFbyRating{get;set;}
Public Map<String, Integer> mTotalHor{get;set;}
Public Map<String, Integer> mOnlyMeets{get; set;}
Public List<String> listContFun{get;set;}
Public List<string> listRatings{get;set;}
Public list <string> listLOB{get;set;}
Public Map<string, integer> mExampler1{get;set;}
Public Map<string, integer> mExampler2{get;set;}
Public Map<string, integer> mExamplerTot{get;set;}
Public Map<string, integer> mNegative{get;set;}
Public Map<string, integer> mNA{get;set;}

Public Map<string, integer> mNegativeExemp{get;set;}
Public Map<string, integer> mSubTotalLB{get;set;}
Public Map<string, integer> mNeedCFF{get;set;}
Public Map<String, Integer> mTotalVer{get;set;}


public String previousYear;
public String currentYear;

Public Map<String, Integer> mTotalCF{get;set;} //used in 2nd report
Public Map<String, Integer> mSubTotalCF{get;set;}
Public Map<String, decimal> mPerCF{get;set;} 
Public Map<String, Integer> mVerTotalCF{get; set;}
Public Integer cfAllTotal{get;set;} 
Public Integer cfAllSubTotal{get;set;} 
Public Integer cfNoRatingTotal{get;set;} 
Public Integer cfNATotal{get;set;} 

public CFFResultsReport()
{
    
     listLOB = new list<string>();
     
     listLOB.add('GBAM');
     listLOB.add('GWIM');
     listLOB.add('Consumer');
     listLOB.add('GT&O');
     listLOB.add('CAO');
//listLOB.add('CEO'); //excluding CEO
     listLOB.add('CFO');
     listLOB.add('Legal');
     listLOB.add('Compliance');
     listLOB.add('Risk');
     listLOB.add('GMCA');
     listLOB.add('GHR');


      listRatings = new list<string>();
    listRatings.Add('Exemplar');
    listRatings.Add('Meets');
    listRatings.Add('Negative');
    listRatings.Add('Not Applicable');
    listRatings.Add('No Rating');
    listRatings.Add('N/A-Sit in CF');
   
 CFF_Setting__mdt[] cff_settings = [SELECT Feedaback_Year__c, DeveloperName FROM CFF_Setting__mdt];
map < String, String > cff_map = new map < String,  String > ();
for (CFF_Setting__mdt cff_data: cff_settings) {
    cff_map.put(cff_data.DeveloperName, cff_data.Feedaback_Year__c);
}

currentYear = cff_map.get('Current_Year'); //'2018'; //String.valueOf(Date.Today().Year());
previousYear = cff_map.get('Previous_Year'); //String.valueOf(Date.Today().Year()-1);


    String MapID;
    Integer cRatings;
    //1st report
    mTotalHor=new Map<String, Integer>();
    mTotalVer =new Map<String, Integer>();
   integer countVerTot =0;
       
    //**************Only Meets Across control functions********************
        
    Map<Id, Sobject> otherMeetsResult= new Map<Id, Sobject>([select   Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' AND (CFF_RATING__c ='Exemplar' OR CFF_RATING__c ='Negative' OR  CFF_RATING__c = 'No Rating')GROUP BY  Covered_Employee_Name__c ]);
   Set<Id> otherMeetsSet=new Set<id> ( otherMeetsResult.Keyset());

    Map<Id, Sobject> noRatingResult =new Map<Id, Sobject>([select  Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c !='Required'  GROUP By   Covered_Employee_Name__c]);
    Set<Id> noRatingSet=new Set<id> (noRatingResult.Keyset());


    Integer countMeets=0;
    mOnlyMeets = new Map<String, Integer>();
    String varLOB ='';
    list <AggregateResult> OnlyMeets =[select  LOB__c, Covered_Employee_Name__c from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c  ='Required' AND (CFF_RATING__c ='Meets') AND Covered_Employee_Name__c NOT IN:otherMeetsSet
     AND  Covered_Employee_Name__c NOT IN:noRatingSet   GROUP BY LOB__c, Covered_Employee_Name__c ];
    for(AggregateResult m:OnlyMeets)
    {
    if(varLOB == string.valueof(m.get('LOB__c')))
    {
        countMeets=countMeets+1;
    }
     else
     {
           if(varLOB =='')
           {
               countMeets=1;
               varLOB =string.valueof(m.get('LOB__c'));
           }
           else
           {
               mOnlyMeets.put(varLOB, countMeets); 
               varLOB =string.valueof(m.get('LOB__c'));
                countMeets=1;

           }
        }       
       }
           mOnlyMeets.put(varLOB, countMeets); 
     for(String c:listLOB )
     {
         if(  mOnlyMeets.containsKey(c))
         {
         }
         else
         {
            mOnlyMeets.put(c, 0);
         }
      countVerTot =  countVerTot+mOnlyMeets.get(c);
          }
      mTotalVer.put('Meets', countVerTot);

//*************end Only meets***************************

//************Starts 1 Exemplar*******************************
 Map<Id, Sobject> Exem1Result= new Map<Id, Sobject>([select   Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' AND CFF_RATING__c ='Exemplar' GROUP BY  Covered_Employee_Name__c having count(Id) =1]);
    Set<Id> Exem1Set=new Set<id> (Exem1Result.Keyset());
//All other ratings except Negative or No Rating----------------------------------

Map<Id, Sobject> otherResult= new Map<Id, Sobject>([select   Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' AND (CFF_RATING__c ='Negative' OR  CFF_RATING__c = 'No Rating')GROUP BY  Covered_Employee_Name__c ]);
    Set<Id> otherSet=new Set<id> (otherResult.Keyset());
list <AggregateResult> Only1Exem = [select  LOB__c, Covered_Employee_Name__c from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Covered_Employee_Name__c  IN:Exem1Set AND Covered_Employee_Name__c NOT IN:otherSet
 AND  Covered_Employee_Name__c NOT IN:noRatingSet   GROUP BY LOB__c, Covered_Employee_Name__c];
    varLOB ='';
    countMeets=0;
     mExampler1 = new Map<string, Integer>();
for(AggregateResult e:Only1Exem)
    {
    if(varLOB == string.valueof(e.get('LOB__c')))
    {
        countMeets=countMeets+1;
    }
     else
     {
           if(varLOB =='')
           {
               countMeets=1;
               varLOB =string.valueof(e.get('LOB__c'));
           }
           else
           {
               mExampler1.put(varLOB, countMeets); 
               varLOB =string.valueof(e.get('LOB__c'));
                countMeets=1;

           }
        }       
       }
    mExampler1.put(varLOB, countMeets); 
   countVerTot =0;
    for(String c:listLOB )
     {
         if(   mExampler1.containsKey(c))
         {
         }
         else
         {
             mExampler1.put(c, 0);
         }
     countVerTot =  countVerTot+mExampler1.get(c);
          }
 mTotalVer.put('Exemplar1', countVerTot);


//*******************end exemplar 1******************************


//************Starts 2 Exemplar*******************************
 Map<Id, Sobject> Exem2Result= new Map<Id, Sobject>([select   Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' AND CFF_RATING__c ='Exemplar' GROUP BY  Covered_Employee_Name__c having count(Id) >1]);
    Set<Id> Exem2Set=new Set<id> (Exem2Result.Keyset());
//All other ratings except NA----------------------------------

Map<Id, Sobject> otherResult2= new Map<Id, Sobject>([select   Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' AND (CFF_RATING__c ='Negative' OR  CFF_RATING__c = 'No Rating')GROUP BY  Covered_Employee_Name__c ]);
    Set<Id> otherSet2=new Set<id> (otherResult2.Keyset());
list <AggregateResult> Only2Exem = [select  LOB__c, Covered_Employee_Name__c from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Covered_Employee_Name__c  IN:Exem2Set AND Covered_Employee_Name__c NOT IN:otherSet2
 AND  Covered_Employee_Name__c NOT IN:noRatingSet    GROUP BY LOB__c, Covered_Employee_Name__c];
    varLOB ='';
    countMeets=0;
     mExampler2 = new Map<string, Integer>();
     mExamplerTot = new Map<string, Integer>();
     integer TotalExem = 0;
for(AggregateResult e:Only2Exem)
    {
    if(varLOB == string.valueof(e.get('LOB__c')))
    {
        countMeets=countMeets+1;
    }
     else
     {
           if(varLOB =='')
           {
               countMeets=1;
               varLOB =string.valueof(e.get('LOB__c'));
           }
           else
           {
               mExampler2.put(varLOB, countMeets); 
               varLOB =string.valueof(e.get('LOB__c'));
                countMeets=1;

           }
        }       
       }
    mExampler2.put(varLOB, countMeets); 
     countVerTot =0;
    Integer verExemTot =0;
    for(String c:listLOB )
     {
         if(   mExampler2.containsKey(c))
         {
                          
         }
         else
         {
             mExampler2.put(c, 0);
         }
     TotalExem  = mExampler1.get(c)+mExampler2.get(c);
      mExamplerTot.put(c, TotalExem);
      countVerTot =  countVerTot+mExampler2.get(c);
      verExemTot =verExemTot+mExamplerTot.get(c);
     }
 mTotalVer.put('Exemplar2', countVerTot);
 mTotalVer.put('ExemplarTotal', verExemTot);

//*******************end exemplar 2******************************

//****************Negative ******************************
mNegative = new Map<String, Integer>();
 Map<Id, Sobject> NegResult= new Map<Id, Sobject>([select   Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' AND CFF_RATING__c ='Negative' GROUP BY  Covered_Employee_Name__c ]);
    Set<Id> NegSet=new Set<id> (NegResult.Keyset());
//All other ratings except Exemlar and No rating ----------------------------------

Map<Id, Sobject> otherNegResult= new Map<Id, Sobject>([select   Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' AND (CFF_RATING__c ='Exemplar' OR  CFF_RATING__c = 'No Rating')GROUP BY  Covered_Employee_Name__c ]);
    Set<Id> otherNegSet=new Set<id> (otherNegResult.Keyset());
list <AggregateResult> NegativeList = [select  LOB__c, Covered_Employee_Name__c from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Covered_Employee_Name__c  IN:NegSet AND Covered_Employee_Name__c NOT IN:otherNegSet
 AND  Covered_Employee_Name__c NOT IN:noRatingSet  GROUP BY LOB__c, Covered_Employee_Name__c];

varLOB ='';
    countMeets=0;
       for(AggregateResult e:NegativeList )
    {
    if(varLOB == string.valueof(e.get('LOB__c')))
    {
        countMeets=countMeets+1;
    }
     else
     {
           if(varLOB =='')
           {
               countMeets=1;
               varLOB =string.valueof(e.get('LOB__c'));
           }
           else
           {
               mNegative.put(varLOB, countMeets); 
               varLOB =string.valueof(e.get('LOB__c'));
                countMeets=1;

           }
        }       
       }
    mNegative.put(varLOB, countMeets); 
    countVerTot =0;

    for(String c:listLOB )
     {
         if(   mNegative.containsKey(c))
         {
                          
         }
         else
         {
             mNegative.put(c, 0);
         }
          countVerTot =  countVerTot+mNegative.get(c);

          }
     mTotalVer.put('Negative', countVerTot);

//**************end Negative***************************

//***************Just only NA Ratings******************
  Map<Id, Sobject> onlyNA= new Map<Id, Sobject>([select Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' and (cff_Rating__c='Not Applicable' OR  cff_Rating__c ='N/A-No Interaction' OR  cff_Rating__c ='N/A-Other' OR  cff_Rating__c = 'N/A-Sit in CF') GROUP BY  Covered_Employee_Name__c  ]);
   Set<Id> OnlyNASet=new Set<id> (onlyNA.Keyset());

 Map<Id, Sobject> OtherNA= new Map<Id, Sobject>([select Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' and (cff_Rating__c='No Rating' OR cff_Rating__c='Meets' OR  cff_Rating__c ='Negative' OR  cff_Rating__c = 'Exemplar') GROUP BY  Covered_Employee_Name__c  ]);
   Set<Id> OtherNASet=new Set<id> (OtherNA.Keyset());

list <AggregateResult> NAAllList = [select  LOB__c, Covered_Employee_Name__c from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Covered_Employee_Name__c  IN:OnlyNASet AND Covered_Employee_Name__c NOT IN:otherNASet
 AND  Covered_Employee_Name__c NOT IN:noRatingSet  GROUP BY LOB__c, Covered_Employee_Name__c];
mNA = new Map<String, Integer>();
varLOB ='';
    countMeets=0;
       for(AggregateResult nl:NAAllList )
    {
    if(varLOB == string.valueof(nl.get('LOB__c')))
    {
        countMeets=countMeets+1;
    }
     else
     {
           if(varLOB =='')
           {
               countMeets=1;
               varLOB =string.valueof(nl.get('LOB__c'));
           }
           else
           {
               mNA.put(varLOB, countMeets); 
               varLOB =string.valueof(nl.get('LOB__c'));
                countMeets=1;

           }
        }       
       }
    mNA.put(varLOB, countMeets); 
    countVerTot =0;

for(String c:listLOB )
     {
         if(   mNA.containsKey(c))
         {
                          
         }
         else
         {
             mNA.put(c, 0);
         }
          countVerTot =  countVerTot+mNA.get(c);

          }
     mTotalVer.put('NA', countVerTot);

//******************Negative & Exemplar ****************
 mSubTotalLB = new Map<String, Integer>();
mNegativeExemp = new Map<string, Integer>();
Map<Id, Sobject> NegExemResult= new Map<Id, Sobject>([select   Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' AND  (CFF_RATING__c='Exemplar') GROUP BY  Covered_Employee_Name__c ]);
    Set<Id> NegExemSet=new Set<id> (NegExemResult.Keyset());
//All other ratings except Exemlar and No rating ----------------------------------

Map<Id, Sobject> otherNEResult= new Map<Id, Sobject>([select   Covered_Employee_Name__c Id from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' AND  CFF_RATING__c = 'No Rating' GROUP BY  Covered_Employee_Name__c ]);
    Set<Id> otherNESet=new Set<id> (otherNEResult.Keyset());
list <AggregateResult> NEList = [select  LOB__c, Covered_Employee_Name__c from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Covered_Employee_Name__c  IN:NegExemSet AND Covered_Employee_Name__c  IN:NegSet AND Covered_Employee_Name__c NOT IN:otherNESet
 AND  Covered_Employee_Name__c NOT IN:noRatingSet  GROUP BY LOB__c, Covered_Employee_Name__c];

varLOB ='';
    countMeets=0;
    Integer subTotal = 0;
       for(AggregateResult e:NEList )
    {
    if(varLOB == string.valueof(e.get('LOB__c')))
    {
        countMeets=countMeets+1;
    }
     else
     {
           if(varLOB =='')
           {
               countMeets=1;
               varLOB =string.valueof(e.get('LOB__c'));
           }
           else
           {
               mNegativeExemp.put(varLOB, countMeets); 
               varLOB =string.valueof(e.get('LOB__c'));
                countMeets=1;

           }
        }       
       }
    mNegativeExemp.put(varLOB, countMeets); 
     countVerTot =0;


    for(String c:listLOB )
     {
         if(    mNegativeExemp.containsKey(c))
         {
                          
         }
         else
         {
            mNegativeExemp.put(c, 0);
         }
          subTotal =  mOnlyMeets.get(c)+mExamplerTot.get(c)+ mNegative.get(c)+mNegativeExemp.get(c)+mNA.get(c); 
           mSubTotalLB.put(c, subTotal); 
            countVerTot =  countVerTot+mNegativeExemp.get(c);

          }

 mTotalVer.put('NegativeExem', countVerTot);

//*************end Negative & Exemplar ****************

//*********Start of Need CFF***************
mNeedCFF = new Map<String, Integer>();
 mTotalHor = new Map<String, Integer>();
 list <AggregateResult> needCFFList = [select  LOB__c,  Covered_Employee_Name__c from Control_Function_Feedback__c   where Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND CFF_RATING__c = 'No Rating' AND Feedback_Status__c ='Required' GROUP By  LOB__c,  Covered_Employee_Name__c];
 varLOB ='';
 countMeets=0;
 integer allTot = 0;
    for(AggregateResult e:needCFFList )
    {
    if(varLOB == string.valueof(e.get('LOB__c')))
    {
        countMeets=countMeets+1;
    }
     else
     {
           if(varLOB =='')
           {
               countMeets=1;
               varLOB =string.valueof(e.get('LOB__c'));
           }
           else
           {
               mNeedCFF.put(varLOB, countMeets); 
               varLOB =string.valueof(e.get('LOB__c'));
                countMeets=1;

           }
        }       
       }
    mNeedCFF.put(varLOB, countMeets); 
    countVerTot=0;
    integer subTotalVer=0;
    integer TotalVer =0;
    for(String c:listLOB )
     {
         if(    mNeedCFF.containsKey(c))
         {
                          
         }
         else
         {
            mNeedCFF.put(c, 0);
         }
         allTot = mSubTotalLB.get(c) +mNeedCFF.get(c);
          mTotalHor.put(c, allTot);
          countVerTot =  countVerTot+ mNeedCFF.get(c);
            subTotalVer = subTotalVer+ mSubTotalLB.get(c);
           TotalVer = TotalVer+mTotalHor.get(c);

      }

 mTotalVer.put('NeedCFF', countVerTot);
 mTotalVer.put('SubTotal', subTotalVer);
mTotalVer.put('Total', TotalVer);



 //*********************end of Need CFF************************8      

 


    //2nd report CFF by Rating along with percentage
                           //Add control functions to list
      listContFun = new list<string>();
    listContFun.Add('Corporate Audit');
    listContFun.Add('CFO Group');
    listContFun.Add('Global Compliance');
    listContFun.Add('Global Human Resources');
    listContFun.Add('Legal');
    listContFun.Add('Global Risk');
    list<string> listSubRatings = new list<String>();
    listSubRatings.add('Meets');
    listSubRatings.add('Exemplar');
     listSubRatings.add('Negative');
     listSubRatings.add('Not Applicable');
     //listSubRatings.add ('N/A-Sit in CF');

     
    //******Calculate Total first***************
     cfAllTotal =0;
     mTotalCF = new Map<String, Integer>();
     list <AggregateResult>  cfTotal = [select  Audit_Type__c, count(Id) cRate from Control_Function_Feedback__c   where  Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' GROUP BY Audit_Type__c];
    for(AggregateResult c:cfTotal)
    {
        mapID =  string.valueof(c.get('Audit_Type__c'));
         cRatings = Integer.valueof(c.get('cRate'));

        mTotalCF.put(mapID , cRatings);
        cfAllTotal = cfAllTotal +cRatings;
    }

    for(String mt:listContFun)
    {
        if(mTotalCF.containsKey(mt))
        {
        }
        else
        {
            mTotalCF.put(mt, 0);
        }
    }
       //Calculate for Exemplar, Meets, Negative, N/A – Sit in CF
    mCFbyRating= new Map<String, Integer>();
     list <AggregateResult>  cfRating = [select  Audit_Type__c,  CFF_Rating__c, count(Id) cRate from Control_Function_Feedback__c   where  Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND
     (CFF_Rating__c ='Meets' OR CFF_Rating__c ='Exemplar' OR CFF_Rating__c ='Negative' OR CFF_Rating__c ='N/A-Sit in CF') and Feedback_Status__c ='Required' GROUP BY Audit_Type__c,CFF_Rating__c];
    for(AggregateResult m:cfRating)
    {
     mapID =  string.valueof(m.get('Audit_Type__c'))+string.valueof(m.get('CFF_Rating__c'));
     cRatings = Integer.valueof(m.get('cRate'));
     mCFbyRating.put(mapID, cRatings); 
     System.debug(mapID + ' '+cRatings);
     }
    //Calculate for NA - No Interaction or Other
        cfNATotal =0;
     list <AggregateResult>  cfNA = [select  Audit_Type__c,  count(Id) cRate from Control_Function_Feedback__c   where  Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND
     (CFF_Rating__c = 'Not Applicable' OR CFF_Rating__c ='N/A-No Interaction' OR CFF_Rating__c ='N/A-Other') and Feedback_Status__c ='Required' GROUP BY Audit_Type__c];
    for(AggregateResult m:cfNA )
    {
     mapID =  string.valueof(m.get('Audit_Type__c'))+'Not Applicable';
     cRatings = Integer.valueof(m.get('cRate'));
     mCFbyRating.put(mapID, cRatings); 
     System.debug(mapID + ' '+cRatings);
     cfNATotal=cfNATotal+cRatings;
     }
     
     mPerCF = new Map<String, decimal>();
    //integer perCalc ;
    decimal perCalc;
    decimal perDivide;
       integer totalSubHor = 0;
    mSubTotalCF = new Map<String, Integer>();
    cfAllSubTotal =0;

    for (string cf:listContFun)
    {
          for(String r: listSubRatings)
            {
                if(mCFbyRating.ContainsKey(cf+r))
                {
                    
                   perDivide = integer.valueof((mTotalCF.get(cf))); 
                    //perCalc = (mCFbyRating.get(cf+r)*100)/mTotalCF.get(cf);
                   perCalc= ( mCFbyRating.get(cf+r)/ perDivide) *100;
                    perCalc= perCalc.setScale(1,System.RoundingMode.HALF_UP);
                    system.debug('LG Test'+mCFbyRating.get(cf+r)/ perDivide );
                                        mPerCF.put(cf+r, perCalc);
                }
                else
                {
                    mCFbyRating.put(cf+r, 0);
                     mPerCF.put(cf+r, 0.0);
                }
            totalSubHor = totalSubHor+mCFbyRating.get(cf+r);
            }
             mSubTotalCF.put(cf,totalSubHor);
             cfAllSubTotal = cfAllSubTotal + totalSubHor;
             totalSubHor = 0;

    }

       for(String l: listContFun)
            {
                if( mSubTotalCF .ContainsKey(l))
                {
                                                    }
                else
                {
                   mSubTotalCF.put(l, 0);
                                     }
            }

//check for Sit in CF
  for (string cf:listContFun)
    {
       if(mCFbyRating.ContainsKey(cf+'N/A-Sit in CF'))
                {
                    
                }
                else
                {
                  mCFbyRating.put(cf+'N/A-Sit in CF', 0);
                  
                }
                 //mCFbyRating.put(cf+'Sits in CF', 0);
                //cfNoRatingTotal =cfNoRatingTotal +  mCFbyRating.get(cf+'Not Applicable');
            }
    //Calculate for No Rating 
     cfNoRatingTotal = 0;
     list <AggregateResult>  cfNoRating = [select  Audit_Type__c,  count(Id) cRate from Control_Function_Feedback__c   where  Category__c ='CAT 2' AND Feedback_Status__c='Required' AND Feedback_Year__c =:currentYear and CFF_Rating__c='No Rating' GROUP BY Audit_Type__c];
    for(AggregateResult m: cfNoRating )
    {
     mapID =  string.valueof(m.get('Audit_Type__c'))+'No Rating';
     cRatings = Integer.valueof(m.get('cRate'));
     mCFbyRating.put(mapID, cRatings); 
     System.debug(mapID + ' '+cRatings);
     }
   
   for (string cf:listContFun)
    {
       if(mCFbyRating.ContainsKey(cf+'No Rating'))
                {
                    
                }
                else
                {
                  mCFbyRating.put(cf+'No Rating', 0);
                  
                }
                 //mCFbyRating.put(cf+'Sits in CF', 0);
                cfNoRatingTotal =cfNoRatingTotal +  mCFbyRating.get(cf+'No Rating');
            }

       //Calculate Total for Vertical
    mVerTotalCF = new Map<String, Integer> ();
    list <AggregateResult>  cfVer =  [select  CFF_Rating__c,   count(Id) cRate from Control_Function_Feedback__c   where  Category__c ='CAT 2' AND Feedback_Year__c =:currentYear AND Feedback_Status__c ='Required' GROUP BY  CFF_Rating__c];
    for(AggregateResult m:cfVer)
    {
     mapID =  string.valueof(m.get('CFF_Rating__c'));
     cRatings = Integer.valueof(m.get('cRate'));
      mVerTotalCF .put(mapID, cRatings); 
         }
     
      for(String r: listRatings)
            {
                if(mVerTotalCF .ContainsKey(r))
                {
                                                    }
                else
                {
                    mVerTotalCF .put(r, 0);
                                     }
            }

  }
  
  public PageReference openResultExcel() {
system.debug('IN OPEN AS EXCEL METHOD-->');
PageReference newPage = Page.CFF_ResultsByLOBXLS;
return newPage.setRedirect(false);
}

}