public class IRFileUploader

{

    public IRFileUploader(ApexPages.StandardController controller) {

    }


 
  Transient public Blob contentFile{get;set;}
   public string sURLs {get;set;}
   public string nameFile{get;set;}
  public Pagereference irReadFile()
    {
    
    
    string nameFile1;
    String[] filelines = new String[]{};
       //Get Picklist values for Behaviour and Result rating
    String[] irBehRating = new String[]{};
    String[] irResultRating = new String[]{};
    String pckval;
    String pckval1;
     Schema.DescribeSObjectResult objSchema = Incentive_Rationale__c.sObjectType.getDescribe();
     Map<String, Schema.SObjectField> fieldMap = objSchema.fields.getmap();
    Schema.DisplayType fielddataType = fieldMap.get('Behavior_Rating__c').getDescribe().getType();  
    if(fielddataType == Schema.DisplayType.Picklist)
            {
                List<Schema.Picklistentry>fld =fieldmap.get('Behavior_Rating__c').getDescribe().getpicklistValues();
                pckval='';
                for(integer i=0;i<fld.Size();i++)
                {
                    pckval= pckval+fld[i].getvalue()+',';
                }    
                 irBehRating  = pckval.split(',');
            }
            
            Schema.DisplayType fielddataType1 = fieldMap.get('Results_Rating__c').getDescribe().getType();
            if(fielddataType1 == Schema.DisplayType.Picklist)
            {
                List<Schema.Picklistentry>fld =fieldmap.get('Results_Rating__c').getDescribe().getpicklistValues();
                pckval1 = '';
                for(integer i=0;i<fld.Size();i++)
                {
                    pckval1= pckval1+fld[i].getvalue()+',';
                } 
                irResultRating = pckval1.split(',');
            }
   try{
    nameFile1 = contentFile.toString();
    contentFile = null;
 
    //Process input file
    nameFile1 = nameFile1.replace('""' ,'DBLQ');
    nameFile1 = nameFile1.replace(',"' ,',"DBLQT');
    nameFile1 = nameFile1.replace('",',  'DBLQT",');
    String [] FileStr =nameFile1.split('"');
        //FileUploader.safeSplit(nameFile1, '"');
    
    if(FileStr[0] == 'ERROR')
        {
        sURLs = '/apex/CFFUploads?Upload=Failed&ErrorMsg='+ 'Large file Size' ;
        PageReference pageRef = new PageReference(sURLs);
       pageRef.setRedirect(true);
        return pageRef;

        }
        String NewContent='';
                 integer k;   
    for(k=0; k<FileStr.size(); k++)
    {
        if(FileStr[k].contains('DBLQT'))
        {
        if(FileStr[k].contains('\n'))
           FileStr[k]= FileStr[k].replace('\n',  'NLB');
        if(FileStr[k].contains(','))
           FileStr[k]= FileStr[k].replace(',', 'COMMA');
            FileStr[k]=null;
            }
     NewContent = NewContent+FileStr[k];
     }
     FileStr=NewContent.split('\n');
      //filelines = FileUploader.safeSplit(NewContent, '\n');
        if(filelines[0] == 'ERROR')
        {
        sURLs = '/apex/IRUploads?Upload=Failed&ErrorMsg='+ 'Large file Size' ;
        PageReference pageRef = new PageReference(sURLs);
        pageRef.setRedirect(true);
        return pageRef;

        }
     //split lines to fields
     integer iCSVCount =0;
     integer iUpdCount =0;
     String sFailedUpload = ''; //for failed ir ids

   String[] inputvalues = new String[]{};
   boolean isValidBehRating = false;
   boolean isValidResultRating = false;

List<Incentive_Rationale__c> IrToUpload = new List<Incentive_Rationale__c>();
 Incentive_Rationale__c irData;

        for (Integer i=1;i<filelines.size();i++)
        {
           // String[] inputvalues = new String[]{};
            //split the Record by fields based on the delimiter
            //system.debug('File Line'+i+filelines[i]);
            if((filelines[i] != '') && (filelines[i].replaceAll(',','').trim().length() != 0))
            {
            //system.debug('FileLine:'+filelines[i]);
            iCSVCount = iCSVCount+1;
            inputvalues = filelines[i].split(',');
            
             //To check the csv rating with picklist values
            for(Integer j=0; j<irBehRating.size() && isValidBehRating == false; j++)
            {
                if(irBehRating[j] == inputvalues[1])
                    isValidBehRating = true;
            }
            
            //To check the csv status with picklist values
            for(Integer j=0; j<irResultRating.size() && isValidResultRating  == false; j++)
            {
                if(irResultRating[j] == inputvalues[2])
                    isValidResultRating = true;
            }
             if(inputvalues[4] != '')
            {
                inputvalues[4]=inputvalues[4].replace('DBLQT', '');
                inputvalues[4]=inputvalues[4].replace('DBLQ', '"');
                inputvalues[4]=inputvalues[4].replace('NLB', '\n');
                 inputvalues[4]=inputvalues[4].replace('COMMA', ',');

            }
            if(inputvalues[5] != '')
            {
                inputvalues[5]=inputvalues[5].replace('DBLQT', '');
                inputvalues[5]=inputvalues[5].replace('DBLQ', '"');
                inputvalues[5]=inputvalues[5].replace('NLB', '\n');
                 inputvalues[5]=inputvalues[5].replace('COMMA', ',');

            }
            system.debug(inputvalues[0]+'LG'+inputvalues[1]+'LG'+inputvalues[2]+'LG'+inputvalues[3]+'LG'+inputvalues[4]+'LG'+inputvalues[5]);
            if(inputvalues[4].length()<3001 && inputvalues[5].length()<256)
            {
                irData = new Incentive_Rationale__c();
                if(inputvalues[0].length() > 0)
                    {
                    irData.Id = inputvalues[0];
                    irData.Results_Rating__c = inputvalues[1];
                    irData.Behavior_Rating__c = inputvalues[2];
                    irData.Conduct__c =inputValues[3];
                    irData.Rationale__c = inputValues[4];
                     irData.Rationale_Reason__c =inputValues[5];
                    
                     IrToUpload.add(irData);
                     iUpdCount++;
                    }
            } 
            else {
                sFailedUpload = inputvalues[0] + ',' +sFailedUpload;}
            } //end if fileline
           if(IrToUpload.size() == 500){
              update IrToUpload;
              IrToUpload.clear();
              }
            } //end of for loop
          if(IrToUpload.size() >0 ){
          update IrToUpload;
          IrToUpload.clear();
            }
      filelines = null;
      system.debug('Comparision: '+ iCSVCount+ '=='+iUpdCount );
        if ((iCSVCount == iUpdCount) && (iCSVCount != 0))
            sURLs = '/apex/CFFUploads?Upload=Success';
        else if((iCSVCount == 0) || (iCSVCount <0) )
            sURLs = '/apex/CFFUploads?Upload=Failed&ErrorMsg='+'File format is not correct';
        else 
            sURLs = '/apex/CFFUploads?Upload=Partial&CFFId='+ sFailedUpload ;
        PageReference pageRef = new PageReference(sURLs);
        pageRef.setRedirect(true);
        return pageRef;
     }
        
     catch(system.Exception ex)
        {
        system.debug('Line Number:'+ ex.getLineNumber());
        sURLs = '/apex/CFFUploads?Upload=Failed&ErrorMsg='+ 'Exceeded length or have special characters' ;
        PageReference pageRef = new PageReference(sURLs);
        pageRef.setRedirect(true);
        return pageRef;

        }
          } //Read file

}