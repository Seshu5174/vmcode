Trigger Phone_trigger on Teacher__c (before insert, before update) {
    for(Teacher__c t: trigger.new){
        if(t.phone__c == null)
        t.phone__c.addError('Please Insert Phone number');
        }
}