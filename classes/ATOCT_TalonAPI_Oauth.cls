public with sharing class ATOCT_TalonAPI_Oauth
{

    public static talonAPIAuthenticationResponse doAuth()
    {
      
      ATOCT_Auth_Setting__mdt custmeta = new ATOCT_Auth_Setting__mdt();
      custmeta = [Select Client_Id__c, Client_Secret__c, EndPoint__c From ATOCT_Auth_Setting__mdt WHERE DeveloperName='Talon_Api_Test' WITH SECURITY_ENFORCED];
        
        Http httpMethod = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(custmeta.EndPoint__c);
        request.setMethod('POST');
        blob headerCredentials = Blob.valueof(custmeta.Client_Id__c+':'+custmeta.Client_Secret__c);
        string authHeader = 'Basic '+EncodingUtil.base64Encode(headerCredentials);
        request.setHeader('Content-Type','application/json'); 
        if(!Test.isRunningTest())
        request.setHeader('Authorization ', authHeader); 
        HttpResponse response = httpMethod.send(request);
        
      
      talonAPIAuthenticationResponse talonAuthResponse = new talonAPIAuthenticationResponse();
          
          talonAuthResponse.statusMessage = response.getStatus();
          talonAuthResponse.statusCode = response.getStatusCode();
          talonAuthResponse.statusResponse = response.getbody();
          
          if(response.getStatusCode() == 200)
          {
              string result = response.getbody();
              authResponseParser authResponse = new authResponseParser();
              authResponse = authResponse.parse(result);
              talonAuthResponse.accessToken = authResponse.access_token;
          }
      return talonAuthResponse;   
    }
    
    public class TalonAPIAuthenticationResponse
    {
        public string accessToken{get;set;}
        public string statusMessage{get;set;}
        public decimal statusCode{get;set;}
        public string statusResponse{get;set;}
    }
    
    public class AuthResponseParser
    {
        public string access_token;
        
        public AuthResponseParser parse(string json)
        {
            return (AuthResponseParser)system.Json.deserialize(json, AuthResponseParser.class);
        }
        
    }
   
}