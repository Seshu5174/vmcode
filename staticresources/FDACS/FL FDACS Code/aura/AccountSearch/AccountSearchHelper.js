// Copyright 2016, Sales Engineering, Salesforce.com Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// - Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// - Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// - Neither the name of the salesforce.com nor the names of its contributors
//   may be used to endorse or promote products derived from this software
//   without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
({
    initSearchParams : function(component) {
        var self = this;  // safe reference
        
        var initAction = component.get("c.initSearchParams");
        initAction.setCallback(self, function(a) {
            component.set("v.searchParams", a.getReturnValue());
        });
        
        // Enqueue the action
        $A.enqueueAction(initAction);
    },
    getCities: function(component) {
        var action = component.get("c.getCities");
        
        var self = this;
        action.setCallback(this, function(actionResult) {
            component.set("v.cities", actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    getSicDesc: function(component) {
        var action = component.get("c.getSicDesc");
        
        var self = this;
        action.setCallback(this, function(actionResult) {
            component.set("v.sicDesc", actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    getAccountList: function(component, searchParams) {
        var action = component.get("c.getAccounts");
        action.setParams({
            "searchParams": JSON.stringify(searchParams) 
        });
        //alert(JSON.stringify(action.getParams());
        
        //Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
            console.log("actionResult: ", JSON.stringify(actionResult));
            component.set("v.accounts", actionResult.getReturnValue());
            
            var markersLayerList = component.get("v.markersLayerList");
            var markersLayer = markersLayerList[0];
            markersLayer.clearLayers();
            
            var locationCoor = []; 
            
            var accounts = actionResult.getReturnValue();   
            
            for (var i=0; i<accounts.length; i++) {
                var account = accounts[i];
                var latLng = [account.BillingLatitude, account.BillingLongitude];
                locationCoor[i] = latLng;
                
                var popup = L.popup()
                .setLatLng(latLng)
                .setContent('<h3><a href=\"/produce/s/detail/' + account.Id + '\">' + account.Name + '</a></h3>' +
                            '<br/>' + account.BillingStreet +
                            '<br/>' + account.BillingCity + ', ' + account.BillingState + ' ' + account.BillingPostalCode);
                
                
                var marker = L.marker(latLng, {account: account});
                marker.bindPopup(popup);
                
                markersLayer.addLayer(marker);
            }  
            
            var map = component.get("v.map");
            var bounds = new L.latLngBounds(locationCoor);
            map.fitBounds(bounds, {padding: [50,50]});
        });
        $A.enqueueAction(action);
    }
})