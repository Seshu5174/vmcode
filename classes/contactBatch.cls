Global class contactBatch implements Database.Batchable<sobject> {
    
    global Database.QueryLocator start (Database.BatchableContext BC){
        
        String query = 'Select Name, sex__c  From Teacher__c';
        return Database.getQueryLocator(query);
        
    }      
	
    global void execute (Database.BatchableContext BC, List<Teacher__c> scope){
      
        for(Teacher__c c: scope){
            if(c.Sex__c==null)
        c.sex__c ='Male';
        }
        update scope;
    }
    
    global void finish (Database.BatchableContext BC){
        
    }
}