public class OPenInChromeController {
    public Boolean isChrome{get;set;}
    public OPenInChromeController(){
        isChrome = false;
    }
    public pagereference renderAction(){
        isChrome = true;
        pagereference pref = new pagereference('/apex/OpenInChromeOnload');
        pref.setRedirect(false);
        return pref;
    }
     public pagereference unrenderAction(){
          isChrome = false;
        pagereference pref = new pagereference('/apex/OpenInChrome');
        pref.setRedirect(false);
        return pref;
     }

}