public class searchAccount {
    public String strAccountName {get; set;}
    public List<Account> lstAccount {get; set;}
    
    public searchAccount()
    {
        strAccountName = '';
        lstAccount = new List<Account>();
    }
    
    public void searchAccount()
    {
        lstAccount = new List<Account>();
        String newSearchText;
        if(String.isNotBlank(strAccountName))
            newSearchText = '%'+string.escapeSingleQuotes(strAccountName)+'%';
            
        for(Account objAccount : [SELECT Id, Name 
                                    From Account 
                                    WHERE Name LIKE: newSearchText 
                                    ]) 
            {
                lstAccount.add(objAccount);
            }
    }

}