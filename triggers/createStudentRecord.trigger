trigger createStudentRecord on Contact (before update) {
 
    List<students__c> stds = new List<students__c>();
    
    for (contact c: trigger.new){
        
        students__c s = new students__c();
        s.name = c.ID;
        stds.add(s);
    }

    insert stds;
}