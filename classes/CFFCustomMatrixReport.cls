public with sharing class CFFCustomMatrixReport{

Public list <string> listLOB{get;set;}
Public list <string> listContFun{get;set;}
Public list <string> listRatings{get;set;}
Public Map<string, integer> RatingFacts{get;set;}
Public Map<string, integer> mTotalHor{get;set;}   //total for each lob by CF
Public Map<string, integer> mTotalVer{get;set;} ////total for each lob by Rating
Public Map<string, integer> mSubTotal{get;set;} //Sub Total
Public Integer TotalVal{get;set;}
Public Integer subAllTotal{get;set;}
Public Integer noRatingTotal{get;set;}
Public Integer NATotal{get;set;}
public String previousYear;
public String currentYear;

  Public string ChkMapKey{get;set;}
 Public string ChkMapKeyVer{get;set;}
 Public string ChkMapKeyFS{get;set;}
 public string byCFF{get;set;}

Public decimal dispPercent;

//Variables for Enterprise report
Public Map<string, integer> lobRatingFacts{get;set;}
Public Map<string, integer> mlobTotalHor{get;set;}  
Public Map<string, integer> mlobTotalVer{get;set;} 
Public Integer allLOBTotal {get;set;} 
Public Integer entNATotal {get;set;} 

Public CFFCustomMatrixReport()
{
    //Apexpages.currentPage().getHeaders().put('content-disposition', 'filename=CFF_result.xlsx');
    listLOB = new list<string>();
   
    listLOB.add('GBAM');
    listLOB.add('GWIM');
    listLOB.add('Consumer');
    listLOB.add('GT&O');
    listLOB.add('CAO');
//listLOB.add('CEO'); //excluding CEO
    listLOB.add('CFO');
    listLOB.add('Legal');
    listLOB.add('Compliance');
    listLOB.add('Risk');
    listLOB.add('GMCA');
    listLOB.add('GHR');

        CFF_Setting__mdt[] cff_settings = [SELECT Feedaback_Year__c, DeveloperName FROM CFF_Setting__mdt];
        map < String, String > cff_map = new map < String,  String > ();
        for (CFF_Setting__mdt cff_data: cff_settings) {
            cff_map.put(cff_data.DeveloperName, cff_data.Feedaback_Year__c);
        }

        currentYear = cff_map.get('Current_Year'); //'2018'; //String.valueOf(Date.Today().Year());
        previousYear = cff_map.get('Previous_Year'); //String.valueOf(Date.Today().Year()-1);
        
        
    listContFun = new list<string>();
    listContFun.Add('Corporate Audit');
    listContFun.Add('CFO Group');
    listContFun.Add('Global Compliance');
    listContFun.Add('Global Human Resources');
    listContFun.Add('Legal');
    listContFun.Add('Global Risk');
  
    listRatings = new list<string>();
    listRatings.Add('Exemplar');
    listRatings.Add('Meets');
    listRatings.Add('Negative');
    listRatings.Add('Not Applicable');
    listRatings.Add('No Rating');
    listRatings.Add('N/A-Sit in CF');
    
    
    
    RatingFacts = new Map<string, integer>();
    mTotalHor = new Map<string, integer>();
    mTotalVer = new Map<string, integer>();
    
    mSubTotal= new Map<string, integer>();

  
    byCFF =ApexPages.currentPage().getParameters().get('CFFName');
    if(byCFF == null)
    {
        callEnterpriseLevel();
    }
    else
    {
    Integer cRatings;
    String mapID;
   list <AggregateResult> grpRating;
    grpRating = [select  LOB__c,  CFF_Rating__c, count(Id) cRate from Control_Function_Feedback__c   where LOB__c != 'CEO' AND Audit_Type__c=:byCFF AND Category__c ='CAT 2' AND Feedback_Year__c =:currentYear 
    AND (CFF_Rating__c ='Exemplar' OR CFF_Rating__c ='Meets' OR  CFF_Rating__c ='Negative' OR  CFF_Rating__c= 'N/A-Sit in CF' ) GROUP BY LOB__c,CFF_Rating__c];
    for(AggregateResult m:grpRating)
    {
     mapID =  string.valueof(m.get('LOB__c'))+string.valueof(m.get('CFF_Rating__c'));
     cRatings = Integer.valueof(m.get('cRate'));
     RatingFacts.put(mapID, cRatings); 
     System.debug(mapID + ' '+cRatings);
             }
       ChkMapKey = string.valueof(RatingFacts.keyset()).replace('{', '').replace('{', ''); 
       
       //Calcutae No Rating
        noRatingTotal =0;
    list <AggregateResult> noRatinglist = [select  LOB__c, count(Id) cRate from Control_Function_Feedback__c   where LOB__c != 'CEO' AND Audit_Type__c=:byCFF AND Category__c ='CAT 2' AND Feedback_Year__c =:currentYear  AND CFF_Rating__c='No Rating' GROUP BY LOB__c];
        for(AggregateResult n:noRatinglist)
    {
     mapID =  string.valueof(n.get('LOB__c'))+'No Rating';
     cRatings = Integer.valueof(n.get('cRate'));
     RatingFacts.put(mapID, cRatings); 
     noRatingTotal = noRatingTotal +cRatings;
             }
            //Calculate total for Na --
       NATotal = 0;
        list <AggregateResult> grpNA = [select  LOB__c,  count(Id) cRate from Control_Function_Feedback__c   where LOB__c != 'CEO' AND Audit_Type__c=:byCFF AND Category__c ='CAT 2' AND Feedback_Year__c =:currentYear 
        AND (CFF_Rating__c ='N/A-No Interaction' OR CFF_Rating__c = 'N/A-Other' OR CFF_Rating__c='Not Applicable') GROUP BY LOB__c];
    for(AggregateResult m:grpNA)
    {
     mapID =  string.valueof(m.get('LOB__c'))+'Not Applicable';
     cRatings = Integer.valueof(m.get('cRate'));
     RatingFacts.put(mapID, cRatings); 
     NATotal = NATotal+cRatings;
     System.debug(mapID + ' '+cRatings);
             }

       //Total Horizontal ---------
      list <AggregateResult> totByLB = [select LOB__c, count(Id) tRate FROM Control_Function_Feedback__c  where LOB__c != 'CEO' AND Audit_Type__c=:byCFF AND Category__c ='CAT 2' AND Feedback_Year__c =:currentYear   GROUP BY  LOB__c];
      Integer hrT;
      for(AggregateResult bCF:totByLB)
      {
           hrT = Integer.valueof(bCF.get('tRate'));
          mTotalHor.put(String.valueOf( bCF.get('LOB__c')), hrT);
      }
            
        for(String hrLB: listLOB)
        {
             if( mTotalHor.ContainsKey(hrLB))
             {
             }
             else
             {
                  mTotalHor.put(hrLB, 0);
             }

        }                    
    
      //Total Vertical Values------
      list <AggregateResult> totByRating=[select CFF_Rating__c, count(Id) tCount from Control_Function_Feedback__c  where LOB__c != 'CEO' AND Audit_Type__c=:byCFF AND Category__c ='CAT 2' AND  Feedback_Year__c =:currentYear  AND (CFF_Rating__c ='Exemplar' OR CFF_Rating__c ='Meets' OR  CFF_Rating__c ='Negative' OR  CFF_Rating__c= 'N/A-Sit in CF' )  GROUP BY CFF_Rating__c];
       for(AggregateResult bRt:totByRating)
      {
          mTotalVer.put(String.valueOf( bRt.get('CFF_Rating__c')), Integer.valueof(bRt.get('tCount')));
      }
         
      for(String vrRating: listRatings)
        {
             if( mTotalVer.ContainsKey(vrRating))
             {
                              }
             else
             {
                   mTotalVer.put(vrRating, 0);
             }

        }      
      //ChkMapKeyVer = string.valueof(mTotalVer.keyset()).replace('{', '').replace('{', ''); 
    
     
   
 // All Total Values----------
 list <AggregateResult> totCount = [Select Count(Id) allTotal FROM Control_Function_Feedback__c where LOB__c != 'CEO'  AND Audit_Type__c =:byCFF AND Category__c ='CAT 2' AND  Feedback_Year__c =:currentYear ]; 
    for(AggregateResult a:totCount )
      {
        TotalVal = Integer.valueof(a.get('allTotal')) ;   
         }


    for(String c:listLOB )
     {
         for(String r: listRatings)
         {
              if(RatingFacts.ContainsKey(c+r))
              {
              //calculate % with total
                system.debug('Key contains:'+c+r);
                }
              else
              {
              RatingFacts.put(c+r, 0);
               system.debug('No Key contains:'+c+r);

              
              }
         }
      }
Integer  sT;
list <AggregateResult> subCount =[select  LOB__c, count(Id) cRate from Control_Function_Feedback__c   where LOB__c != 'CEO' AND  Audit_Type__c=:byCFF AND Category__c ='CAT 2' and (CFF_Rating__c ='Meets' OR
 CFF_Rating__c ='Exemplar' OR CFF_Rating__c ='Negative' OR  CFF_Rating__c='Not Applicable' OR CFF_Rating__c ='N/A-No Interaction' OR CFF_Rating__c ='N/A-Other' ) AND Feedback_Year__c =:currentYear  GROUP BY LOB__c];
 for (AggregateResult sTot:subCount)
 {
     sT = Integer.valueof(sTot.get('cRate'));
     mSubTotal.put(String.valueOf( sTot.get('LOB__c')), sT);

 }
 subAllTotal = 0;
  for(String lb:listLOB )
     {
      if( mSubTotal.ContainsKey(lb))
      {
          subAllTotal = subAllTotal+mSubTotal.get(lb);

      }
      else
      {
          mSubTotal.put(lb, 0);
      }
        }
       } //byCFF 
}
Public void callEnterpriseLevel()
{
    String mapID;
    Integer cRatings;
    lobRatingFacts = new Map<String, Integer>();
    mlobTotalHor = new Map<String, Integer>();
    mlobTotalVer = new Map<String, Integer>();
   // listRatings.add('N/A – Other');
      list <AggregateResult> byLOB = [select  LOB__c,  CFF_Rating__c, count(Id) cRate from Control_Function_Feedback__c   where LOB__c != 'CEO' AND Category__c ='CAT 2' AND Feedback_Year__c =:currentYear  
    AND (cff_rating__c = 'Negative' OR cff_rating__c = 'Meets' OR cff_rating__c = 'Exemplar' OR cff_rating__c = 'No Rating' OR cff_rating__c ='N/A-Sit in CF' )GROUP BY LOB__c,CFF_Rating__c];
    for(AggregateResult b:byLOB)
    {
     mapID =  string.valueof(b.get('LOB__c'))+string.valueof(b.get('CFF_Rating__c'));
     cRatings = Integer.valueof(b.get('cRate'));
     lobRatingFacts.put(mapID, cRatings); 
     System.debug(mapID + ' '+cRatings);
          
     }
    //add few more Rating like 'NA-Other' or Na - No interaction
    entNATotal =0;
     list <AggregateResult> NAList = [select  LOB__c,   count(Id) cRate from Control_Function_Feedback__c   where LOB__c != 'CEO' AND Category__c ='CAT 2' AND Feedback_Year__c =:currentYear  
    AND (cff_rating__c = 'N/A-No Interaction' OR cff_rating__c = 'N/A-Other' OR cff_rating__c = 'Not Applicable'  )GROUP BY LOB__c];
    
     for(AggregateResult m:NAList)
    {
     mapID =  string.valueof(m.get('LOB__c'))+'Not Applicable';
          cRatings = Integer.valueof(m.get('cRate'));
     lobRatingFacts.put(mapID, cRatings); 
     entNATotal =entNATotal +  cRatings;        
     }

    for(String c:listLOB )
     {
         for(String r: listRatings)
         {
              if(lobRatingFacts.ContainsKey(c+r))
              {                }
              else
              {
              lobRatingFacts.put(c+r, 0);
               system.debug('No Key contains:'+c+r);

              
              }
         }
      }
    //Total Horizontal counts for each lob
    list <AggregateResult> allLOB = [select  LOB__c,   count(Id) cRate from Control_Function_Feedback__c   where LOB__c != 'CEO' AND Category__c ='CAT 2' AND Feedback_Year__c =:currentYear  
    GROUP BY LOB__c];
    for(AggregateResult a:allLOB)
    {
     mapID =  string.valueof(a.get('LOB__c'));
     cRatings = Integer.valueof(a.get('cRate'));
     mlobTotalHor.put(mapID, cRatings); 
     System.debug(mapID + ' '+cRatings);
          
     }
     allLOBTotal=0;
     for(String c:listLOB )
     {
         if( mlobTotalHor.ContainsKey(c))
              {                }
        else
        {
            mlobTotalHor.put(c, 0);
        }
     allLOBTotal =  allLOBTotal +mlobTotalHor.get(c);
     }
    //Total Vertical counts
    list <AggregateResult> totVer = [select  CFF_Rating__c,   count(Id) cRate from Control_Function_Feedback__c   where LOB__c != 'CEO' AND Category__c ='CAT 2' AND Feedback_Year__c =:currentYear  
    GROUP BY CFF_Rating__c];
    for(AggregateResult v:totVer)
    {
     mapID =  string.valueof(v.get('CFF_Rating__c'));
     cRatings = Integer.valueof(v.get('cRate'));
     mlobTotalVer.put(mapID, cRatings); 
     System.debug(mapID + ' '+cRatings);
          
     }
    for(String c:listRatings )
     {
         if( mlobTotalVer.ContainsKey(c))
              {                }
        else
        {
            mlobTotalVer.put(c, 0);
        }
     }

}

public PageReference openRepExcel() {
system.debug('IN OPEN AS EXCEL METHOD-->');
PageReference newPage = Page.CFF_StatusByFunctionXLS;
return newPage.setRedirect(false);
}

public PageReference openEntRepExcel() {
system.debug('IN OPEN AS EXCEL METHOD-->');
PageReference newPage = Page.CFF_EnterpriseByLOBXLS;
return newPage.setRedirect(false);
}

Public void  dispMap()
{
        //system.debug(RatingFacts.get('Consumer'+'CFO Group'+'No Rating'));
    system.debug('LG Test: '+ ChkMapKey );
}
}