({
toggleClass: function(component,componentId,className) {
var modal = component.find(componentId);
$A.util.removeClass(modal,className+'hide');
$A.util.addClass(modal,className+'open');
    setTimeout(function(){ 
        if(component.get("v.showTopics"))
       {
            if(component.find("modaldialogSubjectSelection")!=undefined){
                
                component.find("modaldialogSubjectSelection").getElement().focus();
            }
       }
            else if(component.get("v.focusTopics"))
{
                if(component.find("selectTopic")!=undefined){
                    console.log("showTopics"+component.get("v.showTopics"));
                    component.set("v.focusTopics",false);
                    component.find("selectTopic").focus();
                }
            }
        
            else if(component.get("v.replyTo"))
       {
            if(component.find("modaldialogSubjectSelection")!=undefined){
                
                component.find("modaldialogSubjectSelection").getElement().focus();
            }
       }
            else if(component.get("v.focusReplyTo"))
{
                if(component.find("selectReplyTo")!=undefined){
                  
                    component.set("v.focusReplyTo",false);
                    component.find("selectReplyTo").focus();
                }
            }
        
            else if(component.get("v.enablePicking"))
       {
            if(component.find("modaldialogSubjectSelection")!=undefined){
                
                component.find("modaldialogSubjectSelection").getElement().focus();
            }
       }
            else if(component.get("v.focusSubjects"))
{
                if(component.find("selectSubjects")!=undefined){
                  
                    component.set("v.focusSubjects",false);
                    component.find("selectSubjects").focus();
                }
            }         
        
        
        }
, 100);
},

toggleClassInverse: function(component,componentId,className) {
var modal = component.find(componentId);
$A.util.addClass(modal,className+'hide');
$A.util.removeClass(modal,className+'open');
},
checkingRequiredFieldsFilledhelper : function(component,event,helper){

if($A.util.isEmpty(component.find("Inquiry_Method__fld").get("v.value"))||$A.util.isEmpty(component.find("Inquirer_Relationship_to_Subject__fld").get("v.value"))||((component.find("isQCReqField").get("v.value")==true)&&component.find("QC_Reason_fld")!=undefined&&$A.util.isEmpty(component.find("QC_Reason_fld").get("v.value")))){


component.set("v.isRequiredFieldsFilled",false); 


}
else{
if(!component.get("v.isSCRepresetative"))   {
if((component.find("Inquirer_Relationship_to_Subject__fld").get("v.value")=="SC Representative") && (component.find("isQCReqField").get("v.value")==false))
{ 
component.set("v.isRequiredFieldsFilled",false); 
}            

else{
component.set("v.isRequiredFieldsFilled",true); 
} 
}
else{
component.set("v.isRequiredFieldsFilled",true); 
}


}
},
displaySCRepresentativeError : function(component,event,helper){
var relationshiSubjectInfo = component.find("Inquirer_Relationship_to_Subject__fld").get("v.value");
console.log('relationshiSubjectInfo:'+relationshiSubjectInfo);

if(component.get("v.isAnonymousPage")&&!$A.util.isEmpty(relationshiSubjectInfo)){

if(relationshiSubjectInfo!="Family Member"&&relationshiSubjectInfo!="Other"){
component.set("v.isAnonymousError",true);
component.set("v.isRequiredFieldsFilled",false);
}
else{
component.set("v.isAnonymousError",false);

component.set("v.isRequiredFieldsFilled",true); 
}
}
if(!component.get("v.isSCRepresetative")){
if(relationshiSubjectInfo=="SC Representative"){
component.set("v.isSCRepresetativeError",true); 
component.set("v.isRequiredFieldsFilled",false); 
}
else{

component.set("v.isSCRepresetativeError",false); 
}
}

},
checkPrimaryHelper : function(component, event, helper) {

if((component.find("Inquiry_Summary_Topic_fld")!=undefined) &&(component.find("Inquiry_Summary_Topic_Secondary__fld")!=undefined))
{
var primary = component.find('Inquiry_Summary_Topic_fld').get("v.value");
var secondary = component.find('Inquiry_Summary_Topic_Secondary__fld').get("v.value");
}  else{
primary=undefined;
secondary=undefined;
}
if($A.util.isEmpty(primary) && !$A.util.isEmpty(secondary)) 
{
component.set("v.IsPrimaryBlank","true");
}
else
{
component.set("v.IsPrimaryBlank","false");
}

/*var abc=component.get("v.IsPrimaryBlank");
console.log('pri:', primary);
console.log('$A.util.isEmpty(primary):', $A.util.isEmpty(primary));
console.log('primary == null', primary == null);
console.log('value of check box' , abc);
alert('2') */
},

enableCancelHelper : function(component,event,helper){
console.log((component.get("v.isNotOnload")));
console.log("Enable can log");

console.log(!$A.util.isEmpty(component.find("Inquirer_Relationship_to_Subject__fld").get("v.value")));
console.log(!$A.util.isEmpty(component.find("Inquiry_Method__fld").get("v.value")));
console.log(!$A.util.isEmpty(component.find("Inquiry_Summary_Description__fld").get("v.value")));
console.log(!$A.util.isEmpty(component.find("SSCM_Priority__fld").get("v.value")));
console.log(!$A.util.isEmpty(component.find("isQCReqField").get("v.value")));
console.log("Enable can log ends");
if((component.get("v.isNotOnload"))&&(!$A.util.isEmpty(component.find("Inquirer_Relationship_to_Subject__fld").get("v.value"))||!$A.util.isEmpty(component.find("Inquiry_Method__fld").get("v.value"))||!$A.util.isEmpty(component.find("Inquiry_Category_1__fld").get("v.value"))||(component.find("Inquiry_Category_2__fld")!=undefined&&component.find("Inquiry_Category_2__fld")!=null&&(!$A.util.isEmpty(component.find("Inquiry_Category_2__fld").get("v.value"))))||(component.find("Inquiry_Category_2__flhd")!=undefined&&component.find("Inquiry_Category_2__flhd")!=null&&(!$A.util.isEmpty(component.find("Inquiry_Category_2__flhd").get("v.value"))))||(component.find("Inquiry_Category_3__fld")!=undefined&&component.find("Inquiry_Category_3__fld")!=null&&(!$A.util.isEmpty(component.find("Inquiry_Category_3__fld").get("v.value"))))||(component.find("Inquiry_Category_3__flhd")!=undefined&&component.find("Inquiry_Category_3__flhd")!=null&&(!$A.util.isEmpty(component.find("Inquiry_Category_3__flhd").get("v.value"))))||!$A.util.isEmpty(component.find("Inquiry_Summary_Description__fld").get("v.value"))||!$A.util.isEmpty(component.find("SSCM_Priority__fld").get("v.value"))||(component.find("isQCReqField").get("v.value")==true))){
component.set("v.isCancelEnabled",true);        
}
else{
component.set("v.isCancelEnabled",false);        

}
},
createSSCMRecord : function(component,event,helper){

},
primaryOrSecondryFieldChange : function(component,event,helper){
// helper.enableCancelHelper(component,event,helper);
console.log('primaryfieldChanged');
console.log(component.get("v.isRequiredFieldsFilled"));
component.set("v.isTryingToDeleteError",false);
var primary = component.find('Inquiry_Summary_Topic_fld').get("v.value");
console.log(primary);

if(!$A.util.isEmpty(primary)){
component.set("v.isAddtopicButtonVisible",true);
}
if(component.find('Inquiry_Summary_Topic_Secondary__fld')!=undefined){
var secondary = component.find('Inquiry_Summary_Topic_Secondary__fld').get("v.value");
if(!$A.util.isEmpty(secondary)){
component.set("v.isAddtopicButtonVisible",false);


} 

if($A.util.isEmpty(secondary)){
component.set("v.isAddtopicButtonVisible",false);
component.set("v.disablePrimary",false);
helper.checkingRequiredFieldsFilledhelper(component,event,helper);


} 
if($A.util.isEmpty(primary)){
if(!$A.util.isEmpty(secondary)){
component.set("v.isTryingToDeleteError",true);
component.set("v.isRequiredFieldsFilled",false); 
//helper.checkingRequiredFieldsFilledhelper(component,event,helper);


}
else{
component.set("v.isRequiredFieldsFilled",true); 
helper.checkingRequiredFieldsFilledhelper(component,event,helper);

}


} 
//else{
//component.set("v.isRequiredFieldsFilled",true); 


//}

if(!$A.util.isEmpty(primary)){
if(!$A.util.isEmpty(secondary)){
component.set("v.disablePrimary",true);
helper.checkingRequiredFieldsFilledhelper(component,event,helper);

}


}

}
console.log('primaryfieldChanged ends');
console.log(component.get("v.isRequiredFieldsFilled"));

}
})