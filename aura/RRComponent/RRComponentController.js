({
	init : function(component, event, helper) {
        
        component.set('v.columns', [
            {label: 'Request ID', fieldName: 'Name'},            
            {label: 'Employee Full Name', fieldName: 'Employee_Full_Name__c'},
            {label: 'Continuous Service Date', fieldName: 'Continuous_Service_Date__c'},
            {label: 'Eligibility Year', fieldName: 'Eligibility_Year__c'},
            {label: 'Person Number', fieldName: 'Person_Number__c'},
            {label: 'Years Of Service', fieldName: 'YOS__c'},
            {label: 'How Many Weeks Eligible', fieldName: 'How_Many_Weeks_Eligible__c'}
        ]);
        
        var action = component.get('c.getRRrecords');
        action.setCallback(this, function(response){
            var state = response.getState();
           // alert(state);
            if(state === 'SUCCESS'){
           	var responseValue = response.getReturnValue();
                console.log('responseValue', responseValue);
            	component.set('v.data', responseValue);
            }
        });	
        $A.enqueueAction(action);
	}
})