// Copyright 2016, Sales Engineering, Salesforce.com Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// - Redistributions of source code must retain the above copyright notice,
//   this list of conditions and the following disclaimer.
// - Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.
// - Neither the name of the salesforce.com nor the names of its contributors
//   may be used to endorse or promote products derived from this software
//   without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  

({
	doInit : function(component, event, helper) {
		var id = (new Date()).getTime( );
        component.set( "v.id", id );
        $A[ 'youtube-videoplayer' + id ] = {};
        if( component.get( "v.fullscreen") ){
            component.set( "v.width", "100%" );
            component.set( "v.height", "100%" );
            component.set( "v.fullscreen_class", "full_screen" );
        };
        var grayscale = component.get( "v.effect");
        switch( grayscale ){
            case 'Invert':
                component.set( "v.effect_class", "invert" );
                break;
            case 'Grayscale light':
                component.set( "v.effect_class", "gray_light" );
                break;
            case 'Grayscale strong':
                component.set( "v.effect_class", "gray_strong" );
                break;
            case 'Sepia light':
                component.set( "v.effect_class", "sepia_light" );
                break;
            case 'Sepia strong':
                component.set( "v.effect_class", "sepia_strong" );
                break;
            case 'Blur light':
                component.set( "v.effect_class", "blur_light" );
                break;
            case 'Blur strong':
                component.set( "v.effect_class", "blur_strong" );
                break;
            default:
                component.set( "v.effect_class", "" );
        };
	},
    afterScriptsLoaded : function(component, event, helper) { 
        console.log("hey")
        var youtube_videoplayer = $A[ 'youtube-videoplayer' + component.get( "v.id") ];
		youtube_videoplayer = new YoutubeVideo();
        youtube_videoplayer.init( {
            id: component.get( 'v.id'),
            fullscreen: component.get( 'v.fullscreen' )
        });
        
       utility.onPageLoad( function(){
            utility.onWindowResize( youtube_videoplayer.adjustImgSizeOnWinResize );
        });
        
        utility.init( );
	}
})