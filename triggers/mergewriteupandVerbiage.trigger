trigger mergewriteupandVerbiage on Nomination__c (before insert,before update,after insert,after update) {
Map<String,List<Nomination__c>> accountlk = new Map<String,List<Nomination__c>>();
for(Nomination__c nom:trigger.new){
if(trigger.isBefore)
nom.PublicWrite_FinalNomination_Verbiage__c = String.isNotBlank(nom.Final_Nomination_Verbiage__c)?nom.Final_Nomination_Verbiage__c:nom.Public_Write_Up__c;
if(String.isNotBlank(nom.Nominee_Lkp__c)){

if(accountlk.containskey(nom.Nominee_Lkp__c))
accountlk.get(nom.Nominee_Lkp__c).add(nom);
else
accountlk.put(nom.Nominee_Lkp__c,new List<Nomination__c>{nom});
}
}
if(trigger.isAfter){
if(accountlk.keyset().size()>0){
Map<Id,Account> acinfo = new Map<Id,Account> ([Select id,PublicWrite_FinalNomination_Verbiage__c from Account where id in:accountlk.keyset()]);
List<Nomination__c> nominationsInfo = [Select id,PublicWrite_FinalNomination_Verbiage__c from Nomination__c where Nominee_Lkp__c in:acinfo.keyset()];
Map<Id,Account> accounts = new Map<Id,Account>();
set<Id> isUpdated = new set<Id>();
for(Nomination__c nomi: nominationsInfo ){
Account ac = new Account(Id=nomi.Nominee_Lkp__c);
if(!isUpdated.contains(nomi.Nominee_Lkp__c)){
isUpdated.add(nomi.Nominee_Lkp__c);
ac.PublicWrite_FinalNomination_Verbiage__c=nomi.PublicWrite_FinalNomination_Verbiage__c;
}
else{
ac.PublicWrite_FinalNomination_Verbiage__c +=nomi.PublicWrite_FinalNomination_Verbiage__c;
}
accounts.put(nomi.Nominee_Lkp__c,ac);
//ac.PublicWrite_FinalNomination_Verbiage__c
}
update accounts.values();
}
}
}